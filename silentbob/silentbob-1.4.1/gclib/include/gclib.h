/*
 * (c) Oleg Puchinin 2005,2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DLIB_H
#define DEFINE_DLIB_H
#define DLIB_VERSION "1.2"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <dirent.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/mman.h>
#include <sys/poll.h>
#include <gclib/gclib_c.h>

#ifdef __cplusplus
typedef unsigned int uint32_t;
#define __export extern "C"
#else
#define __export extern
#endif
typedef unsigned char uchar_t;

#define PUT(arga) DROP(arga)
#define Dmemused mallinfo().uordblks
#define LPCHAR(arga) (char *) arga
#define DARRAY(arga) ((DArray *) arga)
#define container_of(var, type) ((type) var)

#ifndef min
#define min(arga,argb) (arga < argb ? arga : argb)
#define max(arga,argb) (arga > argb ? arga : argb)
#endif

#define MAXSTRSIZE 256

typedef void (*Dfunc_t)(void *);
typedef void (*Dhash_f)(char *, char *);

enum DHASH_TYPES
{
	DHASH_LPCHAR,
	DHASH_INT,
	DHASH_OTHER
};

#include "dhash.h"
#include "darray.h"
#include "earray.h"
#include "dlist.h"
#include "elist.h"
#include "dpbuf.h"
#include "gcollection.h"
#include "ehash.h"
#include "dconnection.h"
#include "dpoll.h"
#include "dfilter_intf.h"
#include "djobs.h"
#include "dheapsort.h"
#include "dstack.h"
#include "Tree.h"

struct dkey_t {
	char *KEY;
	char *VALUE;
};

DArray * Dsplit (char * STR, char *ch); // 2005. Very old.
int Dsplit (char * STR, char *ch, DList * to_list); // 2006. Old.
int Dsplit (char * buf, size_t buflen, char *str, EList * to_list); // 25/12/06
DArray * Dlstat_r (DArray * m_names);
DArray * Dstat_r (char * __from, dirent * m_dirents, int count);
DList * Dreaddir_r (char * path);
DArray * Dfiles (char * path);

#ifdef I_IN_DLIB_CODE
struct dirent * cur_dir_ent = NULL;
int cur_dir_fd = 0;
int cur_ret = 0;
#else
__export struct dirent * cur_dirent;
__export int cur_dir_fd;
__export int cur_ret;
#endif

#define OBJECT(object,type) ((type) object)
#define DRET(type) ((type) cur_ret)
#define DSRET(ret) cur_ret = (long) ret;

__export int Dreaddir_r (char * NAME, struct dirent ** OUTDIR);
__export int Dpoll_add (EArray * d_array, int fd, short events);
__export int Dpoll_coallesce (EArray * d, struct pollfd ** p);
__export int Dpoll_scan (struct pollfd **p, int count, int position);

/* OpenSSL */
unsigned char * Ddigest (void * data, int len, char *name, unsigned int * out_len);

/* DNS 26/08/06 */
int dns_get_sock ();
void dns_set_timeout (int sec);
char * dns_A (char *host);
char * dns_MX (char *host);	
char * dns_ip2name (char *IP);
/**/

#ifdef __linux
__export char * dcp (char * S);
__export void Dzero (void * ptr);
__export int Dsched_yield ();
#define DCP(arga) dcp ((char *) arga) 
#endif

int Dposix_fadvise (int fd, int offset, int len, int advice);

#undef __export
#define __export

#endif

