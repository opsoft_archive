/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */

#ifndef DEFINE_DPOLL_H
#define DEFINE_DPOLL_H

class DPoll 
{
        public:
                DPoll ();
                ~DPoll ();

                DConnection * init (char * name, uint32_t num = 0, int sd = -1);
                DConnection * attach (DConnection *c);
                DConnection * detach (char *c_name, int num);
                DConnection * by_name (char * name, int num = 0);
                int poll (int timeout);
                DConnection * scan ();
                int unlink (char * c_name, int num);
                int unlink (DConnection * c_conn);
                pollfd * poll_build (int *nfds);
                pollfd * poll_rebuild (int *nfds);                        
                pollfd * get_poll (int *nfds);
                int connections_dump ();
                
        private:
                EHash * connections;
                pollfd * pull;
                int nfds;
                int p_pos;
                __dlist_entry_t * p_ptr;
};


#endif

