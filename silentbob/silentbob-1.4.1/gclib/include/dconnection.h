/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */

#ifndef DEFINE_DCONNECTION_H
#define DEFINE_DCONNECTION_H

class DConnection
{
        public:
                DConnection ();
                ~DConnection ();

                int ipv4_init ();
                int ipv4_init_udp ();
                int ipv4_bind (char *ip, uint16_t port);
                int ipv4_connect (char *ip, uint16_t port);
                int ipv4_udp_connect (char *ip, uint16_t port);
                int ipv4_send (char * buf, int len);
                int ipv4_sendto (char * buf, int len);
                int ipv4_recv (char * buf, int len);
		int ipv4_recvfrom (char *buf, int len, char * IP, int * PORT);
                void update_ctime (time_t d_time = 0);
                void update_mtime (time_t d_time = 0);
                int DIONREAD ();
                DConnection * clone ();
                int broadcast ();
		int close ();
                
                inline int open_ro (char *f_name) {
                        c_sd  = ::open (f_name, O_RDONLY);
                        return c_sd;
                }
                inline int open_rw (char *f_name) {
                        c_sd = ::open (f_name, O_RDWR);
                        return c_sd;
                }
                inline int open (char *f_name, int openmode, int filemode = 0644) {
                        c_sd = ::open (f_name, openmode, filemode);
                        return c_sd;
                }           
                inline int read (char *m_buf, int len) {
                        return ::read (c_sd, m_buf, len);
                }
                inline int write (char *m_buf, int len) {
                        return ::write (c_sd, m_buf, len);
                }
                inline void poll_config (int flags) {
                        c_poll_flags = flags;
                }
                inline char * getsockname () {
                        return c_cname;
                }
                inline char * getpeername () {
                        return c_pname;
                }
                inline uint16_t getpeerport () {
                        return c_pport;
                }
                inline uint16_t getsockport () {
                        return c_cport;
                }
                inline time_t get_ctime () {
                        return c_ctime;
                }
                inline time_t get_mtime () {
                        return c_mtime;
                }

                int c_sd;
                int c_type;
                char * c_name;
                time_t c_mtime;
                time_t c_ctime;
                char * c_cname; // client address
                char * c_pname; // server address                
                uint16_t c_cport;
                uint16_t c_pport;
                char * c_status; // other data
                int c_poll_flags;                
		bool bcast;
};

#endif

