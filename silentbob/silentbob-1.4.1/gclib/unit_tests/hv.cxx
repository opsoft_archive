/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib_c/gclib_c.h>
#include <gclib.h>
#include <hv.h>

int main (int argc, char ** argv)
{
	HV * hash;
	char d_key[512];
	char d_value[512];
	int i;
	int rnd;
	char *S;

	hash = new HV;

	printf ("Stage 1. Prepare.\n");
	fflush (stdout);
	Dtimer ();
	hash->autosort (10000);
	for (i = 0; i < 100000; ++i) {
		sprintf (d_key, "_f%i", i);
		sprintf (d_value, "_value%i", -i);
		hash->set (d_key, d_value);
	}
	print_the_time (stdout);
	fflush (stdout);

	hash->sort ();

	printf ("Stage 2. Access (100000 keys).\n");
	fflush (stdout);
	Dtimer ();
	for (i = 0; i < 100000; ++i) {
		rnd = rand () % 100000;
		sprintf (d_key, "_f%i", rnd);
		sprintf (d_value, "_value%i", -rnd);
		S = hash->get (d_key);
		if (! S) {
			printf ("Hash test fail.\nKey not found (%s).\n", d_key);
			return EXIT_FAILURE;
		}

		if (NE (S, d_value)) {
			printf ("Hash test fail.\n");
			printf ("%i hash[%s]\n", i, d_key);
			return EXIT_FAILURE;
		}
	}
	print_the_time (stdout);
	fflush (stdout);
	delete hash;
}

