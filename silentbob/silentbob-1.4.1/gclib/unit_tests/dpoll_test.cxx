#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/poll.h>

int main (int argc, char ** argv)
{
        pollfd one;
        int count;

        one.fd = 0;
        one.events = POLLIN | POLLHUP;
        one.revents = 0;
        printf ("%i\n", poll (&one, 1, 3));
        
        return EXIT_SUCCESS;
}

