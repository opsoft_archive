/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <gclib_c.h>
#include <gclib.h>
#include <Cache.h>

int main (int argc, char ** argv)
{
	Cache<char [256]> stringCache (4096);
	EArray * ar;
	char * S;
	int i;

	printf ("With malloc ():\n");
	fflush (stdout);
	Dtimer ();
	for (i = 0; i < 4096; ++i) {
		S = (char *) malloc (256);
	}
	print_the_time (stdout);

	printf ("With Cache:\n");
	fflush (stdout);
	Dtimer ();
	for (i = 0; i < 4096; ++i)  
		S = (char *) stringCache.alloc ();
	print_the_time (stdout);

	printf ("brutforce:\n");
	fflush (stdout);

	while (true) {
		ar = new EArray(4096);
		for (i = 0; i < 4096; ++i) {
			S = (char *) stringCache.alloc ();
			ar->add (S);
		}
		
		for (i = 0; i < 4096; ++i) {
			S = (*ar)[i];
			stringCache.free (S);
		}
		delete ar;
	}

	return EXIT_SUCCESS;
}

