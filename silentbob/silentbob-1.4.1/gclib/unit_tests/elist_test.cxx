/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 * < ? program elist_test ?>
 */

#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <time.h>

int main (int argc, char ** argv)
{
        EList * list;
        int i;
        char buf[255];
        char *S;

        list = new EList;
        srand (time (NULL));
        while (true) {
                Drand_str (buf, 254);
                for (i = 0; i < 1000; ++i) 
                        list->add_tail (strdup (buf));

                list->first ();
                while ((S = list->get ()) && S) {
                        if (rand () % 1) {
                                free (S);
                                list->rm ();
                        }
                        if (! list->next ())
                                list->first ();
                }
        }               
        return EXIT_SUCCESS;
}

