/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

KOI8-R

	������.

                DList   
���������� (������) :
	char * add_head (char *);
	char * add_tail (char *);
	char * add_after (__dlist_entry_t *, char *);
	char * add_before (__dlist_entry_t *, char *);
���������� (��������) :
	char * add_entry_head (__dlist_entry_t * one);
	char * add_entry_tail (__dlist_entry_t * one);
	char * add_entry_after (__dlist_entry_t *, __dlist_entry_t *);
	char * add_entry_before (__dlist_entry_t *, __dlist_entry_t *);

	rm / remove / del
�������� �������� (��� ������������ ������).

	void detach (__dlist_entry_t * one);
�������� �������� ("������ �� ������"). ������ �� �������������.

	foreach (Dfunc_t);
��������� ��� ����. � ������� ���������� ������, � �� ��������.

                EList (�� DList)
������ � ��������� ������� �������.
        first / last / next / prev / get / del

