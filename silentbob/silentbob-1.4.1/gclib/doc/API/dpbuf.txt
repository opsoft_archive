/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

UTF8

	DPBuf
Класс предназначен для контроля указателя. Присекаются любые попытки выхода указателя за границы буфера.
В случае попытки такого выхода устанавливается ok в false.

	init (char *ptr, int len);
Инициализировать буфер (задать указатель и размер.

	r8 / r16 / r32
Поптыка прочитать соответственно 8/16/32 бит из буфера, с указателя или из текущей позиции.
Текущая позиция при этом не изменяется. Возвращаемые значения uchar_t/uint16_t/uint32_t соответственно.

	rd (char *ptr, int len)
Попытаться прочитать len байт из ptr или с текущей позиции. Если успешно - возвращает указатель (ptr или
текущаую позицию), иначе NULL.

	s8 / s16 / s32
Произвести сдвиг (+) текущей позиции на 1, 2, или 4 байта.

	sd (N)
Произвести сдвиг (+) на N байт.

	set_pos 
Установить текущую позицию.

	bool check (char *ptr, int count);
Проверить валидность операции чтения count байт с позиции ptr. Класс должен быть проинициализирован 
для буфера, в рамках которого должен быть ptr;

        char * memmem (char *buf, char *needle, int needle_len)
Произвести поиск в буфере, не выходя за границы.  
        
