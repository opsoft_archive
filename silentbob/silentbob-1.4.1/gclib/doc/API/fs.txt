/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

UTF8

	int Dfnwrite (char * f_name, void * p_lp_buffer, int int_size);
	int Dfnread (char * f_name, void * p_lp_buffer, int int_size);
Записать/прочитать буфер из файла.

	int Dreaddir_r (char * NAME, dirent ** OUTDIR);
Рекурсивно прочитать директорию. (устарело)

	int Dselect (int fd, int SEC, int USEC);
Ждать данных от fd. Если SEC==0 && USEC==0, ожидать без таймаута.

	int DFILE (const char * m_filename);
Прочитать весь файл. Возвращает размер. Сам буфер можно получить вызовом DRET ();

	struct stat * DSTAT (const char * S);
	struct stat * DLSTAT (const char * S);
Выполнить stat/lstat для S. Результат - в статическом буфере (повторное выполнение приводит к 
затиранию предыдущего результата).

	int DIONREAD (int fd);
Вернуть количество байт которые могут быть прочитаны из дескриптора без блокировки.

	int fsize (const char * S);
	int fdsize (int fd);
Вернуть размер файла.

	char * DFMAP (int fd);
	char * DFMAP (const char *d_file, int *out_fd, int *d_out_size);
mmap'нуть весь файл. Оба варианта возвращают адрес mmap'нутой зоны, плюс во втором можно получить
дескриптор и размер файла.

	char * Dread_to_eof (int fd, int *d_out_size);
Прочитать все, до конца. Может применяться не только для файлов. Возвращает буфер и его размер.

	int move_stream (int fd_in, int fd_out);
Переписать все данные из одного дескриптора в другой.

	DList * Dreaddir_r (char * path);
Прочитать директорию. (результат - список "struct dirent");

	DArray * Dfiles (char * path);
Прочитать список файлов. Результат - ьассив имен (lpsz).

	DArray * Dlstat_r (DArray * m_names);
Выполнить lstat для массива имен. Результат - массив "struct stat"; 

	int Dnonblock(int fd);
Установить неблокирующий режим O_NONBLOCK. 

	int close_pipe (int *fds)
Если дескрипторы fds[0] и fds[1] не равны -1, то закрыть трубу и установить в -1.

	fdclose (int fd)
Закрыть дескриптор, если не -1, и установить в -1.

	int Dtmpfd (char *name)
Открыть временный файл (запись). Возвращает дескриптор и имя в name. буфер name должен быть не меньше 128 байт.



