/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib.h>

struct gcollection_meta {
	__dlist_entry_t * list_head;
	__dlist_entry_t * list_tail;
	char * base_offset;
	int chunk_size;
	int chunk_count;
	int gcollection_size;
};

GCollection::GCollection ()
{
	entries = new DList;	
	free_buf = false;
	buf = NULL;
	chunks = NULL;
	chunk_size = 0;
	count = 0;		
}

GCollection::~GCollection ()
{
	if (free_buf)
		DROP (buf);

	::free (chunks);
}

inline __dlist_entry_t * GCollection::__o2c (char * ptr)
{
	return &chunks[int((ptr-buf)/chunk_size)];
}

int GCollection::init (int size, int acount, char * abuf, int buf_size)
{
	int i;
	char * offset;
	__dlist_entry_t * chunk;

	/* 
	 * 1. Alloc chunks (__dlist_entry_t array).
	 * 2. Alloc heap.
	 * 3. Init chunks.
	 */
	
	buf = abuf;
	chunk_size = size;
	count = acount;
	if (! buf) {
		free_buf = true;
		buf_size = size * count;
		buf = CNEW (char, buf_size);
	} else if (buf_size < (count * size)) 
		return -1;

	chunks = CNEW (__dlist_entry_t, count);

	offset = buf;
	for (i = 0; i < count; ++i) {
		chunk = __o2c (offset);
		chunk->data = offset;
		entries->add_entry_tail (chunk);
		offset += chunk_size;
	}

	return 0;
}

int GCollection::init_simple (char * heap, int size, int count)
{
	int i;
	char * ptr;
	gcollection_meta * meta;
	__dlist_entry_t * chunk;

	if (! heap)
		return -1;
	
	meta = (gcollection_meta *) heap;
	memset (meta, 0, sizeof (gcollection_meta));
	meta->list_head = entries->get_head ();
	meta->list_tail = entries->get_tail ();
	meta->chunk_size = size;
	meta->chunk_count = count;
	meta->gcollection_size = get_requiredspace (size, count);
	chunk_size = size;

	ptr = ((char *) meta) + sizeof (gcollection_meta);
	chunks = (__dlist_entry_t *) ptr;
	buf = ptr + sizeof (__dlist_entry_t) * count;
	meta->base_offset = buf;

	entries->dont_free = true;
	ptr = buf;
	for (i = 0; i < count; ++i) {
		chunk = __o2c (ptr);
		chunk->data = ptr;
		entries->add_entry_tail (chunk);
		ptr += chunk_size;
	}

	return 0;
}

char * GCollection::alloc ()
{
	__dlist_entry_t * one;
	one = entries->get_head ();
	if (! one)
		return NULL;
	entries->detach(one);
	return one->data;
}

void GCollection::free (char * one)
{
	entries->add_entry_tail (__o2c(one));
}

int GCollection::get_requiredspace (int chunk_size, int count)
{
	return sizeof (gcollection_meta) + 
		sizeof (__dlist_entry_t) * count + 
		chunk_size * count; 
}

bool GCollection::check (char * ptr)
{
	char * d_end = buf + (chunk_size * count);
	if (ptr >= buf && ptr <= d_end)
		return true;
	return false;
}

