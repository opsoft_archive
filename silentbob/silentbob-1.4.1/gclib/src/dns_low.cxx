/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib.h>
#include <dns.h>
#include <gclib_c.h>

extern int dns_last_id;

char * __name2dns (char *name, int *len)
{
	char * d_buf;
	char * ptr;
	DList * list;
	__dlist_entry_t * one;

	list = new DList;
	Dsplit (name, ".", list);
	d_buf = CNEW (char, 70);
	ptr = d_buf;
	
	one = list->get_head ();
	while (one) {
		buf_W8 (&ptr, strlen (one->data));
		buf_WS (&ptr, one->data);
		one = one->next;
	}

	buf_W8 (&ptr, 0);
	delete list;
	if (len)
		*len = ptr - d_buf;
	return d_buf;
}

inline char * __ddup (char * ptr, unsigned char ch)
{
	char *S = CNEW (char, ch+2);
	memcpy (S, ptr, ch);
	S[ch] = '.';
	S[ch+1] = '\0';
	return S;
}

char * __dns_rd_chunk (DPBuf * p, char ** ptr, char * done)
{
	char * ret;
	unsigned char ch;

	if (! p->check (*ptr, 1))
		return NULL;

	ch = buf_R8 (ptr);
	if (! ch) {
		*done = -1;
		return NULL;
	}
	
	if (ch & 0xc0) {
		ret = NULL;
		--(*ptr);
	} else {
		if (! p->check (*ptr, ch))
			return NULL;
		ret = __ddup (*ptr, ch);
		(*ptr) += ch;
	}			
	
	return ret;
}

char * __dns_resp_domain (DPBuf * p, char * domain, char **domain_end)
{
	unsigned char ch;
	uint16_t offset;
	char * ptr;
	char buf[128];
	char *S;
	char done = 0;
	bool d_try = true;
	int n = 20;

	if (! p || ! domain)
		return NULL;
	
	buf[0] = 0;
	memset (buf, 0, 128);
	ptr = domain;

drd_try:
	while (true) {
		S = __dns_rd_chunk (p, &ptr, &done);
		if (done || ! S) 
			break;
		
		strcat (buf, S);
		DROP (S);
	}
	
	if ((domain_end != NULL) & d_try)
		*domain_end = ptr;

	if (! done) {
		if (! p->check (ptr, 2))
			return NULL;
		
		if (! --n)
			return NULL;

		ch = buf_R8 (&ptr);
		offset = (ch &= ~0xc0) << 8;
		offset += buf_R8 (&ptr);
		d_try = false;
		ptr = &p->begin[offset];
		goto drd_try;
	}
	
	if (domain_end && ! d_try)
		*domain_end += 2;
	
	chop (buf);
	if (strlen (buf) > 0)
		return strdup (buf);
	else
		return NULL;
}

char * make_dns_pkt (uint16_t id, uint16_t flags, uint16_t d_type,
		uint16_t d_class, char * domain, int *len)
{
	char * pkt;
	char * ptr;
	char * dns_name;
	int dns_len;
	struct dns_header hdr;

	pkt = CNEW (char, 512);
	ptr = pkt;
	memset (&hdr, 0, sizeof (struct dns_header));
	hdr.id = htons (id);
	hdr.flags = htons (flags);
	hdr.nr = htons (1);
	buf_Wstruct (&ptr, &hdr);
	
	dns_name = __name2dns (domain, &dns_len);
	memcpy (ptr, dns_name, dns_len);
	DROP (dns_name);
	ptr += dns_len;

	buf_W16 (&ptr, htons (d_type)); // "A"
	buf_W16 (&ptr, htons (d_class)); // Internet
	
	if (len)
		*len = ptr-pkt;

	return pkt;
}

DList * dns_resp_split (dns_header *hdr, char *pkt, int len)
{
	dns_header *query_hdr;
	dns_rheader *res_hdr = NULL;
	dns_reply *reply = NULL;
	char * d_end;
	char * d_name;
	uint16_t dr_type;
	uint16_t dr_class;
	DPBuf * p = NULL;
	char * ptr;
	DList * ret;
	
	p = new DPBuf (pkt, len);
	query_hdr = (dns_header *) p->rd (pkt, sizeof (dns_header));
	if (! p->ok)
		goto drs_out;

	if (query_hdr->id != hdr->id)
		goto drs_out;

	ptr = &p->begin[sizeof (dns_header)];
	d_name = __dns_resp_domain (p, ptr, &d_end);
	if (d_name == NULL)
		goto drs_out;

	DROP (d_name);
	ptr = d_end;
	
	if (! p->check (ptr, 4))
		goto drs_out;

	dr_type = htons (buf_R16 (&ptr));
	dr_class = htons (buf_R16 (&ptr));
	
	reply = NULL;
	ret = new DList;

	while (true) {
		reply = CNEW (dns_reply, 1);
		memset (reply, 0, sizeof (dns_reply));
		
		reply->domain = __dns_resp_domain (p, ptr, &d_end);
		if (! reply->domain)  
			break;

		ptr = d_end;
		res_hdr = NULL;
		
		if (! p->check (ptr, sizeof (struct dns_rheader))) 
			break;
	
		res_hdr = buf_Rstruct (&ptr, dns_rheader);
		reply->TTL = htonl (res_hdr->TTL);
		reply->data_len = htons (res_hdr->len);
		reply->dr_type = htons (res_hdr->dr_type);
		reply->dr_class = htons (res_hdr->dr_class);
		if (! p->check (ptr, reply->data_len)) 
			goto drs_out;

		reply->pkt_data_ptr = ptr;
		reply->data = buf_RD (&ptr, reply->data_len);
		ret->add_tail (LPCHAR (reply));
	}
	
	delete p;
	return ret;
	
drs_out:
	if (reply) {
		DROP (reply->domain);
		DROP (reply);
		DROP (reply->data);
	}
	
	DROP (res_hdr);
	delete p;
	return NULL;

}

void dns_resp_clean (DList * list)
{
	__dlist_entry_t *one;
	dns_reply * reply;
	
	if (! list)
		return;

	while ((one = list->get_head ()) && one) {
		reply = (dns_reply *) one->data;
		DROP (reply->domain);
		DROP (reply->data);
		DROP (reply);
		list->rm (one);
	}
	delete list;
}

dns_reply * dns_scan (DList *list, uint16_t dr_type, uint16_t dr_class)
{
	__dlist_entry_t * one;
	dns_reply *Ret = NULL;
	dns_reply *reply = NULL;

	if (! list)
		return NULL;

	one = list->get_head ();
	if (! one)
		return NULL;

	while (one) {
		reply = (dns_reply *) one->data;
		if (reply->dr_type == dr_type && reply->dr_class == dr_class) {
			Ret = reply;
			break;
		}
		one = one->next;
	}

	return Ret;
}

