/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib.h>
#include <dns.h>
#include <gclib_c.h>

int dns_sock = -1;
int dns_last_id = 0;
int dns_timeout = 30;
DList * dns_servers = NULL;

int dns_init ()
{
	FILE * f_resolv;
	dns_server * one;
	char m_buf[256];
	int n = 0;
	char *S;

	f_resolv = fopen ("/etc/resolv.conf", "r");
	if (! f_resolv)
		return -1;

	dns_sock = Dsocket_udp ();
	if (dns_sock < 0)
		return -1;

	m_buf[255] = 0;
	dns_servers = new DList;
	while (fgets (m_buf, 255, f_resolv)) {
		chomp (m_buf);
		if (strncmp (m_buf, "nameserver", 10)) // != nameserver
                        continue;
                
                S = &m_buf[10];
                ++S;
                while (*S && (*S == ' ' || *S == '\t'))
                        ++S;
                
                if (! strlen (m_buf))
                        continue;
                
                one = CNEW (dns_server, 1);
                one->IP = strdup (S);
                one->ms = 0;
                dns_servers->add_tail (LPCHAR (one));
                ++n;		
	}
	
	dns_last_id = rand ();
	fclose (f_resolv);

	if (! n) {
		close (dns_sock);
		dns_sock = -1;
		return -1;
	}
	
	return 0;
}

char * dns_find_best_server ()
{
	dns_server * d_server;
	__dlist_entry_t * one;
	char * Ret;

	one = dns_servers->get_head ();
	d_server = (dns_server *) one->data;
	Ret = d_server->IP;
	dns_servers->rm (one);
	dns_servers->add_tail (LPCHAR (d_server));

	return Ret;
}

__export void dns_set_timeout (int sec)
{
	dns_timeout = sec;
}

__export int dns_get_sock ()
{
	return dns_sock;
}

DList * dns_query_sync(uint16_t flags, uint16_t dr_type, uint16_t dr_class, 
		char * host, char ** d_resp = NULL, int *d_resp_len = NULL)
{
	char * pkt;
	int len;
	int count = 0;
	int resp_len;
	char * buf;
	struct dns_header *hdr;
	DList * list;

	if (dns_sock == -1) {
		if (dns_init () == -1)
			return NULL;
	}
	
	if (! host)
		return NULL;
	
	pkt = make_dns_pkt (++dns_last_id, 0x0100, dr_type, dr_class, host, &len);
	if (Dsendto (dns_sock, pkt, len, dns_find_best_server (), 53) < 0) {
		DROP (pkt);
		return NULL;
	}

	if (Dselect (dns_sock, dns_timeout, 0) <= 0) {
		DROP (pkt);
		return NULL;
	}
	
	count = DIONREAD (dns_sock);
	buf = CNEW (char, count);
	resp_len = Drecvfrom (dns_sock, buf, count, NULL, 0);
	if (resp_len <= 0) 
		return NULL;
	
	hdr = (dns_header *) pkt;
	list = dns_resp_split (hdr, buf, resp_len);
	
	if (d_resp) 
		*d_resp = buf;
	else
		DROP (buf);

	if (d_resp_len)
		*d_resp_len = resp_len;
	
	return list;
}

__export char * dns_A (char *host)
{
	struct dns_reply *reply;
	DList * list;
	char * Ret = NULL;

	list = dns_query_sync (0x0100, DNS_A, 1, host);
	reply = dns_scan (list, DNS_A, 1);
	if (reply)
		Ret = strdup (inet_ntoa (*((in_addr *) reply->data)));

	dns_resp_clean (list);
	return Ret;
}

__export char * dns_MX (char *host)
{
	struct dns_reply *reply;
	struct dns_reply *best_reply;
	__dlist_entry_t * one;
	DList * list;
	char * Ret = NULL;
	char * resp;
	int resp_len;
	uint16_t d_min = 0xFFFF;
	uint16_t i;
	char * ptr;
	char * mx;
	DPBuf * p;

	list = dns_query_sync (0x0100, DNS_MX, 1, host, &resp, &resp_len);
	if (! list)
		return NULL;

	reply = NULL;
	best_reply = NULL;
	one = list->get_head ();
	if (! one)
		return NULL;

	while (one) {
		reply = (dns_reply *) one->data;
		if (reply->dr_type == 15) {
			ptr = one->data;
			i = htons (buf_R16(&ptr));
			if (i < d_min) {
				d_min = i;
				best_reply = reply;
			}
		}
		one = one->next;
	}

	if (! best_reply) {
		dns_resp_clean (list);
		return NULL;
	}

	mx = best_reply->pkt_data_ptr;
	mx += 2;
	p = new DPBuf (resp, resp_len);
	Ret = __dns_resp_domain (p, mx, NULL);

	delete p;
	DROP (resp);
	dns_resp_clean (list);
	return Ret;
}

char * __ip2arpa (char *IP)
{
	char buf[256];
	unsigned int tmp[4];
	
	sscanf (IP, "%u.%u.%u.%u", &tmp[3], &tmp[2], &tmp[1], &tmp[0]);
	sprintf (buf, "%u.%u.%u.%u.in-addr.arpa", tmp[0], tmp[1], tmp[2], tmp[3]);
	return strdup (buf);
}

__export char * dns_ip2name (char *IP)
{
	struct dns_reply *reply;
	char *buf;
	DList * list;
	char * Ret = NULL;
	char * resp;
	int resp_len;
	DPBuf * p;
	char * name;
	
	if (! IP)
		return NULL;

	buf = __ip2arpa (IP);
	list = dns_query_sync (0x0100, DNS_PTR, 1, buf, &resp, &resp_len);
	DROP (buf);
	reply = dns_scan (list, DNS_PTR, 1);

	if (! reply) 
		goto dip_out;

	name = reply->pkt_data_ptr;
	p = new DPBuf (resp, resp_len);
	Ret = __dns_resp_domain (p, name, NULL);
	delete p;
	DROP (resp);
	
dip_out:
	dns_resp_clean (list);
	return Ret;
}

