/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <string.h>
#include <stdlib.h>
#define __export

__export void Dfastsort_s(char ** a, long N)
{
	long i = 0, j = N;
	char * temp, *p;
	
	p = a[ N>>1 ];
	
	do {
		while ( strcmp (a[i], p) < 0) i++;
		while ( strcmp (a[j], p) > 0) j--;
		
		if (i <= j) {
			temp = a[i]; a[i] = a[j]; a[j] = temp;
			i++; j--;
		}
	} while ( i<=j );
	
	if ( j > 0 ) Dfastsort_s(a, j);
	if ( N > i ) Dfastsort_s(a+i, N-i);
}

