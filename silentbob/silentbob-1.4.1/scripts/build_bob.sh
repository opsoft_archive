export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I..//include -I..//gclib/include"
export OPTS="-fPIC -DPIC"
export LDFLAGS=""
export LIBS=""

$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/base64.o ..//gclib/gclib_c/base64.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/buf.o ..//gclib/gclib_c/buf.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/dexec.o ..//gclib/gclib_c/dexec.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/dfastsort_s.o ..//gclib/gclib_c/dfastsort_s.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/dfork.o ..//gclib/gclib_c/dfork.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/dprogram_read.o ..//gclib/gclib_c/dprogram_read.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/gcio.o ..//gclib/gclib_c/gcio.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/ipc.o ..//gclib/gclib_c/ipc.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/misc.o ..//gclib/gclib_c/misc.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/network.o ..//gclib/gclib_c/network.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/scode.o ..//gclib/gclib_c/scode.c
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/gclib_c/string.o ..//gclib/gclib_c/string.c
gcc $LDFLAGS $LIBS -shared ..//gclib/gclib_c/base64.o ..//gclib/gclib_c/buf.o ..//gclib/gclib_c/dexec.o ..//gclib/gclib_c/dfastsort_s.o ..//gclib/gclib_c/dfork.o ..//gclib/gclib_c/dprogram_read.o ..//gclib/gclib_c/gcio.o ..//gclib/gclib_c/ipc.o ..//gclib/gclib_c/misc.o ..//gclib/gclib_c/network.o ..//gclib/gclib_c/scode.o ..//gclib/gclib_c/string.o -o libgclib_c.so 
export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I..//include -I..//gclib/include"
export OPTS="-fPIC -DPIC"
export LDFLAGS=""
export LIBS="-L./ -lgclib_c"

$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/darray.o ..//gclib/src/darray.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dconnection.o ..//gclib/src/dconnection.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/deprecated_dsplit.o ..//gclib/src/deprecated_dsplit.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dhash.o ..//gclib/src/dhash.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dheapsort.o ..//gclib/src/dheapsort.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/digests.o ..//gclib/src/digests.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/djobs.o ..//gclib/src/djobs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dlist.o ..//gclib/src/dlist.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dns.o ..//gclib/src/dns.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dns_low.o ..//gclib/src/dns_low.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dpbuf.o ..//gclib/src/dpbuf.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dpoll.o ..//gclib/src/dpoll.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dsplit.o ..//gclib/src/dsplit.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dstack.o ..//gclib/src/dstack.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/earray.o ..//gclib/src/earray.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/ehash.o ..//gclib/src/ehash.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/elist.o ..//gclib/src/elist.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/fs.o ..//gclib/src/fs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/gclib.o ..//gclib/src/gclib.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/gcollection.o ..//gclib/src/gcollection.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/hv.o ..//gclib/src/hv.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/linux_specific.o ..//gclib/src/linux_specific.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/network.o ..//gclib/src/network.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/Tree.o ..//gclib/src/Tree.cxx
gcc $LDFLAGS $LIBS -shared ..//gclib/src/darray.o ..//gclib/src/dconnection.o ..//gclib/src/deprecated_dsplit.o ..//gclib/src/dhash.o ..//gclib/src/dheapsort.o ..//gclib/src/digests.o ..//gclib/src/djobs.o ..//gclib/src/dlist.o ..//gclib/src/dns.o ..//gclib/src/dns_low.o ..//gclib/src/dpbuf.o ..//gclib/src/dpoll.o ..//gclib/src/dsplit.o ..//gclib/src/dstack.o ..//gclib/src/earray.o ..//gclib/src/ehash.o ..//gclib/src/elist.o ..//gclib/src/fs.o ..//gclib/src/gclib.o ..//gclib/src/gcollection.o ..//gclib/src/hv.o ..//gclib/src/linux_specific.o ..//gclib/src/network.o ..//gclib/src/Tree.o -o libgclib.so 
export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I..//include -I..//gclib/include"
export OPTS="-fPIC -DPIC"
export LDFLAGS=""
export LIBS="-L./ -lsblib -lgclib -lgclib_c -ldl"

$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/bob_init_compile_env.o ..//sblib/bob_init_compile_env.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/bob_prname.o ..//sblib/bob_prname.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/bob_set.o ..//sblib/bob_set.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/bob_tags.o ..//sblib/bob_tags.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/bugs.o ..//sblib/bugs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/ctags_tag.o ..//sblib/ctags_tag.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/cts.o ..//sblib/cts.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/def_test.o ..//sblib/def_test.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/find_cfiles.o ..//sblib/find_cfiles.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/find.o ..//sblib/find.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/get_onett_tag.o ..//sblib/get_onett_tag.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/globalsBlock.o ..//sblib/globalsBlock.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/globalsPrint.o ..//sblib/globalsPrint.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/globalVariables.o ..//sblib/globalVariables.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/got_tag.o ..//sblib/got_tag.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/macro_name.o ..//sblib/macro_name.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/make_pattern.o ..//sblib/make_pattern.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/mk_tag.o ..//sblib/mk_tag.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/mk_tags.o ..//sblib/mk_tags.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/name2obj.o ..//sblib/name2obj.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/nogui_fdump.o ..//sblib/nogui_fdump.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/nogui_tagsdump.o ..//sblib/nogui_tagsdump.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/offset_to_line.o ..//sblib/offset_to_line.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/py_tt.o ..//sblib/py_tt.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/skip_macro.o ..//sblib/skip_macro.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/split_tmp_files.o ..//sblib/split_tmp_files.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/split_to_words.o ..//sblib/split_to_words.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/sstrend.o ..//sblib/sstrend.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/sstrkill.o ..//sblib/sstrkill.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/the_fly.o ..//sblib/the_fly.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/the_tt.o ..//sblib/the_tt.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/t_op2.o ..//sblib/t_op2.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/t_op.o ..//sblib/t_op.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/wit.o ..//sblib/wit.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/words_count.o ..//sblib/words_count.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/ww_begin_line.o ..//sblib/ww_begin_line.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/ww_begin_offset.o ..//sblib/ww_begin_offset.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/ww_last_word.o ..//sblib/ww_last_word.cxx
gcc $LDFLAGS $LIBS -shared ..//sblib/bob_init_compile_env.o ..//sblib/bob_prname.o ..//sblib/bob_set.o ..//sblib/bob_tags.o ..//sblib/bugs.o ..//sblib/ctags_tag.o ..//sblib/cts.o ..//sblib/def_test.o ..//sblib/find_cfiles.o ..//sblib/find.o ..//sblib/get_onett_tag.o ..//sblib/globalsBlock.o ..//sblib/globalsPrint.o ..//sblib/globalVariables.o ..//sblib/got_tag.o ..//sblib/macro_name.o ..//sblib/make_pattern.o ..//sblib/mk_tag.o ..//sblib/mk_tags.o ..//sblib/name2obj.o ..//sblib/nogui_fdump.o ..//sblib/nogui_tagsdump.o ..//sblib/offset_to_line.o ..//sblib/py_tt.o ..//sblib/skip_macro.o ..//sblib/split_tmp_files.o ..//sblib/split_to_words.o ..//sblib/sstrend.o ..//sblib/sstrkill.o ..//sblib/the_fly.o ..//sblib/the_tt.o ..//sblib/t_op2.o ..//sblib/t_op.o ..//sblib/wit.o ..//sblib/words_count.o ..//sblib/ww_begin_line.o ..//sblib/ww_begin_offset.o ..//sblib/ww_last_word.o -o libsblib.so 

export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I..//include -I..//gclib/include"
export OPTS=""
export LDFLAGS=""
export LIBS="-L./ -lsblib -lgclib -lgclib_c -ldl"

$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/bob_call_tags.o ..//src/bob_call_tags.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/bob_cfiles.o ..//src/bob_cfiles.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/bob_cgrep.o ..//src/bob_cgrep.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/bob_flist.o ..//src/bob_flist.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/bob_globals.o ..//src/bob_globals.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/bob_got_structs.o ..//src/bob_got_structs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/bob_make_ctags.o ..//src/bob_make_ctags.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/bob_nogui_indent.o ..//src/bob_nogui_indent.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/bobParser.o ..//src/bobParser.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/bob_tree.o ..//src/bob_tree.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/brave.o ..//src/brave.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/init.o ..//src/init.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/log.o ..//src/log.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/main.o ..//src/main.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/modding.o ..//src/modding.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/opts_funcs.o ..//src/opts_funcs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/opts_globals.o ..//src/opts_globals.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/opts_settings.o ..//src/opts_settings.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/usage.o ..//src/usage.cxx
export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I..//include -I..//gclib/include"
export OPTS="-fPIC -DPIC"
export LDFLAGS=""
export LIBS="-L./ -lsblib -lgclib -lgclib_c -ldl"

$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_perl.o ..//plugins/plugin_perl.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_perl.o -o libplugin_perl.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_perlpackages.o ..//plugins/plugin_perlpackages.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_perlpackages.o -o libplugin_perlpackages.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_cache.o ..//plugins/plugin_cache.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_cache.o -o libplugin_cache.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_editor.o ..//plugins/plugin_editor.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_editor.o -o libplugin_editor.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_grep.o ..//plugins/plugin_grep.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_grep.o -o libplugin_grep.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_compile.o ..//plugins/plugin_compile.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_compile.o -o libplugin_compile.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_aap.o ..//plugins/plugin_aap.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_aap.o -o libplugin_aap.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_python.o ..//plugins/plugin_python.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_python.o -o libplugin_python.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_scons.o ..//plugins/plugin_scons.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_scons.o -o libplugin_scons.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_foreach.o ..//plugins/plugin_foreach.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_foreach.o -o libplugin_foreach.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_make.o ..//plugins/plugin_make.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_make.o -o libplugin_make.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_xml_project.o ..//plugins/plugin_xml_project.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_xml_project.o -o libplugin_xml_project.so 
