#!/bin/bash

export "prefix=../package"
rm -fr $prefix/*
mkdir -p $prefix/usr/bin/
cp ./silent_bob $prefix/usr/bin/
ln -s silent_bob $prefix/usr/bin/bob
ln -s silent_bob $prefix/usr/bin/tags
ln -s silent_bob $prefix/usr/bin/the_tt
ln -s silent_bob $prefix/usr/bin/gc_indent

mkdir -p $prefix/usr/lib/silent_bob
cp ./libplugin_*.so $prefix/usr/lib/silent_bob/
cp ./libsblib.so $prefix/usr/lib

