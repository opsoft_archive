/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_BOBPARSER_H
#define DEFINE_BOBPARSER_H

#include <best_names.h>
namespace BobParser {

enum LANG {
	LANG_C = 1,
	LANG_PERL,
	LANG_PYTHON
};

enum Flags {
	FL_DEBUG = (1<<0),
	FL_SAVE_OPS = (1<<1),
};

class Parser 
{
	public:
		Parser ();
		~Parser ();

		tt_state_t * initC (char * fileName);
		tt_state_t * initPerl (char * fileName);
		tt_state_t * initPython (char * fileName);

		Array * parse ();
		int opNo ();
		char * opText ();
		int nextOp ();
		int line ();
		int wit ();
		void opDump (FILE * stream);
		void fullDump (FILE * stream);
		__block_t * lastBlock ();
		int lastBlockLine ();

	private:
		void pushBlock ();
		void popBlock ();
		void addOp ();

		int m_opNo;
		char * d_out;
		char * d_ptr;
		int m_blockLevel;
		tt_state_t * m_tt;
		DStack * m_blockStack;
		Array * m_opList;
};

} // namespace BobParser
#endif

