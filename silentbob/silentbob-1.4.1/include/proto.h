/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

void nogui_tagsdump (char * f_name, int n_trip) ;
void opMacro (char ** d_ptr, char ** d_out, char ch) ;
int ww_begin_line (struct tt_state_t * d_tt_state, char *d_out, int d_offset) ;
char * ww_begin_offset (struct tt_state_t * d_tt_state, char *d_out, int d_offset) ;

char * make_pattern (char *op) ;

#include <inline.h>

/* sblib */
bool def_test (char * d_op) ;
int what_is_this (char * d_op, char ch) ;
char * sstrend (char * d_ptr) ;
void free_cword (c_word * S) ;
inline struct c_word * new_cword (char *word, char sym) ;
DArray * split_to_words (char * d_op) ;
char * cts (struct c_word * d_word) ;
char * ww_last_word (char *d_op) ;
int words_count (char *S) ;
struct tt_state_t * get_onett_tag (char * f_name, char ** d_tt_buf) ;
int sblib_find (char * path, char * name, char * f_outname) ;
int find_cfiles () ;
int split_tmp_files () ;
EHash * bob_init_compile_env () ;
void setParam (char *opt) ;
char * name2obj (char * name) ;
void globalsPrint (tt_state_t * tt, char * d_out, int d_found_type) ;
/**/

/*** ../sblib/globalsBlock.cxx ***/
bool globalsCheckUp (DStack * blocks_stack, int d_found_type) ;
void globalsPushBlock (DStack *stack, int type) ;
void globalsPopBlock (DStack *stack) ;

/*** ./brave.cpp ***/
char * try_name (char * ptr) ;
int fdecl_parse (char * d_out, DArray * d_vars) ;
char * sstrkill (char *OP) ;

/*** ./call_tags.cpp ***/
void call_tags_main (tt_state_t * d_tt_state, DArray * dout) ;
int call_tags (EArray * d_files) ;

/*** ./cgrep.cpp ***/
namespace cgrep {
	int file (tt_state_t * d_tt_state) ;
	int cgrep (EArray * d_files) ;
}

/*** ./flist.cpp ***/
char * flist_iter (char * d_ptr, char * d_name, bool b_repair) ;
void flist_main (char * d_file) ;

/*** ./globals.cpp ***/
void globals_main (struct tt_state_t * d_tt_state, int d_type) ;
void bob_globals (char * d_file, int d_type) ;

/*** ./got_structs.cpp ***/
void got_structs (char * d_file) ;

/*** ./got_tag.cpp ***/
int parse_regexp (char * d_file, char * S) ;
bool bob_tag (char *d_str, char * d_name, d_tag_t * d_tag) ;
EArray * got_tag (char * d_tag) ;

/*** ./init.cpp ***/
int sb_init () ;

/*** ./main.cpp ***/
int sb_prname (char *arg) ;
int main (int argc, char ** argv) ;

/*** ./make_tags.cpp ***/
void mk_tags (char *f_name, DArray *d_in) ;
void make_ctags (EArray * d_files) ;

/*** ./mk_tag.cpp ***/
char * macro_name (char * d_op, char * d_macro_name) ;
void mk_tag_macro (char * d_op, char * d_file, int t_line) ;
void mk_tag (char * d_op, char * d_file, int line, int d_found_type) ;
DArray * mk_tag_structtail_split (char *S) ;
void mk_tag_structtail (char * S, char * d_file, int t_line) ;

/*** ./modding.cpp ***/
env_t * sb_getenv (void) ;
char modding_optparse (int * i, int step) ;
int linux_modding_init () ;
void mods_info ();

/*** ./nogui_fdump.cpp ***/
bool brace_count (char * d_str, int * d_count, bool b_force_block) ;
void nogui_fdump (struct fdump_param_t * d_param) ;

/*** ./nogui_indent.cpp ***/
void nogui_indent () ;

/*** ./tags.cpp ***/
void tags (DArray * d_names, char * d_file_output) ;

/*** ./t_op.cpp ***/
char t_op (char ** d_in, char ** d_prev, bool b_no_operators = true) ;

/*** ./tree.cpp ***/
inline bool tree_immune (char * m_fname) ;
void rctree_iter (char * f_name, int n_trip) ;
void reverse_calltree (char * f_name) ;
void tree_echo (int n_trip, char *S) ;
inline void tree_mkpos (int N) ;
void call_tree (char * f_name, int n_trip) ;

/*** ./usage.cpp ***/
void usage () ;

/*** ./opts_globals.cpp **/
int opts_globals (DArray * d_opts, int & i) ;
int opts_settings (DArray * d_opts, int & i) ;
int opts_funcs (DArray * d_opts, int & i) ;

/*** ./sb_prnmame.cpp **/
int sb_prname (char *arg) ;

int bob_cfiles ();

