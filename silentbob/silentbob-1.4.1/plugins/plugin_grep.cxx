/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

extern "C" DArray * plugin_init (struct env_t *env);

struct grep_env_t {
        bool ignore_case;
        char * f_name;
};
struct grep_env_t ENV_grep;

void grep_one (char * buf, int size, char * search, int s_len)
{
        int nlines;
        char *s_line;
        char *ptr;
        char *end;
        char *end2;
        char * line_ptr;
        
        if (size < s_len)
                return;

        nlines = 1;
        ptr = buf;
        end = &buf[size - s_len - 1];
        end2 = &buf[size - 1];
	line_ptr = ptr;

        while (ptr <= end) {
                if (*ptr == '\n') {
                        line_ptr = ptr+1;
                        ++nlines;
                }
              
                if (! memcmp (ptr, search, s_len)) {
                        s_line = Dmid_getstr (line_ptr, end2);
                        if (! s_line) 
                                break;
                        chomp (s_line);
                        if (SB_FLGET (SB_FLTAGSTYLE)) 
                                printf ("%s\t%s\t%i\n", search, ENV_grep.f_name, nlines);
                        else
                                printf ("%s:%i: %s\n", ENV_grep.f_name, nlines, s_line);
                        free (s_line);
                        ptr += s_len;
                        continue;
                }
                ++ptr;
        }
}

void grep_loop (DArray * files, char * search)
{
        char * ptr;
        int s_len;
        int nfiles;
        int size;
        int i;
       
        if (! files || ! search)
                return;
       
        s_len = strlen (search);
        nfiles = files->get_size ();
        for (i = 0; i < nfiles; ++i) {
		size = 0;
                ptr = DFILE (files->get (i), &size);
                if (! ptr) {
			perror ("DFILE");
                        continue;
		}
                ENV_grep.f_name = files->get (i);
                grep_one (ptr, size, search, s_len);
		DROP (ptr);
        }
}

char grep_opt (DArray * d_opts, int * pos)
{
	int count;
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);
	if (EQ (S, "--grep")) {
		++(*pos);
		return 1;
	}

	return 0;
}

char grep_opt2 (DArray * d_opts, int * pos)
{
	char * S;
	
	if (! d_opts || ! pos)
		return 0;
	
	S = d_opts->get (*pos);
	if (EQ (S, "--grep")) {
                ++(*pos);
                S = d_opts->get (*pos);
                if (! S) {
                        printf ("SilentBob grep require parameter.\n");
                        exit (1);
                }
                grep_loop (ENV->d_files, S);
		exit (0);
	}

	return 0;
}

void grep_info ()
{
	printf ("Search of text.\n");
	printf ("Version: 1.0\n");
	printf ("options: --grep\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.short_info = grep_info;
	pm->opt = grep_opt;
	pm->opt2 = grep_opt2;

	Ret->add (LPCHAR (pm));
	return Ret;
}

