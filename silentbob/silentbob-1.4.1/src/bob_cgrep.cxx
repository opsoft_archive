/* 
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "head.h"
#include "structs.h"
#include "the_tt.h"
#include "dbg.h"

namespace cgrep {
	
/// смещение (в строках) строки str в буфере d_buf.
int stringOffset (char ** d_buf, char *str) {
	char * S;
	int len = strlen (str);
	int fix = 0;;

	S = *d_buf;
	while (true) {
		if (! strncmp (S, str, len))
			break;
		if (*S == '\n')
			fix--;
		--S;
	}

	*d_buf = S;
	return fix;
}
	
int fixPosition (tt_state_t *tt, int t_op_no, DArray * exp) {
	int fix = 0;
	char * d_buf;
	int i;
	
	if (! tt || ! exp)
		return 0;

	d_buf = tt->d_file_in + tt->d_attachment[t_op_no].offset;
	for (i = exp->get_size () - 1; i >= 0; --i) 
		fix += stringOffset (&d_buf, exp->get (i));

	return fix;
}

void print (tt_state_t * tt, DArray * d_array, DArray * d_lines) {
	int line;

	line = tt->d_attachment[ENV->t_op_no].pair_line + 1;
	line += fixPosition (tt, ENV->t_op_no, d_array);
	if (SB_FLGET (SB_FLTAGSTYLE)) 
		printf ("%s\t%s\t+%i\n", ENV->cgrep_exp, tt->d_file_name, 
				line);
	else 
		printf ("%s +%i: %s", tt->d_file_name, line, d_lines->get (line-1));

}

bool scan (char *S, DArray * d_array) {
	char * one;
	int size;
	int i;

	size = d_array->get_size ();
	for (i = 0; i < size; i++) {
		one = d_array->get (i);
		S = strstr (S, one);
		if (! S)  
			break;
		S += strlen (one);
	}

	if (i == size)
		return true;
	return false;
}


int file (tt_state_t * tt) {
	char * d_ptr, *d_out;
	DArray * d_array;
	DArray * d_lines = NULL;
	char ch;
	
	if (tt == NULL || 
			ENV->cgrep_exp == NULL ||
			tt->d_output == NULL)
		return -1;

	d_out = tt->d_output;
	d_ptr = tt->d_output;
	ENV->t_op_no = 0;
	
	d_array = Dsplit (ENV->cgrep_exp, ",");
	if (d_array == NULL) 
		return -2;
	
	if (! SB_FLGET (SB_FLTAGSTYLE)) {
		d_lines = new DArray (1024);
		d_lines->from_file (tt->d_file_name);
	}

	while (true) {
		ch = t_op (&d_ptr, &d_out);
		ENV->t_op_no++;
		if (! ch)
			break;
		if (! scan (d_out, d_array))
			continue;
		print (tt, d_array, d_lines);
	}

	if (d_lines) {
		d_lines->foreach (free);
		delete d_lines;
	}

	return 0;
}

int cgrep (EArray * d_files) {
	struct tt_state_t * tt;
	int i;

	if (! d_files)
		return -1;

	if (strchr (ENV->cgrep_exp, ' ') ||
	    strchr (ENV->cgrep_exp, '\t')) {
		fprintf (stderr, "You can't use spaces and tabs in expression.\n");
		return -1;
	}
	
	for (i = 0; i < d_files->get_size (); i++) {
		tt = CNEW (tt_state_t, 1);
		memset (tt, 0, sizeof (tt_state_t));
		
		tt->d_file_name = d_files->get (i);
		if (THE_TT::do_tt_file (tt) == NULL)
			continue; // broken file 

		file (tt);
		free_tt_state (tt);
	}		

	return 0;
}

} // namespace cgrep

