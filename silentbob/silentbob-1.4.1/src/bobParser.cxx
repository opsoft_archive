/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */
#include <head.h>

namespace BobParser {

Parser::Parser ()
{
	m_opNo = 0;
	d_out = 0;
	d_ptr = 0;
	m_tt = NULL;
}

Parser::~Parser ()
{
}

tt_state_t * Parser::initC (char * fileName)
{
	return NULL;
}

tt_state_t * Parser::initPerl (char * fileName)
{
	return NULL;
}

tt_state_t * Parser::initPython (char * fileName)
{
	return NULL;
}

Array * Parser::parse ()
{
}

int Parser::nextOp ()
{
	return 0;	
}

int Parser::line ()
{
	return 0;
}

int Parser::wit ()
{
	return 0;
}

void Parser::opDump (FILE * stream)
{
}

void Parser::fullDump (FILE * stream)
{
}

void Parser::pushBlock ()
{
}

void Parser::popBlock ()
{
}

__block_t * Parser::lastBlock ()
{
	return NULL;
}

int Parser::lastBlockLine ()
{
	return 0;
}

} // namespace BobParser

