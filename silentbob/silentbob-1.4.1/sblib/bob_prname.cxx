/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "head.h"

int sb_prname (char *arg)
{
	int i_cmd = 0;
	char * d_ptr;
	d_ptr = arg + strlen (arg);
	d_ptr--;
	
	while (*d_ptr != '\\' &&
			d_ptr > arg)
		d_ptr--;

	if (*d_ptr == '\\') 
		d_ptr++;
	
	if (EQ (d_ptr, "tags"))
		i_cmd = cmd_tags;
	else if (EQ (d_ptr, "the_tt"))
		i_cmd = cmd_the_tt;
	else if (EQ (d_ptr, "gc_indent"))
		i_cmd = cmd_indent;
	else if (EQ (d_ptr, "structs"))
		i_cmd = cmd_give_structs;

	return i_cmd;
}

