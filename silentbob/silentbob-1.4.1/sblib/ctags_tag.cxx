/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <head.h>

bool bob_tag (char *d_str, char * d_name, d_tag_t * d_tag)
{
	char m_buf[256];
	char *d_file;
	char *S;

	strcpy (m_buf, d_str);
	d_str = m_buf;
	
	strncpy (d_tag->d_name, d_name, 255);
	d_tag->d_name[255] = 0;
	S = strchr (d_str, '\t');
	if (! S)
		return false;

	S++;
	d_file = S;
	S = strchr (d_file, '\t');
	if (! S)
		return false;
	
	*S = 0;
	strncpy (d_tag->d_file, d_file, 255);
	d_tag->d_file[255] = 0;
	
	S++;

	if (if_digit (S)) 
		d_tag->d_line = atoi (S);
	else 
		return false;

	return true;
}

