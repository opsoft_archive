/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include "head.h"
#include "dbg.h"
/*
 * February 2006 - comments and strings BUG fixed...
 */

bool b_in_comment;

bool brace_count (char * d_str, int * d_count, bool b_force_block) // "nice"
{
	bool Ret = false;

	if (! d_str || ! d_count)
		return false; 
	
	while (*d_str != 0) {	
		if (!strncmp (d_str, "/*", 2)) {
			b_in_comment = true;
			d_str += 2;
			continue;
		}
		
		if (b_in_comment) {
			if (strncmp (d_str, "*/", 2)) {
				d_str ++;
				continue;
			} else {
				d_str += 2;
				b_in_comment = false;
				continue;
			}
		}		
		
		if (!strncmp (d_str, "//", 2)) 
			break;

		if (*d_str == '\"' || *d_str == '\'') {
			d_str = sstrend (d_str);
			if (d_str == NULL || *d_str == 0) {
				assert (true, "HimTeh 4");
				break;
			}
		}
		
		if (*d_str == '{') {
			Ret = true;
			(*d_count)++;
		}
		
		if (*d_str == '}') {
			Ret = true;
			(*d_count)--;
		}

		if (*d_str == ';' && *d_count == 0 && !b_force_block) {
			Ret = true;
			break;
		}
		d_str++;
	}
	
	return Ret;
}

void nogui_fdump (struct fdump_param_t * d_param)
{
	int d_count = 0;
	DArray d_array;
	FILE * d_file;
	int d_size;
	char * S;
	int i,a;

	if (!d_array.from_file (d_param->d_file_name))
		return;

	if (d_param->d_file_output == NULL)
		d_file = stdout;
	else
		d_file = fopen (d_param->d_file_output, "w");
	
	if (! d_file)
		return;
	
	if (! d_param->linear) {
		for (a = 0; a	< d_param->n_trip; a++)
			fprintf (d_file, "\t");
		fprintf (d_file, "//<***>\n");
	}
	
	i = d_param->d_line-1;
	d_size = d_array.get_size ();
	b_in_comment = false;
	
	if (d_array.get (i)[0] != '#') {
		while (i < d_size) {
			if (!d_param->linear) {
				for (a = 0; a	< d_param->n_trip; a++)
					fprintf (d_file, "\t");
			}
			fprintf (d_file, "%s", d_array.get(i));
				
			if (brace_count (d_array.get(i), &d_count, d_param->b_force_block) && !d_count)
				break;
			
			if (!d_count && ((i - d_param->d_line) > 2) && !d_param->b_force_block)
				break;
			
			i++;
		}
	} else {
		do {
			S = d_array.get (i);
			fprintf (d_file, "%s", S);
			S = &S[strlen (S)-2];
			while ((*S == ' ') || (*S == '\t'))
				S--;
			
			if (*S != '\\')
				break;
			i++;
		} while (i < d_size);
	}
	
	if (!d_param->linear) {
		for (a = 0; a	< d_param->n_trip; a++)
			fprintf (d_file, "\t");
		fprintf (d_file, "//</***>\n");
	}

	if (d_param->d_file_output != NULL)
		fclose (d_file);

	d_array.foreach (free); 

}

