/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include <head.h>
#include <dbg.h>
#include <inline.h>

DArray * split_to_words (char * d_op)	
{
	DArray * d_Ret = new DArray (16);
	char * d_old = strdup (d_op);
	bool b_done = false;
	char * S = d_old;
	char * d_end;
	char ch;
	
	if (d_Ret == NULL || d_old == NULL)
		return NULL;
	
	while (true) {
		b_done = false;
		d_end = S;
		
		if (*d_end == ' ')
			d_end++;
		
		while (*d_end) {
			if (!(if_abc(d_end) ||
				  if_digit (d_end) ||
				  *d_end == '_' || 
				  *d_end == ' ') )
				break;
			d_end ++;
		}

		if (! *d_end) {
			ch = 0;
			b_done = true;
			goto split_to_words_L1;
		}

		ch = *d_end;
		if (d_end[-1] == ' ')
			d_end[-1] = 0;
		else 
			*d_end = 0;

		while (*S && *S == ' ')
			S++;
		
split_to_words_L1:
		d_Ret->add (LPCHAR(new_cword (S, ch)));

		if (b_done)
			break;
		
		if (ch == '\"' || ch == '\'') {
			*d_end = ch;
			d_end = sstrend (d_end);
			assert (d_end == NULL, "Lena 1");
			if (*d_end == '\0' || *(++d_end) == '\0')
				break;
		}
		
		S = d_end + 1;
	}

	DROP (d_old);	
	return d_Ret;
}


