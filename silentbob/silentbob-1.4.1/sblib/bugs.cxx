/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include <head.h>

void bug_longmacro ()
{
	printf ("Too big macro."
			"If your macro have more than 300 lines, please "
			"contact <graycardinal@pisem.net>\n"
			"Program stopped.\n");
	
	exit (0);
}

void bug_nosuch_tag (char * f_name)
{
	printf ("Tag \"%s\" not found. Broken \"tags\" file ? "
			"Try \"silent-bob --make-ctags\".\n", f_name);
}

void bug_nocalltags ()
{
	printf ("File \"call_tags\" not found. "
			"Try \"silent-bob --call-tags [-L] <files>\"\n");
	exit (1);
}

void bug_system ()
{
	printf ("Can't make tags file. Maybe you do not have write permissions ?\n");
	exit (1);
}

void bug_fork ()
{
	perror ("fork");
	exit (1);
}

void bug_plugin (char *name)
{
	fprintf (stderr, "Can't load plugin (%s)\n", name);
}

void bug_notsupported ()
{
	printf ("SilentBob (or language plugin)"
	       " don't support this feature !\n");
}
