/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>
#include <env.h>
#include <bob_flags.h>

char * __bice_get (char *optname)
{
	char * value;
	value = ENV->settings->get (optname);
	if (! value)
		value = getenv (optname);
	return value;
}

EHash * bob_init_compile_env ()
{
	EHash * compile_env;
	char *cc;
	char *cxx;
	char *cflags;
	char *cxxflags;
	char *opts;
	char *include;
	char *libs;
	char *ldflags;

	compile_env = new EHash;

	cc = __bice_get ("CC");
	cxx = __bice_get ("CXX");
	cflags = __bice_get ("CFLAGS");
	cxxflags = __bice_get ("CXXFLAGS");
	opts = __bice_get ("OPTS");
	include = __bice_get ("INCLUDE");
	libs = __bice_get ("LIBS");
	ldflags = __bice_get ("LDFLAGS");

	if (! cc)
		cc = "gcc";
	if (! cxx)
		cxx = "g++";
	if (! cflags)
		cflags = "-O3 -Wall -pipe";
	if (! cxxflags)
		cxxflags = cflags;
	if (! opts)
		opts = "";
	if (! include)
		include = "";
	if (! libs)
		libs = "";
	if (! ldflags)
		ldflags = "";
	
	if (SB_FLGET (SB_FLVERBOSE)) {
		printf ("C compiler: %s\n", cc);
		printf ("C++ compiler: %s\n", cxx);
		printf ("C flags: %s\n", cflags);
		printf ("C++ flags: %s\n", cxxflags);
		printf ("OPTS: %s\n", opts);
		printf ("INCLUDE: %s\n", include);
		printf ("LIBS: %s\n", libs);
		printf ("LDFLAGS: %s\n", ldflags);
	}

	compile_env->set ("CC", strdup (cc));
	compile_env->set ("CXX", strdup (cxx));
	compile_env->set ("CFLAGS", strdup (cflags));
	compile_env->set ("CXXFLAGS", strdup (cxxflags));
	compile_env->set ("OPTS", strdup (opts));
	compile_env->set ("INCLUDE", strdup (include));
	compile_env->set ("LIBS", strdup (libs));
	compile_env->set ("LDFLAGS", strdup (ldflags));

	return compile_env;
}

