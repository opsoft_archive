/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <string.h>

char * name2obj (char * name)
{
	char *S;
	char m_buf[512];
	if (! name)
		return NULL;
	strcpy (m_buf, name);
	S = rindex (m_buf, '.');
	if (! S)
		return NULL;
	strcpy (S, ".o");
	return strdup (m_buf);	
}

