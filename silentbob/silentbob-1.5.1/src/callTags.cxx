/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include <head.h>
#include <wit.h>
#include <the_tt.h>
#include <dbg.h>
#include <fcntl.h>

/*! \brief Обработка "тэгов вызовов" для одного файла (после препроцессора).
 * \param tt - результат работы прецпроцессора.
 * \param dout - массив строчек результата. 
 */
void do_call_tags (tt_state_t * tt)
{
	DArray * d_words = NULL;
	bool b_inmacro = false;
	char * f_name = NULL;
	char *d_ptr, *d_out; // for t_op
	int bracket_depth = 0;
	char * d_old = NULL;
	char m_name[256];
	char * S;
	int wis;
	char ch;
	int i;

	if (! tt)
		return;
	
	ENV->t_op_no = 0;
	d_ptr = tt->result;
	d_out = d_ptr;

	while (true) {
		ch = t_op (&d_ptr, &d_out);
		ENV->t_op_no++;
		if (ch == 0)
			break;
	
		if ((*d_out == '#') && (def_test (d_out))) {
			if (macro_name (d_out, m_name) == NULL) {
				FDBG ("\tBUG: macro_name == NULL\n");
				fprintf (ENV->d_stream_dbg, "%s", d_out);
				LN;
			} else 
				f_name = m_name;
			b_inmacro = true;			
		}
		
		if (! bracket_depth) {
			wis = what_is_this (d_out, ch);
			if (wis == OT::Function) {
				f_name = ww_last_word (d_out);
				d_old = f_name;
			}
		}

		if (ch == '{')
			++bracket_depth;

		if (ch == '}') {
			--bracket_depth;
			if (bracket_depth < 0)
				bracket_depth = 0;
		/*	if (! bracket_depth) {
				f_name = NULL;
			}*/
		}

		if (ch == '\n' && d_out[strlen(d_out) - 1] != '\\') {
			b_inmacro = false;
			f_name = d_old;
		}
	
		if (!b_inmacro && !bracket_depth)
			continue;
		
		d_words = split_to_words (d_out);
				
		for (i = 0; i < d_words->get_size (); i++) {
			S  = cts ((c_word *) d_words->get (i));
			if (! S) 
				continue;

			if (f_name != NULL) {
				printf ("%s\t%s\t%i\t;\tby\t%s\n", S, 
						tt->fileName,
						tt->attachment[ENV->t_op_no].pair_line+1,
						f_name);
			} else {				
				printf ("%s\t%s\t%i\n", S,
					tt->fileName,
					tt->attachment[ENV->t_op_no].pair_line+1);
			}
		}

		d_words->foreach ((Dfunc_t) free_cword);
		delete d_words; 
	}

	fflush (stdout);
}

int call_tags_file (char * fileName)
{
	struct tt_state_t * tt;
	
	tt = CNEW (tt_state_t, 1);
	memset (tt, 0, sizeof (tt_state_t));
	tt->fileName = strdup (fileName);
	if (THE_TT::do_tt_file (tt) == NULL) {
		DROP (tt);
		return -1;
	}
		
	do_call_tags (tt);		
	free_tt_state (tt);
	return 0;
}

int call_tags_multi (EArray * d_files)
{
	char m_buf[512];
	__djob_t * j;
	int i;

	if (! d_files)
		return -1;

	d_files->strings_to_file (ENV->tmp_files);
	split_tmp_files ();

	for (i = 0; i < ENV->max_proc; ++i) {
		j = ENV->proc_list->fork ();
		if (j->child) {
			sprintf (m_buf, "silent_bob -L %s%i --thread --call-tags", ENV->tmp_files, i);
			exit (execlp ("sh", "sh", "-c", m_buf, NULL));
		}
		usleep (500);
	}

	while ((j = ENV->proc_list->wait_all ()) && j) 
		Dexec_done (j);

	mk_tags ("./call_tags", NULL);
	remove_tmp_files ();
	return 0;
}


/// Точка входа для --call-tags
int call_tags (EArray * d_files)
{
	struct tt_state_t * tt;
	int fd;
	int i;

	if (!d_files || d_files->get_size () == 0) {
		fprintf (stderr, "No such files.\n");
		return -1;
	}

	if (! SB_FLGET (SB_FLTHREAD))
		unlink (ENV->tmp_tags);

	if (ENV->max_proc > 1) 
		return call_tags_multi (d_files);		

	fd = open (ENV->tmp_tags, O_APPEND | O_WRONLY, 0666);
	if (fd < 0) {
		fd = open (ENV->tmp_tags, O_CREAT | O_APPEND | O_WRONLY, 0666);
		if (fd < 0)
			return -1;
	}

	dup2 (fd, 1);
	for (i = 0; i < d_files->get_size (); i++)
		call_tags_file (d_files->get (i));
   
	close (fd);
	fclose (stdout);
	if (! SB_FLGET (SB_FLTHREAD)) {
		mk_tags ("./call_tags", NULL);
		remove_tmp_files ();
	}
	
	return 0;
}
