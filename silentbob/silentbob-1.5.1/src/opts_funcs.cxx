/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <head.h>
#include <wit.h>

int opts_funcs (DArray * d_opts, int & i)
{
	char * opt;

	if (! d_opts)
		return -1;
	
	opt = d_opts->get (i);

	if (EQ (opt, "--help") || EQ (opt, "-h")) {
		usage ();
		exit (0);
	}
	
	if (EQ (opt, "-V") || EQ (opt, "--version")) {
		usage ();
		exit (0);
	}
	
	if (EQ (opt, "--file")) {
		ENV->sb_cmd = cmd_file;
		return 0;
	}
	
	if (EQ (opt, "--the-tt")) {
		ENV->sb_cmd = cmd_the_tt;		
		return 0;
	}
	
	if (EQ (opt, "--structs")) {
		ENV->sb_cmd = cmd_give_structs;
		return 0;
	}
	
	if (EQ (opt, "--indent")) {
		ENV->sb_cmd = cmd_indent;
		return 0;
	}
	
	if (EQ (opt, "--tags")) {
		ENV->sb_cmd = cmd_tags;
		return 0;
	}
	
	if (EQ (opt, "--make-ctags") ||
			EQ (opt, "-c")) {
		ENV->sb_cmd = cmd_makectags;
		return 0;	
	}
	
	if (EQ (opt, "--call-tags") ||
			EQ (opt, "-ct")) {
		SB_FLSET (SB_FLTAGSTYLE); 
		ENV->sb_cmd = cmd_call_tags;
		return 0;
	}
	
	if (EQ (opt, "--cgrep")) {
		if (++i >= d_opts->get_size ())
			return 0;
		
		ENV->sb_cmd = cmd_cgrep;
		ENV->cgrep_exp = d_opts->get (i);
		return 0;
	}
	
	if (EQ (opt, "--plugins-info")) {
		mods_info ();
		exit (0);
	}
	
	if (EQ (opt, "--tags")) {
		ENV->sb_cmd = cmd_tags;
		return 0;
	}
	
	if (EQ (opt, "--cfiles") ||
			EQ (opt, "-f")) {
		bob_cfiles ();
		exit (0);
	}

	if (EQ (opt, "--time-test")) 
		Dtimer ();

	return -1;
}

