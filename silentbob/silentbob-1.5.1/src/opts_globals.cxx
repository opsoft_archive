/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <head.h>
#include <wit.h>

int opts_globals (DArray * d_opts, int & i)
{
	int old_cmd = ENV->sb_cmd;

	ENV->sb_cmd = cmd_globals;
	if (EQ (d_opts->get (i), "--globals")) {
		ENV->d_global_type = OT::Typedef |
			OT::Variable |
			OT::Struct |
			OT::Function |
			OT::Define |
			OT::Class |
			OT::Namespace;
	} else if (EQ (d_opts->get (i), "--globals-typedef")) 
		ENV->d_global_type |= OT::Typedef;
	else if (EQ (d_opts->get (i), "--globals-extern")) 		
		ENV->d_global_type |= OT::Extern;
	else if (EQ (d_opts->get (i), "--globals-function")) 
		ENV->d_global_type |= OT::Function;
	else if (EQ (d_opts->get (i), "--globals-struct"))
		ENV->d_global_type |= OT::Struct;
	else if (EQ (d_opts->get (i), "--globals-variable"))	
		ENV->d_global_type |= OT::Variable;
	else if (EQ (d_opts->get (i), "--globals-define"))
		ENV->d_global_type |= OT::Define;
	else if (EQ (d_opts->get (i), "--globals-class")) 
		ENV->d_global_type |= OT::Class;
	else {
		ENV->sb_cmd = old_cmd;
		return -1;
	}

	return 0;
}

