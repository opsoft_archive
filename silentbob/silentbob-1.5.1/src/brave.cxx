/* 
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#if 0
#include "head.h"
#include "dbg.h"

char * try_name (char * ptr) 
{
	int len;
	char * end;
	
	if (! abc_test (ptr))
		return NULL;

	while (*ptr == '*' || *ptr == '&')
		ptr++;
	
	len = strlen (ptr);
	end = &ptr[len-1];

	while (end > ptr && (*end == '&' || *end == '*'))
	   end--;	
	
	end++;
	*end = 0;
	return ptr;	
}

struct hash_t * parse_variable (char *OP)
{
	hash_t * Ret = NULL;
	DArray * words; 
	char * op;
	char * S;
	int size;
	char * var = NULL;
	char * type = NULL;
	 
 	if (! OP)
	  return NULL;

	op = strdup (OP);
	ww_last_word (op);

	words = Dsplit (op, " ");
	if (! words)
	  goto tpv_out;
	
	size = words->get_size();
	if (size < 2)
		goto tpv_out;
	
	while (size > 0) {
		S = words->get (size-1);

		S = try_name (S);
		if (! S) {
			--size;
			continue;
		}
		
		if (var == NULL) {
			var = S;
		} else {
			type = S;
			break;
		}

		--size;
	}

	if (var && type) 
		Ret = hash (var, type);	
	
tpv_out:	
	if (words)
	  words->foreach (free);
	DROP (words);
	DROP (op);
	return Ret;
}

int fdecl_parse (char * d_out, DArray * d_vars)
{
	DArray * words;
	char * ptr;
	int i;
	int size;
	struct hash_t * one;
	
	/* 
	 * 1. Arguments check.
	 * 2. Function declaration parse.
	 * 3. Function arguments parse.
	 *
	 */
	
	if (d_out == NULL || d_vars == NULL)
		return -1;

	ptr = Dstrmid (d_out, "(", ")");
	if (ptr == 0) 
	  return -1;

	sstrkill (ptr);

	words = Dsplit (ptr, ",");
	if (! words)
		return -1;

	size = words->get_size ();
	if (! size)
		return 0;

	for (i = 0; i < size; i++) {
		one = parse_variable (words->get (i));
		if (one)
			d_vars->add (LPCHAR(one));
	}
	
	DROP (ptr);
	return 0;
}
#endif

