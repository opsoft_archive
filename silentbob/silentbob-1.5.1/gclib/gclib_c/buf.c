/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib_c.h>
#define __export

__export void buf_W8 (char ** pkt, unsigned char data)
{
	unsigned char *ptr = (unsigned char *) *pkt;
	*ptr = data;
	(*pkt)++;
}

__export void buf_W16 (char ** pkt, uint16_t data)
{
	uint16_t *ptr = (uint16_t *) *pkt;
	*ptr = data;
	(*pkt)+=2;
}

__export void buf_W32 (char ** pkt, unsigned int data)
{
	unsigned int *ptr = (unsigned int *) *pkt;
	*ptr = data;
	(*pkt)+=4;
}

__export void buf_WS (char ** pkt, char *S)
{
	char * ptr = *pkt;
	memcpy (ptr, S, strlen (S));
	(*pkt) += strlen (S);	
}

__export void buf_WSZ (char ** pkt, char *S)
{
	char * ptr = *pkt;
	strcpy (ptr, S);
	(*pkt) += strlen (S) + 1;
}

__export void buf_WD (char ** pkt, char *S, int size)
{
	char * ptr = *pkt;
	memcpy (ptr, S, size);
	(*pkt) += size;
}

__export unsigned char buf_R8 (char ** pkt)
{
	unsigned char ch = 0;
	ch = *((unsigned char *) *pkt);
	++(*pkt);
	return ch;			
}

__export uint16_t buf_R16 (char ** pkt)
{
	uint16_t ret = 0;
	ret = *((uint16_t *) *pkt);
	(*pkt) += 2;
	return ret;
}

__export uint32_t buf_R32 (char ** pkt)
{
	uint32_t ret = 0;
	ret = *((uint32_t *) *pkt);
	(*pkt) += 4;
	return ret;
}

__export char * buf_RD (char ** pkt, int len)
{
	char * ret = NULL;
	ret = (char *) memdup (*pkt, len);
	(*pkt) += len;
	return ret;
}

