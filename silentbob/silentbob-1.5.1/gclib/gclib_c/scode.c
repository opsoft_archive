/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

void bxor (char * data, int size, char *key, int key_size)
{
	int i,k;
	k = 0;
	
	for (i = 0; i < size; ++i || ++k) {
		if (k == key_size)
			k = 0;
		data[i] ^= key[k];
	}		
}
