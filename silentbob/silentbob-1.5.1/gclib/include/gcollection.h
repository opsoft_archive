/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_GCOLLECTION_H
#define DEFINE_GCOLLECTION_H
class GCollection {
	public:
		GCollection ();
		~GCollection ();
		int init (int size, int count, char * buf, int buf_size);
		int init_simple (char * heap, int chunk_size, int count);
		char * alloc ();
		void free (char * one);
		int get_requiredspace (int chunk_size, int count);
		bool check (char * ptr);
		
	private:
		 __dlist_entry_t * __o2c (char * ptr);
		char * buf;
		bool free_buf;
		DList * entries;
		int count;
		int chunk_size;
		__dlist_entry_t * chunks;
};

#endif

