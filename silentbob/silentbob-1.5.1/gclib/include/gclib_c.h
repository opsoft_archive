/*
 * (c); Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_GCLIBC_H
#define DEFINE_GCLIBC_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h> 
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/poll.h>

#ifdef gclib_c__export
#undef gclib_c__export
#endif

#ifdef __cplusplus
#define gclib_c__export extern "C"
#else
#define gclib_c__export extern
typedef char bool;
#endif

#include "djob_t.h"
#include "dexec.h"

/* buf.c */
gclib_c__export void buf_W8 (char ** pkt, unsigned char data);
gclib_c__export void buf_W16 (char ** pkt, uint16_t data);
gclib_c__export void buf_W32 (char ** pkt, unsigned int data);
gclib_c__export void buf_WS (char ** pkt, char *S);
gclib_c__export void buf_WSZ (char ** pkt, char *S);
gclib_c__export void buf_WD (char ** pkt, char *S, int size);
gclib_c__export unsigned char buf_R8 (char ** pkt);
gclib_c__export uint16_t buf_R16 (char ** pkt);
gclib_c__export uint32_t buf_R32 (char ** pkt);
gclib_c__export char * buf_RD (char ** pkt, int len);
#define buf_Wstruct(pkt, s) do { buf_WD (pkt, (char *) s, sizeof (*s)); } while (0) 
#define buf_Rstruct(arga, argb) (argb *) buf_RD (arga, sizeof (argb))
/**/

gclib_c__export void print_the_time (FILE * file_my);
gclib_c__export int close_pipe (int *fds);
gclib_c__export int Dfork (char *exec, int *fd_in, int *fd_out, int *fd_err);
gclib_c__export void Dfastsort_s(char ** a, long N);
gclib_c__export void Dtimer ();
gclib_c__export struct timeval *the_time ();

gclib_c__export int Dterm_one_kick (int fd);
gclib_c__export char *Dversion ();
gclib_c__export int Dfnwrite (char * p_lpsz_filename, void * p_lp_buffer,int int_size);
gclib_c__export int Dfnread (char * f_name, void * p_lp_buffer, int int_size);
gclib_c__export int Dselect (int FILENO, int SEC, int USEC);
gclib_c__export char * DFILE (const char * m_filename, int *rsize);
gclib_c__export struct stat * DSTAT (const char * S);
gclib_c__export struct stat * DLSTAT (const char * S);
gclib_c__export int DIONREAD (int fd);
gclib_c__export int fsize (const char * S);
gclib_c__export int fdsize (int fd);
gclib_c__export char * DFDMAP (int fd);
gclib_c__export char * DFMAP (const char *d_file, int *out_fd, int *d_out_size);
gclib_c__export char * Dread_to_eof (int fd, int *d_out_size);
gclib_c__export int move_stream (int fd_in, int fd_out);
gclib_c__export int Dnonblock(int fd);
gclib_c__export char * gc_realloc (char * PTR, int old_size, int new_size);
gclib_c__export void * memdup (void * PTR, int size);
gclib_c__export int Dsplit(char * lpsz_String, char *ch, char ** outbuffer, int int_buffersize);
gclib_c__export char * Dstrmid(char * lpsz_string,char * param1, char * param2);
gclib_c__export char * chomp (char * S);
gclib_c__export char * DSTR (FILE * m_file);
gclib_c__export char * strchr_r (char * S, char ch, int d_len);
gclib_c__export char * strchrs (char *S, char ch, char ch2, char ch3, char ch4);
gclib_c__export char * Dstrstr_r (char *where, char * str); 
gclib_c__export char * Dstrndup (char *ptr, int n);
gclib_c__export char * Dtimestr (char * buf, int max);
gclib_c__export int Dsyms (char * from, char * to, char sym);
gclib_c__export char * Dmemchr (char *from, int n, char ch);
gclib_c__export int Dunix_sendto(void * lp_data,int int_size,char * lpsz_path);
gclib_c__export int Dsendto (int sock, void * lp_data, int int_size, const char * address,int port);
gclib_c__export int Dsendto2 (void * lp_data, int int_size, const char * address, int port);
gclib_c__export int Dbind (int sock, char * addr, int port);
gclib_c__export int Dsocket ();
gclib_c__export int Dsocket_udp ();
gclib_c__export int Dconnect (int sock, const char * addr, int port);
gclib_c__export int Dgethostbyname(const char * lpsz_hostname,struct in_addr * address);
gclib_c__export char * Dgetnamebyhost(struct in_addr * params);
gclib_c__export int Drecvfrom (int fd, char * buf, int size, char * peer_ip, int * peer_port);
gclib_c__export int Dbroadcast (int fd); 
gclib_c__export int Dpoll_scan (struct pollfd **p, int count, int position);
gclib_c__export char * Dmid_strchr (char *ptr, char *end, char ch);
gclib_c__export char * Dmid_getstr (char *buf, char *end);
gclib_c__export int Dtransmit (char *f_name, int sd);
gclib_c__export int Dtransmit_udp (char *f_name, int sd, char *ip, uint16_t port);
gclib_c__export char * Drand_str (char * buf, int count);
gclib_c__export char * int2str (int i);
gclib_c__export char * Dprogram_read (char *EXEC, int * len_out);
gclib_c__export char * stail (char *S);
gclib_c__export char * strmov (char *buf, char * S);
gclib_c__export char * strip (char *str) ;
gclib_c__export char * strip2 (char *str) ;
gclib_c__export char * Dmemmem (char *haystack, size_t haystacklen, char *needle, size_t needlelen) ;
gclib_c__export char * Dmid_memmem (char * begin, char * last, 	char * needle, size_t needlelen) ;
gclib_c__export int Dtmpfd (char *name) ;
gclib_c__export int fdclose (int * fd) ;
gclib_c__export char * fext (char *name) ;
gclib_c__export char * Dsprintf (char * fmt, ...) ;
gclib_c__export int logToFile (char * fileName, char * fmt, ...) ;
gclib_c__export int copyFile (char * sourceName, char * destName) ;

/* IPC */
gclib_c__export int sem_init (int * KEY);
gclib_c__export int sem_init_rw (int * KEY);
gclib_c__export int down (int sem);
gclib_c__export int up (int sem);
gclib_c__export int down_read (int sem);
gclib_c__export int down_write (int sem);
gclib_c__export int up_read (int sem);
gclib_c__export int up_write (int sem);
/**/

gclib_c__export void bxor (char * src, char *key, int count, int key_size);

#define if_digit(S) ((*S >= '0') && (*S <= '9'))
#define if_abc(S) ((*S >= 'a' && *S <= 'z') || (*S >= 'A' && *S <= 'Z'))
#define OR(var,val1,val2) ((var == val1) || (var == val2))

#ifndef EQ
#define EQ(arga, argb) (!strcmp (arga, argb))
#endif

#ifndef NE
#define NE(arga, argb) (strcmp (arga, argb))
#endif

#ifndef chop
#define chop(arg1) arg1[strlen(arg1) - 1] = 0
#endif

#define DROP(arga) if (arga) { free (arga); arga = NULL; }
#define CNEW(arga,argb) (arga *)malloc (sizeof (arga)*(argb))

#undef gclib_c__export
#endif

