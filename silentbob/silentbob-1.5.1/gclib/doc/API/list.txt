/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

UTF8

	Список.

                DList   
Добавление (данные) :
	char * add_head (char *);
	char * add_tail (char *);
	char * add_after (__dlist_entry_t *, char *);
	char * add_before (__dlist_entry_t *, char *);
Добавление (элементы) :
	char * add_entry_head (__dlist_entry_t * one);
	char * add_entry_tail (__dlist_entry_t * one);
	char * add_entry_after (__dlist_entry_t *, __dlist_entry_t *);
	char * add_entry_before (__dlist_entry_t *, __dlist_entry_t *);

	rm / remove / del
Удаление элемента (без освобождение памяти).

	void detach (__dlist_entry_t * one);
Удаление элемента ("убрать из списка"). Память не освобождается.

	foreach (Dfunc_t);
Выполнить для всех. В функцию передаются данные, а не элементы.

                EList (от DList)
Список с контролем текущей позиции.
        first / last / next / prev / get / del

