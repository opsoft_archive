/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

KOI8-R

	�������������� ������.

	DArray (int asize = 0);
	init (int asize);
�����������. ���������������� ������ ��� asize ���������. ���� asize==0, ����������� �������� 16. 

	get / set
�������� / ���������� ������� � �������� �������. 

	get_size
������� ���������� ���������.

	void foreach (Dfunc_t FUNC);
��������� ��� ���� ���������.

	bool from_file (char * __from);
��������� ������ ����� �� �����.

	add 
�������� �������. 

	drop
�������� (��������) ������.

