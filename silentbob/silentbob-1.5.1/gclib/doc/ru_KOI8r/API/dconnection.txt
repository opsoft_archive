/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

KOI8-R
        DConnection - ����� "�����������".
�����, ����������� ����� - ���� �����������. ������� ���������� -
TCP/IP, UDP/IP ������, � �����.

        ipv4_init () / ipv4_init_udp ();
���������������� TCP / UDP �����.

        ipv4_bind (char *ip, uint16_t port) / ipv4_connect (char *ip, uint16_t port)
bind/connect ����������. ���� - �� ������� ������.
                
        int ipv4_udp_connect (char *ip, uint16_t port);
���������� ����� �������� ����� ���������� UDP'���.

        ipv4_send (char * buf, int len) / ipv4_recv (char * buf, int len)
������� / �������� ������.

        update_ctime (time_t d_time) / update_mtime (time_t d_time)
�������� ����� �������� / ����������� ���������� ����������. ����
�������� �� ������, ������� ������� �����.

        DIONREAD ()
������� ���������� ����, ��������� ��� ������.

        open_ro / open_rw
������� ���� �� ������ / ������+������.

        open
������� ���� (��������� ���������� open(2))

        read / write / close
���������� ���������, �������� ����������� ����������.

        poll_config (int flags)
���������� ����� ��� poll(2) ����������.

        getsockname / getpeername / getpeerport / getsockport 
�� ��, ��� � ���������. IP - ������ ("xxx.xxx.xxx.xxx"), ���� - �� ������� ������.

        get_ctime / get_mtime () 
���������� ����� �������� / ����������� ����������.

	��������� ���������� :
c_sd - ����� ����������. ����� -1 ���� �� ���������������.
c_ctime - ����� ����� ���� ����������� ����������. ������������� ����������� � ipv4_connect � ipv4_udp_connect
c_cname / c_cport - ����� / ���� �������.
c_pname / c_pport - ����� / ���� ���������� �����.

