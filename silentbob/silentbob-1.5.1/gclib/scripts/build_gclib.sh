#!/bin/sh -v
#!/bin/sh

export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I../include -I../"
export OPTS="-fPIC"
export LDFLAGS=""
export LIBS=""

$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/darray.o ../src/darray.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/dconnection.o ../src/dconnection.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/deprecated_dsplit.o ../src/deprecated_dsplit.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/dhash.o ../src/dhash.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/dheapsort.o ../src/dheapsort.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/digests.o ../src/digests.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/djobs.o ../src/djobs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/dlist.o ../src/dlist.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/dns.o ../src/dns.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/dns_low.o ../src/dns_low.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/dpbuf.o ../src/dpbuf.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/dpoll.o ../src/dpoll.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/dsplit.o ../src/dsplit.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/dstack.o ../src/dstack.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/earray.o ../src/earray.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/ehash.o ../src/ehash.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/elist.o ../src/elist.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/fs.o ../src/fs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/gclib.o ../src/gclib.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/gcollection.o ../src/gcollection.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/hv.o ../src/hv.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/linux_specific.o ../src/linux_specific.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ../src/network.o ../src/network.cxx
gcc $LDFLAGS $LIBS -shared ../src/darray.o ../src/dconnection.o ../src/deprecated_dsplit.o ../src/dhash.o ../src/dheapsort.o ../src/digests.o ../src/djobs.o ../src/dlist.o ../src/dns.o ../src/dns_low.o ../src/dpbuf.o ../src/dpoll.o ../src/dsplit.o ../src/dstack.o ../src/earray.o ../src/ehash.o ../src/elist.o ../src/fs.o ../src/gclib.o ../src/gcollection.o ../src/hv.o ../src/linux_specific.o ../src/network.o -o libgclib.so 
#!/bin/sh

export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I../include -I../gclib_c"
export OPTS="-fPIC"
export LDFLAGS=""
export LIBS=""

$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/base64.o ../gclib_c/base64.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/buf.o ../gclib_c/buf.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/dexec.o ../gclib_c/dexec.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/dfastsort_s.o ../gclib_c/dfastsort_s.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/dfork.o ../gclib_c/dfork.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/dprogram_read.o ../gclib_c/dprogram_read.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/gcio.o ../gclib_c/gcio.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/ipc.o ../gclib_c/ipc.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/misc.o ../gclib_c/misc.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/network.o ../gclib_c/network.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/scode.o ../gclib_c/scode.c
$CC $CXXFLAGS $OPTS $INCLUDE -c -o ../gclib_c/string.o ../gclib_c/string.c
gcc $LDFLAGS $LIBS -shared ../gclib_c/base64.o ../gclib_c/buf.o ../gclib_c/dexec.o ../gclib_c/dfastsort_s.o ../gclib_c/dfork.o ../gclib_c/dprogram_read.o ../gclib_c/gcio.o ../gclib_c/ipc.o ../gclib_c/misc.o ../gclib_c/network.o ../gclib_c/scode.o ../gclib_c/string.o -o libgclib_c.so 
