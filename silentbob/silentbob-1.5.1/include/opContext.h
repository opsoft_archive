/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef OPCONTEXT_H
#define OPCONTEXT_H

struct opContext
{
	char * op; 	
	char ch;
	int opNo;
	int line;
	int opType;
	int blockLevel;
};

#endif

