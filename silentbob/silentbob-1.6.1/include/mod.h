/* 
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_MOD_H
#define DEFINE_MOD_H

enum plug_type_t {
	TYPE_LANGUAGE = 0,
	TYPE_OUTPUT,
	TYPE_INPUT,
	TYPE_GUI,
	TYPE_INDENT,
	TYPE_FEATURE	
};

struct mod_t {
	int Type;
	int ID;
	char * Version;
	char * Name;
	char Enabled;
        char * mod_file;
	void (*short_info) ();
	void (*usage) ();
};

struct mod_feature {
	struct mod_t mod;
	char (*opt) (DArray * d_opts, int * pos);
	char (*opt2) (DArray * d_opts, int * pos);
};

/* Language plugin. */
struct mod_language {
	struct mod_t mod;
	char *language;
	char *(*the) (struct tt_state_t *d_tt_state); // --the-tt
	int (*make_ctags) (char *f_name, FILE *ofile); // SilentBob --make-ctags code body.
	int (*call_tags) (char *f_name); // SilentBob --call-tags code body.
	int (*print_tags) (char * f_name); // tags <tag>
	void (*file) (DArray * d_files); // sb <files> --file
};

int modding_init ();
int modding_start (int i_cmd);
int modding_load_plugin (char * name, char * path);
#endif
