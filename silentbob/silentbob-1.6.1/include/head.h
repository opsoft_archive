/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#ifndef DEFINE_HEAD_H
#define DEFINE_HEAD_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <structs.h>
#include <env.h>
#include <proto.h>
#include <log.h>
#include <bob_flags.h>

#ifdef HAVE_CONFIG_H
# include <config.h>
#else
# define PACKAGE "silent_bob"
#endif

#endif 

