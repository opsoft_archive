/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 * (??? :) )
 *
 * --cgrep and --grep for C should work correctly with C#
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>
#include <the_tt.h>
#include <TT.h>

extern "C" DArray * plugin_init (struct env_t *env);
struct env_t *ENV;

// At the first operation cycle of options.
char csharp_opt (DArray * d_opts, int * pos)
{
	int count;
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);
	if (EQ (S, "--csharp")) { 
		ENV->language = "csharp";
		return 1;
	}

/*	if (EQ (S, "--other-option")) {
//		if (++(*pos) >= count) // For an option the parameter is necessary.
//			return 0;
	
		return 1;
	} */

	return 0;
}

// At the second operation cycle of options.
char csharp_opt2 (DArray * d_opts, int * pos)
{
	if (! d_opts || ! pos)
		return 0;
	
	if (NE (ENV->language, "charp"))
		return 0;

	if (EQ (d_opts->get (*pos), "--files")) {
	/*	unlink ("./csharp_files");
		sblib_find ("./", "*.cs", "./csharp_files");
		*/
		fprintf (stderr, "Option not supported currently\n");
		exit (0);
	}

	return 0;
}


/// It is carried out for each file
/// $ bob --csharp <files> --make-ctags 
int csharp_make_ctags (char * f_name, FILE * of)
{
	TT * tt;
	char * op;
	int m_type;

	if (! f_name) {
		// To find all C# files. (see sblib_find sblib/Sblib.cpp)
		 
		return 0;
	}

	tt = new TT;
	tt->loadFile (f_name);
	tt->init ();

	// Cycle of pass on operators
	while (true) {
		op = tt->nextOperator ();
		if (tt->ch == 0)
			break;
/*
		tt->line (); // Current line
		tt->ch; // Closing symbol of the operator ( {,},;,\n)
		tt->bracketDepth; // Degree of an enclosure {/}

		// See also (sblib/sblib.cxx) :
		// DArray * split_to_words (char * d_op); 
		// int words_count (char *S);
		// char * ww_last_word (char *d_op); */

		// To deduce a line in "of" for each found tag.
		// line format : <tagname>\t<file>\t<line>\n"
		printf ("OP: %s\n", op);
	}

	delete tt;
	return 0;
}

// It is carried out at start $ bob - plugins-info
void csharp_plugin_info ()
{
	printf ("C# plugin.\n");
	printf ("Version: 0.1\n");
	printf ("options: --csharp\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_language * ml;
	struct mod_feature * mf;

	ml = new mod_language;
	mf = new mod_feature;
	Ret = new DArray (2);
	
	memset (ml, 0, sizeof (mod_language));
	memset (mf, 0, sizeof (mod_feature));
	ENV = env;

	ml->mod.Type = TYPE_LANGUAGE;
	ml->mod.Version = strdup ("0.1");
	ml->mod.short_info = csharp_plugin_info;
	ml->language = strdup ("csharp");
	ml->the = THE_TT::do_tt_file;
	ml->make_ctags = csharp_make_ctags;
//	ml->call_tags = 

	mf->mod.Type = TYPE_FEATURE;
	mf->mod.Version = strdup ("0.1");
	mf->opt = csharp_opt;
	mf->opt2 = csharp_opt2;

	Ret->add (LPCHAR (ml));
	Ret->add (LPCHAR (mf));
	
	ENV->listOptions->add ("--csharp");
//	ENV->listOptions->add ("--other-option");
	ENV->listOptions->add ("--files");


	return Ret;
}

