Document: silentbob
Title: Debian silentbob Manual
Author: <insert document author here>
Abstract: This manual describes what silentbob is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/silentbob/silentbob.sgml.gz

Format: postscript
Files: /usr/share/doc/silentbob/silentbob.ps.gz

Format: text
Files: /usr/share/doc/silentbob/silentbob.text.gz

Format: HTML
Index: /usr/share/doc/silentbob/html/index.html
Files: /usr/share/doc/silentbob/html/*.html

  
