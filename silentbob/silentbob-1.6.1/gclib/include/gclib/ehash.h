/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_EHASH_H
#define DEFINE_EHASH_H

class EHash : public EList 
{
	public:
		EHash ();
		~EHash ();

		char * set (char *KEY, char *SET);
		char * get (char *KEY);
		char * del (char *KEY);
		int from_file (char *f_name);
		void foreach (Dhash_f f); 
		char * operator [] (char *S);

		inline char * get () {
			return EList::get ();
		}
};

#endif

