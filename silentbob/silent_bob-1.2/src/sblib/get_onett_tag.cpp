/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include "../head.h"
#include "../the_tt.h"
#include <sys/mman.h>

#ifdef HAVE_CONFIG_H
# include <config.h>
# define TMP_FILE_NAME "./"PACKAGE".tmp"
#else
# define TMP_FILE_NAME "./silent_bob.tmp"
#endif

struct tt_state_t * get_onett_tag (char * f_name, char ** d_tt_buf) 
{
	DArray d_array;
	tt_state_t * Ret = NULL;
	char * S;

	if (d_tt_buf)
		*d_tt_buf = NULL;	
	
	d_array.add (f_name);
	Ret = CNEW (tt_state_t, 1);

	unlink (TMP_FILE_NAME);
	tags (&d_array,  TMP_FILE_NAME);

	while( 1 ) {
		if (access (TMP_FILE_NAME, R_OK) != 0)
			break;
	
		Ret->d_file_name =  TMP_FILE_NAME;
		S = THE_TT::do_tt_file (Ret);
	
		if (S == NULL)
			break;
	
		if (Ret->b_mmap)
			munmap (Ret->d_file_in, Ret->d_filein_size);

		if (Ret->d_fd)
			close (Ret->d_fd);

		if (d_tt_buf)
			*d_tt_buf = S;
	
                unlink (TMP_FILE_NAME);
		return Ret;
	}

	DROP (Ret);
        unlink (TMP_FILE_NAME);
	return NULL;	
}

