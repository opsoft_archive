/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "../head.h"
#include <errno.h>

int split_tmp_files ()
{
	FILE * f_tmpfiles;
	FILE ** proc_files;
	char m_buf[512];
	int i = 0;

	f_tmpfiles = fopen (ENV->tmp_files, "r");
	if (! f_tmpfiles)
		return -1;
	
	proc_files = CNEW (FILE *, ENV->max_proc);
	memset (proc_files, 0, sizeof (FILE *) * ENV->max_proc);
	errno = 0;
	for (i = 0; i < ENV->max_proc; ++i) {
		sprintf (m_buf, "%s%i", ENV->tmp_files, i);
		unlink (m_buf);
		proc_files[i] = fopen (m_buf, "w");
		if (! proc_files[i]) {
			perror ("fopen");
			return -1;
		}
	}	
	
	i = 0;
	while (fgets (m_buf, 512, f_tmpfiles)) {
		fprintf (proc_files[i], "%s", m_buf);
		if (++i >= ENV->max_proc)
			i = 0;		
	}
	
	for (i = 0; i < ENV->max_proc; ++i) 
		fclose (proc_files[i]);
	
	return ENV->max_proc;
}

