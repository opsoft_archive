/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include "../head.h"
#include "../dbg.h"
#include "wit.h"

/* code for "linear" functionality, */
void nogui_tagsdump (char * f_name, int n_trip) {
	DArray * d_tags;			
	d_tag_t * d_tag;
	struct fdump_param_t d_param;
	int a,i;
	
	d_tags = got_tag_ctags (f_name);
	
	assert (d_tags->get_size () == 0, "HimTeh 1");
	for (i = 0; i < d_tags->get_size (); i++) {
		d_tag = (d_tag_t *) d_tags->get (i);
		if (i != 0)
			fputc ('\n', stdout);

		if (!SB_FLGET(SB_FLLINEAR)) {
			for (a = 0; a < n_trip; a++)
				fputc ('\t', stdout);
		}

		printf ("// file %s line %i\n", d_tag->d_file, d_tag->d_line);
		
		memset (&d_param, 0, sizeof (struct fdump_param_t));
		d_param.n_trip = n_trip;
		d_param.d_file_name = d_tag->d_file;
		d_param.d_line = d_tag->d_line;
		d_param.linear = SB_FLGET (SB_FLLINEAR);
		if (d_tag->d_type & GLOBAL_TYPE_FUNCTION)
			d_param.b_force_block = true;
		nogui_fdump (&d_param);
	}
	
	d_tags->foreach (free);
	delete d_tags;
	printf ("\n");
}


