/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "../head.h"

void mk_tags (char *f_name, DArray *d_in)
{
	DArray * d_array = NULL;
	char ** d_skeleton;
	FILE * my_file;
	int d_size;
	int i;

	if (d_in == NULL) {
		d_array = new DArray (1024);
		d_array->from_file (ENV->tmp_tags);
	} else 
		d_array = d_in;
		
	d_size = d_array->get_size ();
	if (d_size > 1) {
		d_skeleton = d_array->get_skeleton ();
		Dfastsort_s (d_skeleton, d_size - 1);
	}
	
	my_file = fopen (f_name, "w");
	if (my_file == NULL) {
		fprintf (stderr, "file %s:\n", f_name);
		perror ("fopen");
		return;
	}			

	fprintf (my_file, "!_TAG_FILE_FORMAT\t2\n");
	fprintf (my_file, "!_TAG_FILE_SORTED\t1\n");
	fprintf (my_file, "!_TAG_PROGRAM_AUTHOR\tOleg Puchinin (graycardinalster@gmail.com)\n");
	fprintf (my_file, "!_TAG_PROGRAM_NAME\tSilent Bob\n");
	fprintf (my_file, "!_TAG_PROGRAM_URL\thttp://sf.net/projects/silentbob\n");
	fprintf (my_file, "!_TAG_PROGRAM_VERSION\t1.1\n");

	for (i = 0; i < d_size; i++) 
		fprintf (my_file, "%s", d_array->get (i));
	
	if (d_in == NULL) {
		d_array->foreach (free);
		delete d_array;
	}
	fclose (my_file);
}

