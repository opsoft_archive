/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

/*
 * "Oleg, THE_TT and BUGs"
 *  
 * 01/01/06 00:00 - Started...
 * January 2006 - string BUG fixed.
 * 		"//" BUG fixed. It's work currently ?!
 * 
 * January 2006, last day.  added operator to line conversion.
 * February 2006 - 					operator to line conversion work properly.
 * February 2006 -					"macro BUG" fixed.
 * March 2006 -	"Style BUG" fixed (?).
 **/

#include "../head.h"
#include <sys/mman.h>
#include "../the_tt.h"

extern FILE * d_stream_dbg;

namespace THE_TT {

#define IF(arg) if (t_map[i] == arg) 
#define IF2(arga, argb) if (t_map[i] == arga && t_map[i+1] == argb)
#define is_space(arg) (arg == ' ' || arg == '\t' || arg == '\n')
#define IF_breaker if (t_map[i]==';' || t_map[i] == '}' || t_map[i] == '{')
#define T t_map[i]
#define T2 t_map[i+1]
#define Toff(arg) t_map[i+arg]
#define ATTACH t_attach (); 

int i; 
int ii;
int t_size;
char * t_map;
char * t_new;
int t_input_line;
int t_op_no;
int brace_depth = 0; // for '()'
int block_depth = 0; // for '{}'
bool b_make_attachment;
bool t_in_macro;
pair_t * d_attachment = NULL;

char * tt_out_buf = NULL;
int tt_outbuf_size = 0;

inline void t_attach ()
{
	t_op_no++;
	d_attachment[t_op_no].pair_op = t_op_no;
	d_attachment[t_op_no].pair_line = t_input_line;
	d_attachment[t_op_no].offset = i;
	brace_depth = 0; // Paranoid
}

inline void TN (char arg)
{
	t_new[ii] = arg; 
	++ii;
	if (arg == '\n' || 
		arg == '{' || arg == '}' ||
		(arg == ';' && !brace_depth)) {

		if (arg == '\n') {
			--t_input_line;
			ATTACH;
			++t_input_line;
		} else
			ATTACH;
	}
}

#define TNs(arg) do { t_new[ii] = arg; ++ii; } while (0)

inline void tt_comment ()
{
	while (i < t_size) {
		IF2('*',  '/')
			break;
		IF ('\n') {
			t_input_line++;
		}
		i++;
	}
	i++;
}

inline void tt_skip ()
{
	while (T != '\n' && i < t_size)
		++i;
	--i;
}

// NOTE: you _must_ allocate d_input and d_output before call this.
int the_tt_for_buffers (char * d_input,
		int t_new_size, 
		char * d_output)
{
	unsigned int t_spaces = 1;
	bool b_mustlined = false;
	bool b_instring = false;
	bool b_newline = true;
	int d_slash_count = 0;
	bool b_lined = true;
	char ch_last = 0;
	
	i = 0; 
	ii = 0;
	b_lined = true;
	b_mustlined = false;
	b_newline = true;
	t_size = t_new_size;
	t_map = d_input;
	t_new = d_output;
	t_input_line = 0;
	t_op_no = 0;
	brace_depth = 0;
	block_depth = 0;

	if (t_map[0] == '\n') {
		++i; // "mmap BUG" fixed :))
		++t_input_line;
	}
	
	if (Toff (t_size-1) == '\n')
		t_size--;	// mmap'ed TT not work without this !

	for (; i < t_size; ++i)	{
		if (if_abc (&t_map[i]) || if_digit (&t_map[i]))	{
			b_lined = false;
			t_spaces = 0;
			goto abc;
		}

		IF ('\n')
			++t_input_line;
		
		if (T == '\'' || T == '\"') {
			if (b_instring && ch_last != T)	{
				TN(T);
				continue;
			}
			
			if (b_instring)	{	
				if (Toff(-1) != '\\') // Normal skip \" and... \\" :)
					b_instring = false;
				else {
					d_slash_count = 1;
					while (Toff (-(d_slash_count)) == '\\') // Yes, I'm don't like this.
							d_slash_count++;
				
					if (d_slash_count & 1) 
						b_instring = false;
				}
			} else {
				ch_last = T;
				b_instring = true;
			}
		}
		
		if (b_instring)	{
			if (T != '\n')
				TNs (T);
			else {
				if (Toff(-1) == '\\')
					--ii;
			}
			continue;
		}

		if (T == '\n') {
			if (Toff(-1) == '\\') {
				if (t_in_macro)
					TN(T);
				else
					ii--;
				continue;
			} else
				t_in_macro = false;
		}
			
		IF2('/','/') {
			tt_skip ();
			continue;
		}

		IF2('/', '*') {
			tt_comment ();
			continue;
		}
	
		if (T == '(')
			++brace_depth;
		
		if (T == ')')
			--brace_depth;
		
		if (brace_depth < 0)
			brace_depth = 0; 
		
		if (is_space (T)) {
			if (T == '\n') {
				b_newline = true;
				if (Toff(1) == '#' && !b_lined)	{
					TN ('\n');
					++t_spaces;
					b_lined = true;
					b_mustlined = false;
					continue;
				}
					
				if (b_mustlined) {
					TN('\n');
					++t_spaces;
					if (!(Toff (-1) == '\\')) {
						b_mustlined = false;
						b_lined = true;
					}
				}
				
				if (t_spaces == 0 && !b_lined) {
					++t_spaces; 
					TN(' ');
				}
				continue;
			} else {
				++t_spaces;
				if (t_spaces == 1)
					TN(' ');
			}
			continue;
		}

		b_lined = false;
		if (T == '(' && t_spaces == 0 && b_mustlined == 0) {
			++t_spaces; // No space after '('.
			TN(' ');
			TN('(');
			continue;
		}
		
		if (T == ')' && t_spaces == 0) {
			++t_spaces;
			TN(')');
			TN(' ');
			continue;
		}
				
		t_spaces = 0;
		IF_breaker {
			TN(T);
			TN(' ');
			++t_spaces;
			continue;
		}
	
		IF('#' && b_newline) {
			TN ('#');
			t_in_macro = true;
			b_mustlined = true;
			continue;
		}
	
		IF (0x0D)
			continue;

abc:
		b_newline = false;
//		TN(T);
		TNs(T);
	} // for

	return ii;
}

char * do_tt_file (tt_state_t * d_tt_state)
{
	char * t_output = NULL;	
	char * f_name;
	int t_Ret;

	d_attachment = NULL;
	
	if (! d_tt_state)
		return 0;

	f_name = d_tt_state->d_file_name;
	
	if (EQ(f_name, "-")) {
		d_tt_state->d_file_in = Dread_to_eof (fileno (stdin), &d_tt_state->d_filein_size);
		if (d_tt_state->d_filein_size <= 0) 
			exit (1);
	} else 
		tt_map (d_tt_state);	
	
	t_size = d_tt_state->d_filein_size;

	t_output = CNEW (char, t_size<<1); // Paranoid.
	if (t_output == NULL) {
		perror ("no such memory");
		return NULL;
	}
	/**/

	if (t_size < 4096) 
		d_attachment = CNEW (pair_t, 4096);
	else
		d_attachment = CNEW (pair_t, t_size>>1); 
	
	d_tt_state->d_attachment = d_attachment;

	t_Ret = the_tt_for_buffers (d_tt_state->d_file_in, t_size, t_output);
	t_output[t_Ret] = 0; // Required.
	d_tt_state->d_output_size = t_Ret;
	d_tt_state->d_output = t_output;

	return t_output;
}

// $ silent_bob --the-tt
int the_tt_main (char * f_name) 
{
	char * t_output;
	tt_state_t * d_tt_state;	
	
	d_tt_state = CNEW (tt_state_t, 1);
	memset (d_tt_state, 0, sizeof (tt_state_t));
	d_tt_state->d_file_name = f_name;
	t_output = do_tt_file (d_tt_state);

	if (! SB_FLGET (SB_FLSIMULATE))
		write (fileno (stdout), t_output, d_tt_state->d_output_size);
	
	free_tt_state (d_tt_state);
	return EXIT_SUCCESS;
}

} // namespace THE_TT

void free_tt_state (struct tt_state_t * S)
{
	if (S->b_mmap)
		munmap (S->d_file_in, S->d_filein_size);
	else
		DROP (S->d_file_in);

	if (S->d_fd)
		close (S->d_fd);

	DROP (S->d_output);
	DROP (S->d_attachment);
	DROP (S);
}

int tt_map (tt_state_t *tt) 
{
	tt->b_mmap = true;
	tt->d_file_in = DFMAP (tt->d_file_name, &tt->d_fd, &tt->d_filein_size);
	
	if (tt->d_file_in == NULL) {
		tt->d_filein_size = fsize (tt->d_file_name);
		tt->b_mmap = false;
		tt->d_file_in = CNEW (char, tt->d_filein_size);
		if (Dfnread (tt->d_file_name, tt->d_file_in, tt->d_filein_size) < 0)
			DROP (tt->d_file_in);
	}	

	if (! tt->d_file_in) {
		fprintf (stderr, "can't open/mmap file %s\n", tt->d_file_name);
		perror ("open/mmap");
		return -1;
	}

	return 0;
}

