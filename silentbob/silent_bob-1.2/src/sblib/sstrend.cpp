/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

char * sstrend (char * d_ptr)
{
	bool t_instring = false;
	int d_slash_count;
	char ch_last;
	char *d_old;
	unsigned limit = 1024;
	
	if (! d_ptr)
		return (char *) 0;

	if (!(*d_ptr))
		return (char *) 0;
	
	ch_last = *d_ptr;
	d_old = d_ptr;
	limit--;
	while (*d_ptr && (limit > 0)) {
		if (*d_ptr == '\'' || *d_ptr == '\"') {
			if (t_instring && *d_ptr != ch_last) {
				d_ptr++;
				continue; // Mmm...
			}
			
			if (t_instring) {
				if (d_ptr[-1] == '\\') {
					d_slash_count = 1;
					while (d_ptr [-(d_slash_count)] == '\\')
							d_slash_count++;
				
					if (d_slash_count & 1) 
						t_instring = false;
				} else {
					d_ptr++;
					t_instring = false;
					continue;
				}
			} else {
				ch_last = *d_ptr;
				t_instring = true;
			}
		}
		
		if (t_instring)	{
			d_ptr++;
			continue;
		} else
			break;
	}

	d_ptr --;
	
	if (*d_ptr == 0)
		return 0;

	return d_ptr;
}


