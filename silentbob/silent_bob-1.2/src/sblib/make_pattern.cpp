/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>

char * make_pattern (char *op)
{
	int i;
	char * pt;
	char *pos;

	pt = CNEW (char, 128);
	i = 0;
	pos = pt;
	*pos = '/';
	++pos;	
	while (op[i] && i < 64) {
		if (if_abc (&op[i]) || if_digit (&op[i]) || op[i] == '_') 
			*pos = op[i];
		else if (op[i] == ' ') {
			*pos = '\\';
			++pos;
			*pos = 's';
		} else {
			*pos = '.';
		}

		++i;
		++pos;
	}

	if (pos[-1] == 's')
		pos -= 2;
	*pos = '/';
	++pos;
	*pos = '\0';
	return pt;
}


