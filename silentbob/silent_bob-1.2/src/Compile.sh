g++ -O3 -pipe -c -o brave.o brave.cpp
g++ -O3 -pipe -c -o call_tags.o call_tags.cpp
g++ -O3 -pipe -c -o cgrep.o cgrep.cpp
g++ -O3 -pipe -c -o flist.o flist.cpp
g++ -O3 -pipe -c -o globals.o globals.cpp
g++ -O3 -pipe -c -o got_structs.o got_structs.cpp
g++ -O3 -pipe -c -o got_tag.o got_tag.cpp
g++ -O3 -pipe -c -o init.o init.cpp
g++ -O3 -pipe -fPIC -c -o plugins/plugin_c-files.os plugins/plugin_c-files.cpp
g++ -shared -o libplugin_c-files.so plugins/plugin_c-files.os -lgclib -lgclib_c -lsblib -ldl
g++ -O3 -pipe -fPIC -c -o plugins/plugin_cache.os plugins/plugin_cache.cpp
g++ -shared -o libplugin_cache.so plugins/plugin_cache.os -lgclib -lgclib_c -lsblib -ldl
g++ -O3 -pipe -fPIC -c -o plugins/plugin_editor.os plugins/plugin_editor.cpp
g++ -shared -o libplugin_editor.so plugins/plugin_editor.os -lgclib -lgclib_c -lsblib -ldl
g++ -O3 -pipe -fPIC -c -o plugins/plugin_grep.os plugins/plugin_grep.cpp
g++ -shared -o libplugin_grep.so plugins/plugin_grep.os -lgclib -lgclib_c -lsblib -ldl
g++ -O3 -pipe -fPIC -c -o plugins/plugin_perl.os plugins/plugin_perl.cpp
g++ -O3 -pipe -fPIC -c -o plugins/the_fly.os plugins/the_fly.cpp
g++ -O3 -pipe -fPIC -c -o plugins/t_op2.os plugins/t_op2.cpp
g++ -shared -o libplugin_perl.so plugins/plugin_perl.os plugins/the_fly.os plugins/t_op2.os -lgclib -lgclib_c -lsblib -ldl
g++ -O3 -pipe -fPIC -c -o plugins/plugin_perlpackages.os plugins/plugin_perlpackages.cpp
g++ -shared -o libplugin_perlpackages.so plugins/plugin_perlpackages.os plugins/the_fly.os plugins/t_op2.os -lgclib -lgclib_c -lsblib -ldl
g++ -O3 -pipe -fPIC -c -o plugins/python.os plugins/python.cpp
g++ -O3 -pipe -fPIC -c -o plugins/plugin_python.os plugins/plugin_python.cpp
g++ -shared -o libplugin_python.so plugins/python.os plugins/plugin_python.os -lgclib -lgclib_c -lsblib -ldl
g++ -O3 -pipe -fPIC -c -o sblib/bugs.os sblib/bugs.cpp
g++ -O3 -pipe -fPIC -c -o sblib/cts.os sblib/cts.cpp
g++ -O3 -pipe -fPIC -c -o sblib/def_test.os sblib/def_test.cpp
g++ -O3 -pipe -fPIC -c -o sblib/find_cfiles.os sblib/find_cfiles.cpp
g++ -O3 -pipe -fPIC -c -o sblib/find.os sblib/find.cpp
g++ -O3 -pipe -fPIC -c -o sblib/get_onett_tag.os sblib/get_onett_tag.cpp
g++ -O3 -pipe -fPIC -c -o sblib/make_pattern.os sblib/make_pattern.cpp
g++ -O3 -pipe -fPIC -c -o sblib/mk_tags.os sblib/mk_tags.cpp
g++ -O3 -pipe -fPIC -c -o sblib/nogui_tagsdump.os sblib/nogui_tagsdump.cpp
g++ -O3 -pipe -fPIC -c -o sblib/skip_macro.os sblib/skip_macro.cpp
g++ -O3 -pipe -fPIC -c -o sblib/split_tmp_files.os sblib/split_tmp_files.cpp
g++ -O3 -pipe -fPIC -c -o sblib/split_to_words.os sblib/split_to_words.cpp
g++ -O3 -pipe -fPIC -c -o sblib/sstrend.os sblib/sstrend.cpp
g++ -O3 -pipe -fPIC -c -o sblib/sstrkill.os sblib/sstrkill.cpp
g++ -O3 -pipe -fPIC -c -o sblib/the_tt.os sblib/the_tt.cpp
g++ -O3 -pipe -fPIC -c -o sblib/wit.os sblib/wit.cpp
g++ -O3 -pipe -fPIC -c -o sblib/words_count.os sblib/words_count.cpp
g++ -O3 -pipe -fPIC -c -o sblib/ww_begin_line.os sblib/ww_begin_line.cpp
g++ -O3 -pipe -fPIC -c -o sblib/ww_begin_offset.os sblib/ww_begin_offset.cpp
g++ -O3 -pipe -fPIC -c -o sblib/ww_last_word.os sblib/ww_last_word.cpp
g++ -shared -o libsblib.so sblib/bugs.os sblib/cts.os sblib/def_test.os sblib/find_cfiles.os sblib/find.os sblib/get_onett_tag.os sblib/make_pattern.os sblib/mk_tags.os sblib/nogui_tagsdump.os sblib/skip_macro.os sblib/split_tmp_files.os sblib/split_to_words.os sblib/sstrend.os sblib/sstrkill.os sblib/the_tt.os sblib/wit.os sblib/words_count.os sblib/ww_begin_line.os sblib/ww_begin_offset.os sblib/ww_last_word.os -lgclib -lgclib_c -lsblib -ldl
g++ -O3 -pipe -c -o log.o log.cpp
g++ -O3 -pipe -c -o main.o main.cpp
g++ -O3 -pipe -c -o make_tags.o make_tags.cpp
g++ -O3 -pipe -c -o mk_tag.o mk_tag.cpp
g++ -O3 -pipe -c -o modding.o modding.cpp
g++ -O3 -pipe -c -o nogui_fdump.o nogui_fdump.cpp
g++ -O3 -pipe -c -o nogui_indent.o nogui_indent.cpp
g++ -O3 -pipe -c -o opts_globals.o opts_globals.cpp
g++ -O3 -pipe -c -o opts_settings.o opts_settings.cpp
g++ -O3 -pipe -c -o sb_prname.o sb_prname.cpp
g++ -O3 -pipe -c -o tags.o tags.cpp
g++ -O3 -pipe -c -o t_op.o t_op.cpp
g++ -O3 -pipe -c -o tree.o tree.cpp
g++ -O3 -pipe -c -o usage.o usage.cpp
g++ -o silent_bob brave.o call_tags.o cgrep.o flist.o globals.o got_structs.o got_tag.o init.o log.o main.o make_tags.o mk_tag.o modding.o nogui_fdump.o nogui_indent.o opts_globals.o opts_settings.o sb_prname.o tags.o t_op.o tree.o usage.o -lgclib -lgclib_c -lsblib -ldl
