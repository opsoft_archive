/* 
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "head.h"
#include "structs.h"
#include "the_tt.h"
#include "dbg.h"

int fix_one (char ** d_buf, char *str)
{
	char * S;
	int len = strlen (str);
	int fix = 0;;

	S = *d_buf;

	while (true) {
		--S;
		if (!strncmp (S, str, len))
			break;
	
		if (*S == '\n')
			fix--;
	}

	*d_buf = S;
	return fix;
}
	
int cgrep_fix (tt_state_t *tt, int t_op_no, DArray * exp) 
{
	int fix = 0;
	int i;
	char * d_buf;
	
	if (! tt || ! exp)
		return 0;

	d_buf = tt->d_file_in + tt->d_attachment[t_op_no].offset;

	for (i = exp->get_size () - 1; i >= 0; i--) 
		fix += fix_one (&d_buf, exp->get (i));

	return fix;
}

int cgrep_file (tt_state_t * tt, char * exp)
{
	char * d_ptr, *d_out;
	DArray * d_array;
	DArray * d_lines = NULL;
	int size;
	char * one;
	char * S;
	char ch;
	int i;
	int line;
	
	if (tt == NULL || 
			exp == NULL ||
			tt->d_output == NULL)
		return -1;

	d_out = tt->d_output;
	d_ptr = tt->d_output;
	ENV->t_op_no = 0;
	
	d_array = Dsplit (exp, ",");
	if (d_array == NULL) 
		return -2;
	
	size = d_array->get_size ();
	while (true) {
		ch = t_op (&d_ptr, &d_out);
		ENV->t_op_no++;

		if (ch == 0)
			break;

		S = d_out;
		for (i = 0; i < size; i++) {
			one = d_array->get (i);
			S = strstr (S, one);
			if (! S)  
				break;
		
			S += strlen (one);
		}
	
		if (i != size) 
			continue;
		
		if (! d_lines) {
			d_lines = new DArray (1024);
			d_lines->from_file (tt->d_file_name);
		}

		line = tt->d_attachment[ENV->t_op_no].pair_line + 1;
		line += cgrep_fix (tt, ENV->t_op_no, d_array);
		if (SB_FLGET (SB_FLTAGSTYLE)) 
			printf ("%s\t%s\t+%i\n", exp, tt->d_file_name, 
					line);
		else 
			printf ("%s +%i: %s", tt->d_file_name, line, d_lines->get (line-1));
	}

	if (d_lines) {
		d_lines->foreach (free);
		delete d_lines;
	}

	return 0;
}

int cgrep (EArray * d_files, char * exp)
{
	struct tt_state_t * tt;
	int i;

	if (! d_files)
		return -1;
	
	for (i = 0; i < d_files->get_size (); i++) {
		tt = CNEW (tt_state_t, 1);
		memset (tt, 0, sizeof (tt_state_t));
		
		tt->d_file_name = d_files->get (i);
		tt->d_attachment = (pair_t *) CNEW (char, tt->d_filein_size << 1);
		if (tt->d_file_name == NULL)
			continue;

		if (THE_TT::do_tt_file (tt) == NULL)
			continue; // broken file 

		cgrep_file (tt, exp);
		free_tt_state (tt);
	}		

	return 0;
}
