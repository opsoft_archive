/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "head.h"

int opts_settings (DArray * d_opts, int & i)
{
	int argc;

	if (! d_opts)
		goto opts_settings_done;

	argc = d_opts->get_size ();
	
	if (d_opts->get (i)[0] != '-') {
		ENV->d_files->add (strdup (d_opts->get (i)));
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "--verbose")) {
		SB_FLSET (SB_FLVERBOSE);
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "-u")) {
		SB_FLSET (SB_FLRTREE);			
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "--linear") ||
			EQ (d_opts->get (i), "-l")) {
		SB_FLSET (SB_FLLINEAR);
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "-C") && i < argc) {
		++i;
		chdir (d_opts->get (i));
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "--linux")) {
		chdir ("/usr/src/linux");
		goto opts_settings_done;
	}
		
	if (EQ (d_opts->get (i), "--debug")) {
		fclose (ENV->d_stream_dbg);
		ENV->d_stream_dbg = fdopen (fileno (stderr), "w");
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "--simulate"))	{
		Dtimer ();
		SB_FLSET (SB_FLSIMULATE);
		goto opts_settings_done;
	}
		
	if (EQ (d_opts->get (i), "--no-links"))	{
		SB_FLSET (SB_FLNOLINKS);
		goto opts_settings_done;
	}

	if (EQ (d_opts->get (i), "-a") || EQ (d_opts->get (i), "--all")) {
		SB_FLSET (SB_FLALL);
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "--test") || EQ (d_opts->get (i), "-t")) {
		SB_FLSET (SB_FLTEST);
		goto opts_settings_done;
	}

	if (EQ (d_opts->get (i), "--ctags-append")) {
		SB_FLSET (SB_FLCTAGSAPPEND);
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "-A")) {
		if (++i >= argc)
			goto opts_settings_done;
		
		ENV->cgrep_A = atoi (d_opts->get (i));
		goto opts_settings_done;
	}

	if (EQ (d_opts->get (i), "-B")) {
		if (++i >= argc) 
			goto opts_settings_done;
		
		ENV->cgrep_B = atoi (d_opts->get (i));
		goto opts_settings_done;
	}

	if (EQ (d_opts->get (i), "-j")) {
		if (++i >= argc)
			goto opts_settings_done;
		ENV->max_proc = atoi (d_opts->get (i));
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "--tag-style") || EQ (d_opts->get (i), "-ts")) {
		SB_FLSET (SB_FLTAGSTYLE); 
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "-L") && (i+1) < argc)	{
		++i;
		ENV->d_files->from_file (d_opts->get (i));
		ENV->d_files->foreach ((Dfunc_t) chomp);
		goto opts_settings_done;
	}
	
	if (EQ (d_opts->get (i), "--depth")) {
		if (++i >= argc) 
			goto opts_settings_done;
		
		ENV->d_depth = atoi (d_opts->get (i));
		goto opts_settings_done;
	}
		
	if (EQ (d_opts->get (i), "-fn")) {
		SB_FLSET (SB_FLFNAMES);
		goto opts_settings_done;
	}

	return -1;

opts_settings_done:
	return 0;
}




		


