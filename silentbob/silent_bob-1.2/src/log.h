/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_LOG_H
#define DEFINE_LOG_H

int log_init ();
int log_globals (char *file, int type, int size);
int log_write (char *log_name, char *log_str);

#endif

