/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "head.h"
#include "wit.h"

int opts_globals (DArray * d_opts, int & i, int * i_cmd)
{
	int old_cmd = *i_cmd;

	if (! i_cmd)
		return -1;
	
	*i_cmd = cmd_globals;
	if (EQ (d_opts->get (i), "--globals")) {
		ENV->d_global_type = GLOBAL_TYPE_TYPEDEF |
			GLOBAL_TYPE_VARIABLE |
			GLOBAL_TYPE_STRUCT |
			GLOBAL_TYPE_FUNCTION |
			GLOBAL_TYPE_DEFINE |
			GLOBAL_TYPE_CLASS |
			GLOBAL_TYPE_NAMESPACE;
	} else if (EQ (d_opts->get (i), "--globals-typedef")) 
		ENV->d_global_type = GLOBAL_TYPE_TYPEDEF;
	else if (EQ (d_opts->get (i), "--globals-extern")) 		
		ENV->d_global_type = GLOBAL_TYPE_EXTERN;
	else if (EQ (d_opts->get (i), "--globals-function")) 
		ENV->d_global_type = GLOBAL_TYPE_FUNCTION;
	else if (EQ (d_opts->get (i), "--globals-struct"))
		ENV->d_global_type = GLOBAL_TYPE_STRUCT;
	else if (EQ (d_opts->get (i), "--globals-variable"))	
		ENV->d_global_type = GLOBAL_TYPE_VARIABLE;
	else if (EQ (d_opts->get (i), "--globals-define"))
		ENV->d_global_type = GLOBAL_TYPE_DEFINE;
	else {
		*i_cmd = old_cmd;
		return -1;
	}

	return 0;
}

