/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>
#include <sys/wait.h>
#include "../mod.h"
#include "../head.h"
#include "../dbg.h"

extern "C" DArray * plugin_init (struct env_t *env);
int __aap_vars;

void __do_aap_program (char * pr_name)
{
	DArray * d_files;
	int count;
	int i = 0;

	d_files = ENV->d_files;
	if (! d_files || d_files->get_size () <= 0)
		return;
	
	count = d_files->get_size ();
	if (__aap_vars) {
		printf ("CC=gcc\n");
		printf ("CXX=g++\n");
		printf ("CFLAGS=-O3 -Wall -pipe\n");
		printf ("CXXFLAGS=$CFLAGS\n");
	}
	printf (":program %s : %s\n", pr_name, d_files->get (i));

	for (i = 1; i < count; ++i) 
		printf ("\t%s\n", d_files->get (i));

	printf ("\n");
}

char aap_program_opt2 (DArray * d_opts, int * pos)
{
	int count;
	char m_buf[512];
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);
	if (EQ (S, "--aap-program")) {
		if (++(*pos) >= count)
			return 0;
		S = d_opts->get (*pos);
		__do_aap_program (S);
		exit (0);
		return 1;
	}

	if (EQ (S, "--aap-vars")) {
		__aap_vars = 1;
		return 1;
	}

	return 0;
}

char aap_program_opt (DArray * d_opts, int * pos)
{
	int count;
	char *S;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);
	if (EQ (S, "--aap-program")) {
		if (++(*pos) >= count)
			return 0;
		return 1;
	}
	return 0;	
}

void aap_program_info ()
{
	printf ("AAP file.\n");
	printf ("Version: 1.0\n");
	printf ("options: --aap-program --aap-vars\n");
}

DArray * plugin_init (struct env_t *env)
{
	struct mod_feature * pm;
	DArray * Ret;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("0.1");
	pm->mod.info = aap_program_info;
	pm->opt = aap_program_opt;
	pm->opt2 = aap_program_opt2;

	Ret->add (LPCHAR (pm));
	__aap_vars = 0;
	return Ret;
}

