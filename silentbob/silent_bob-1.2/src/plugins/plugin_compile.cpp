/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>
#include <sys/wait.h>
#include "../mod.h"
#include "../head.h"
#include "../dbg.h"

extern "C" DArray * plugin_init (struct env_t *env);
struct env_t *ENV;
EHash * compile_env;

char * __name2obj (char * name)
{
	char *S;
	char m_buf[512];
	strcpy (m_buf, name);
	S = rindex (m_buf, '.');
	if (! S)
		return NULL;
	strcpy (S, ".o");
	return strdup (m_buf);	
}

void __aa (EArray * a, DArray * b)
{
	int count;
	int i;

	if (! a || ! b)
		return;

	count = b->get_size ();
	for (i = 0; i < count; ++i) 
		a->add (strdup (LPCHAR (b->get (i))));
}

void __astr (EArray * a, char *str)
{
	DArray * d_split;
	
	if (! a || ! str)
		return;

	d_split = Dsplit (str, " ");
	if (! d_split)
		return;
	
	__aa (a, d_split);
	d_split->foreach (free);
	delete d_split;
}

void __array_log (EArray * a)
{
	int i;
	if (! a)
		return;
	for (i = 0; i < a->get_size (); ++i) {
		if (a->get (i))
			printf ("%s ", a->get (i));
	}
	printf ("\n");
}

int compile_cppfile (char * name)
{
	int i;
	int pid;
	int status;
	char * oname = __name2obj (name);
	EArray * d_param;

	if (! oname)
		return -1;
	
	d_param = new EArray(32);
	d_param->add (compile_env->get ("CXX"));
	__astr (d_param, compile_env->get ("CXXFLAGS"));
	if (compile_env->get ("OPTS"))
		__astr (d_param, compile_env->get ("OPTS"));
	d_param->add ("-c");
	d_param->add ("-o");
	d_param->add (oname);
	d_param->add (name);
	d_param->add (LPCHAR (NULL));

	if (SB_FLGET (SB_FLVERBOSE))
		__array_log (d_param);
	else
		printf ("\tCXX\t%s\n", oname);
	
	pid = vfork ();
	if (pid == 0)
		execvp (compile_env->get ("CXX"), d_param->get_skeleton ());
	else 
		waitpid (pid, &status, 0);

	free (oname);	
}

int compile_cfile (char * name)
{
	int i;
	int pid;
	int status;
	char * oname = __name2obj (name);
	EArray * d_param;

	if (! oname)
		return -1;
	
	d_param = new EArray(32);
	d_param->add (compile_env->get ("CC"));
	__astr (d_param, compile_env->get ("CFLAGS"));
	if (compile_env->get ("OPTS"))
		__astr (d_param, compile_env->get ("OPTS"));
	d_param->add ("-c");
	d_param->add ("-o");
	d_param->add (oname);
	d_param->add (name);
	d_param->add (LPCHAR (NULL));

	if (SB_FLGET (SB_FLVERBOSE)) 
		__array_log (d_param);
	else
		printf ("\tCC\t%s\n", oname);
	
	pid = vfork ();
	if (pid == 0)
		execvp (compile_env->get ("CC"), d_param->get_skeleton ());
	else 
		waitpid (pid, &status, 0);

	free (oname);	
}

int compile_files ()
{
	char *S;
	int count;
	int i;

	count = ENV->d_files->get_size ();
	for (i = 0; i < count; ++i) {
		S = rindex (ENV->d_files->get (i), '.');
		if (! S)
			continue;
		if (EQ (S, ".cpp") || EQ (S, ".cxx"))
			compile_cppfile (ENV->d_files->get (i));
		if (EQ (S, ".c")) 
			compile_cfile (ENV->d_files->get (i));
	}

	return 0;	
}

void __init_compile_env ()
{
	char *cc;
	char *cxx;
	char *cflags;
	char *cxxflags;
	char *opts;

	compile_env = new EHash;
	cc = ENV->settings->get ("CC");
	cxx = ENV->settings->get ("CXX");
	cflags = ENV->settings->get ("CFLAGS");
	cxxflags = ENV->settings->get ("CXXFLAGS");
	opts = ENV->settings->get ("OPTS");	
	
	if (! cc)
		cc = "gcc";
	if (! cxx)
		cxx = "g++";
	if (! cflags)
		cflags = "-O3 -Wall -pipe";
	if (! cxxflags)
		cxxflags = cflags;
	
	if (SB_FLGET (SB_FLVERBOSE)) {
		printf ("C compiler: %s\n", cc);
		printf ("C++ compiler: %s\n", cxx);
		printf ("C flags: %s\n", cflags);
		printf ("C++ flags: %s\n", cxxflags);
	}
	compile_env->set ("CC", strdup (cc));
	compile_env->set ("CXX", strdup (cxx));
	compile_env->set ("CFLAGS", strdup (cflags));
	compile_env->set ("CXXFLAGS", strdup (cxxflags));
	if (opts)
		compile_env->set ("OPTS", strdup (opts));
}

char compile_opt2 (DArray * d_opts, int * pos)
{
	char m_buf[512];
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	S = d_opts->get (*pos);
	if (EQ (S, "--compile")) {
		__init_compile_env ();
		compile_files ();		
		exit (0);
	}

	return 0;
}

void compile_info ()
{
	printf ("C/C++ compilation plugin.\n");
	printf ("Version: 0.2\n");
	printf ("options: --compile\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.info = compile_info;
	pm->opt2 = compile_opt2;

	Ret->add (LPCHAR (pm));
	return Ret;
}

