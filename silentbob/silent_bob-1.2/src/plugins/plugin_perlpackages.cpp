/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <gclib/gclib.h>
#include "../mod.h"
#include "../head.h"
#include "../dbg.h"
#include "../the_tt.h"
#include "the_fly.hpp"

extern "C" DArray * plugin_init (struct env_t *env);
char t_op2 (char ** d_in, char ** d_prev);

void __perl_files (char * f_name)
{
	unlink (f_name);
	sblib_find ("./", "*.pm", f_name);
	sblib_find ("./", "*.pl", f_name);
	sblib_find ("./", "*.ph", f_name);
}

void plpkg_one (struct tt_state_t * tt)
{
	char * d_out, *d_ptr; // for t_op2
	int block_depth = 0;
	char ch;
	char * S;

	d_out = tt->d_output;
	d_ptr = d_out;

	while (true) {
		ch = t_op2 (&d_ptr, &d_out);
		if (ch == '\0')
			break;

		S = d_out;
		if (*S == ' ')
			++S;
		
		if (! block_depth && !strncmp (S, "package ", 8)) 
			printf ("%s\n", S);

		if (ch == '{') 
			++block_depth;

		if (ch == '}')
			--block_depth;

		if (block_depth < 0)
			block_depth = 0;

	}
}

void try_packages ()
{
	int i;
	struct tt_state_t * tt;
	DArray * d_files;

	d_files = ENV->d_files;
	if (d_files->get_size () == 0) {
		__perl_files (ENV->tmp_files);
		d_files->from_file (ENV->tmp_files);
		d_files->foreach ((Dfunc_t)chomp);
		unlink (ENV->tmp_files);
	}

	for (i = 0; i < d_files->get_size (); i++) {
		tt = CNEW (tt_state_t, 1);

		memset (tt, 0, sizeof (struct tt_state_t));
		if (! d_files->get (i))
			continue;

		tt->d_file_name = strdup (d_files->get (i));
		THE_FLY::fly_for_file (tt);
		plpkg_one (tt);
		free_tt_state (tt);
	}
}

char pl_opt (DArray * d_opts, int * i) 
{
	if (! d_opts || ! i)
		return 0;

	if (EQ (d_opts->get (*i), "--packages")) {
		if (EQ (ENV->language, "Perl")) {
			try_packages ();
			exit (1);
		}
	}
	
	return 0;
}

void plug_info ()
{
	printf ("Perl packages.\n");
	printf ("Version: 1.0\n");
	printf ("options: --perl --packages\n");
}

DArray * plugin_init (struct env_t * env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (32);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.info = plug_info;
	pm->opt2 = pl_opt;
	
	Ret->add (LPCHAR (pm));
	return Ret;
}

