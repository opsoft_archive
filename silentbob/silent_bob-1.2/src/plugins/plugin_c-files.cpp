/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>
#include <sys/wait.h>
#include "../mod.h"
#include "../head.h"
#include "../dbg.h"

extern "C" DArray * plugin_init (struct env_t *env);

char cfiles_opt (DArray * d_opts, int * pos)
{
	char m_buf[512];
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	S = d_opts->get (*pos);
	if (EQ (S, "--cfiles") ||
	    EQ (S, "-f")) {
		unlink ("./cfiles");
                find_cfiles ();
		sprintf (m_buf, "mv \"%s\" ./cfiles", ENV->tmp_files);
		system (m_buf);
		exit (0);
	}

	return 0;
}

void cfiles_info ()
{
	printf ("C/C++ files.\n");
	printf ("Version: 1.0\n");
	printf ("options: --cfiles\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.info = cfiles_info;
	pm->opt = cfiles_opt;

	Ret->add (LPCHAR (pm));
	return Ret;
}

