#ifndef _THE_FLY_HPP
# define _THE_FLY_HPP
# include "../structs.h"

namespace THE_FLY {
	char * fly_for_file (struct tt_state_t * tt);
}

#endif
