/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <dlfcn.h>
#include "head.h"
#include "mod.h"
#include "the_tt.h"
#include "dbg.h"
#include "bugs.h"

#define PLUGINS_MASK "find ~/plugins/ -name \"libplugin_*.so\""

typedef struct DArray *(*plug_init_t) (struct env_t *env);

env_t * sb_getenv (void) 
{
	return ENV;
}

char modding_optparse (int * i, int step)
{
	int pos;
	mod_t * plug;
	mod_feature * f;

	for (pos = 0; pos < ENV->modding->get_size (); pos++) {
		plug = (mod_t *) ENV->modding->get (pos);
		if (plug->Type == TYPE_FEATURE) {
			f = container_of (plug, struct mod_feature *);
			
			if (step == 1) {
				if (f->opt && f->opt (ENV->d_opts, i))
					return 1;
			}

			if (step == 2) {
				if (f->opt2 && f->opt2 (ENV->d_opts, i))
						return 1;
			}
		}
	}

	return 0;
}

struct mod_language * find_language (char *S)
{
	struct mod_language *pm = NULL;
	int i;
	
	//printf ("Plugins count : %i\n", ENV->modding->get_size ());
	for (i = 0; i < ENV->modding->get_size (); i++) {
		pm = (struct mod_language *) ENV->modding->get (i);
		if (! pm)
			continue;

		if (pm->mod.Type != TYPE_LANGUAGE)
			continue;

		if (EQ (pm->language, S))
		       break;
	}

	if (i < ENV->modding->get_size ())
		return pm;

	return NULL;
}	

void mods_info ()
{
	struct mod_t *pm = NULL;
	int i;

	printf ("Available plugins: \n");
	for (i = 0; i < ENV->modding->get_size (); i++) {
		pm = (mod_t *) ENV->modding->get (i);
		if (! pm)
			continue;

		if (! pm->info)
			continue;

		pm->info ();
		printf ("\n");	
	}
}

FILE * mkctags_prepare ()
{
	FILE * ofile;
	
	if (SB_FLGET (SB_FLCTAGSAPPEND)) {
		if (rename ("./tags", ENV->tmp_tags) < 0) {
			fprintf (stderr, "$ rename: ./tags %s\n", ENV->tmp_tags);
			perror ("rename");
			exit (1);
		}
		ofile = fopen (ENV->tmp_tags, "a");
	} else {
		unlink (ENV->tmp_tags);
		unlink ("./tags");
		ofile = fopen (ENV->tmp_tags, "w");
	}

	if (ofile == NULL) {
		fprintf (stderr, "fopen (\"%s\", \"w\")\n", ENV->tmp_tags);
		perror ("fopen");
		exit (1);
	}

	return ofile;
}

int modding_start (int i_cmd)
{
	struct mod_language *pm = NULL;
	struct tt_state_t * tt;
	int i;
	int size;
	FILE * ofile = NULL;

	pm = find_language (ENV->language);
	if (pm == NULL || pm->the == NULL) {
		assert (true, "Language plugin not found !\n");
		return 1;
	}

	if (i_cmd == cmd_makectags) 
		ofile = mkctags_prepare ();
					
	size = ENV->d_files->get_size ();
	if (! size) {
		switch (i_cmd) {
			case cmd_makectags:
				if (! pm->make_ctags)
					return 2;
				pm->make_ctags (NULL, ofile);				
		}
		goto mod_out;
	}
	
	for (i = 0; i < size; i++) {
		if (S_ISDIR (DLSTAT (ENV->d_files->get (i))->st_mode)) 
			continue;
		
		switch (i_cmd) {
			case cmd_the_tt:
				tt = CNEW (tt_state_t, 1);
				memset (tt, 0, sizeof (struct tt_state_t));
				tt->d_file_name = ENV->d_files->get (i);

				if (! pm->the)
					return 2;

				pm->the (tt);
				if (! SB_FLGET (SB_FLSIMULATE)) {
					write (fileno (stdout), tt->d_output, 
						tt->d_output_size);
					printf ("\n");
				}

				free_tt_state (tt);
				break;

			case cmd_makectags:
				if (! pm->make_ctags || !pm->the)
					return 2;

				pm->make_ctags (ENV->d_files->get (i), ofile);
				break;

			case cmd_call_tags:
				if (! pm->call_tags)
					return 2;

				pm->call_tags (ENV->d_files->get (i));
				break;

			default:
				bug_notsupported ();
				i = size;
				break;
		}
	}	

mod_out:
	if (ofile)
		fclose (ofile);

	if (i_cmd == cmd_makectags) 
		mk_tags ("tags", NULL);

	return 0;
}

int modding_load_dir (char * path)
{
	int i;
	DArray * files;
	
	files = Dfiles (path);
	if (! files)
		return -1;
	
	for (i = 0; i < files->get_size (); i++) {
		if (EQ (files->get (i), "."))
			continue;
		if (EQ (files->get (i), ".."))
			continue;
		modding_load_plugin (files->get (i), path);
	}		

	files->foreach (free);
	delete files;
	return 0;
}

int modding_init ()
{
	ENV->modding = new EArray(32);
	modding_load_dir (ENV->home_plugins);
	modding_load_dir (ENV->shared_plugins);
	return 0;
}

int modding_load_plugin (char * name, char * path)
{
	int n;
	struct stat st;
	char * s_dlerror;
	void * lib_handle;
	DArray *plug_list;
	struct mod_t * sb_plugin;
       	plug_init_t func_handle;
	char m_buf[512];

	if (! path || ! name)
		return -1;

	if (*stail (path) == '/')
		*stail(m_buf) = '\0';

	m_buf[511] = 0;
	snprintf (m_buf, 511, "%s/%s", path, name);

	stat (m_buf, &st);
	if (! S_ISREG (st.st_mode))
		return -1;
	
	dlerror ();
	lib_handle = dlopen (m_buf, RTLD_NOW);
	s_dlerror = dlerror ();
	if (s_dlerror) {
		bug_plugin (s_dlerror);
		return -1;
	}

	func_handle = (plug_init_t) dlsym (lib_handle, "plugin_init");
	s_dlerror = dlerror ();
	if (s_dlerror) {
		bug_plugin (s_dlerror);
		return -1;
	}

	plug_list = func_handle (ENV);
	if (plug_list == NULL)
		return -1;
	
	for (n = 0; n < plug_list->get_size (); n++) {
		sb_plugin = (struct mod_t *) plug_list->get (n);
                sb_plugin->mod_file = strdup (m_buf); 
		if (sb_plugin) 
			ENV->modding->add (LPCHAR (sb_plugin));
	}

	return 0;
}

