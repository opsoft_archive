/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "head.h"
#include "wit.h"
#include "base.h"
#include "the_tt.h"
#include "mod.h"
#include "dbg.h"

extern "C" struct env_t *ENV;
struct env_t *ENV;

void bob_set (char *opt)
{
	char m_buf[512];
	char *S;

	if (! opt)
		return;
	strcpy (m_buf, opt);
	S = index (m_buf, '=');
	if (! S)
		return;
	
	*S = '\0';
	++S;
	strip2 (m_buf);
	strip (S);
	ENV->settings->set (m_buf, strdup (S));	
}

/* MAIN */
int main (int argc, char ** argv) 
{
	char * opt;
	int n_files = 0;
	EArray *d_files;
	DArray * d_opts;
	int i_cmd = 0;
	int i;
	char * exp = NULL; // for cgrep

	sb_init ();
	i_cmd = sb_prname (argv[0]);
		
	if (argc == 1 && !i_cmd) {
		usage ();
		exit (0);
	}
		
	d_files = new EArray (32);
	ENV->d_files = d_files;
	ENV->language = "C";
	d_opts = new EArray (argc);
	ENV->d_opts = d_opts;
	for (i = 0; i < argc; i++) 
		d_opts->add (argv[i]);
	
	for (i = 1; i < d_opts->get_size (); i++) {
		opt = d_opts->get (i);
		if (opt[0] != '-' && index (opt, '=')) {
			bob_set (opt);
			continue;
		}
		
		if (opt[0] != '-') {
			n_files++;
			d_files->add (strdup (d_opts->get (i)));
			continue;
		}
		
		if (EQ (opt, "--help") || 
			EQ (opt, "-h")) {
			usage ();
			exit (0);
		}

		if (EQ (opt, "-V") || EQ (opt, "--version")) {
			usage ();
			exit (0);
		}

		if (EQ (opt, "--file")) {
			i_cmd = cmd_file;
			continue;
		}
		
		if (EQ (opt, "--the-tt")) {
			i_cmd = cmd_the_tt;
			continue;
		}

		if (EQ (opt, "--give-structs")) {
			i_cmd = cmd_give_structs;
			continue;
		}
				
		if (EQ (opt, "--indent")) {
			i_cmd = cmd_indent;
			continue;
		}
	
		if (EQ (opt, "--tags")) {
			i_cmd = cmd_tags;
			continue;
		}

		if (EQ (opt, "--make-ctags") ||
		    EQ (opt, "-c")) {
			i_cmd = cmd_makectags;
			continue;	
		}
				
		if (EQ (opt, "--call-tags") ||
				EQ (opt, "-ct")) {
			SB_FLSET (SB_FLTAGSTYLE); 
			i_cmd = cmd_call_tags;
			continue;
		}

		if (EQ (opt, "--cgrep")) {
			if (++i >= argc)
				break;
			
			i_cmd = cmd_cgrep;
			exp = d_opts->get (i);
		}

		if (EQ (opt, "--plugins-info")) {
			mods_info ();
			exit (0);
		}

		if (EQ (opt, "--tags")) {
			i_cmd = cmd_tags;
			continue;
		}

		if (! opts_settings (d_opts, i))
			continue;
		
		if (! opts_globals (d_opts, i, &i_cmd)) 
			continue;
		
		if (modding_optparse (&i, 1))
			continue;
		
		if (EQ (opt, "--time-test")) 
			Dtimer ();

/*		if (d_opts->get (i)[0] == '-') {
			fprintf (stderr, "unknown option : %s\n", d_opts->get (i));
			exit (1);
		}*/
	} // for (i = 1; i < argc; i++)

	n_files = ENV->d_files->get_size ();
	
	for (i = 0; i < argc; i++) 
		modding_optparse (&i, 2);

	if (NE (ENV->language, "C")) {
		i = modding_start (i_cmd);
		exit (i);
	}

	switch (i_cmd) {
		case cmd_makectags:
			make_ctags (d_files);
			break;
		case cmd_indent:
			nogui_indent ();
			break;
		case cmd_cgrep:
			cgrep (d_files, exp);
			break;
		case cmd_call_tags:
			call_tags (d_files);
			goto out;
		case cmd_tags:
			tags (d_files, NULL);
			goto out;
		
	}

	if (i_cmd == cmd_makectags ||
	    i_cmd == cmd_indent ||
	    i_cmd == cmd_cgrep) {
		print_the_time (NULL);
		exit (0);
	}

	for (i = 0; i < n_files; i++) {
		switch (i_cmd) {
			case cmd_file:
				flist_main (d_files->get (i));
				break;
			case cmd_the_tt:
				THE_TT::the_tt_main (d_files->get(i));	
				break;
			case cmd_give_structs:
				got_structs (d_files->get (i));	
				break;
			case cmd_globals:
				globals (d_files->get(i), ENV->d_global_type);
				break;
		}
	}

	if (i_cmd == cmd_file || 
			i_cmd == cmd_the_tt || 
			i_cmd == cmd_give_structs || 
			i_cmd == cmd_globals)
		goto out;


	ENV->sb_cmd = i_cmd;
	
	if (i_cmd == cmd_the_tt && !n_files) // THE_TT for stdin
		THE_TT::the_tt_main ("-");
	else if (i_cmd == cmd_give_structs && !n_files) {
		got_structs ("-");
		print_the_time (NULL);
		exit (0);
	}
	
	if ((i_cmd == cmd_globals || i_cmd == cmd_file) && !n_files) {
		char d_buf[1024];
	
		while (fgets (d_buf, 1023, stdin)) {
			chomp (d_buf);
			switch (i_cmd) {
				case cmd_globals:
					globals (d_buf, ENV->d_global_type);
					break;
				case cmd_file:
					flist_main (d_buf);
					break;
			}
		}
	}
	
	if (!SB_FLGET (SB_FLRTREE))
		call_tree (d_files->get (0));
	else
		reverse_calltree (d_files->get (0));

out:
	print_the_time (NULL);

	if (ENV->immune_list)
		delete ENV->immune_list;
	
	d_files->foreach (free);
	delete d_files;
	fflush (stdout);

	return EXIT_SUCCESS;
}

