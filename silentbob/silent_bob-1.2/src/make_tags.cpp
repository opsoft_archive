/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include "head.h"
#include "bugs.h"
#include <errno.h>

void make_ctags_fork ()
{
	char m_buf[512];
	int i;
	__djob_t * j;
	
	split_tmp_files ();

	for (i = 0; i < ENV->max_proc; ++i) {
		j = ENV->proc_list->fork ();
		if (j->child) {
			sprintf (m_buf, "silent_bob -L %s%i -ts --globals >%s%i", 
					ENV->tmp_files, i, ENV->tmp_tags, i);
			exit (execlp ("sh", "sh", "-c", m_buf, NULL));
		}
	}
	
	printf ("%s", "waiting threads...\n"); 
	fflush (stdout);
	while ((j = ENV->proc_list->wait_all ()) && j) 
		Dexec_done (j);

	printf ("%s", "Sorting ...\n"); 
	fflush (stdout);
	for (i = 0; i < ENV->max_proc; ++i) {
		sprintf (m_buf, "cat %s%i >>%s", ENV->tmp_tags,
				i, ENV->tmp_tags);
		system (m_buf);
		unlink (m_buf);
		sprintf (m_buf, "%s%i", ENV->tmp_files, i);
		unlink (m_buf);
	}
}

void make_ctags (EArray * d_files)
{
	char m_buf[1024];
	int i;

	unlink (ENV->tmp_files);
	unlink (ENV->tmp_tags);

	if (! d_files || d_files->get_size () == 0) 
		find_cfiles ();
	else {
		if (d_files->strings_to_file (ENV->tmp_files) <= 0)
			return;
	}
	
	printf ("Make ... \\\n");
	fflush (stdout);	

	if (SB_FLGET (SB_FLCTAGSAPPEND)) {
		sprintf (m_buf, "cp ./tags %s", ENV->tmp_tags);
		system (m_buf);
	} else 
		unlink ("./tags");
	
	printf ("Parse code...\n");
	fflush (stdout);
	if (ENV->max_proc > 1) 
		make_ctags_fork ();
	else {
		sprintf (m_buf, "silent_bob -L %s -ts --globals >>%s", 
				ENV->tmp_files, ENV->tmp_tags);
		i = system (m_buf);
		if (i != 0)
			bug_system ();	
	}

	printf ("Sorting...\n");
	fflush (stdout);
	mk_tags ("tags", NULL);
	
	unlink (ENV->tmp_files);
	unlink (ENV->tmp_tags);
}

