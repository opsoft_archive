/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

/*
 * January 2006 - typedef's find FIXED.
 * 
 **/

#include <gclib/gclib.h>
#include <sys/mman.h>
#include "head.h"
#include "wit.h"
#include "dbg.h"

// Get (real) tag and return filename and line (*d_line)
#define NOT_VALID 0
#define CALL_BACK 1
#define VALID 2

int offset_to_line (char * d_file, int d_offset)
{
	int size = 0;
	int line = 1;
	char * d_map;
	int fd = 0;
	int i;
	
	d_map = DFMAP (d_file, &fd, &size);
	if (! d_map)
		return -1;

	for (i = 0; i < d_offset; i++) {
		if (d_map[i] == '\n')
			line++;
	}	

	if (fd > 0)
		close (fd);
	
	munmap (d_map, size);
	return line;
}

int parse_regexp (char * d_file, char * S)
{
	char d_grep_cmd [1024];
	FILE * file_grep_out;
	char d_buf[1024];
	int fd_grep_out;
	char * d_begin;
	char * d_end;
	int d_pid;
	int i = 0;
	
	S++;
	d_end = strchr_r (S, '/', 0);
	if (! d_end)
		return -1;

	*d_end = 0;
	d_begin = S;
	d_buf[i] = '\"';
	i++;
	while (*S) {
		if (*S == '*' || *S == '[' || *S == ']' || *S == '\"') {
			d_buf[i] = '\\';
			i++;
			d_buf[i] = *S;			
		} else 
			d_buf[i] = *S;
		S++;
		i++;
	}
	d_buf[i] = '\"';
	i++;
	d_buf[i] = 0;
	
	snprintf (d_grep_cmd, 1023, "grep -e \"%s\" %s -b", d_buf, d_file);
	printf ("%s\n", d_grep_cmd);
	
	d_pid = Dfork (d_grep_cmd, NULL, &fd_grep_out, NULL);
	if (d_pid < 0)
		return -1;
	
	fault (! d_pid);
	
	file_grep_out = fdopen (fd_grep_out, "r");
	fgets (d_buf, 1023, file_grep_out);
	
	fclose (file_grep_out);
	return offset_to_line (d_file, atoi (d_buf));
}

bool ctags_tag (char *d_str, char * d_name, d_tag_t * d_tag)
{
	char m_buf[256];
	char *d_file;
	char * ptr;
	char *S;

	strcpy (m_buf, d_str);
	d_str = m_buf;
	
	strncpy (d_tag->d_name, d_name, 255);
	d_tag->d_name[255] = 0;
	S = strchr (d_str, '\t');
	if (! S)
		return false;

	S++;
	d_file = S;
	S = strchr (d_file, '\t');
	if (! S)
		return false;
	
	*S = 0;
	strncpy (d_tag->d_file, d_file, 255);
	d_tag->d_file[255] = 0;
	
	S++;

	if (if_digit (S)) 
		d_tag->d_line = atoi (S);
	else {
		ptr = strstr (S, "\t;");
		if (ptr)
			*ptr = 0;
		d_tag->d_line = parse_regexp (d_file, S);
	}

	return true;
}

EArray * got_tag_ctags (char * d_tag)
{
	struct d_tag_t *d_new_tag;
	char d_buf[256];
	EArray * d_ret;
	int d_len;
	char *S;
	int i;

	d_ret = new EArray;

	if (ENV->d_tags_file == NULL) {
		ENV->d_tags_file = new EArray;
		ENV->d_tags_file->from_file ("./tags");	
	}

	snprintf (d_buf, 255, "%s\t", d_tag);
	d_len = strlen (d_buf);

	if (ENV->d_tags_file->get_size () == 0)
		return d_ret;
	
	i = ENV->d_tags_file->snfind_fast (d_buf);
	
	if (i == -1) {
		fprintf (ENV->d_stream_dbg, "\tENV->d_tags_file->snfind_fast == -1");	LN;
		return d_ret;
	}
	
	d_len = strlen (d_buf);
	do {
		i++;
		S = ENV->d_tags_file->get (i);
		if (! S)
			break;
	} while (!strncmp (S, d_buf, d_len));
	i--;
	
	while (true) {
		S = ENV->d_tags_file->get (i);
		fprintf (ENV->d_stream_dbg, "\ttag : %s\n", S); fflush (ENV->d_stream_dbg);

		d_new_tag = CNEW (d_tag_t, 1);
		memset (d_new_tag, 0, sizeof (d_tag_t));
		if (strstr (S, ";\tf"))
			d_new_tag->d_type = GLOBAL_TYPE_FUNCTION;

		if (ctags_tag (S, d_tag, d_new_tag) == false) {
			DROP (d_new_tag);
			fprintf (ENV->d_stream_dbg, "\tBAD tag : %s\n", S);
			return d_ret;
		}
		
		d_ret->add ((long) d_new_tag);
		
		i --;
		if (i < 0)
			break;
		
		S = ENV->d_tags_file->get (i);
		if (strncmp (S, d_tag, strlen (d_tag)))	 	
			break;
	}	

	return d_ret;
}

