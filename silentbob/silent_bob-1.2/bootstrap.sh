#!/bin/sh
cd "`dirname $0`"
make -s -f Makefile.cvs
if [ $? = "0" ]; then
	echo Now you must run:
	echo -e "\t./configure - to configure project"
	echo -e "\tmake all - to build project"
	echo -e "\tmake install - to install project"
else
	echo "**** ERROR while bootstrapping ****" >&2
fi
