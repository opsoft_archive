/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_BOBPARSER_H
#define DEFINE_BOBPARSER_H

class bobParser 
{
	public:
		bobParser ();
		~bobParser ();

		tt_state_t * parseC (char * fileName);
		tt_state_t * parsePerl (char * fileName);
		tt_state_t * parsePython (char * fileName);

		int nextOp ();
		int line ();
		int wit ();
		void opDump (FILE * stream);
		void fullDump (FILE * stream);
		void pushBlock ();
		void popBlock ();
		__block_t * lastBlock ();
		int lastBlockLine ();

		int opNo;
		char * d_out;
		char * d_ptr;
		int blockLevel;
		tt_state_t * tt;
		DStack * blockStack;
};

#endif

