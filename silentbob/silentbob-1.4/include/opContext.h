/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef OPCONTEXT_H
#define OPCONTEXT_H

struct opContext
{
	int opNo;
	int beginLine;
	int endLine;
	char * op; 	
	int opType;
	char ch;
	int blockLevel;
};

#endif

