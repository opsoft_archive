/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include "head.h"
#include "wit.h"

void mk_tag_macro (char * d_op, char * d_file, int t_line)
{
	char S[256];
	
	if (! macro_name (d_op, S))
		return;
	
	printf ("%s\t%s\t%i\n", S, d_file, t_line);
}

void mk_tag (char * d_op, char * d_file, int line, int d_found_type)
{
	char * d_format = "%s\t%s\t%i\n";
	char * S;
	
	if (d_found_type & GLOBAL_TYPE_DEFINE) {
		mk_tag_macro (d_op, d_file, line);
		return;
	}

	if (d_found_type & GLOBAL_TYPE_CLASS) {
		S = strchr (d_op, ':');
		if (S) 
			*S = 0;		
	}
	
	S = ww_last_word (d_op);
	if (! S || !(*S) || (*S == ' '))
		return;

	if (*S == 's') {
		if (! strncmp (S, "static", 6))
			return;
		if (! strncmp (S, "struct", 6))
			return;
	}
	
	if (*S == 'u' && !strncmp (S, "union", 5))
		return;
	if (*S == 'e' && !strncmp (S, "enum", 4))
		return;	
		
	if (d_found_type & GLOBAL_TYPE_FUNCTION)
		d_format = "%s\t%s\t%i\t;\tf\n";

	printf (d_format, S, d_file, line);
}

DArray * mk_tag_structtail_split (char *S)
{
	bool b_macro = false;
	char * d_old = NULL;	
	int brace_depth = 0;
	DArray * d_array;

	d_array = new DArray (128);
	if (! d_array)
		return NULL;
	
	d_old = S;
	
	while (true) {		
		if (*S == '\"' || *S == '\'') {
			S = sstrend (S);
			if (S == NULL || *S == '\0')
				break;
		}

		if (*S == '(')
			brace_depth++;
		
		if (*S == ')') {
			brace_depth--;
			if (! brace_depth) {
				S++;
				break;
			}
		}			
		
		if (*S == 0) {
			if (! b_macro)
				d_array->add (d_old);
			break;
		}
		
		if (brace_depth) {
			S++;
			continue;
		}
	
		if (S[1] == '(') {
			b_macro = true;
			S++;
			continue;
		}			
			
		if (*S == ' ' || *S == ',') {
			*S = 0;
			S++;
			b_macro = false;
			while (*S == ' ' || *S == '*')
				S++;
			if (! b_macro)
				d_array->add (d_old);
			d_old = S;
			
			continue;
		}
		S++;
	}

	return d_array;
}

void mk_tag_structtail (char * S, char * d_file, int t_line)
{
	char *w;
	DArray * d_array;
	int i;

	d_array = mk_tag_structtail_split (S);
	if (! d_array) 
		return;
	
	for (i = 0; i < d_array->get_size (); i++) {
		w = ww_last_word (d_array->get (i));
		if (! w || !(*w) || *w == ' ')
			continue;
		printf ("%s\t%s\t%i\n", w, d_file, t_line);
	}

	delete d_array;		
}

