/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <sys/mman.h>
#include <gclib/gclib_c.h> 

int offset_to_line (char * d_file, int d_offset)
{
	int size = 0;
	int line = 1;
	char * d_map;
	int fd = 0;
	int i;
	
	d_map = DFMAP (d_file, &fd, &size);
	if (! d_map)
		return -1;

	for (i = 0; i < d_offset; i++) {
		if (d_map[i] == '\n')
			line++;
	}	

	if (fd > 0)
		close (fd);
	
	munmap (d_map, size);
	return line;
}

