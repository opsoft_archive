/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include <head.h>

char * cts (struct c_word * d_word)
{
	char * S;

	if (d_word == NULL)
		return NULL;

	S = d_word->S;
	if (!strncmp (S, "else ", 5))
		S += 5;
	
	if (d_word->ch != '(')
		return NULL;
	
	while (!strncmp (S, "do ", 3))  
		S += 3;
	
	if (!strncmp (S, "return ", 7))
		S += 7;
	
	if (ENV->d_cops->sfind (S) != -1)
		return NULL;
	
	if (words_count (S) != 1)
		return NULL;
	
	return S;
}


