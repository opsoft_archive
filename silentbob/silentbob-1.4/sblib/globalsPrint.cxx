/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include <head.h>
void globalsPrint (tt_state_t * tt, char * d_out, int d_found_type)
{
	int t_line;

	if (SB_FLGET (SB_FLSIMULATE))
		return;
	
	t_line = tt->d_attachment[ENV->t_op_no].pair_line+1;
	t_line += ww_begin_line (tt, d_out, 
			tt->d_attachment[ENV->t_op_no].offset);
	
	if (SB_FLGET(SB_FLTAGSTYLE))
		mk_tag (d_out, tt->d_file_name, 
				t_line, d_found_type); 
	else
		printf ("%s\t\t//file %s //line %i\n", d_out, 
				tt->d_file_name, t_line);
}

