/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include <head.h>
#include <the_tt.h>

char * ww_begin_offset (struct tt_state_t * d_tt_state, char *d_out, int d_offset)
{
	char *S = &d_tt_state->d_file_in [d_offset] - 1;
	char * d_real = &d_out[strlen (d_out)] - 1;

	while (d_real != d_out) {		
		if (*d_real == ' ' || *d_real == '\t') {
			while (*S == ' ' || *S == '\t')
				S--;
			
			while ((*d_real == ' ' || *d_real == '\t') 
					&& (d_real != d_out))
				--d_real;
			
			continue;
		}
		
		if (*S == *d_real)
			--d_real;

		--S;
	}

	return S;
}

