/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include <head.h>

char * ww_last_word (char *d_op)
{
	char * S = d_op;
	char * d_word;
	
	while (*S) {
		if (*S == '(' || *S == '=' || *S == '[')
			break;
		S++;
	}

	if (S[-1] == ' ')
		S--;

	*S = 0;
	d_word = d_op;
	while (true) {
		S = strchr (d_word, ' ');
		if (S == NULL)
			break;
		d_word = S+1;
	}

	while (*d_word == '*' ||
			*d_word == '&' ||
			*d_word == ' ')
		d_word++;
	
	return d_word;
}

