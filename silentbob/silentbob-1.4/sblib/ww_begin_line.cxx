/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include <structs.h>
#include <string.h>
#include <the_tt.h>

int ww_begin_line (struct tt_state_t * d_tt_state, char *d_out, int d_offset)
{
	char *S = &d_tt_state->d_file_in [d_offset] - 1;
	char * d_end = &d_out[strlen (d_out)] - 1;
	int Ret = 0;

	while (d_end > d_out) {		
		if (*d_end == ' ' || *d_end == '\t') {
			while ((S >= d_tt_state->d_file_in) && (*S == ' ' || *S == '\t'))
				--S;
			
			if (S < d_tt_state->d_file_in)
				return Ret;

			while ((d_end >= d_out) && (*d_end == ' ' || *d_end == '\t'))
				--d_end;

			if (d_end <= d_out)
				return Ret;

			continue;
		}
		
		if (*S == '\n')
			--Ret;

		if (*S == *d_end)
			--d_end;

		--S;
	}

	return Ret;
}


