/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include <string.h>

char * macro_name (char * d_op, char * d_macro_name)
{
	char *d_begin;
	char *S;
	char m_buf[256];

	strncpy (m_buf, d_op, 255);
	m_buf[255] = 0;
	
	S = strstr (m_buf, "define");
	
	if (! S)
		return NULL;
	
	S = strchr (S, ' ');
	if (! S)
		return NULL;
	
	while (*S == ' ')
		S++;

	d_begin = S;
	S = strchr (d_begin, ' ');
	if (S)
		*S = 0;

	S = strchr (d_begin, '(');
	if (S)
		*S = 0;
	
	strcpy (d_macro_name, d_begin);
	return d_macro_name;
}

