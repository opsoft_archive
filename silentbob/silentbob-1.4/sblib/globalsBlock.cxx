/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include <head.h>
#include <wit.h>

bool globalsCheckUp (DStack * blocks_stack, int d_found_type)
{
	__block_t * b;
	b = (__block_t *) blocks_stack->last ();

	while (b && ! b->type)
		b = (__block_t *) blocks_stack->prev ();

	if (! b)
		return true;

	if (OR (b->type, OP_TYPE_FUNCTION, OP_TYPE_VARIABLE))
		return false;

	if (OR (b->type, OP_TYPE_CLASS, OP_TYPE_STRUCT) &&
	    OR (d_found_type, OP_TYPE_CLASS, OP_TYPE_STRUCT))
		return true;

	if (b->type == OP_TYPE_CLASS) {
		if (! (OR (d_found_type, OP_TYPE_FUNCTION, OP_TYPE_DEFINE)))
			return false;
	}
	
	return true;
}

void globalsPushBlock (DStack *stack, int type)
{
	__block_t * b;
	b = CNEW (__block_t, 1);
	b->type = type;
	b->op_no = ENV->t_op_no;
	stack->push (LPCHAR (b));
}

void globalsPopBlock (DStack *stack) 
{
	__block_t * b;
	b = (__block_t *) stack->pop ();
	delete b;
}


