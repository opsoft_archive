/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include <head.h>
#include <bugs.h>

void skip_macro (char ** d_ptr, char ** d_out, char ch)
{
	char *macro_start;
	char * d_my;
	int n = 0;
	
	d_my = *d_out;
	macro_start = *d_out;
	while (true) {
		if (ch == '\n')
			n++;

		if ((ch == '\n') && (d_my[strlen (d_my) - 1] != '\\'))
			break;

//		if (n > 300) 
//			break;

		ch = t_op (d_ptr, d_out);
		ENV->t_op_no ++;		
		if (ch == 0)
			break;
		d_my = *d_out;
	}
}

