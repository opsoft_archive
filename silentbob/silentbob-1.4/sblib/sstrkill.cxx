/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <string.h>

char * sstrend (char *);

char * sstrkill (char *OP)
{
	char *S;
	char *tail;

	if (! OP)
		return NULL;

	S = OP;
	while (*S) {
		if (*S == '\"' || *S == '\'') {
			tail = sstrend (S);
			if (! tail)
				break;

			if (*tail == '\0' ||*(tail+1) == '\0') {
				*S = '\0';
				break;
			}

			++S;
			strcpy (S, tail);
		}
		++S;
	}
	
	return OP;
}


