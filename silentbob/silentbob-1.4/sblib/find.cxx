/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include <head.h>
#include <sys/wait.h>

int sblib_find (char * path, char * name, char * f_outname)
{
	int devnull;
	int fd;
	int pid;
	int status = -1;

	pid = fork ();
	if (pid == 0) {
		devnull = open ("/dev/null", O_WRONLY, 0600);
		fd = open (f_outname, O_WRONLY, 0600);
		if (fd == -1) {
			fd = open (f_outname, O_WRONLY | O_CREAT, 0600);
			if (fd == -1) {
				close (devnull);
				return -1;
			}
		} else
			lseek (fd, 0, SEEK_END);
		dup2 (devnull, 2);
		dup2 (fd, 1);
		execlp ("find", path, "-name", name, NULL);
	} else if (pid > 0) {
		waitpid (pid, &status, 0);
		return status;
	}

	return status;		
}

