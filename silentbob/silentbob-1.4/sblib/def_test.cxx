/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include <string.h>

bool def_test (char * d_op)
{
	char * S = d_op;
	++S;
	while (*S == ' ' || *S == '\t')
		++S;

	if (! strncmp (S, "define", 6))
		return true;
	return false;
}

