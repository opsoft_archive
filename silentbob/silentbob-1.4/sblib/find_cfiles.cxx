/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <head.h>

int find_cfiles ()
{
	find_one_ext ("*.h");
	find_one_ext ("*.hpp");
	find_one_ext ("*.cpp");
	find_one_ext ("*.c");
	find_one_ext ("*.cc");
	find_one_ext ("*.cxx");
        return 0;
}

