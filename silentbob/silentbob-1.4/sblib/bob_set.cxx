/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <head.h>

void bob_set (char *opt)
{
	char m_buf[512];
	char *S;

	if (! opt)
		return;
	strcpy (m_buf, opt);
	S = index (m_buf, '=');
	if (! S)
		return;
	
	*S = '\0';
	++S;
	strip2 (m_buf);
	strip (S);
	ENV->settings->set (m_buf, strdup (S));	
}

