/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib_c.h>
#define __export

__export int Dfork (char *exec, int *fd_in, int *fd_out, int *fd_err)
{
	int ret;
	int pipe_in[2];
	int pipe_out[2];
	int pipe_err[2];

	if (fd_in)
		pipe (pipe_in);

	if (fd_out)
		pipe (pipe_out);

	if (fd_err)
		pipe (pipe_err);

	ret = fork ();
	if (ret < 0) {
		if (fd_in) 
			close_pipe (pipe_in);
		
		if (fd_out) 
			close_pipe (pipe_out);
		
		if (fd_err) 
			close_pipe (pipe_err);

		return ret;
	}

	if (ret == 0) {
		if (fd_in) {
			dup2 (pipe_in[0], fileno (stdin));
			close (pipe_in[1]);
		}

		if (fd_out) {
			dup2 (pipe_out[1], fileno (stdout));
			close (pipe_out[0]);
		}

		if (fd_err) {
			dup2 (pipe_err[1], fileno (stderr));
			close (pipe_err[0]);
		}

		if (exec)
			execlp ("sh", "sh", "-c", exec, NULL);

		return 0;
	}

	if (fd_in) {
		close (pipe_in[0]);
		*fd_in = pipe_in[1];
	}

	if (fd_out) {
		close (pipe_out[1]);
		*fd_out = pipe_out[0];
	}

	if (fd_err) {
		close (pipe_err[1]);
		*fd_err = pipe_err[0];
	}

	return ret;
}

