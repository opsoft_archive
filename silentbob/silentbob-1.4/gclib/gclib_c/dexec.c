/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib_c.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/mman.h>

#define __export

int __dexec_file_rflags = O_RDONLY;
int __dexec_file_wflags = O_WRONLY;

int __dexec_child (struct __dexec_t *buf, int count, struct __djob_t *ctx)
{
	int rf = __dexec_file_rflags;
	int wf = __dexec_file_wflags;
	int fd;
	int i;

	if (! ctx || ! buf)
		return -1;

	ctx->child = 1;
	for (i = 0; i < count; ++i) {
		switch (buf[i].cmd) {
			case DEXEC_INULL:
				fd = open ("/dev/null", O_RDONLY);
				dup2 (fd, 0);
				break;
			case DEXEC_ONULL:
				fd = open ("/dev/null", O_WRONLY);
				dup2 (fd, 1);
				break;
			case DEXEC_ENULL:
				fd = open ("/dev/null", O_WRONLY);
				dup2 (fd, 2);
				break;
			case DEXEC_IFILE:
				fd = open ((char *) buf[i].param, rf);
				if (fd >= 0)
					dup2 (fd, 0);
				break;
			case DEXEC_OFILE:
				fd = open ((char *) buf[i].param, wf);
				if (fd >= 0)
					dup2 (fd, 1);
				break;
			case DEXEC_EFILE:
				fd = open ((char *) buf[i].param, wf);
				if (fd >= 0)
					dup2 (fd, 2);
				break;
			case DEXEC_EXEC:
				execlp ("sh", "sh", "-c", buf[i].param, NULL);
				break;
			case DEXEC_IPIPE:
				close (ctx->pipe_in[1]);
				break;
			case DEXEC_OPIPE:
				close (ctx->pipe_out[0]);
				break;
			case DEXEC_EPIPE:
				close (ctx->pipe_err[0]);
				break;
			case DEXEC_OTMP:
				break;
			case DEXEC_ETMP:
				break;
		}
	}		

	if (ctx->stdin != -1) 
		dup2 (ctx->stdin, fileno (stdin));
	if (ctx->stdout != -1)
		dup2 (ctx->stdout, fileno (stdout));
	if (ctx->stderr != -1)
		dup2 (ctx->stderr, fileno (stderr));

	return 0;
}

int __dexec_parent (struct __dexec_t *buf, int count, struct __djob_t *ctx)
{
	int i;
	
	if (! buf || ! ctx || count <= 0)
		return -1;

	for (i = 0; i < count; ++i) {
		if (buf[i].cmd == DEXEC_IPIPE) {
			close (ctx->pipe_in[0]);
			ctx->stdin = ctx->pipe_in[1];
		}
		if (buf[i].cmd == DEXEC_OPIPE) {
			close (ctx->pipe_out[1]);
			ctx->stdout = ctx->pipe_out[0];
		}
		if (buf[i].cmd == DEXEC_EPIPE) {
			close (ctx->pipe_err[1]);
			ctx->stderr = ctx->pipe_err[0];
		}
		if (buf[i].cmd == DEXEC_WAIT) { 
			waitpid (ctx->pid, &ctx->exit_status, 0);
			ctx->status_ready = 1;
		}
	}

	return 0;
}

__export int dexec_wflags (int flags)
{
	if (flags) 
		__dexec_file_wflags = flags;
	return __dexec_file_wflags;
}

__export int dexec_rflags (int flags)
{
	if (flags)
		__dexec_file_rflags = flags;
	return __dexec_file_rflags;
}

__export int Dexec_op (struct __dexec_t *buf, int count, struct __djob_t *ctx)
{
	int pid;
	int i;
	
	if (! buf || count < 0 || ! ctx)
		return -1;

	memset (ctx, 0, sizeof (struct __djob_t));

	for (i = 0; i < count; ++i) {
		if (buf[i].cmd == DEXEC_IPIPE) 
			pipe (ctx->pipe_in);
		if (buf[i].cmd == DEXEC_OPIPE) 
			pipe (ctx->pipe_out);
		if (buf[i].cmd == DEXEC_EPIPE) 
			pipe (ctx->pipe_err);
		if (buf[i].cmd == DEXEC_OTMP) 
			ctx->stdout = Dtmpfd (ctx->otmp_name);
		if (buf[i].cmd == DEXEC_ETMP) 
			ctx->stderr = Dtmpfd (ctx->etmp_name);
	}
	
	pid = fork ();
	if (pid < 0)
		return pid;

	if (pid == 0) 
		return __dexec_child (buf, count, ctx);
	else {
		ctx->pid = pid;
		return __dexec_parent (buf, count, ctx);
	}
}

__export void Djob_init (struct __djob_t * ctx)
{
	memset (ctx, 0, sizeof (struct __djob_t));
	ctx->stdin = -1;
	ctx->stdout = -1;
	ctx->stderr = -1;
	ctx->pipe_in[0] = -1;
	ctx->pipe_in[1] = -1;
	ctx->pipe_out[0] = -1;
	ctx->pipe_out[1] = -1;
	ctx->pipe_err[0] = -1;
	ctx->pipe_err[1] = -1;
	ctx->otmp_name = malloc (128);
	ctx->etmp_name = malloc (128);
	ctx->otmp_name[0] = '\0';
	ctx->etmp_name[0] = '\0';
	ctx->shared_mem = NULL;
}

__export int Dexec (int ops, struct __dexec_t *other_buf, 
		int count, char *cmd, struct __djob_t *ctx)
{
	struct __dexec_t * buf; 
	int Ret;
	int i = 0;
	int k;
	
	if (! ctx)
		return -1;

	buf = malloc (sizeof (struct __dexec_t) * (32+count));
	memset (buf, 0, (sizeof (struct __dexec_t) * (32+count)));
	Djob_init (ctx);
	
#define __S513(arg) if (ops & arg) { buf[i].cmd = arg; buf[i].param = 0; ++i; }
	__S513 (DEXEC_INULL);
	__S513 (DEXEC_ONULL);
	__S513 (DEXEC_ENULL);
	__S513 (DEXEC_IPIPE);
	__S513 (DEXEC_OPIPE);
	__S513 (DEXEC_EPIPE);
	__S513 (DEXEC_OTMP);
	__S513 (DEXEC_ETMP);
	__S513 (DEXEC_WAIT);
#undef __S513	

	for (k = 0; k < count; ++k) {
		buf[i].cmd = other_buf[k].cmd;
		buf[i].param = other_buf[k].param;
		++i;
	}

	Ret = Dexec_op (other_buf, ++i, ctx);
	free (buf);
	return Ret;
}

__export int Dexec_done (struct __djob_t *ctx)
{
	if (! ctx)
		return 0;

	if (ctx->otmp_name)
		free (ctx->otmp_name);

	if (ctx->etmp_name)
		free (ctx->etmp_name);

	if (ctx->shared_mem)
		munmap (ctx->shared_mem, ctx->shm_size);
	
	fdclose (&ctx->stdin);
	fdclose (&ctx->stdout);
	fdclose (&ctx->stderr);
	free (ctx);
	return 0;
}

