/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEXEC_H
#define DEXEC_H

enum {
	DEXEC_OFILE = (1<<0),
	DEXEC_IFILE = (1<<1),
	DEXEC_EFILE = (1<<2),
	DEXEC_INULL = (1<<3),
	DEXEC_ONULL = (1<<4),
	DEXEC_ENULL = (1<<5),
	DEXEC_EXEC = (1<<6),
	DEXEC_IPIPE = (1<<7),
	DEXEC_OPIPE = (1<<8),
	DEXEC_EPIPE = (1<<9),
	DEXEC_OTMP = (1<<10),
	DEXEC_ETMP = (1<<11),
	DEXEC_WAIT = (1<<12),
};

struct __dexec_t {
	int cmd;
	long param;
};

gclib_c__export void Djob_init (struct __djob_t * ctx);
gclib_c__export int dexec_wflags (int flags);
gclib_c__export int dexec_rflags (int flags);
gclib_c__export int Dexec_op (struct __dexec_t *buf, int count, struct __djob_t *ctx);
gclib_c__export int Dexec (int ops, struct __dexec_t *other_buf, int count, char *cmd, struct __djob_t *ctx);
gclib_c__export int Dexec_done (struct __djob_t *ctx);
	
#endif

