/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <gclib/gclib.h>

int main (int argc, char ** argv)
{
	Tree * myTree;
	node_t * node;
	int i;

	while (true) {
		myTree = new Tree;
		node = myTree->newNode (NULL, NULL);
		for (i = 0; i < 100; ++i) {
			for (int k = 0; k < 1000; ++k) 
				myTree->newNode (node, NULL);
			node = myTree->newNode (node, NULL);
		}
		delete myTree;
	}

	return EXIT_SUCCESS;
}

