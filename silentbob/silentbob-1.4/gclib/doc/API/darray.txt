/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

UTF8

	Масштабируемый массив.

	DArray (int asize = 0);
	init (int asize);
Конструктор. Инициализировать массив для asize элементов. Если asize==0, принимается значение 16. 

	get / set
Получить / установить элемент с заданным номером. 

	get_size
Вернуть количество элементов.

	void foreach (Dfunc_t FUNC);
Выполнить для всех элементов.

	bool from_file (char * __from);
Прочитать массив строк из файла.

	add 
Добавить элемент. 

	drop
Сбросить (обнулить) массив.

