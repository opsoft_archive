/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

UTF8

	gc_realloc
Изменить размер блока памяти (с копированием данных).
Параметры - указатель и два размера (старый и нужный). 

	memdup
Скопировать память. 
Параметры - указатель и размер.

	Dmemmem
см. memmem (3)

	Dmid_memmem (char * begin, char * last, char * needle, size_t needlelen);
Ищет needle от байта до begin до last. last - последний байт буфера, в котором будет производиться поиск.

	Dsplit (char * String, char *sep, char ** out, int len)
Разбить строку String разделителем sep в буфер out (размером не более len).

	Dstrmid 
param1 =~ m/param2(.*)param3/
(взять все символы между указанных строк)

	chomp
Убрать конец строки (если есть).

	DSTR
Прочитать строку не более 255 символов из файла.

	strchrs (char *S, char ch, char ch2, char ch3, char ch4)
Найти первый из четырех символов в строке.

	Dstrstr_r
Найти строку (с конца).

	Dsyms (char * from, char * to, char sym)
Посчитать количество символов sym от from до to (указатель на байт после последнего).

	Dmemchr
Найти символ в буфере.

	Dstrndup (char *ptr, int n);
Копировать не более n байт строки ptr;

	Dmid_strchr (char *ptr, char *end, char ch)
Найти символ ch между ptr и end (первый и последний).

	Dmid_getstr (char *buf, char *end)
Прочитать строку. buf и end - указатели на первый и последний символ буфера.

	Drand_str (char * buf, int count)
Рандомная строка размером count.

	int2str (int i)
Создать строку с числом.

	stail
Вернет конец строки (указатель на последний ноль).

	strmov (char *buf, char * S)
Копировать S в buf. Результат - указатель на последний ноль строки.

	strip / strip2
Убить начальные / конечные пробельные символы в строке.
	
