/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

KOI8-R

	gc_realloc
�������� ������ ����� ������ (� ������������ ������).
��������� - ��������� � ��� ������� (������ � ������). 

	memdup
����������� ������. 
��������� - ��������� � ������.

	Dmemmem
��. memmem (3)

	Dmid_memmem (char * begin, char * last, char * needle, size_t needlelen);
���� needle �� ����� �� begin �� last. last - ��������� ���� ������, � ������� ����� ������������� �����.

	Dsplit (char * String, char *sep, char ** out, int len)
������� ������ String ������������ sep � ����� out (�������� �� ����� len).

	Dstrmid 
param1 =~ m/param2(.*)param3/
(����� ��� ������� ����� ��������� �����)

	chomp
������ ����� ������ (���� ����).

	DSTR
��������� ������ �� ����� 255 �������� �� �����.

	strchrs (char *S, char ch, char ch2, char ch3, char ch4)
����� ������ �� ������� �������� � ������.

	Dstrstr_r
����� ������ (� �����).

	Dsyms (char * from, char * to, char sym)
��������� ���������� �������� sym �� from �� to (��������� �� ���� ����� ����������).

	Dmemchr
����� ������ � ������.

	Dstrndup (char *ptr, int n);
���������� �� ����� n ���� ������ ptr;

	Dmid_strchr (char *ptr, char *end, char ch)
����� ������ ch ����� ptr � end (������ � ���������).

	Dmid_getstr (char *buf, char *end)
��������� ������. buf � end - ��������� �� ������ � ��������� ������ ������.

	Drand_str (char * buf, int count)
��������� ������ �������� count.

	int2str (int i)
������� ������ � ������.

	stail
������ ����� ������ (��������� �� ��������� ����).

	strmov (char *buf, char * S)
���������� S � buf. ��������� - ��������� �� ��������� ���� ������.

	strip / strip2
����� ��������� / �������� ���������� ������� � ������.
	
