/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

KOI8-R

	������� ����������.
��� IP ������ - ������������ � ���� ����� ���� ("xxx.xxx.xxx.xxx\0"); ���� - �� ������� ������.
htons ������ �� ����.

	Dsocket | Dsocket_udp
������� ����� (AF_INET, SOCK_STREAM | SOCK_UDP, 0);

	int Dsendto(void * lp_data,int int_size,char * lpsz_path);
	int Dsendto(int sock, void * lp_data, int int_size, const char * address, int port);
	int Dsendto(void * lp_data, int int_size, const char * address, int port);
��������� UDP. ������ ������� ��� AF_UNIX, ��������� - ��� AF_INET.

	int Drecvfrom (int fd, char * buf, int size, char * peer_ip, int * peer_port);
�������� UDP.

	int Dbind (int sock, char * addr, int port);
	int Dconnect (int sock, const char * addr, int port);
bind'���� � ���������� ����� ��������������.

	int Dgethostbyname(const char * lpsz_hostname,struct in_addr * address);
	char * Dgetnamebyhost(struct in_addr * params);
������� ��� ����������� ����������� �������. ��������, ����������� ������ DNS.

	int Dbroadcast (int fd);
���������� ����������������� �����.

	int Dpoll_add (EArray * d_array, int fd, short events);
	int Dpoll_coallesce (EArray * d, struct pollfd ** p);
���������� ������ fd � ������ ������ ��������, ����� � ������� ������ ��������� pollfd* ��� poll(2);

	int Dpoll_scan (struct pollfd **p, int count, int position);
��������� p �� �������� ������� �� revent'�.


