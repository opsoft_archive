/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

KOI8-R

        DPoll - ��� ������������.
        
        DConnection * init (char * name, uint32_t num = 0, int sd = 0)
���������������� ����� �����������. �������� ���������� - ("%s%i", name,
num), ��� ������ name, ���� num == 0. sd - ���������� ������� ����� �������������� � �����������.

        attach / detach
�������� / ����������� ������������ �����������.

        by_name (char * name, int num)
����� ���������� �� ����� � ������.

        poll_build (int *nfds) / poll_rebuild (int *nfds);  
�������/����������� ���� ��� poll(2).

        poll (int timeout) 
��������� poll_build, ���� ����������, � ����� poll(2). timeout - �
�������������.

        unlink
������� (�����) ����������. � �������� ���������� - ���� ���+�����, ����
���� ����������.

        scan
����������� ���. ����������� ����� ����� poll. ��������� - ����������,
���� NULL.

