/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib.h>
#include <sys/poll.h>

DPoll::DPoll ()
{
     connections = new EHash;
     pull = NULL;
     p_pos = 0;
     p_ptr = NULL;
}

DPoll::~DPoll ()
{
        dkey_t * key;
        __dlist_entry_t * one;
        DConnection * c;

        one = connections->get_head ();
        while (one) {
                key = (dkey_t *) one->data;
                c = (DConnection *) key->VALUE;
                delete c;
                one = one->next;
        }
}

DConnection * DPoll::init (char * name, uint32_t num, int fd)
{
        char m_buf[512];
        DConnection * one;

        if (! name)
                return NULL;
        
        one = new DConnection;
        one->c_sd = fd;
        if (num)
                sprintf (m_buf, "%s%i", name, num);
        else
                sprintf (m_buf, "%s", name);
        one->c_name = strdup (m_buf);
        one->c_poll_flags = POLLIN | POLLHUP;
        connections->set (m_buf, LPCHAR (one));

	return one;
}

DConnection * DPoll::attach (DConnection *c)
{
        connections->set (c->c_name, LPCHAR (c));
        return c;
}

DConnection * DPoll::by_name (char * c_name, int num)
{
        char m_buf[512];
        if (num)
                sprintf (m_buf, "%s%i", c_name, num);
        else
                sprintf (m_buf, "%s", c_name);
       return (DConnection *) connections->get (m_buf);
}

int DPoll::unlink (char * c_name, int num)
{
        DConnection *c;
        char m_buf[512];
        
        if (num)
                sprintf (m_buf, "%s%i", c_name, num);
        else 
                strcpy (m_buf, c_name);

        c = (DConnection *) connections->del (m_buf);
        if (! c)
                return -1;
        delete c;
        return 0;
}

int DPoll::unlink (DConnection * c_conn)
{
        if (! c_conn)
                return -1;
        connections->del (c_conn->c_name);
        delete c_conn;
        return 0;
}

pollfd * DPoll::poll_build (int *d_nfds)
{
        DConnection *c;
        dkey_t * key;
        __dlist_entry_t * one;
        int count;
        int i = 0;

        count = connections->count ();
        pull = CNEW (pollfd, count);
        
        one = connections->get_head ();
        while (one) {
                key = (dkey_t *) one->data;
                c = (DConnection *) key->VALUE;
                pull[i].fd = c->c_sd;
                pull[i].events = c->c_poll_flags;
                pull[i].revents = 0;
                one = one->next;                
                ++i;
        }
        
        if (d_nfds)
                *d_nfds = count;
        nfds = count;
        return pull;
}

pollfd * DPoll::poll_rebuild (int *d_nfds)
{
        if (pull) 
                DROP (pull);
        return poll_build (d_nfds);
}

int DPoll::poll (int timeout)
{
        int Ret = 0;
       
        if (! pull)
                poll_build (&nfds);
        if (nfds <= 0)
                return 0;
        Ret = ::poll (pull, nfds, timeout);
        connections->first ();
        p_pos = 0; 
        return Ret;       
}

DConnection * DPoll::scan ()
{
        DConnection * c;
        dkey_t * key;
        
        key = (dkey_t *) connections->get ();
        while (key) {
                c = (DConnection *) key->VALUE;
                key = (dkey_t *) connections->next ();
                if (pull[p_pos].revents)
                        return c;
                ++p_pos;
        }
        
        return NULL;
}

int DPoll::connections_dump ()
{
        dkey_t * key;
        __dlist_entry_t * one;
        DConnection * c;
        int i = 0;
        
        one = connections->get_head ();
        while (one) {
                key = (dkey_t *) one->data;
                c = (DConnection *) key->VALUE;
                one = one->next;
                ++i;
        }

        return i;
}


DConnection * DPoll::detach (char *c_name, int num)
{
        DConnection *c;
        char m_buf[512];

        if (num)
                sprintf (m_buf, "%s%i", c_name, num);
        else
                strcpy (m_buf, c_name);

        c = (DConnection *) connections->del (m_buf);
        return c;        
}

pollfd * DPoll::get_poll (int * count)
{
        if (count)
                *count = nfds;
        return pull;
}

