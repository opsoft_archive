/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib.h>
#include <gclib_c.h>
#include <time.h>
#include <sys/ioctl.h>

DConnection::DConnection ()
{
	c_sd = -1;
	c_type = 0;
	c_name = NULL;
	c_mtime = 0;
	c_ctime = 0;
	c_cname = NULL; // client address
	c_pname = NULL; // server address
	c_cport = 0;
	c_pport = 0;
	c_status = NULL; // other data
	c_poll_flags = 0;
	bcast = false;
}

DConnection::~DConnection ()
{
	if (c_sd != -1)
		::close (c_sd);
	if (c_cname)
		DROP (c_cname);
	if (c_pname)
		DROP (c_pname);
}

int DConnection::ipv4_init ()
{
	if (c_sd != -1) 
		::close (c_sd);
	c_sd = socket (AF_INET, SOCK_STREAM, 0);
	return c_sd;
}

int DConnection::ipv4_init_udp ()
{
	if (c_sd != -1)
		return -1;
	c_sd = socket (AF_INET, SOCK_DGRAM, 0);
	return c_sd;
}

int DConnection::ipv4_bind (char *ip, uint16_t port)
{
	if (! ip)
		return -1;
	
	DROP (c_cname);
	c_cname = strdup (ip);
	c_cport = port;
	return Dbind (c_sd, ip, port);
}

int DConnection::ipv4_connect (char *ip, uint16_t port)
{
	sockaddr_in s_addr;
	socklen_t len;
	int ret;
	
	ret = Dconnect (c_sd, ip, port);
	if (ret < 0)
		return -1;
	
	c_pname = strdup (ip);
	c_pport = port;
	if (! c_cname) {
		len = sizeof (struct sockaddr_in);
		::getsockname (c_sd, (sockaddr *) &s_addr, &len);
		DROP (c_cname);
		c_cname = strdup (inet_ntoa (s_addr.sin_addr));
		c_cport = htons (s_addr.sin_port);
	}
	c_ctime = time (NULL);
	return ret;
}

int DConnection::ipv4_udp_connect (char *ip, uint16_t port)
{
	if (! ip || c_sd < 0)
		return -1;
	
	DROP (c_pname);
	c_pname = strdup (ip);
	c_pport = port;
	return 0;
}

int DConnection::ipv4_send (char * buf, int len)
{
	return send (c_sd, buf, len, 0);
}

int DConnection::ipv4_sendto (char * buf, int len)
{
	return Dsendto (c_sd, buf, len, c_pname, c_pport);
}

int DConnection::ipv4_recv (char * buf, int len)
{
	return recv (c_sd, buf, len, 0);
}

int DConnection::ipv4_recvfrom (char *buf, int len, char * IP, int * PORT)
{
	return Drecvfrom (c_sd, buf, len, IP, PORT);
}

int DConnection::DIONREAD ()
{
	int ret;
	if (ioctl (c_sd, FIONREAD, &ret) != 0)
		return -1;
	return ret;
}

void DConnection::update_ctime (time_t d_time)
{
	if (d_time)
		c_ctime = d_time;
	else
		c_ctime = time (NULL);
}

void DConnection::update_mtime (time_t d_time)
{
	if (d_time)
		c_mtime = d_time;
	else
		c_mtime = time (NULL);
}

DConnection * DConnection::clone ()
{
	DConnection *c;
        
	c = new DConnection;
	if (c_name)
		c->c_name = strdup (c_name);
	if (c_cname)
                c->c_cname = strdup (c_cname);
        if (c_pname)
                c->c_pname = strdup (c_pname);
	
	c->c_type = c_type;
	c->c_mtime = c_mtime;
	c->c_ctime = c_ctime;
	c->c_cport = c_cport;
	c->c_pport = c_pport;
	c->c_status = c_status;
	c->c_poll_flags = c_poll_flags;
	c->bcast = bcast;
	return c;
}

int DConnection::broadcast ()
{
	return Dbroadcast (c_sd);
}

int DConnection::close ()
{
	if (c_sd != -1) {
		::close (c_sd);
		c_sd = -1;
	}
	
	DROP (c_name);
	DROP (c_cname);
	DROP (c_pname);
	return 0;
}

