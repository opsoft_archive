/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <sys/poll.h>
#include <gclib.h>

__export int Dpoll_add (EArray * d_array, int fd, short events)
{
	struct pollfd *d;
	if (d_array == NULL)
		return -1;

	d = CNEW (struct pollfd, 1);
	memset (d, 0, sizeof (struct pollfd));
	d->fd = fd;
	d->events = events;
	d->revents = 0;
	d_array->add (LPCHAR (d));
	
	return 0;
}

__export int Dpoll_coallesce (EArray * d, struct pollfd ** p)
{
	int size;
	int i;
	struct pollfd *my;
	
	if (d == NULL || p == NULL)
		return -1;

	size = d->get_size ();
	if (size == 0)
		return 0;
	
	*p = CNEW (pollfd, d->get_size ());

	for (i = 0; i < size; i++) {
		my = (struct pollfd *) d->get (i);
		(*p)[i].fd = my->fd;
		(*p)[i].events = my->events;
		(*p)[i].revents = 0;
	}	

	return size;
}

