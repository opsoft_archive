/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "head.h"

#if 0
int log_write (char *log_name, char *log_str)
{
	FILE * myfile;
	char buf[1024];
	char ts[512];
	
	sprintf (buf, "/home/oleg/bob_%s.log", log_name);	
	myfile = fopen (buf, "a");
	if (! myfile)
		return -1;
	
	Dtimestr (ts, 512);
	fprintf (myfile, "%s - %s\n", ts, log_str);
	fclose (myfile);

	return 0;
}

int log_globals (char *file, int type, int size)
{
	char buf[512];
	sprintf (buf, "file=%s type=%i size=%i", file, type, size);
	return log_write ("globals", buf);
}

int log_init ()
{
	char str[512];
	sprintf (str, "home=%s tmp_files=%s tmp_tags=%s",
			ENV->home_dir, ENV->tmp_files, ENV->tmp_tags);
	return log_write ("init", str);	
}

#else
int log_write (char *log_name, char *log_str) 
{
	return 0;
}

int log_globals (char *file, int type, int size)
{
	return 0;
}

int log_init ()
{
	return 0;
}
#endif

