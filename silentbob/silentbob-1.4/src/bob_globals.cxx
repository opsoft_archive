/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

/*
 * January 2006 - most bugs is killed.
 * January 2007 - C++ support (v0.2) 
 */

#include <head.h>
#include <the_tt.h>
#include <wit.h>

void globals_main (struct tt_state_t * tt, int d_type)
{
	DStack * blocks_stack;
	struct pair_t * d_attachment;
	bool b_function_header = false;
	int d_found_type = 0;
	bool b_found = false;
	char *d_out, *d_ptr; // for t_op
	int brace_depth = 0;
	int bb_line = 0;
	int bb = 0;
	char ch;

	ENV->t_op_no = 0;

	d_ptr = tt->d_output;
	d_attachment = tt->d_attachment;

	if (d_ptr == NULL)
		return;

	blocks_stack = new DStack;

	while (true) {
		ch = t_op (&d_ptr, &d_out);
		ENV->t_op_no++;		
		d_found_type = 0;
		b_found = false;

		switch (bb) {
			case BB_END: 
				bb = BB_TAIL;
				break;
			case BB_TAIL:
				bb = 0;
		}

		if (ch == '\0')
			break;

		if (*d_out == '#') {
			if (! def_test (d_out))
				skip_macro (&d_ptr, &d_out, ch);
			else if (d_type & GLOBAL_TYPE_DEFINE) {
				d_found_type |= GLOBAL_TYPE_DEFINE;
				b_found = true;
				goto out;
			}
			continue;
		}

		if (b_function_header) {
			b_found = false;
			goto check_end;
		}
		
		if (brace_depth && (! SB_FLGET (SB_FLWITHOUTCOPTIMIZATION))) { // Simple "C" syntax
			b_found = false;
			goto check_end;
		}

		if (! d_out || ch == '\n') {
			b_found = false;
			goto check_end;				
		}
		
		d_found_type = d_type & what_is_this (d_out, ch);
		if (d_found_type & GLOBAL_TYPE_FUNCTION)
			b_function_header = true;

		b_found = d_found_type ? true : false;
	
check_end:
		if (b_found && d_type != OP_TYPE_CALL && brace_depth) // C++
			b_found = globalsCheckUp (blocks_stack, d_found_type);

		switch (ch) {
			case '{':
				++brace_depth;
				globalsPushBlock (blocks_stack, d_found_type);
				b_function_header = false;
				break; 
			case '}':
				--brace_depth;
				globalsPopBlock (blocks_stack);			
				if (bb == BB_BEGIN && !brace_depth)
					bb = BB_END;
				break;
		}

		if (brace_depth < 0) { // Suddenly head :-))
			brace_depth = 0;
			while (blocks_stack->first ())
				globalsPopBlock (blocks_stack);
		}

out:		
		if (! b_found)
			continue;

		if (d_found_type & GLOBAL_TYPE_STRUCT && ch == '{') {
			bb_line =  tt->d_attachment[ENV->t_op_no].pair_line+1;
			bb_line += ww_begin_line (tt, 
					d_out,  tt->d_attachment[ENV->t_op_no].offset);
			bb = BB_BEGIN;
		}
		
		if (bb == BB_TAIL) { // struct { ... } .*? ; 
			bb = 0;
			b_found = false;
			if (strlen (d_out) > 1)
				mk_tag_structtail (d_out, tt->d_file_name, bb_line);
		}

		globalsPrint (tt, d_out, d_found_type);
		
		if (d_found_type & GLOBAL_TYPE_DEFINE) 
			skip_macro (&d_ptr, &d_out, ch);
	}

	delete blocks_stack;
}

void bob_globals (char * d_file, int d_type)
{
	struct tt_state_t * tt;
	tt = CNEW (tt_state_t, 1);
	tt->d_file_name = d_file;

	if (! do_tt(tt)) {
		fprintf (ENV->d_stream_dbg, "\tglobals, do_tt result is NULL\n");
		return;
	}

	log_globals (d_file, d_type, tt->d_filein_size);
	globals_main (tt, d_type);
	free_tt_state (tt);
}

