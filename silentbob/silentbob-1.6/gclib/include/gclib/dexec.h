/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DEXEC_H
#define DEFINE_DEXEC_H

gclib_c__export void Djob_init (struct __djob_t * ctx);
gclib_c__export int Dexec_done (struct __djob_t *ctx);

#endif

