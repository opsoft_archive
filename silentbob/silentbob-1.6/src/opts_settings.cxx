/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <head.h>

int opts_settings (DArray * d_opts, int & i)
{
	int argc;

	if (! d_opts)
		return 0;

	argc = d_opts->get_size ();
	
	if (d_opts->get (i)[0] != '-') {
		ENV->d_files->add (strdup (d_opts->get (i)));
		return 0;
	}
	
	if (EQ (d_opts->get (i), "--verbose")) {
		SB_FLSET (SB_FLVERBOSE);
		return 0;
	}
	
	if (EQ (d_opts->get (i), "-u")) {
		SB_FLSET (SB_FLRTREE);			
		return 0;
	}
	
	if (EQ (d_opts->get (i), "--linear") ||
			EQ (d_opts->get (i), "-l")) {
		SB_FLSET (SB_FLLINEAR);
		return 0;
	}
	
	if (EQ (d_opts->get (i), "-C") && i < argc) {
		++i;
		chdir (d_opts->get (i));
		return 0;
	}
	
	if (EQ (d_opts->get (i), "--linux")) {
		chdir ("/usr/src/linux");
		return 0;
	}
		
	if (EQ (d_opts->get (i), "--debug")) {
		SB_FLSET (SB_FLDEBUG);
		return 0;
	}
	
	if (EQ (d_opts->get (i), "--simulate"))	{
		Dtimer ();
		SB_FLSET (SB_FLSIMULATE);
		return 0;
	}
		
	if (EQ (d_opts->get (i), "--no-links"))	{
		SB_FLSET (SB_FLNOLINKS);
		return 0;
	}

	if (EQ (d_opts->get (i), "-a") || EQ (d_opts->get (i), "--all")) {
		SB_FLSET (SB_FLALL);
		return 0;
	}
	
	if (EQ (d_opts->get (i), "--test") || EQ (d_opts->get (i), "-t")) {
		SB_FLSET (SB_FLTEST);
		return 0;
	}

	if (EQ (d_opts->get (i), "--ctags-append")) {
		SB_FLSET (SB_FLCTAGSAPPEND);
		return 0;
	}
	
	if (EQ (d_opts->get (i), "-A")) {
		if (++i >= argc)
			return 0;
		
		ENV->cgrep_A = atoi (d_opts->get (i));
		return 0;
	}

	if (EQ (d_opts->get (i), "-B")) {
		if (++i >= argc) 
			return 0;
		
		ENV->cgrep_B = atoi (d_opts->get (i));
		return 0;
	}

	if (EQ (d_opts->get (i), "-j")) {
		if (++i >= argc)
			return 0;
		ENV->max_proc = atoi (d_opts->get (i));
		return 0;
	}
	
	if (EQ (d_opts->get (i), "--tag-style") || EQ (d_opts->get (i), "-ts")) {
		SB_FLSET (SB_FLTAGSTYLE); 
		return 0;
	}
	
	if (EQ (d_opts->get (i), "-L") && (i+1) < argc)	{
		++i;
		ENV->d_files->from_file (d_opts->get (i));
		ENV->d_files->foreach ((Dfunc_t) chomp);
		return 0;
	}
	
	if (EQ (d_opts->get (i), "--depth")) {
		if (++i >= argc) 
			return 0;
		
		ENV->d_depth = atoi (d_opts->get (i));
		return 0;
	}
		
	if (EQ (d_opts->get (i), "-fn")) {
		SB_FLSET (SB_FLFNAMES);
		return 0;
	}

	if (EQ (d_opts->get (i), "--lang")) {
		if (++i >= argc)
			return 0;

		ENV->language = strdup (d_opts->get (i));
		if (EQ (ENV->language, "cpp") ||
			EQ (ENV->language, "cxx") ||
			EQ (ENV->language, "c++")) {
			delete ENV->language;
			ENV->language = strdup ("C++");
			SB_FLSET (SB_FLCPP);
		}
		return 0;
	}

	if (EQ (d_opts->get (i), "--thread")) {
		SB_FLSET (SB_FLTHREAD);
		return 0;
	}

	return -1;
}




		


