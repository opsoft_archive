/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#ifndef DEFINE_ENV_H
#define DEFINE_ENV_H

struct env_t {
 EArray * d_tags_file;
 bool d_dbg_SDBG_active;
 int t_op_no;
 FILE *d_stream_dbg;
 EArray * immune_list;
 EArray * full_list;
 int d_depth;
 EArray * modding;
 EArray * d_cops;
 DArray * d_opts;
 uint32_t sb_flags;
 char * language;
 DArray * d_files;
 int cgrep_A;
 int cgrep_B;
 char * home_dir;
 char * tmp_files;
 char * tmp_tags;
 char * home_plugins;
 char * shared_plugins;
 int sb_cmd;
};

extern "C" struct env_t *ENV;
extern "C" struct env_t *get_env ();
#endif
