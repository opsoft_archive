/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include "head.h"
#include "bugs.h"

void find_one_ext (char * ext) 
{
	sblib_find ("./", ext, ENV->tmp_files);
}

void make_ctags (EArray * d_files)
{
	FILE * d_tmpfile = NULL;
	int i;
	char m_buf[1024];
	
	unlink (ENV->tmp_files);
	unlink (ENV->tmp_tags);
	if (! d_files || d_files->get_size () == 0) {
		find_one_ext ("*.c");
		find_one_ext ("*.h");
		find_one_ext ("*.cpp");
		find_one_ext ("*.cc");
		find_one_ext ("*.cxx");
	} else {
		d_tmpfile = fopen (ENV->tmp_files, "w");
		if (d_tmpfile == NULL) {
			fprintf (stderr, "file :%s\n", ENV->tmp_files);
			perror ("open");
			return;
		}
		
		for (i = 0; i < d_files->get_size (); i++)
			fprintf (d_tmpfile, "%s\n", d_files->get (i));
		
		fclose (d_tmpfile);
	}
	
	printf ("Making ... \\\n");
       	fflush (stdout);	

	if (SB_FLGET (SB_FLCTAGSAPPEND)) {
		sprintf (m_buf, "cp ./tags %s", ENV->tmp_tags);
		system (m_buf);
	} else 
		unlink ("./tags");

	sprintf (m_buf, "silent_bob -L %s -ts --globals >>%s", ENV->tmp_files, ENV->tmp_tags);
	i = system (m_buf);
	if (i != 0)
		bug_system ();	

	mk_tags ("tags", NULL);
	
	unlink (ENV->tmp_files);
	unlink (ENV->tmp_tags);
}

