/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dlib.h>
#include <sys/wait.h>
#include "../mod.h"
#include "../head.h"
#include "../dbg.h"

extern "C" DArray * plugin_init (struct env_t *env);

void ra_file (char * file)
{
	int fd;
	
	fd = open (file, O_RDONLY);
	if (fd < 0)
		return;

	Dreadahead (fd, 0, fsize (fd));
	close (fd);
	return;
}

char ra_opt (DArray * d_opts, int * pos)
{
	DArray * files = ENV->d_files;
	char * S;
	int i;
	
	if (! d_opts || ! pos)
		return 0;

	S = d_opts->get (*pos);
	if (EQ (S, "--readahead") || EQ (S, "-ra")) {
		for (i = 0; i < files->get_size (); i++) {
			ra_file (files->get (i));
		}
		exit (0);
		return 1;
	}

	return 0;
}

void ra_info ()
{
	printf ("ReadAhead.\n");
	printf ("Version: 1.0\n");
	printf ("Load files to system cache.\n");
	printf ("options: --readahead\n");
	printf ("NOTE: linux-specific.\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	bzero (pm, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.Name = strdup ("ReadAhead");
	pm->opt2 = ra_opt;
	pm->mod.info = ra_info;

	Ret->add (LPCHAR (pm));
	return Ret;
}
