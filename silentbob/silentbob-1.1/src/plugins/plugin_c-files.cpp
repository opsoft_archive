/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dlib.h>
#include <sys/wait.h>
#include "../mod.h"
#include "../head.h"
#include "../dbg.h"

extern "C" DArray * plugin_init (struct env_t *env);

void find_one_ext (char * ext) 
{
	sblib_find ("./", ext, "./cfiles");
}

char cfiles_opt (DArray * d_opts, int * pos)
{
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	S = d_opts->get (*pos);
	if (EQ (S, "--cfiles")) {
		find_one_ext ("*.c");
		find_one_ext ("*.h");
		find_one_ext ("*.cpp");
		find_one_ext ("*.cc");
		find_one_ext ("*.cxx");
		exit (0);
	}

	return 0;
}

void cfiles_info ()
{
	printf ("C/C++ files.\n");
	printf ("Version: 1.0\n");
	printf ("options: --cfiles\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	bzero (pm, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.info = cfiles_info;
	pm->opt = cfiles_opt;

	Ret->add (LPCHAR (pm));
	return Ret;
}

