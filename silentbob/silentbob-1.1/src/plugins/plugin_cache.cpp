/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dlib.h>
#include <sys/wait.h>
#include "../mod.h"
#include "../head.h"
#include "../dbg.h"

extern "C" DArray * plugin_init (struct env_t *env);

void cache_file (char * file, int adv)
{
	int fd;
	
	fd = open (file, O_RDONLY);
	if (fd < 0)
		return;
	
	Dposix_fadvise (fd, 0, fsize (fd), adv);
	close (fd);
	return;
}

char cache_opt (DArray * d_opts, int * pos)
{
	DArray * files = ENV->d_files;
	char * S;
	int i;
	
	if (! d_opts || ! pos)
		return 0;

	S = d_opts->get (*pos);
	if (EQ (S, "--cache-load")) {
		for (i = 0; i < files->get_size (); i++) 
			cache_file (files->get (i), POSIX_FADV_WILLNEED);
		exit (0);
		return 1;
	}

	if (EQ (S, "--cache-free")) {
		for (i = 0; i < files->get_size (); i++)
			cache_file (files->get (i), POSIX_FADV_DONTNEED);
		exit (0);
	}

	return 0;
}

void cache_info ()
{
	printf ("System cache.\n");
	printf ("Version: 1.0\n");
	printf ("Load files to system cache.\n");
	printf ("options: [--cache-load | --cache-free]\n");
	printf ("Note: x86_64 specific.\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	bzero (pm, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.Name = strdup ("Cache");
	pm->opt2 = cache_opt;
	pm->mod.info = cache_info;

	Ret->add (LPCHAR (pm));
	return Ret;
}
