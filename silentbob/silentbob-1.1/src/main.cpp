/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "head.h"
#include "wit.h"
#include "base.h"
#include "the_tt.h"
#include "mod.h"

extern "C" struct env_t *ENV;
struct env_t *ENV;

int sb_prname (char *arg)
{
	int i_cmd = 0;
	char * d_ptr;
	d_ptr = arg + strlen (arg);
	d_ptr--;
	
	while (*d_ptr != '\\' &&
			d_ptr > arg)
		d_ptr--;

	if (*d_ptr == '\\') 
		d_ptr++;
	
	if (EQ (d_ptr, "tags"))
		i_cmd = cmd_tags;
	else if (EQ (d_ptr, "the_tt"))
		i_cmd = cmd_the_tt;
	else if (EQ (d_ptr, "gc_indent"))
		i_cmd = cmd_indent;
	else if (EQ (d_ptr, "structs"))
		i_cmd = cmd_give_structs;

	return i_cmd;
}

/* MAIN */
int main (int argc, char ** argv) 
{
	int d_global_type = GLOBAL_TYPE_VARIABLE;
	int n_files = 0;
	EArray *d_files;
	DArray * d_opts;
	int i_cmd = 0;
	int i;
	char * exp = NULL; // for cgrep

	sb_init ();
	i_cmd = sb_prname (argv[0]);
		
	if (argc == 1 && !i_cmd) {
		usage ();
		exit (0);
	}
		
	d_files = new EArray (32);
	ENV->d_files = d_files;
	ENV->language = "C";
	d_opts = new EArray (argc);
	ENV->d_opts = d_opts;
	for (i = 0; i < argc; i++) 
		d_opts->add (argv[i]);
	
	for (i = 1; i < d_opts->get_size (); i++) {
		if (d_opts->get (i)[0] != '-') {
			n_files++;
			d_files->add (strdup (d_opts->get (i)));
			continue;
		}
		
		if (EQ (d_opts->get (i), "--help") || EQ (d_opts->get (i), "-h")) {
			usage ();
			exit (0);
		}

		if (EQ (d_opts->get (i), "-u")) {
			SB_FLSET (SB_FLRTREE);			
			continue;
		}

		if (EQ (d_opts->get (i), "--verbose")) {
			SB_FLSET (SB_FLVERBOSE);
			continue;
		}

		if (EQ (d_opts->get (i), "--linear") || EQ (d_opts->get (i), "-l")) {
			SB_FLSET (SB_FLLINEAR);
			continue;				
		}

		if (EQ (d_opts->get (i), "-C") && i < argc) {
			i++;
			chdir (d_opts->get (i));
			continue;
		}

		if (EQ (d_opts->get (i), "--linux")) {
			chdir ("/usr/src/linux");
			continue;
		}
		
		if (EQ (d_opts->get (i), "--file")) {
			i_cmd = cmd_file;
			continue;
		}
		
		if (EQ (d_opts->get (i), "--the-tt")) {
			i_cmd = cmd_the_tt;
			continue;
		}

		if (EQ (d_opts->get (i), "--give-structs")) {
			i_cmd = cmd_give_structs;
			continue;
		}

		if (EQ (d_opts->get (i), "--no-links"))	{
			SB_FLSET (SB_FLNOLINKS);
			continue;
		}
				
		if (EQ (d_opts->get (i), "--indent")) {
			i_cmd = cmd_indent;
			continue;
		}
	
		if (EQ (d_opts->get (i), "--tags")) {
			i_cmd = cmd_tags;
			continue;
		}

		if (EQ (d_opts->get (i), "--debug")) {
			fclose (ENV->d_stream_dbg);
			ENV->d_stream_dbg = fdopen (fileno (stderr), "w");
			continue;
		}

		if (EQ (d_opts->get (i), "--globals")) {
				d_global_type = GLOBAL_TYPE_TYPEDEF |
				GLOBAL_TYPE_VARIABLE |
				GLOBAL_TYPE_STRUCT |
				GLOBAL_TYPE_FUNCTION |
				GLOBAL_TYPE_DEFINE |
				GLOBAL_TYPE_CLASS |
				GLOBAL_TYPE_NAMESPACE;
			i_cmd = cmd_globals;
			continue;
		}

		if (EQ (d_opts->get (i), "--globals-typedef")) {		
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_TYPEDEF;
			continue;
		}
		
		if (EQ (d_opts->get (i), "--globals-extern")) {		
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_EXTERN;
			continue;
		}

		if (EQ (d_opts->get (i), "--globals-function")) {
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_FUNCTION;
			continue;
		}

		if (EQ (d_opts->get (i), "--globals-struct")) {
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_STRUCT;
			continue;
		}
		
		if (EQ (d_opts->get (i), "--globals-variable"))	{
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_VARIABLE;
			continue;
		}

		if (EQ (d_opts->get (i), "--globals-define")) {
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_DEFINE;
			continue;
		}
			
		if (EQ (d_opts->get (i), "--time-test")) {
			Dtimer ();
			continue;
		}

		if (EQ (d_opts->get (i), "--simulate"))	{
			Dtimer ();
			SB_FLSET (SB_FLSIMULATE);
			continue;
		}
		
		if (EQ (d_opts->get (i), "--make-ctags")) {
			i_cmd = cmd_makectags;
			continue;	
		}
		
		if (EQ (d_opts->get (i), "--tag-style") || EQ (d_opts->get (i), "-ts")) {
			SB_FLSET (SB_FLTAGSTYLE); 
			continue;
		}
	
		if (EQ (d_opts->get (i), "-L") && (i+1) < argc)	{
			i++;
			d_files->from_file (d_opts->get (i));
			d_files->foreach ((Dfunc_t) chomp);
			n_files = d_files->get_size ();
			continue;
		}
		
		if (EQ (d_opts->get (i), "-V") || EQ (d_opts->get (i), "--version")) {
			usage ();
			exit (0);
		}
		
		if (EQ (d_opts->get (i), "--call-tags") || EQ (d_opts->get (i), "-ct")) {
			SB_FLSET (SB_FLTAGSTYLE); 
			i_cmd = cmd_call_tags;
			continue;
		}

		if (EQ (d_opts->get (i), "--depth")) {
			if (++i >= argc) 
				break;
			
			ENV->d_depth = atoi (d_opts->get (i));
			continue;
		}
		
		if (EQ (d_opts->get (i), "-fn")) {
			SB_FLSET (SB_FLFNAMES);
			continue;
		}

		if (EQ (d_opts->get (i), "--cgrep")) {
			if (++i >= argc)
				break;
			
			i_cmd = cmd_cgrep;
			exp = d_opts->get (i);
		}

		if (EQ (d_opts->get (i), "-a") || EQ (d_opts->get (i), "--all")) {
			SB_FLSET (SB_FLALL);
			continue;
		}
		
		if (EQ (d_opts->get (i), "--test") || EQ (d_opts->get (i), "-t")) {
			SB_FLSET (SB_FLTEST);
			continue;
		}

		if (EQ (d_opts->get (i), "--ctags-append")) {
			SB_FLSET (SB_FLCTAGSAPPEND);
			continue;
		}

		if (EQ (d_opts->get (i), "-A")) {
			if (++i >= argc)
				break;
			
			ENV->cgrep_A = atoi (d_opts->get (i));
			continue;
		}

		if (EQ (d_opts->get (i), "-B")) {
			if (++i >= argc) 
				break;

			ENV->cgrep_B = atoi (d_opts->get (i));
			continue;
		}

		if (EQ (d_opts->get (i), "--plugins-info")) {
			mods_info ();
			exit (0);
		}

		if (EQ (d_opts->get (i), "--tags")) {
			i_cmd = cmd_tags;
			continue;
		}

		if (modding_optparse (&i, 1))
			continue;

/*		if (d_opts->get (i)[0] == '-') {
			fprintf (stderr, "unknown option : %s\n", d_opts->get (i));
			exit (1);
		}*/
	} // for (i = 1; i < argc; i++)
	
	for (i = 0; i < argc; i++) 
		modding_optparse (&i, 2);
	
	if (NE (ENV->language, "C")) {
		i = modding_start (i_cmd);
		exit (i);
	}

	switch (i_cmd) {
		case cmd_makectags:
			make_ctags (d_files);
			break;
		case cmd_indent:
			nogui_indent ();
			break;
		case cmd_cgrep:
			cgrep (d_files, exp);
			break;
		case cmd_call_tags:
			call_tags (d_files);
			goto out;
		case cmd_tags:
			tags (d_files, NULL);
			goto out;
		
	}

	if (i_cmd == cmd_makectags ||
	    i_cmd == cmd_indent ||
	    i_cmd == cmd_cgrep) {
		print_the_time ();
		exit (0);
	}
	
	for (i = 0; i < n_files; i++) {
		switch (i_cmd) {
			case cmd_file:
				flist_main (d_files->get (i));
				break;
			case cmd_the_tt:
				THE_TT::the_tt_main (d_files->get(i));	
				break;
			case cmd_give_structs:
				got_structs (d_files->get (i));	
				break;
			case cmd_globals:
				globals (d_files->get(i), d_global_type);
				break;
		}
	}

	ENV->sb_cmd = i_cmd;
	
	if (i_cmd == cmd_the_tt && !n_files) // THE_TT for stdin
		THE_TT::the_tt_main ("-");
	else if (i_cmd == cmd_give_structs && !n_files) {
		got_structs ("-");
		print_the_time ();
		exit (0);
	}
	
	if ((i_cmd == cmd_globals || i_cmd == cmd_file) && !n_files) {
		char d_buf[1024];
	
		while (fgets (d_buf, 1023, stdin)) {
			chomp (d_buf);
			switch (i_cmd) {
				case cmd_globals:
					globals (d_buf, d_global_type);
					break;
				case cmd_file:
					flist_main (d_buf);
					break;
			}
		}
	}
	
	if (i_cmd == cmd_file || 
			i_cmd == cmd_the_tt || 
			i_cmd == cmd_give_structs || 
			i_cmd == cmd_globals)
		goto out;

	if (!SB_FLGET (SB_FLRTREE))
		call_tree (d_files->get (0));
	else
		reverse_calltree (d_files->get (0));

out:
	print_the_time ();

	if (ENV->immune_list)
		delete ENV->immune_list;
	
	d_files->foreach (free);
	delete d_files;
	fflush (stdout);

	return EXIT_SUCCESS;
}

