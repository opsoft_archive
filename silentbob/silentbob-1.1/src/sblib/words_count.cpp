/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include "../head.h"

int words_count (char *S)
{
	bool b_begin = true;
	int d_ret = 0;

	if (S == 0)
		return 0;
	
	while (*S) {
		if (*S == ' ') {
			b_begin = true;
			S++;
			continue;
		}
				
		if (b_begin) {
			if (if_abc (S) ||
					(*S == '_') ||
					(*S == '*') ||
					(*S == '&')) {
				S++; 
				d_ret ++; 
				b_begin = false;
				continue;
			} else
				break;
		} else {
			if (!(if_abc (S) || (*S == '_') 
				|| (*S == '*') || (if_digit (S))))
				break;
		}
		
		S++;
		b_begin = false;
	}

	return d_ret;	
}


