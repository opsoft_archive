/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 */

#include "../head.h"
#include "../the_tt.h"
#include "../dbg.h"

int ww_begin_line (struct tt_state_t * d_tt_state, char *d_out, int d_offset)
{
	char *S = &d_tt_state->d_file_in [d_offset] - 1;
	char * d_end = &d_out[strlen (d_out)] - 1;
	int Ret = 0;

	while (d_end != d_out) {		
		if (*d_end == ' ' || *d_end == '\t') {
			while (*S == ' ' || *S == '\t')
				S--;
			
			while ((*d_end == ' ' || *d_end == '\t') 
					&& (d_end != d_out))
				d_end--;

			continue;
		}
		
		if (*S == '\n')
			Ret--;

		if (*S == *d_end)
			d_end--;

		S--;
		d_offset--;
		assert (d_offset < 0, "Lena 3");
	}

	return Ret;
}


