/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#ifndef DEFINE_HEAD_H
#define DEFINE_HEAD_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dlib.h>
#include "structs.h"
#include "env.h"
#include "temp_names.h"
#include "proto.h"

enum {
	SB_FLVERBOSE = 0,
	SB_FLLINEAR,
	SB_FLNOLINKS,
	SB_FLSIMULATE,
	SB_FLNOCSCOPE,
	SB_FLTAGSTYLE,
	SB_FLORIGINAL,
	SB_FLFNAMES,
	SB_FLALL,
	SB_FLPERL,
	SB_FLTEST,
	SB_FLRTREE,
	SB_FLCTAGSAPPEND
	
};

enum {
	cmd_give_structs = 1,
	cmd_file,
	cmd_globals,
	cmd_indent,
	cmd_tags,
	cmd_the_tt,
	cmd_call_tags,
	cmd_cgrep,
	cmd_makectags,
};

#define SB_FLSET(arg) (ENV->sb_flags |= (1<<arg))
#define SB_FLGET(arg) (ENV->sb_flags & (1<<arg))
#define container_of(var, type) ((type) var)

#ifdef HAVE_CONFIG_H
# include <config.h>
#else
# define PACKAGE "silent_bob"
#endif

inline bool abc_test (char * ptr)
{
	while (*ptr) {
	  if (if_abc (ptr))
		return true;
	  ptr++;
	}  
	return false;
}

#endif 
