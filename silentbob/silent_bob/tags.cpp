/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include "functions.h"
#include "dbg.h"

// SilentBob --tags
void tags (DArray * d_names, char * d_file_output)
{
	struct fdump_param_t d_param;
	EArray * d_tags;			
	struct d_tag_t * d_tag;
	int a, i, n_names;
	
	if (! d_names)
		return;
	
	n_names = d_names->get_size ();
	
	for (i = 0; i < n_names; i++) {
		d_tags = got_tag_ctags (d_names->get (i));
		if (d_tags == NULL) {
			if (d_tags->get_size () == 0) {
				fprintf (d_stream_dbg, "Tag not found : %s\n", d_names->get (i));
				fflush (d_stream_dbg);
				delete d_tags;
				continue;
			}
		}
		
		if (! d_tags)
			continue;
		
		if(! d_tags->get_size ()) {
			delete d_tags;
			d_tags = NULL;
			continue;
		}
		
		fault (! d_tags);
		
		for (a = 0; a < d_tags->get_size (); a++) {
			d_tag = (d_tag_t *) d_tags->get (a);

			fault (! d_tag);
			
			if (! d_file_output)
				printf ("// file %s line %i\n",
						d_tag->d_file, d_tag->d_line);
			
			bzero (&d_param, sizeof (struct fdump_param_t));
			d_param.n_trip = 0;
			d_param.d_file_name = d_tag->d_file;
			d_param.d_line = d_tag->d_line;
			d_param.linear = true;
			d_param.d_file_output = d_file_output;
			if (d_tag->d_type == GLOBAL_TYPE_FUNCTION)
				d_param.b_force_block = true;
			
			nogui_fdump (&d_param);
			if (! d_file_output)
				fputc ('\n', stdout);
	
			DROP (d_tag);				
		}
		
		if (d_tags) {
			d_tags->drop ();
			delete d_tags;
		}
	}
	
	fflush (stdout);
}
