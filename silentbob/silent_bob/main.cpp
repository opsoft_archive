/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#define I_IN_MAIN
#include <fcntl.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <sys/wait.h>
#include "functions.h"
#include "base.h"

#define DPRINTF

enum {
	cmd_give_structs = 1,
	cmd_file,
	cmd_globals,
	cmd_indent,
	cmd_tags,
	cmd_the_tt,
	cmd_call_tags,
	cmd_cgrep,
	cmd_makectags
};

/* MAIN */
int main (int argc, char ** argv)
{
	int d_global_type = GLOBAL_TYPE_VARIABLE;
	pid_t m_pid = 0;
	int n_files = 0;
	EArray d_files;
	int i_cmd = 0;
	int fd = 0;
	int i;
	char * exp = NULL; // for cgrep

	sb_init ();
	
	{
		char * d_ptr;
		d_ptr = argv[0] + strlen (argv[0]);
		d_ptr--;
		
		while (*d_ptr != '\\' &&
				d_ptr > argv[0])
			d_ptr--;

		if (*d_ptr == '\\')
			d_ptr++;

		if (EQ (d_ptr, "tags"))
			i_cmd = cmd_tags;
		else if (EQ (d_ptr, "the_tt"))
			i_cmd = cmd_the_tt;
		else if (EQ (d_ptr, "gc_indent"))
			i_cmd = cmd_indent;
		else if (EQ (d_ptr, "structs"))
			i_cmd = cmd_give_structs;
	}
	
	if (argc == 1 && !i_cmd) {
		usage ();
		exit (0);
	}
	
	for (i = 1; i < argc; i++) {
		if (argv[i][0] != '-') {
			n_files++;
			d_files.add (strdup (argv[i]));
			continue;
		}
		
		if (EQ (argv[i], "--help") || EQ (argv[i], "-h")) {
			usage ();
			exit (0);
		}

		if (EQ (argv[i], "-u")) {
			SB_FLSET (SB_FLRTREE);			
			continue;
		}

		if (EQ (argv[i], "--verbose")) {
			SB_FLSET (SB_FLVERBOSE);
			continue;
		}

		if (EQ (argv[i], "--linear") || EQ (argv[i], "-l")) {
			SB_FLSET (SB_FLLINEAR);
			continue;				
		}

		if (EQ (argv[i], "--gvim") || EQ (argv[i], "-g")) {
			unlink ("/tmp/silent_bob.result.c");
			fd = open ("/tmp/silent_bob.result.c", O_WRONLY|O_CREAT);
			dup2 (fd, fileno (stdout));
			SB_FLSET (SB_FLGVIM);
			continue;
		}

		if (EQ (argv[i], "-C") && i < argc) {
			i++;
			chdir (argv[i]);
			continue;
		}

		if (EQ (argv[i], "--linux")) {
			chdir ("/usr/src/linux");
			continue;
		}
		
		if (EQ (argv[i], "--file")) {
			i_cmd = cmd_file;
			continue;
		}
		
		if (EQ (argv[i], "--the-tt")) {
			i_cmd = cmd_the_tt;
			continue;
		}

		if (EQ (argv[i], "--give-structs")) {
			i_cmd = cmd_give_structs;
			continue;
		}

		if (EQ (argv[i], "--no-links"))	{
			SB_FLSET (SB_FLNOLINKS);
			continue;
		}
				
		if (EQ (argv[i], "--indent")) {
			i_cmd = cmd_indent;
			continue;
		}
	
		if (EQ (argv[i], "--tags")) {
			i_cmd = cmd_tags;
			continue;
		}

		if (EQ (argv[i], "--debug")) {
			fclose (d_stream_dbg);
			d_stream_dbg = fdopen (fileno (stderr), "w");
			continue;
		}

		if (EQ (argv[i], "--globals")) {
				d_global_type = GLOBAL_TYPE_TYPEDEF |
				GLOBAL_TYPE_VARIABLE |
				GLOBAL_TYPE_STRUCT |
				GLOBAL_TYPE_FUNCTION |
				GLOBAL_TYPE_DEFINE |
				GLOBAL_TYPE_CLASS |
				GLOBAL_TYPE_NAMESPACE;
			i_cmd = cmd_globals;
			continue;
		}

		if (EQ (argv[i], "--globals-typedef")) {		
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_TYPEDEF;
			continue;
		}
		
		if (EQ (argv[i], "--globals-extern")) {		
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_EXTERN;
			continue;
		}

		if (EQ (argv[i], "--globals-function")) {
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_FUNCTION;
			continue;
		}

		if (EQ (argv[i], "--globals-struct")) {
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_STRUCT;
			continue;
		}
		
		if (EQ (argv[i], "--globals-variable"))	{
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_VARIABLE;
			continue;
		}

		if (EQ (argv[i], "--globals-define")) {
			i_cmd = cmd_globals;
			d_global_type = GLOBAL_TYPE_DEFINE;
			continue;
		}
			
		if (EQ (argv[i], "--time-test")) {
			Dtimer ();
			continue;
		}

		if (EQ (argv[i], "--simulate"))	{
			Dtimer ();
			SB_FLSET (SB_FLSIMULATE);
			continue;
		}
		
		if (EQ (argv[i], "--make-ctags")) {
			i_cmd = cmd_makectags;
			continue;	
		}
		
		if (EQ (argv[i], "--tag-style")) {
			SB_FLSET (SB_FLTAGSTYLE); 
			continue;
		}
	
		if (EQ (argv[i], "-L") && (i+1) < argc)	{
			i++;
			d_files.from_file (argv[i]);
			d_files.foreach ((Dfunc_t) chomp);
			n_files = d_files.get_size ();
			continue;
		}
		
		if (EQ (argv[i], "-V") || EQ (argv[i], "--version")) {
			usage ();
			exit (0);
		}
		
		if (EQ (argv[i], "--call-tags") || EQ (argv[i], "-ct")) {
			SB_FLSET (SB_FLTAGSTYLE); 
			i_cmd = cmd_call_tags;
			continue;
		}

		if (EQ (argv[i], "--depth")) {
			if (++i >= argc) 
				break;
			
			d_depth = atoi (argv[i]);
			continue;
		}
		
		if (EQ (argv[i], "-fn")) {
			SB_FLSET (SB_FLFNAMES);
			continue;
		}

		if (EQ (argv[i], "--cgrep")) {
			if (++i >= argc)
				break;
			
			i_cmd = cmd_cgrep;
			exp = argv[i];
		}

		if (EQ (argv[i], "-a") || EQ (argv[i], "--all")) {
			SB_FLSET (SB_FLALL);
			continue;
		}
		
		if (EQ (argv[i], "--perl")) {
			SB_FLSET (SB_FLPERL);
			continue;
		}

		if (EQ (argv[i], "--test") || EQ (argv[i], "-t")) {
			SB_FLSET (SB_FLTEST);
			continue;
		}

		if (EQ (argv[i], "--ctags-append")) {
			SB_FLSET (SB_FLCTAGSAPPEND);
			continue;
		}

		if (argv[i][0] == '-') {
			fprintf (stderr, "unknown option : %s\n", argv[i]);
			exit (1);
		}
	} // for (i = 1; i < argc; i++)

	if (i_cmd == cmd_makectags) {
		make_ctags (&d_files);
		print_the_time ();
		exit (0);
	}
	
	if (i_cmd == cmd_indent) {
		nogui_indent ();
		print_the_time ();
		exit (0);
	}

	if (i_cmd == cmd_cgrep) {
		cgrep (&d_files, exp);
		print_the_time ();
		exit (0);
	}
	
	for (i = 0; i < n_files; i++) {
		if (i_cmd == cmd_file)
			flist_main (d_files.get (i));
		if (i_cmd == cmd_the_tt)
			THE_TT::the_tt_main (d_files.get(i));	
		if (i_cmd == cmd_give_structs)
			got_structs (d_files.get (i));	
		if (i_cmd == cmd_globals)
			globals (d_files.get(i), d_global_type);
	}
	
	if (i_cmd == cmd_the_tt && !n_files) // THE_TT for stdin
		THE_TT::the_tt_main ("-");
	else if (i_cmd == cmd_give_structs && !n_files) {
		got_structs ("-");
		print_the_time ();
		exit (0);
	}
	
	if ((i_cmd == cmd_globals || i_cmd == cmd_file) && !n_files) {
		char d_buf[1024];
	
		while (fgets (d_buf, 1023, stdin)) {
			chomp (d_buf);
			if (i_cmd == cmd_globals)
				globals (d_buf, d_global_type);
			else if (i_cmd == cmd_file)
				flist_main (d_buf);
		}
	}
	
	if (i_cmd == cmd_file || 
			i_cmd == cmd_the_tt || 
			i_cmd == cmd_give_structs || 
			i_cmd == cmd_globals)
		goto out;

	if (i_cmd == cmd_call_tags) {
		call_tags (&d_files);
		goto out;
	}
	
	if (i_cmd == cmd_tags) {
		tags (&d_files);
		goto out;
	}

	/* Call trees. */
	immune_list = new EArray;
	immune_list->from_file ("~/.silent_bob_immune");
	immune_list->foreach ((Dfunc_t) chomp);
	full_list = new EArray;

	if (SB_FLGET (SB_FLRTREE))
		reverse_calltree (d_files.get (0));
	else
		call_tree (d_files.get (0));

out:
	print_the_time ();

	if (cscope_in)
		fclose (cscope_in);
	
	if (cscope_out)
		fclose (cscope_out);

	if (SB_FLGET (SB_FLGVIM)) {
		system ("gvim -R /tmp/silent_bob.result.c");
		fflush (stdout);
		close (fd);
		close (fileno (stdout));
	}

	if (m_pid > 0)
		kill (m_pid, SIGINT);

	if (immune_list)
		delete immune_list;

	fflush (stdout);

	return EXIT_SUCCESS;
}

