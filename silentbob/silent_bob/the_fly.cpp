/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 * Oleg, "THE FLY", and BUGS.
 * 
 * "THE FLY" - "THE TT" analog for Perl code.
 * 1 May 2006 - started.
 *
 * RUS, KOI8-r.
 * �� "��". "����". ;)
 *
 */

#include "functions.h"
#include "dbg.h"

#if 1
int do_fly_for_buffers (char * d_input, int size, char * output)
{
/* "No such code. Sorry." :-)) */
	fprintf (d_stream_dbg, "\"Under construction.\"\n");
	printf ("\"Under construction.\"\n");
	exit (0);
/**/

	return 0;
}

#else
namespace THE_FLY {

char * t_map;
char * t_new;
int i;
int ii;
int t_size;

#define T t_map[i]

inline void TN (char ch) {
	t_new[ii] = ch;
	++ii;
}

inline void tt_skip ()
{
	while (T != '\n' && i < t_size)
		++i;
	--i;
}

int fly_for_buffers (char * d_input, int size, char * d_output)
{
	char ch;
	char * S;

	i = 0;
	ii = 0;
	t_size = size;
	t_map = d_input;
	t_new = d_output;
	
	for (i = 0; i < size; i++) {
		ch = T;
			
		if (ch == '<' && d_input[i+1] == '<') {
		}

		if (ch == '\'' || ch == '\"') {
		}
		
		if (ch == '\n') {
		}

		if (ch == ' ' || ch == '\t') {
		}

		if (ch == '#') {
			tt_skip ();
			continue;
		}
	}
	
	return 0;
}

char * fly_for_file (struct tt_state_t * d_tt_state)
{
	if (! d_tt_state)
		return NULL;

	return NULL;
}

char * perl_parse_tags (struct tt_state_t * d_tt_state)
{
	if (! d_tt_state)
		return NULL;

	return NULL;
}

char * perl_parse_calltags (struct tt_state_t * d_tt_state)
{
	if (! d_tt_state)
		return NULL;

	return NULL;
}

int perl_print_tags (char * f_name)
{

	return 0;
}

void perl_file (DArray * d_array)
{

}

int perl_init ()
{
	struct mod * d_newmodule;
	
	d_newmodule = CNEW (mod, 1);
	d_newmodule->Lang = strdup ("perl");
	d_newmodule->Version = strdup ("0.1");
	d_newmodule->the = fly_for_file;
	d_newmodule->print_tags = perl_print_tags;
	d_newmodule->file = perl_file;
	
	return 0;
}

}
#endif


