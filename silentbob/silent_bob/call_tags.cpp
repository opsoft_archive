/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include "functions.h"
#include "dbg.h"

void call_tags_main (tt_state_t * d_tt_state, DArray * dout)
{
	DArray * d_words = NULL;
	bool b_inmacro = false;
	char * f_name = NULL;
	char *d_ptr, *d_out; // for t_op
	int brace_depth = 0;
	char * d_old = NULL;
	char m_name[256];
	char d_buf[256];
	char * S;
	int wis;
	char ch;
	int i;

	if (dout == NULL || d_tt_state == NULL)
		return;
	
	t_op_no = 0;
	d_ptr = d_tt_state->d_output;
	d_out = d_ptr;

	while (true) {
		ch = t_op (&d_ptr, &d_out);
		t_op_no++;
		if (ch == 0)
			break;
	
		if ((*d_out == '#') && (local_define_test (d_out))) {
			if (macro_name (d_out, m_name) == NULL) {
				FDBG ("\tBUG: macro_name == NULL\n");
				fprintf (d_stream_dbg, "%s", d_out);
				LN;
			}
			else 
				f_name = m_name;
			b_inmacro = true;			
		}
		
		if (! brace_depth) {
			wis = what_is_this (d_out, ch);
			if (wis == OP_TYPE_FUNCTION) {
				f_name = ww_last_word (d_out);
				d_old = f_name;
			}
		}

		if (ch == '{')
			++brace_depth;

		if (ch == '}') {
			--brace_depth;
			if (brace_depth < 0)
				brace_depth = 0;
			if (! brace_depth) {
				f_name = NULL;
			}
		}

		if (ch == '\n' && d_out[strlen(d_out) - 1] != '\\') {
			b_inmacro = false;
			f_name = d_old;
		}
	
		if (!b_inmacro && !brace_depth)
			continue;
		
		d_words = split_to_words (d_out);
				
		for (i = 0; i < d_words->get_size (); i++) {
			S  = cts ((c_word *) d_words->get (i));
			if (! S) 
				continue;

			if (f_name != NULL) {
				snprintf (d_buf, 255, "%s\t%s\t%i\t;\tby\t%s\n", S, 
						d_tt_state->d_file_name,
						d_tt_state->d_attachment[t_op_no].pair_line+1,
						f_name);
			} else {				
				snprintf (d_buf, 255, "%s\t%s\t%i\n", S,
					d_tt_state->d_file_name,
					d_tt_state->d_attachment[t_op_no].pair_line+1);
				fprintf (d_stream_dbg, "\tBUG: %s", d_buf);
			}
			
			d_buf[255] = 0;
			dout->add (strdup (d_buf));
		}

		d_words->foreach ((Dfunc_t) free_cword);
		delete d_words; 
	}
}

int call_tags (EArray * d_files)
{
	struct tt_state_t * d_tt_state;
	DArray * d_out;
	int i;

	d_out = new DArray (1024);

	if (d_out == NULL)
		return -1;

	if (!d_files || d_files->get_size () == 0) {
		printf ("No such files.\n");
		return -1;
	}
		
	for (i = 0; i < d_files->get_size (); i++) {
		d_tt_state = CNEW (tt_state_t, 1);
		bzero (d_tt_state, sizeof (tt_state_t));
		d_tt_state->d_file_name = d_files->get (i);
		if (THE_TT::do_tt_file (d_tt_state) == NULL) {
			DROP (d_tt_state);
			continue;
		}
		
		call_tags_main (d_tt_state, d_out);		

		if (SB_FLGET (SB_FLVERBOSE))
			printf ("%s\n", d_tt_state->d_file_name); 
		THE_TT::free_tt_state (d_tt_state);
	}
   
	if (d_files->get_size () && d_out->get_size ()) 
		mk_tags ("./call_tags", d_out);
	
	d_out->foreach (free);
	delete d_out;
	return 0;
}
