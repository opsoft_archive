/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

/*
 * KOI8-r, RUS:
 * ������� ��������� ��� ������ � ��/��++ �����.
 *
 */

#include "functions.h"
#include <sys/mman.h>
#include "dbg.h"

/* code for "linear" functionality, */
void nogui_tagsdump (char * f_name, int n_trip) {
	DArray * d_tags;			
	d_tag_t * d_tag;
	struct fdump_param_t d_param;
	int a,i;
	
	d_tags = got_tag_ctags (f_name);
	
	assert (d_tags->get_size () == 0, "HimTeh 1");
	for (i = 0; i < d_tags->get_size (); i++) {
		d_tag = (d_tag_t *) d_tags->get (i);
		if (i != 0)
			fputc ('\n', stdout);

		if (!SB_FLGET(SB_FLLINEAR)) {
			for (a = 0; a < n_trip; a++)
				fputc ('\t', stdout);
		}

		printf ("// file %s line %i\n", d_tag->d_file, d_tag->d_line);
		
		bzero (&d_param, sizeof (struct fdump_param_t));
		d_param.n_trip = n_trip;
		d_param.d_file_name = d_tag->d_file;
		d_param.d_line = d_tag->d_line;
		d_param.linear = SB_FLGET (SB_FLLINEAR);
		if (d_tag->d_type & GLOBAL_TYPE_FUNCTION)
			d_param.b_force_block = true;
		nogui_fdump (&d_param);
	}
	
	d_tags->foreach (free);
	delete d_tags;
	printf ("\n");
}

void skip_macro (char ** d_ptr, char ** d_out, char ch)
{
	char *macro_start;
	char * d_my;
	int n = 0;
	
	d_my = *d_out;
	macro_start = *d_out;
	while (true) {
		if (ch == '\n')
			n++;

		if ((ch == '\n') &&	(d_my[strlen (d_my) - 1] != '\\'))
			break;

		if (n > 300) 
			bug_longmacro ();

		ch = t_op (d_ptr, d_out);
		t_op_no ++;		
		if (ch == 0)
			break;
		d_my = *d_out;
	}
}

int ww_begin_line (struct tt_state_t * d_tt_state, char *d_out, int d_offset)
{
	char *S = &d_tt_state->d_file_in [d_offset] - 1;
	char * d_end = &d_out[strlen (d_out)] - 1;
	int Ret = 0;

	while (d_end != d_out) {		
		if (*d_end == ' ' || *d_end == '\t') {
			while (*S == ' ' || *S == '\t')
				S--;
			
			while ((*d_end == ' ' || *d_end == '\t') 
					&& (d_end != d_out))
				d_end--;

			continue;
		}
		
		if (*S == '\n')
			Ret--;

		if (*S == *d_end)
			d_end--;

		S--;
		d_offset--;
		assert (d_offset < 0, "Lena 3");
	}

	return Ret;
}

inline bool ww_call_cmp (char * d_op, char * d_cmp, int d_len)
{
	if (!strncmp (d_op, d_cmp, d_len)
			&& (d_op[d_len] == '(' || d_op[d_len+1] == '('))
		return true;

	return false;
}

inline bool ww_case_cmp (char * d_op, char * d_cmp, int d_len)
{
	if (!strncmp (d_op, d_cmp, d_len) 
			&& (d_op[d_len] == ':' || d_op[d_len+1] == ':'))
		return true;

	return false;
}

inline bool local_ftest (char *S) // test for pair "()"
{
	int d_count1 = 0;
	int d_count2 = 0;
	
	while (*S) {
		if (*S == '(')
			d_count1++;
		
		if (*S == ')')
			d_count2++;
		
		if (!d_count1 || (d_count1 == d_count2)) {
			if (*S == '[')
				return false;
			if (*S == ']')
				return false;
			if (*S == '=')
				return false;
		}
		S++;
	}

	return d_count1 && d_count2;
}

inline char last_ch (char *S)
{
	S = &S[strlen (S)] - 1;
	while (if_digit (S) || if_abc(S) || *S == '_')
		S--;
	if (*S == ' ')
		S--;
	return *S;
}

inline char ww_after_word (char *S)
{
	while (true) {
		if (! (if_abc (S) || *S =='_' || if_digit (S)))
			break;
		S++;		
	}
	if (*S == ' ')
		S++;

	return *S;
}

bool local_define_test (char * d_op)
{
	char * S;

	S = d_op;
	S++;
	while (*S == ' ' || *S == '\t')
		S++;

	if (!strncmp (S, "define", 6))
		return true;
	return false;
}

int what_is_this (char * d_op, char ch)
{
	bool b_local_ftest;
	int d_words_count;
	char d_last_ch;
	char * S;
	int Ret = 0;

	if (*d_op == '#' || ch == '\n') {
		if (local_define_test (d_op))
			return OP_TYPE_DEFINE;	
		return OP_TYPE_MACRO;
	}

	d_words_count = words_count (d_op);

	if (words_count <= 0)
		return OP_TYPE_OTHER;
	
	if (d_words_count == 1) {
		if (ww_call_cmp (d_op, "if", 2) ||
				ww_call_cmp (d_op, "else", 4) ||
				ww_call_cmp (d_op, "do", 2) ||
				ww_call_cmp (d_op, "while", 5) ||
				ww_call_cmp (d_op, "switch", 6) ||
				ww_case_cmp (d_op, "case", 4))
			return OP_TYPE_OP;

		if (ww_after_word (d_op) == '(')
			return OP_TYPE_CALL;

		return OP_TYPE_OTHER; // Macro or operations (e.g. "+-=*/%^" etc...)		
	}

	d_last_ch = last_ch (d_op);

	if (d_last_ch == '=')
		return OP_TYPE_VARIABLE;
	
	b_local_ftest = local_ftest (d_op);
	if ((ch == '{' && d_last_ch == ')')) {
		if (b_local_ftest) // Paranoid.
			return OP_TYPE_FUNCTION;
	}

	if (!strncmp (d_op, "extern ", 7))
		return OP_TYPE_EXTERN;
	
	if (ch == '{') {
		Ret = 0;
		if (!strncmp (d_op, "typedef ", 8)) {
			Ret |= OP_TYPE_TYPEDEF;
			d_op += 8;
		}
			
		if (!strncmp (d_op, "static ", 7))
			d_op += 7;
		
		if (!strncmp (d_op, "const ", 6)) // "static const struct"
			d_op += 6;
				
		if (!strncmp (d_op, "union ", 6))
			return Ret | OP_TYPE_OTHER; 

		if (! strncmp (d_op, "enum ", 5))
			return Ret | OP_TYPE_OTHER; 
		
		if (!strncmp (d_op, "struct ", 7)) {
			return Ret | OP_TYPE_STRUCT;
		}

		if (!strncmp (d_op, "class ", 6))
			return OP_TYPE_CLASS;

		if (!strncmp (d_op, "namespace ", 10))
			return OP_TYPE_NAMESPACE;

		if ((words_count (d_op) > 1) && !b_local_ftest)
			return OP_TYPE_VARIABLE;
		
		if (Ret)
			return Ret;
		return OP_TYPE_OTHER;
	}
	
	if (ch == ';') {
		if (!strncmp (d_op, "typedef ", 8))
			return OP_TYPE_TYPEDEF;

		if (b_local_ftest) {
			S = strchr (d_op, '(');
			if (! S)
				return OP_TYPE_OTHER;
			
			S++;
			if (words_count (S) <= 1) {
				S = strchr_r (S, ')');
				S++;
				if (words_count (S) > 1)
					return OP_TYPE_FUNCTION; // declaration... or not ?
			}
		} else {
			if (d_words_count <= 1)
				return OP_TYPE_OTHER;

			if (!strncmp (d_op, "struct ", 7) && d_words_count == 2)  
				return OP_TYPE_OTHER;
			
			if (!strncmp (d_op, "return ", 7))
				return OP_TYPE_OTHER;
			
			if (!strncmp (d_op, "delete ", 7))
				return OP_TYPE_OTHER;
			
			return OP_TYPE_VARIABLE;
		}
		// Function defenition, callback defenition... it's all ?
	}

	return OP_TYPE_OTHER;
}

/* 1.0-rc1 -> 1.0b. Regression. */
/* KOI-r, RUS: ��� �������� ���� ��������... */
char * sstrend (char * d_ptr)
{
	bool t_instring = false;
	int d_slash_count;
	char ch_last;
	char *d_old;
	unsigned limit = 1024;
	
	if (! d_ptr)
		return (char *) 0;

	if (!(*d_ptr))
		return (char *) 0;
	
	ch_last = *d_ptr;
	d_old = d_ptr;
	limit--;
	while (*d_ptr && (limit > 0)) {
		if (*d_ptr == '\'' || *d_ptr == '\"') {
			if (t_instring && *d_ptr != ch_last) {
				d_ptr++;
				continue; // Mmm...
			}
			
			if (t_instring) {
				if (d_ptr[-1] == '\\') {
					d_slash_count = 1;
					while (d_ptr [-(d_slash_count)] == '\\')
							d_slash_count++;
				
					if (d_slash_count & 1) 
						t_instring = false;
				} else {
					d_ptr++;
					t_instring = false;
					continue;
				}
			} else {
				ch_last = *d_ptr;
				t_instring = true;
			}
		}
		
		if (t_instring)	{
			d_ptr++;
			continue;
		} else
			break;
	}

	d_ptr --;
	
	if (*d_ptr == 0)
		return (char *) 0;

	return d_ptr;
}

void free_cword (c_word * S) 
{
	free (S->S);
	free (S);
}

inline struct c_word * new_cword (char *word, char sym) {
	c_word * Ret = CNEW (c_word, 1);
	Ret->S = strdup (word);
	Ret->ch = sym;
	return Ret;
}

DArray * split_to_words (char * d_op)	
{
	DArray * d_Ret = new DArray (16);
	char * d_old = strdup (d_op);
	bool b_done = false;
	char * S = d_old;
	char * d_end;
	char ch;
	
	if (d_Ret == NULL || d_old == NULL)
		return NULL;
	
	while (true) {
		b_done = false;
		d_end = S;
		
		if (*d_end == ' ')
			d_end++;
		
		while (*d_end) {
			if (!(if_abc(d_end) ||
				  if_digit (d_end) ||
				  *d_end == '_' || 
				  *d_end == ' ') )
				break;
			d_end ++;
		}

		if (! *d_end) {
			ch = 0;
			b_done = true;
			goto split_to_words_L1;
		}

		ch = *d_end;
		if (d_end[-1] == ' ')
			d_end[-1] = 0;
		else 
			*d_end = 0;

		while (*S && *S == ' ')
			S++;
		
split_to_words_L1:
		d_Ret->add (LPCHAR(new_cword (S, ch)));

		if (b_done)
			break;
		
		if (ch == '\"' || ch == '\'') {
			*d_end = ch;
			d_end = sstrend (d_end);
			assert (d_end == NULL, "Lena 1");
			if (*d_end == '\0' || *(++d_end) == '\0')
				break;
		}
		
		S = d_end + 1;
		/**/
	}

	DROP (d_old);	
	return d_Ret;
}

// Call test simple. Find call in "word".
char * cts (struct c_word * d_word)
{
	char * S;

	if (d_word == NULL)
		return NULL;

	S = d_word->S;
	if (!strncmp (S, "else ", 5))
		S += 5;
	
	if (d_word->ch != '(')
		return NULL;
	
	while (!strncmp (S, "do ", 3))  
		S += 3;
	
	if (!strncmp (S, "return ", 7))
		S += 7;
	
	if (d_cops->sfind (S) != -1)
		return NULL;
	
	if (words_count (S) != 1)
		return NULL;
	
	return S;
}

char * ww_last_word (char *d_op)
{
	char * S = d_op;
	char * d_word;
	
	while (*S) {
		if (*S == '(' || *S == '=' || *S == '[')
			break;
		S++;
	}

	if (S[-1] == ' ')
		S--;

	*S = 0;
	d_word = d_op;
	while (true) {
		S = strchr (d_word, ' ');
		if (S == NULL)
			break;
		d_word = S+1;
	}

	while (*d_word == '*' ||
			*d_word == '&' ||
			*d_word == ' ')
		d_word++;
	
	return d_word;
}

int words_count (char *S)
{
	bool b_begin = true;
	int d_ret = 0;

	if (S == 0)
		return 0;
	
	while (*S) {
		if (*S == ' ') {
			b_begin = true;
			S++;
			continue;
		}
				
		if (b_begin) {
			if (if_abc (S) ||
					(*S == '_') ||
					(*S == '*') ||
					(*S == '&')) {
				S++; 
				d_ret ++; 
				b_begin = false;
				continue;
			} else
				break;
		} else {
			if (!(if_abc (S) || (*S == '_') 
				|| (*S == '*') || (if_digit (S))))
				break;
		}
		
		S++;
		b_begin = false;
	}

	return d_ret;	
}

struct tt_state_t *
get_onett_tag (char * f_name, char ** d_tt_buf) 
{
	DArray d_array;
	tt_state_t * Ret = NULL;
	char * S;

	if (d_tt_buf)
		*d_tt_buf = NULL;	
	
	d_array.add (f_name);
	Ret = CNEW (tt_state_t, 1);

	unlink ("./silent_bob.tmp");
	tags (&d_array, "./silent_bob.tmp");

	if (access ("./silent_bob.tmp", R_OK) != 0) 
		goto out_err;
	
	Ret->d_file_name = "./silent_bob.tmp";
	S = THE_TT::do_tt_file (Ret);
	
	if (S == NULL)
		goto out_err;
	
	if (Ret->b_mmap)
		munmap (Ret->d_file_in, Ret->d_filein_size);

	if (Ret->d_fd)
		close (Ret->d_fd);

	if (d_tt_buf)
		*d_tt_buf = S;
	
	return Ret;

out_err:	
	DROP (Ret);
	return NULL;	
}

void bug_longmacro ()
{
	printf ("Too big macro."
			"If your macro have more than 300 lines, please "
			"contact <graycardinal@pisem.net>\n"
			"Program stopped.\n");
	
	exit (0);
}

void bug_nosuch_tag (char * f_name)
{
	printf ("Tag \"%s\" not found. Broken \"tags\" file ? "
			"Try \"silent_bob --make-ctags\".\n", f_name);
}

void bug_nocalltags ()
{
	printf ("File \"call_tags\" not found. "
			"Try \"silent_bob --call-tags [-L] <files>\"\n");
	exit (1);
}

void bug_system ()
{
	printf ("Can't make tags file. Maybe you do not have write permissions ?\n");
	exit (1);
}

