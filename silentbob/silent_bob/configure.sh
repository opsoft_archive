#!/usr/bin/perl

cat >./.dlib_test.tmp.cpp <<EOF
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlib.h>

int main (int argc, char ** argv)
{
	return EXIT_SUCCESS;
}
EOF

g++ ./.dlib_test.tmp.cpp -ldlib -o ./.dlib_test_result.tmp
if test $? != 0; then  
	echo "DLib is not installed !\n";
	rm -f ./.dlib_test.tmp.cpp
	exit 0;
fi

rm -f ./.dlib_test.tmp.cpp ./.dlib_test_result.tmp

