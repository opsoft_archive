/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

/*
 * January 2006 - most bugs is killed.
 * 
 */
#include "functions.h"

char * globals_main (struct tt_state_t * d_tt_state, int d_type)
{
	struct pair_t * d_attachment;
	bool b_infunction = false;
	int d_found_type = 0;
	bool b_found = false;
	char *d_out, *d_ptr; // for t_op
	int brace_depth = 0;
	int bb_line = 0;
	char * d_file;
	int t_line;
	int bb = 0;
	char ch;

	t_op_no = 0;

	d_ptr = d_tt_state->d_output;
	d_file = d_tt_state->d_file_name;
	d_attachment = d_tt_state->d_attachment;

	if (d_ptr == NULL)
		return NULL;

	while (true) {
		ch = t_op (&d_ptr, &d_out);
		t_op_no++;		
		d_found_type = 0;
		b_found = false;
		
		if (bb == BB_END)
			bb = BB_TAIL;
		
		if (ch == '\0')
			break;

		if (*d_out == '#') {
			if (! local_define_test (d_out)) {
				skip_macro (&d_ptr, &d_out, ch);
			} else {
				if (d_type & GLOBAL_TYPE_DEFINE) {
					d_found_type |= GLOBAL_TYPE_DEFINE;
					b_found = true;
					goto out;
				}
			}
			continue;
		}

		if (b_infunction) {
			b_found = false;
			goto check_end;
		}
		
		if (brace_depth && (d_type != OP_TYPE_CALL)) {
			b_found = false;
			goto check_end;
		}

		if (*d_out == 0 || ch == '\n') {
			b_found = false;
			goto check_end;				
		}
		
		d_found_type = d_type & what_is_this (d_out, ch);
		if (d_found_type & GLOBAL_TYPE_FUNCTION)
			b_infunction = true;

		if (d_found_type)
			b_found = true;
		else
			b_found = false;
	
check_end:
		if (ch == '{') {
			b_infunction = false;
			brace_depth++;
		}

		if (ch == '}') {
			brace_depth--;	
			if (bb == BB_BEGIN && !brace_depth)
				bb = BB_END;
		}

		if (brace_depth < 0) // Suddenly head
			brace_depth = 0;

out:			
		if (d_found_type & GLOBAL_TYPE_STRUCT && ch == '{') {
			bb_line = d_attachment[t_op_no].pair_line+1;
			bb_line += ww_begin_line (d_tt_state, 
					d_out, d_attachment[t_op_no].offset);
			bb = BB_BEGIN;
		}
		
		if (bb == BB_TAIL) { // struct { ... } .*? ; 
			bb = 0;
			b_found = false;
			if (strlen (d_out) > 1)
				mk_tag_structtail (d_out, d_file, bb_line);
		}
		
		if (b_found && !SB_FLGET (SB_FLSIMULATE)) {
			t_line = d_attachment[t_op_no].pair_line+1;
			t_line += ww_begin_line (d_tt_state, 
					d_out, d_attachment[t_op_no].offset);
			
			if (SB_FLGET(SB_FLTAGSTYLE))
				mk_tag (d_out, d_file, t_line, d_found_type); 
			else
				printf ("%s\t\t//file %s //line %i\n", d_out, d_file, t_line);			
		}
		
		if (b_found && d_found_type & GLOBAL_TYPE_DEFINE) 
			skip_macro (&d_ptr, &d_out, ch);
	}

	return NULL;
}

void globals (char * d_file, int d_type)
{
	char *d_ptr;
	char *d_first;
	struct tt_state_t * d_tt_state;

	d_tt_state = CNEW (tt_state_t, 1);
	d_tt_state->d_file_name = d_file;

	d_ptr = do_tt(d_tt_state);

	if (d_ptr == NULL) {
		fprintf (d_stream_dbg, "\tglobals, do_tt result is NULL\n");
		return;
	}

	d_first = d_ptr;
	globals_main (d_tt_state, d_type);

	THE_TT::free_tt_state (d_tt_state);
}
