/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include "functions.h"

extern char * d_lpsz_operators[]; // from nogui_indent.cpp

char * flist_iter (char * d_ptr, char * d_name, bool b_repair)
{
	EArray d_operators;
	int d_len = 0;
	char *d_out;
	char ch;
	
	d_operators.push (d_lpsz_operators);
	
	while (true) {
		ch = t_op (&d_ptr, &d_out);
		d_len = strlen (d_out);
		
		if (ch == 0)
			break;

		if (d_operators.snfind (d_out) != -1)
			continue;

		if (what_is_this (d_out, ch) == GLOBAL_TYPE_FUNCTION) {
			if (b_repair)
				d_out[d_len] = ch;
			return d_out;
		}
	}
	
	return NULL;
}

// $ silent_bob --file
// Get functions list from file.
void flist_main (char * d_file)
{
	struct tt_state_t * d_tt_state;
	EArray d_operators;
	char * d_ptr;
	
	if (access (d_file, R_OK) != F_OK) {
		printf ("/*** %s ***/\n", d_file);
		perror ("");
		return;
	}
	
	d_tt_state = CNEW (tt_state_t, 1);
	d_tt_state->d_file_name = d_file;
	d_ptr = THE_TT::do_tt_file (d_tt_state);

	d_ptr = do_tt(d_tt_state);
	
	printf ("/*** %s ***/\n", d_file);
	while (true) {
		d_ptr = flist_iter (d_ptr, NULL, false);
		if (d_ptr == NULL)
			break;

		printf ("%s{\n", d_ptr);
		d_ptr += strlen (d_ptr);
		d_ptr++;
	}

	fputc ('\n', stdout);
	THE_TT::free_tt_state (d_tt_state);
}

