/* 
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "functions.h"
#include "dbg.h"

int cgrep_file (tt_state_t * d_tt_state, char * exp)
{
	char * d_ptr, *d_out;
	DArray * d_array;
	int size;
	char * ptr;
	char * S;
	char ch;
	int i;
	
	if (d_tt_state == NULL || 
			exp == NULL ||
			d_tt_state->d_output == NULL)
		return -1;

	d_out = d_tt_state->d_output;
	d_ptr = d_tt_state->d_output;
	t_op_no = 0;
	
	d_array = Dsplit (exp, ",");
	if (d_array == NULL) {
		assert (d_array == NULL, "Lena 26");
		return -2;
	}
	
	size = d_array->get_size ();
	while (true) {
		ch = t_op (&d_ptr, &d_out);
		t_op_no++;

		if (ch == 0)
			break;

		S = d_out;
		for (i = 0; i < size; i++) {
			ptr = d_array->get (i);
			S = strstr (S, ptr);
			if (! S) { 
				break;
			}
		
			S += strlen (ptr);
		}
		
		if (i == size) {
			printf ("%s\t%s\t+%i\n", exp, d_tt_state->d_file_name, 
				d_tt_state->d_attachment[t_op_no].pair_line + 1);
		}
	}

	return 0;
}

int cgrep (EArray * d_files, char * exp)
{
	struct tt_state_t * d_tt_state;
	int i;

	if (! d_files)
		return -1;
	
	for (i = 0; i < d_files->get_size (); i++) {
		d_tt_state = CNEW (tt_state_t, 1);
		bzero (d_tt_state, sizeof (tt_state_t));
		
		d_tt_state->d_file_name = d_files->get (i);
		d_tt_state->d_attachment = (pair_t *) CNEW (char, d_tt_state->d_filein_size << 1);
		if (d_tt_state->d_file_name == NULL)
			continue;

		if (THE_TT::do_tt_file (d_tt_state) == NULL)
			continue; // broken file 

		cgrep_file (d_tt_state, exp);
		THE_TT::free_tt_state (d_tt_state);
	}		

	return 0;
}
