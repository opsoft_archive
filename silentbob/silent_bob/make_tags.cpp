/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include "functions.h"

/*
 * Make tags file F_NAME from "./silent_bob_tags.tmp" data or from D_IN.
 */
void mk_tags (char *f_name, DArray *d_in)
{
	DArray * d_array = NULL;
	char ** d_skeleton;
	FILE * my_file;
	int d_size;
	int i;
	
	if (d_in == NULL) {
		d_array = new DArray (1024);
		d_array->from_file ("./silent_bob_tags.tmp");
	} else 
		d_array = d_in;
		
	d_size = d_array->get_size ();

	d_skeleton = d_array->get_skeleton ();
	Dfastsort_s (d_skeleton, d_size - 1);
	
	my_file = fopen (f_name, "w");
	if (my_file == NULL) {
		perror ("fopen");
		return;
	}			
		
	fprintf (my_file, "!_TAG_FILE_SORTED\t1\n");
	for (i = 0; i < d_size; i++) {
		fprintf (my_file, "%s", d_array->get (i));
	}
	
	if (d_in == NULL) {
		d_array->foreach (free);
		delete d_array;
	}
	fclose (my_file);
}

void make_ctags (EArray * d_files)
{
	FILE * d_tmpfile = NULL;
	int i;
	char m_buf[1024];
	
	unlink ("./silent_bob_files.tmp");
	if (! d_files || d_files->get_size () == 0) {
		system ("find ./ -name \"*.c\" >./silent_bob_files.tmp");
		system ("find ./ -name \"*.h\" >>./silent_bob_files.tmp");
		system ("find ./ -name \"*.cpp\" >>./silent_bob_files.tmp");	
		system ("find ./ -name \"*.cc\" >>./silent_bob_files.tmp");
		system ("find ./ -name \"*.cxx\" >>./silent_bob_files.tmp");
	} else {
		d_tmpfile = fopen ("./silent_bob_files.tmp", "w");
		if (d_tmpfile == NULL) {
			perror ("fopen (\"./silent_bob_files.tmp\", \"w\")");
			return;
		}
		
		for (i = 0; i < d_files->get_size (); i++)
			fprintf (d_tmpfile, "%s\n", d_files->get (i));
		
		fclose (d_tmpfile);
	}
	
	printf ("Making ... \\\n");
       	fflush (stdout);	

	if (SB_FLGET (SB_FLCTAGSAPPEND)) {
		system ("cp ./tags ./silent_bob_tags.tmp");
		i = system ("silent_bob -L ./silent_bob_files.tmp --tag-style --globals >>./silent_bob_tags.tmp");
		if (i != 0)
			bug_system ();	
	} else {
		i = system ("silent_bob -L ./silent_bob_files.tmp --tag-style --globals >./silent_bob_tags.tmp");
		if (i != 0)
			bug_system ();
	}
	mk_tags ("tags");
	
	unlink ("./silent_bob_files.tmp");
	unlink ("./silent_bob_tags.tmp");
}
