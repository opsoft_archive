/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include "functions.h"

char * c_ops [] = { "if", "else", "do", "while",
	"switch", "case", "for", "return", NULL };

int sb_init ()
{
	d_cops = new EArray(10);
	d_cops->push (c_ops);

	d_depth = 6;
	sb_flags = 0;

	cscope_in = NULL;
	cscope_out = NULL;
	d_tags_file = NULL;
	immune_list = NULL;
	
	/*
	 * Debug stream is _normal_ stream.
	 * I don't want make two copy of program.
	 */
	
	d_stream_dbg = fopen ("/dev/null", "w");	
	return 0;
}
