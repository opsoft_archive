/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#include <stdio.h>

void usage ()
{
	printf ("\"Silent Bob\" version 1.0\n");
	printf ("(c) Oleg Puchinin 2006\n");
	printf ("usage:\tsilent_bob [<kernel_function>|<files>] [<options>]\n");
	printf ("\ttags <tag> <tag2> ... <tagN> [--gvim]\n");
	printf ("\t options:\n"
			"\t\t -C <dir>\t\t-\tchdir to <dir>\n"
			"\t\t--cgrep\t\t\t-\tsearch in code\n"
			"\t\t--call-tags\t\t-\tcreate \"call_tags\" file\n"
			"\t\t--ctags-append\t\t-\tdon't rewrite \"tags\" file\n"
			"\t\t--depth <arg>\t\t-\tset reentrant level\n"
			"\t\t--file <files>\t\t-\tshow file functions\n"
			"\t\t -fn\t\t\t-\techo filenames\n"
			"\t\t--give-structs\t\t-\tshow all structs in stream\n"
			"\t\t--globals\t\t-\tfind globals.\n"
			"\t\t--globals-extern\t-\tfind all extern's\n"
			"\t\t--globals-function\t-\tfind all functions\n"
			"\t\t--globals-typedef\t-\tfind all typedef's\n"
			"\t\t--globals-variable\t-\tfind all global variables\n"
			"\t\t--gvim\t\t\t-\topen gvim with results.\n"
			"\t\t--indent\t\t-\tindention\n"
			"\t\t--L <file>\t\t-\tread files list from file\n"
			"\t\t--linear\t\t-\tmove all code to one stream...\n"
			"\t\t--linux\t\t\t-\tchdir to /usr/src/linux\n"
			"\t\t--make-ctags\t\t-\tcreate \"tags\" file\n"
			"\t\t--no-links\t\t-\tdon't show \"links\"\n"
			"\t\t--the-tt\t\t-\tstart the C preprocessor\n"
			"\t\t -u\t\t\t-\treverse call-tree\n"
			"\t\t--verbose\t\t-\tprint more other info\n"
			);
	
	printf ("\nCONTACTS\nOleg Puchinin <graycardinalster@gmail.com>\n");
}
