struct mod {
	char * Lang;
	char * Version;
	char *(*the) (struct tt_state_t *d_tt_state); // External. First step.
	char *(*parse_tags) (struct tt_state_t *d_tt_state); // SilentBob --make-ctags code body.
	char *(*parse_calltags) (struct tt_state_t *d_tt_state); // SilentBob --call-tags code body.
	int (*print_tags) (char * f_name); // tags <tag>
	void (*file) (DArray * d_files); // sb <files> --file
};
