/*
 * (c) Oleg Puchinin (aka GrayCardinal) 2006
 * graycardinalster@gmail.com
 * 
 */ 

#ifndef DEFINE_STRUCTS_H
#define DEFINE_STRUCTS_H
struct d_tag_t
{
	char d_name[256];
	char d_file[256];
	int d_op;
	int d_line;
	int d_type; // Minimal support.	
};

struct pair_t
{
	int pair_op;
	int pair_line;
	int offset;
};

struct tt_state_t 
{
	char * d_file_name;
	int d_fd;
	char * d_file_in;
	int d_filein_size;
	bool b_mmap;
	char * d_output;
	int d_output_size;	
	pair_t * d_attachment;
};

struct fdump_param_t 
{
	int n_trip;
	int d_line;
	bool linear;
	bool b_force_block;
	char * d_file_name; 
	char *d_file_output;
};

struct c_word
{
	char * S;
	char ch;
};

void free_cword (c_word *);

/* 
 * "struct {" -- BB_BEGIN
 * ...
 * } - BB_END
 *  <name>; - BB_TAIL
 */

enum {
	BB_BEGIN = 1,
	BB_END = 2,
	BB_TAIL = 3 
};
#endif
