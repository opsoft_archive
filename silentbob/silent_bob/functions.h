/*
 * (c) Oleg Puchinin (aka GrayCardinal) 2006
 * graycardinalster@gmail.com
 * 
 */ 

#ifndef __FUNCTIONS_H
#define __FUNCTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dlib.h>
#include "structs.h"
#include "mod.h"

/*** ./got_tag.cpp ***/
EArray * got_tag_ctags (char * d_tag) ;

void make_ctags (EArray * d_files);
void mk_tags (char *f_name, DArray *d_in = NULL);
bool local_define_test (char * d_op);
char * cts (struct c_word * d_word);
DArray * split_to_words (char * d_op);	
char * ww_last_word (char *d_op);
int ww_begin_line (struct tt_state_t * d_tt_state, char *d_out, int d_offset);
void mk_tag_macro (char * d_op, char * d_file, int t_line);
void mk_tag (char * d_op, char * d_file, int line, int d_found_type);
void mk_tag_structtail (char * S, char * d_file, int t_line);
void tags (DArray * d_names, char * d_file_output = NULL);
void nogui_tagsdump (char * f_name, int n_trip);
void tree_echo (int n_trip, char *S);
struct tt_state_t * get_onett_tag (char * f_name, char ** d_tt_buf);
struct hash_t * parse_variable (char *OP);
int fdecl_parse (char * d_out, DArray * d_vars);
char * sstrkill (char *OP);
void bug_system ();
	
#define GLOBAL_TYPE_VARIABLE (1<<0)
#define GLOBAL_TYPE_TYPEDEF (1<<1)
#define GLOBAL_TYPE_EXTERN (1<<2)
#define GLOBAL_TYPE_FUNCTION (1<<3)
#define GLOBAL_TYPE_STRUCT (1<<4)

// defenitions
#define OP_TYPE_VARIABLE GLOBAL_TYPE_VARIABLE
#define OP_TYPE_TYPEDEF GLOBAL_TYPE_TYPEDEF
#define OP_TYPE_EXTERN GLOBAL_TYPE_EXTERN
#define OP_TYPE_FUNCTION GLOBAL_TYPE_FUNCTION
#define OP_TYPE_STRUCT GLOBAL_TYPE_STRUCT
#define OP_TYPE_MACRO (1<<5)
// others
#define OP_TYPE_OP (1<<6)
#define OP_TYPE_CALL (1<<7)
#define OP_TYPE_DEFINE (1<<8)
#define OP_TYPE_NOP (1<<9)
#define OP_TYPE_OTHER (1<<31)
#define GLOBAL_TYPE_DEFINE OP_TYPE_DEFINE

#define GLOBAL_TYPE_CLASS (1<<10)
#define OP_TYPE_CLASS GLOBAL_TYPE_CLASS
#define GLOBAL_TYPE_NAMESPACE (1<<11)
#define OP_TYPE_NAMESPACE GLOBAL_TYPE_NAMESPACE

char * sstrend (char * d_ptr);
int words_count (char *S);
int what_is_this (char * d_op, char ch);

/*** ./nogui_fdump.cpp ***/
void nogui_fdump (fdump_param_t * d_param);

/*** ./the_tt.cpp ***/
namespace THE_TT {
	void tt_skip () ;
	void tt_comment () ;
	int the_tt_main (char * f_name) ; 
	char * do_tt_file (tt_state_t * d_tt_state);
	void free_tt_state (struct tt_state_t * S);
}
#define do_tt THE_TT::do_tt_file

void skip_macro (char ** d_ptr, char ** d_out, char ch);
char * macro_name (char * d_op, char * mname);
char t_op (char ** d_in, char ** d_out, bool b_no_operator = false) ;

// Informational functions
void bug_longmacro ();
void bug_nosuch_tag (char *);
void bug_nocalltags ();

inline bool abc_test (char * ptr)
{
	while (*ptr) {
	  if (if_abc (ptr))
		return true;
	  ptr++;
	}  
	return false;
}

// Hmm. It's not globals. It's environments.
/***/
#ifndef I_IN_MAIN
#define __EXTERN extern
#else
#define __EXTERN
#endif

__EXTERN uint32_t sb_flags;

enum {
	SB_FLVERBOSE = 0,
	SB_FLLINEAR,
	SB_FLGVIM,
	SB_FLNOLINKS,
	SB_FLSIMULATE,
	SB_FLNOCSCOPE,
	SB_FLTAGSTYLE,
	SB_FLORIGINAL,
	SB_FLFNAMES,
	SB_FLALL,
	SB_FLPERL,
	SB_FLTEST,
	SB_FLRTREE,
	SB_FLCTAGSAPPEND
	
};

#define SB_FLSET(arg) (sb_flags |= (1<<arg))
#define SB_FLGET(arg) (sb_flags & (1<<arg))

__EXTERN EArray * d_tags_file;
__EXTERN bool d_dbg_SDBG_active;
__EXTERN int t_op_no;
__EXTERN FILE *cscope_in, *cscope_out;
__EXTERN FILE *d_stream_dbg;
__EXTERN pid_t gvim_pid;
__EXTERN EArray * immune_list;
__EXTERN EArray * full_list;
__EXTERN int d_depth;

inline bool tree_immune (char * m_fname)
{
	return immune_list->sfind (m_fname) == -1 ? false : true;
}

// Initial constants.
__EXTERN EArray * d_cops;
/***/

#endif
