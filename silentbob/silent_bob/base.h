/*
 * (c) Oleg Puchinin (aka GrayCardinal) 2006
 * graycardinalster@gmail.com
 * 
 */ 

#ifndef DEFINE_BASE_H
#define DEFINE_BASE_H

int call_tags (EArray *);
int cgrep (EArray * d_files, char * exp);
int sb_init ();
pid_t init_cscope ();
void call_tree (char * f_name, int n_trip = 1);
void globals (char * d_file, int d_type);
void got_structs (char * d_file);
void nogui_indent ();
void reverse_calltree (char * f_name);
void flist_main (char * d_file);
void usage ();

#endif
