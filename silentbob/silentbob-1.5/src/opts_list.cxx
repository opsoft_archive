/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#if 0

#include <optarg.h>

struct option long_options[] = {
	{"help", 0, 0, 'h'},
	{"version", 0, 0, 'v'},
	{"file", 0, 0, 0},
	{"the-tt", 0, 0, 0},
	{"give-structs", 0, 0, 0},
	{"indent", 0, 0, 0},
	{"tags", 0, 0, 0},
	{"make-ctags", 0, 0, 0},
	{"call-tags", 0, 0, 0},
	{"cgrep", 1, 0, 0},
	{"plugins-info", 0, 0, 0},
	{"cfiles", 0, 0, 0},
	{"time-test", 0, 0, 0}

	{"globals", 0, 0, 0},
	{"globals-typedef", 0, 0, 0},
	{"globals-extern", 0, 0, 0},
	{"globals-function", 0, 0, 0},
	{"globals-struct", 0, 0, 0},
	{"globals-variable", 0, 0, 0},
	{"globals-define", 0, 0, 0},

	{"debug", 0, 0, 0},
	{"simulate", 0, 0, 0},
	{"no-links", 0, 0, 0},
	{"all", 0, 0, 'a'},
	{"test", 0, 0, 0},
	{"ctags-append", 0, 0, 0},
	{"tag-style", 0, 0, 0},
	{"depth", 0, 0, 0},
	{"lang", 1, 0, 0}

};

#endif

