/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdarg.h>
#include <head.h>
#include <the_tt.h>

namespace Log {
	
#if 0
int write (char	*logName, const char *logStrFormat, ...) {
	FILE * myfile;
	char buf[1024];
	char timeString[512];
	va_list vaList;

	sprintf (buf, "/home/oleg/bob_%s.log", logName);	
	myfile = fopen (buf, "a");
	if (! myfile)
		return -1;
	
	Dtimestr (timeString, 512);
	fprintf (myfile, "%s - ", timeString);
	va_start (vaList, logStrFormat);
	vfprintf (myfile, logStrFormat, vaList);
	va_end (vaList);
	fclose (myfile);

	return 0;
}

int done (char * logName) {
	write (logName, "OK\n");
}

int globals (char *file, int type, int size) {
	return write ("globals", "file=%s type=%i size=%i\n", file, type, size);
}

int tt (tt_state_t * tt) {
	write ("TT", "file=%s size=%i, mmap=%i\n",
			tt->fileName, tt->fileDataSize, tt->mmaped ? 1 : 0);
}

int init () {
	return write ("init", "home=%s tmp_files=%s tmp_tags=%s\n",
			ENV->home_dir, ENV->tmp_files, ENV->tmp_tags);
}

#else

int write (char	*log_name, const char *logStrFormat, ...) {
	return 0;
}

int done (char * logName) {
	return 0;
}

int globals (char *file, int type, int size) {
	return 0;
}

int tt (tt_state_t * tt) {
	return 0;
}

int init () {
	return 0;
}

#endif

}

