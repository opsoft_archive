/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 * 
 */ 

#include <head.h>
#include <TT.h>

int globalsFile (char * fileName, int type)
{
	TT * tt;
	char * op;
	int m_type;

	tt = new TT;
	tt->loadFile (fileName);
	tt->init ();

	while (true) {
		op = tt->nextOperator ();
		if (tt->ch == 0)
			break;

		if (! SB_FLGET (SB_FLCPP)) {
			if (tt->bracketDepth)
				continue;
		}

		m_type = tt->wit ();
		if (m_type & type)
			globalsPrint (tt->tt, tt->op (), m_type);
	}

	delete tt;
	return 0;
}



