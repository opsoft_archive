/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib_c.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdarg.h>

#define __export

struct stat *cur_stat = NULL;

__export int Dfnwrite (char * p_lpsz_filename, void * p_lp_buffer,int int_size)
{
	int result;
	FILE * myfile;
	myfile = fopen(p_lpsz_filename,"w");
	if(!myfile)
		return 0;
	result = fwrite(p_lp_buffer,1,int_size,myfile);
	fclose(myfile);
	return result;
}

__export int Dfnread (char * f_name, void * p_lp_buffer, int int_size)
{
	int n_bytes = int_size > fsize (f_name) ? fsize (f_name) : int_size;
	int fd;

	fd = open (f_name, O_RDONLY);
	if (fd < 0)
		return fd;
	
	if (read (fd, p_lp_buffer, n_bytes) < 0) 
		n_bytes = -1;
		
	close (fd);
	return n_bytes;
}

__export int Dselect (int FILENO, int SEC, int USEC)
{
	struct timeval m_timeval;
	fd_set m_fdset;

	FD_ZERO (&m_fdset);
	FD_SET (FILENO, &m_fdset);
	m_timeval.tv_sec = SEC;
	m_timeval.tv_usec = USEC;

	if (!m_timeval.tv_sec && !m_timeval.tv_usec) 
		return select (FILENO+1, &m_fdset, NULL, NULL, NULL);// &m_timeval);
	else
		return select (FILENO+1, &m_fdset, NULL, NULL, &m_timeval);
}

__export char * DFILE (const char * m_filename, int *rsize)
{
	char * m_file;
	struct stat m_stat;
	char * ptr;
	int count;
	int len;
	int fd;

	if (m_filename == NULL)
		return NULL;

	fd = open (m_filename, O_RDONLY);
	if (fd < 0)
		return NULL;

	if (lstat (m_filename, &m_stat) < 0)
		return NULL;

	m_file = malloc (m_stat.st_size); 
	if (m_file == NULL)
		return NULL;

	ptr = m_file;
	len = m_stat.st_size;
	while (-1) {
		count = read (fd, ptr, len);
		if (count <= 0)
			break;
		ptr+=count;
		len-=count;
	}	

	if (rsize)
		*rsize = m_stat.st_size;

	close (fd);
	return m_file;
}

__export struct stat * DSTAT (const char * S)
{
	if (!cur_stat)
		cur_stat = malloc (sizeof (struct stat));
	stat (S, cur_stat);
	return cur_stat;
}

__export struct stat * DLSTAT (const char * S)
{
	if (! cur_stat)
		cur_stat = malloc (sizeof (struct stat));
	lstat (S, cur_stat);
	return cur_stat;
}

__export int DIONREAD (int fd)
{
	int ret = -1;

	if (ioctl (fd, FIONREAD, &ret) != 0)
		return -1;

	return ret;
}

__export int fsize (const char * S)
{
	struct stat m_stat;

	if (lstat (S, &m_stat) < 0)
		return -1;

	return m_stat.st_size;
}

__export int fdsize (int fd)
{
	struct stat m_stat;

	if (fstat (fd, &m_stat) < 0)
		return -1;

	return m_stat.st_size;
}

__export char * DFDMAP (int fd)
{
	return (char *) mmap (NULL, fdsize (fd), PROT_READ, MAP_SHARED, fd, 0);
}

__export char * DFMAP (const char *d_file, int *out_fd, int *d_out_size)
{
	char *S = NULL;
	int d_size;
	int fd;

	fd = open (d_file, O_RDONLY);
	if (fd < 0)
		return NULL;

	d_size = fdsize (fd);

	if (d_out_size)
		*d_out_size = d_size;

	if (out_fd)
		*out_fd = fd;

	S = (char *) mmap (NULL, d_size, PROT_READ, MAP_SHARED, fd, 0);

	if ((long) S == -1) {
		close (fd);
		return NULL;
	}

	return S;
}

__export char * Dread_to_eof (int fd, int *d_out_size)
{
	char * d_buf = (char *) malloc (4096);
	int d_size = 4096;
	int d_pos = 0;
	int d_ret = 0;

	if (fd < 0)
		return NULL;

	if (d_out_size)
		*d_out_size = 0;

	while (-1) {
		d_ret = read (fd, &d_buf[d_pos], d_size - d_pos - 1);
		if (d_ret == -1)
			return NULL;

		if (d_ret == 0) //EOF
			break;

		d_pos += d_ret;
		if ((d_size - d_pos) < 4096) {
			d_buf = gc_realloc (d_buf, d_size, d_size << 1);
			d_size<<=1;
			if (d_buf == NULL) {
				if (d_out_size)
					*d_out_size = 0;
				return NULL;
			}
		}
	}

	if (d_out_size)
		*d_out_size = d_pos;

	d_buf[d_pos] = 0;
	return d_buf;
}

__export int move_stream (int fd_in, int fd_out)
{
	char * m_buf = NULL;
	int i;
	int Ret = 0;

	m_buf = malloc (4096);

	while (-1) {
		i = read (fd_in, m_buf, 4096);
		if (i <= 0)
			break;
		Ret += i;
		write (fd_out, m_buf, i);
	}

	free (m_buf);
	return Ret;
}

__export int Dnonblock(int fd)
{
	int flags = fcntl(fd, F_GETFL);
	return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

__export int close_pipe (int *fds)
{
	int Ret1 = 0;
	int Ret2 = 0;

	if (fds[0] != -1) {
		Ret1 = close (fds[0]);
		fds[0] = -1;
	}

	if (fds[1] != -1) {
		Ret2 = close (fds[1]);
		fds[1] = -1;
	}

	return Ret1 ? Ret1 : Ret2;	
}

__export int Dtmpfd (char *name)
{
	char m_buf[128];
	char tmpstr[64];
	int fd;

	Drand_str (tmpstr, 63);
	sprintf (m_buf, "/tmp/%s", tmpstr);
	fd = open (m_buf, O_CREAT | O_RDWR);
	if (name) {
		if (fd >= 0) 
			strcpy (name, m_buf);
		else 			
			name[0] = '\0';
	}
	
	return fd;
}

__export int fdclose (int * fd)
{
	if (! fd)
		return 0;
	
	if (*fd != -1) {
		close (*fd);
		*fd = -1;
	}
	return 0;
}

__export char * fext (char *name)
{
	if (! name)
		return NULL;
	return rindex (name, '.');
}

__export int logToFile (char * fileName, char * fmt, ...)
{
	va_list alist;
	FILE * myfile;
	myfile = fopen (fileName, "a");
	if (! myfile)
		return -1;

	va_start (alist, fmt);
	vfprintf (myfile, fmt, alist);
	va_end (alist);
	fclose (myfile);
	return 0;
}

__export int copyFile (char * sourceName, char * destName)
{
	int sourceFD = -1;;
	int destFD = -1;
	struct stat st;
	char * copyBuf = NULL;
	int ret;
	int count = 0;

	if (! sourceName || ! destName)
		return -1;

	sourceFD = open (sourceName, O_RDONLY);
	fstat (sourceFD, &st);
	destFD = open (destName, O_WRONLY | O_CREAT, st.st_mode);
	if ((sourceFD < 0) || (destFD < 0)) {
		count = -1;
		goto copyFile_out;
	}

	copyBuf = CNEW (char, 4096);
	while (-1) {
		ret = read (sourceFD, copyBuf, 4096);
		if (ret <= 0)
			break;
		if (write (destFD, copyBuf, ret) < 0)
			break;
		count += ret;
	}

copyFile_out:

	DROP (copyBuf);
	fdclose (&sourceFD);
	fdclose (&destFD);
	return count;
}

