/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib_c.h>
#include <netdb.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <fcntl.h>

#define __export
__export int Dunix_sendto(void * lp_data,int int_size,char * lpsz_path)
{
	int sock;
	int Res;
	struct sockaddr_un name;

	if (! lp_data)
		return -1;
	if (! lpsz_path)
		return -1;

	memset (&name, 0, sizeof (struct sockaddr_un));
	sock = socket(AF_LOCAL,SOCK_DGRAM, 0);
	name.sun_family = AF_LOCAL;
	strncpy (name.sun_path, lpsz_path, 108);
	Res = sendto (sock, lp_data, int_size, 0, (struct sockaddr *) &name, sizeof (struct sockaddr_un));
	close (sock);
	return Res;
}

__export int Dsendto (int sock, void * lp_data, int int_size, 
		const char * address,int port)
{
	int Res;
	struct sockaddr_in inet_address;

	memset (&inet_address, 0, sizeof (struct sockaddr_in));
	inet_address.sin_family = AF_INET;
	inet_address.sin_port = htons(port);
	inet_aton (address, &inet_address.sin_addr);

	Res = sendto(sock,lp_data,int_size,0,
			(struct sockaddr *) &inet_address, sizeof (struct sockaddr_in));
	return Res;
}

__export int Dsendto2 (void * lp_data, int int_size, 
		const char * address, int port)
{
	int sock;
	int Res;
	struct sockaddr_in inet_address;

	if (! lp_data || ! address)
		return -1;
	
	memset (&inet_address, 0, sizeof (struct sockaddr_in));
	inet_address.sin_family = AF_INET;
	inet_address.sin_port = htons(port);
	inet_aton (address, &inet_address.sin_addr);
	sock = socket(AF_INET,SOCK_DGRAM,0);
	Res = sendto(sock,lp_data,int_size,0,
			(struct sockaddr *) &inet_address,sizeof (struct sockaddr_in));
	close (sock);
	return Res;
}

__export int Dbind (int sock, char * addr, int port)
{
	struct sockaddr_in m_addr;
	
	memset (&m_addr, 0, sizeof (struct sockaddr_in));
	inet_aton (addr, &m_addr.sin_addr);
	m_addr.sin_family = AF_INET;
	m_addr.sin_port = htons (port);
	
	return bind (sock, (struct sockaddr *) &m_addr, sizeof (struct sockaddr_in));
}

__export int Dsocket ()
{
	return socket (AF_INET, SOCK_STREAM, 0);
}

__export int Dsocket_udp ()
{
	return socket (AF_INET, SOCK_DGRAM, 0);
}

__export int Dconnect (int sock, const char * addr, int port)
{
	struct sockaddr_in m_addr;
	
	memset (&m_addr, 0, sizeof (struct sockaddr_in));
	inet_aton (addr, &m_addr.sin_addr);
	m_addr.sin_family = AF_INET;
	m_addr.sin_port = htons (port);
	
	return connect (sock, (struct sockaddr *) &m_addr, sizeof (struct sockaddr_in));
}

__export int Dgethostbyname(const char * lpsz_hostname,struct in_addr * address)
{
	struct hostent * hostent_hostent;
	if((lpsz_hostname == 0) | (address == 0)) return -1;
	
	hostent_hostent = gethostbyname(lpsz_hostname);
	if(! hostent_hostent) 
		return -1; 
	
	*address = *((struct in_addr *) &hostent_hostent->h_addr_list[0][0]);
	endhostent();
	return 0;
}

__export char * Dgetnamebyhost(struct in_addr * params)
{
	struct sockaddr_in address;
	struct hostent * hostent_hostent;

	if(! params) 
		return 0;
	address.sin_addr = *params;
	hostent_hostent = gethostbyaddr((const char *) &address.sin_addr, 4,PF_INET);
	if(! hostent_hostent) 
		return 0;
	return hostent_hostent->h_name;
}

/* 2006 year */
__export int Drecvfrom (int fd, char * buf, int size, char * peer_ip, int * peer_port)
{
	struct sockaddr_in addr;
	socklen_t al;
	char * S;
	int len;
	
	if (buf == NULL)
		return -1;
	
	memset (&addr, 0, sizeof (struct sockaddr_in));
	al = sizeof (struct sockaddr_in);
	len = recvfrom (fd, buf, size, 0, (struct sockaddr *) &addr, &al);
	if (len < 0)
		return len;
	
	if (peer_ip) {
		peer_ip[0] = '\0';
		S = inet_ntoa (addr.sin_addr);
		if (! S) 
			return len;
		strcpy (peer_ip, S);
	}		
	
	if (peer_port)
		*peer_port = ntohs (addr.sin_port);
			
	return len;
}

__export int Dbroadcast (int fd) 
{
	int one = 1;
	return setsockopt (fd, SOL_SOCKET, SO_BROADCAST, &one, 4);
}

__export int Dpoll_scan (struct pollfd **p, int count, int pos)
{
	if (! p)
		return -1;

	if (pos >= count)
		return 0;

	for (; pos < count; ++pos) {
		if ((*p)[pos].revents)
			return pos;	
	}			

	return 0;
}

__export int Dtransmit (char *f_name, int sd)
{
        int pid;
        int len;
        int fd;
        char *m_buf;
        
        pid = fork ();
        if (pid < 0)
                return pid;

        if (pid == 0) {
                m_buf = malloc (4096);
                fd = open (f_name, O_RDONLY);
                if (! fd)
                        exit (1);

                while (-1) {
                        len = read (fd, m_buf, 4096);
                        if (len == 0)
                                exit (0);

                        if (len < 0)
                                exit (1);
                        
                        if (send (sd, m_buf, len, 0) != len)
                                exit (1);
                }
                exit (1);
        }
         
        return pid;
}

__export int Dtransmit_udp (char *f_name, int sd, char *ip, uint16_t port)
{
        struct sockaddr_in s_addr;
        int pid;
        int len;
        int fd;
        char *m_buf;
        
        pid = fork ();
        if (pid < 0)
                return pid;

        if (pid == 0) {
                memset (&s_addr, 0, sizeof (struct sockaddr_in));
                s_addr.sin_family = AF_INET;
                s_addr.sin_port = htons (port);
                inet_aton (ip, &s_addr.sin_addr);
                m_buf = malloc (1440);
                fd = open (f_name, O_RDONLY);
                if (! fd)
                        exit (1);
                
                while (-1) {
                        len = read (fd, m_buf, 1440);
                        if (len == 0)
                                exit (0);

                        if (len < 0)
                                exit (1);
                        
                        if (sendto (sd, m_buf, len, 0, (struct sockaddr *) &s_addr,
                                                sizeof (struct sockaddr_in)) != len)
                                exit (1);
                }
                exit (1);
        }
         
        return pid;        
}

