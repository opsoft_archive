/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib_c.h>
#include <sys/wait.h>
#define __export

__export char * Dprogram_read (char *EXEC, int * ALEN)
{
        int pid;
        int status;
        int fdo[2];
        char * ret;
        int len;

        pipe (fdo);
        pid = Dfork (EXEC, NULL, fdo, NULL);
        if (pid <= 0)
                return NULL;

        ret = Dread_to_eof (fdo[0], &len);
        if (ALEN)
                *ALEN = len;

        waitpid (pid, &status, 0);
        return ret;
}


