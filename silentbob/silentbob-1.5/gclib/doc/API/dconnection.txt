/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

UTF8

        DConnection - класс "соединиение".
Класс, управляющий каким - либо соединением. Текущий функционал -
TCP/IP, UDP/IP сокеты, и файлы.

        ipv4_init () / ipv4_init_udp ();
Инициализировать TCP / UDP сокет.

        ipv4_bind (char *ip, uint16_t port) / ipv4_connect (char *ip, uint16_t port)
bind/connect соединения. Порт - НЕ сетевой формат.
                
        int ipv4_udp_connect (char *ip, uint16_t port);
Установить адрес которому будут посылаться UDP'шки.

        ipv4_send (char * buf, int len) / ipv4_recv (char * buf, int len)
Послать / получить данные.

        update_ctime (time_t d_time) / update_mtime (time_t d_time)
Обновить время создания / модификации метаданных соединения. Если
параметр не указан, берется текущее время.

        DIONREAD ()
Вернуть количество байт, доступных для чтения.

        open_ro / open_rw
Открыть файл на чтение / чтение+запись.

        open
Открыть файл (аргументы аналогичны open(2))

        read / write / close
Аналогичны системным, параметр дескриптора опускается.

        poll_config (int flags)
Установить флаги для poll(2) соединения.

        getsockname / getpeername / getpeerport / getsockport 
То же, что и системные. IP - строка ("xxx.xxx.xxx.xxx"), порт - НЕ сетевой формат.

        get_ctime / get_mtime () 
ВОзвращает время создания / модификации соединения.

	Доступные переменные :
c_sd - сокет соединения. Равен -1 если не инициализирован.
c_ctime - время когда было произведено соединение. Автоматически обновляется в ipv4_connect м ipv4_udp_connect
c_cname / c_cport - адрес / порт клиента.
c_pname / c_pport - адрес / порт удаленного конца.

