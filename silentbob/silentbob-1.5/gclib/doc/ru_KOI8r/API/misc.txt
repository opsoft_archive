/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

KOI8-R

	void Dfastsort_s(char ** a, long N);
������� ���������� �����. N - ����� ���������� ��������.

	Dtimer () / timeval *the_time () / print_the_time (FILE * file_my)
���������������� ������ / ������� ��������� ����� � ������� ������������� / ���������� ��������� � file_my 
(��� � stdout).

	int Dterm_one_kick (int fd);
����������� �������� � ������������ �����.

	int Dfork (char *exec, int *fd_in, int *fd_out, int *fd_err);
��������� ��������� ��������. exec - ������� ��� ���������� � sh ("sh -c <exec>"). ��������� ��������� -
����������� �����/������/������ ������ ��������. ��������������, ���� ����� �����. ��������� :
� *fd_in ����� ������ (�������� stdin �������), � *fd_out � *fd_err ������ (stdout, stderr).
������ ��� popen. ��������� (fd*) ��� ��� ������� �� ���� ������������.

