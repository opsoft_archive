/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

	Доступ к файловой системе.

Пример номер Ф-001. Базовые операции.
<code>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

int fd;
int fd2;
int f_size;
char m_buf[4096];

fd = open ("/dev/zero", O_RDWR);		// открытие файла на чтение
fdsize (fd);					// получение размера файла
fsize ("/dev/zero");				// получение размера файла
DIONREAD (fd);					// количество доступных байт
read (fd, m_buf, 100);				// чтение ста байт
lseek (fd, SEEK_CUR, -100);			// вернуться на сто байт назад
write (fd, m_buf, 100);				// запись ста байт
DFMAP (fd);					// mmap'нуть весь файл.
DFMAP ("/dev/mem", &fd2, &f_size);		// mmap'нуть весь файл, получить дескриптор и размер
close (fd);					// закрытие файла
close (fd2);					// закрытие файла
</code>

Пример номер Ф-002. Базовые операции на базе libc.
<code>
FILE * d_file;
char m_buf[4096];
int i;

d_file = fopen ("/dev/zero", "rw");		// открытие файла
fileno (d_file);				// получить дескриптор файла
fread (m_buf, 100, 1, d_file);			// чтение ста байт
fwrite (m_buf, 100, 1, d_file);			// запись ста байт
fgets (m_buf, 4095, d_file);			// прочитать строку
fputc ('C', d_file);				// записать символ 'C' в файл
fscanf (d_file, "%i", &i);			// прочитать целочисленное чило
fclose (d_file);				// закрыть файл
</code>

Пример номер Ф-003. Операции с директориями.
<code>
#include <dirent.h>
DIR * d_dir;
struct dirent * d_dirent;
d_dir = opendir ("/");				// открыть директорию
d_dirent = readdir (d_dir);			// прочитать одну запись
printf ("%s\n", d_dirent->d_name);		// вывести имя файла
closedir (d_dir);				// закрыть директорию
</code>

Пример номер Ф-004. Базовые операции на базе Qt3
<code>
QStringList lines;				// список строк
QFile file( "file.txt" );			// объект - файл
file.open( IO_ReadOnly );			// открыть на чтение
file.size ();					// получить размер
file.readBlock (buf, 100);			// чтение ста байт
QTextStream stream( &file );			// объект - текстовые данные
QString line;					// строка
line = stream.readLine();			// прочитать строку 
lines += line;					// добавить строку к списку
file.close();					// закрыть объект - файл
</code>

