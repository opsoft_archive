#!/bin/bash

export "prefix=../package"
rm -fr $prefix/*
mkdir -p $prefix/usr/lib
mkdir -p $prefix/usr/include/gclib/
cp ./*.so $prefix/usr/lib
cp ../include/* $prefix/usr/include/gclib
cp ../gclib_c/gclib_c.h $prefix/usr/include/gclib

