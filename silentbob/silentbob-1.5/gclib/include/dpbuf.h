/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DPBUF_H
#define DEFINE_DPBUF_H

class DPBuf {
	public:
		DPBuf ();
		DPBuf (char *, int len);
		~DPBuf ();

		unsigned char r8 (char *ptr = NULL);
		uint16_t r16 (char *ptr = NULL);
		uint32_t r32 (char *ptr = NULL);
		char * rd (char *ptr, int len);
		char * set_pos (char * ptr);
		void init (char *ptr, int len);
		void s8 (void);
		void s16 (void);
		void s32 (void);
		void sd (int count);
		bool check (char *ptr, int count);
		char * strcat (char *ptr, char *S);
                char * memmem (char * buf, char * needle, size_t needlelen);
                char * ch (char *buf, char ch);
                
		bool ok;
		char * begin;

	private:
		char * carret;
		char * end;
};

#endif

