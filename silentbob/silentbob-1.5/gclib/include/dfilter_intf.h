/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DFILTERINTF_H
#define DEFINE_DFILTERINTF_H

class DFilterIntf
{
        public:
                int (*read) (char * buf, int len);
                int (*write) (char * buf, int len);
                int (*seek) (int pos, int from);
                int (*transmit) (int count);
                int (*init) (char *user, int user_len);
                
                DFilterIntf () {
                        in_fd = -1;
                        out_fd = -1;
                        user_info = NULL;
                        user_len = 0;
                }
                
                ~DFilterIntf () {
                        return;
                }
                
                int in_fd;
                int out_fd;
                char * user_info;
                int user_len;
};

#endif

