/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef CACHE_H
#define CACHE_H
#include <strings.h>
#include <string.h>
#include <best_names.h>

template <typename T>
class CacheMem {
	public:
	
		CacheMem (int initSize = 32);
		inline ~CacheMem () {
			delete [] m_heap;
			delete [] bitmask;
		}
	
		inline bool contains (T* ptr) {
			if (ptr < m_heap)
				return false;
			if (ptr > (m_heap + m_size * sizeof (T)))
				return false;
		}

		T* alloc ();
		void free (T*);

	private:
		bool resize (int newSize);
		T* m_heap;
		int m_size;
		int * bitmask;
		int last_free;
};

template <typename T>
CacheMem<T>::CacheMem (int initSize) {
	int count;
	int last;
	int i;
	
	m_heap = new T[initSize];
	m_size = initSize;
	count = m_size >> 6;
	last = m_size % 32;
	if (last)
		++count;
	bitmask = new int [count];
	for (i = 0; i < count; ++i) 
		bitmask[i] = -1;
	
	if (last) {
		last = ~(((-1) >> last) << last);
		bitmask[count-1] = last;
	}

	last_free = 0;
}

template <typename T> 
T* CacheMem<T>::alloc () {
	int count;
	int i;
	int k;
	T* Ret;

	if (last_free > 0) {
		k = ffs (bitmask[last_free]);
		if (k)
			goto __cache_alloc_done;
	}

	count = m_size >> 6;
	if (m_size % 32)
		++count;
	for (i = 0; i < count; ++i) {
		k = ffs (bitmask[i]);
		if (k) {
			last_free = i;
			goto __cache_alloc_done;
		}
	}

	last_free = -1;
	return Ret;

__cache_alloc_done:
	--k;
	bitmask[i] |= (1<<k);
	return &m_heap[i + k];
}

template <typename T>
void CacheMem<T>::free (T* ptr)
{
	int offset;
	int k;
	if (! contains (ptr))
		return;
	offset = ((ptr - m_heap) / sizeof (T));
	k = offset % 32;
	offset = offset >> 6;
	bitmask[offset] &= ~k;
}

template <typename T>
class Cache {
	public:
		Cache (int newChunkSize = 32);
		~Cache ();		
		
		T* alloc ();
		template <typename C> 
		void free (C *);
	private:
		Array * chunks; 
		int chunkSize;

};

template <typename T>
Cache<T>::Cache (int newChunkSize) {
	CacheMem<T> * cacheMem;
	chunks = new Array(16);
	cacheMem = new CacheMem<T>(newChunkSize);
	chunks->add (LPCHAR (cacheMem));
}

template <typename T>
Cache<T>::~Cache () {
	CacheMem<T> * cacheMem;
	int count;
	int i;

	count = chunks->get_size ();
	for (i = 0; i < count; ++i) {
		cacheMem = (CacheMem<T> *) (*chunks)[i];
		delete cacheMem;
	}
	delete chunks;
}

template <typename T>
T* Cache<T>::alloc () {
	T* ptr = NULL;
	CacheMem<T> * cacheMem;
	int count;
	int i;

	count = chunks->get_size ();
	for (i = 0; i < count; ++i) {
		cacheMem = (CacheMem<T> *) (*chunks)[i];
		ptr = cacheMem->alloc ();
		if (ptr)
			return ptr;
	}
	if (! ptr) {
		cacheMem = new CacheMem<T>(chunkSize);
		return cacheMem->alloc ();
	}

	return ptr;
}

template <typename T> template <typename C>
void Cache<T>::free (C * ptr) {
	CacheMem<T> * cacheMem;
	int count;
	int i;

	for (i = 0; i < count; ++i) {
		cacheMem = (CacheMem<T> *) (*chunks)[i];
		if (cacheMem->contains ((T *) ptr)) {
			cacheMem->free ((T *) ptr);
			break;
		}
	}
}

#endif

