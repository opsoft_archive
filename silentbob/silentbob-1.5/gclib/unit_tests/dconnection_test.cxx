/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <gclib/gclib_c.h>
#include <gclib/gclib.h>


void usage () 
{
        printf ("usage: dconnection_test <IP> <port>\n");
}

int main (int argc, char **argv)
{
        uint16_t port;
        DConnection *c;

        if (argc != 3) {
                usage ();
                return 1;
        }
        
        port = atoi (argv[2]);
        c = new DConnection;
        c->ipv4_init ();
        if (c->ipv4_connect (argv[1], port) != 0) {
                perror ("ipv4_connect");
                return 1;
        }

        delete c;
        return EXIT_SUCCESS;
}

