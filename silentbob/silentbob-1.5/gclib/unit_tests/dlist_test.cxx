/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 * < ? program elist_test ?>
 */

#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <time.h>

int main (int argc, char ** argv)
{
        DList * list;
        __dlist_entry_t * one, *ptr;
        int i;
        char buf[255];
        char *S;

        list = new DList;
        srand (time (NULL));
        while (true) {
                Drand_str (buf, 254);
                for (i = 0; i < 1000; ++i) 
                        list->add_tail (buf);

                one = list->get_head ();
                while (one) {
                        ptr = one->next;
                        list->rm (one);
                        one = ptr;
                }
        }               
        return EXIT_SUCCESS;
}

