/*
 * (c) Oleg Puchinin 2005,2006
 * graycardinalster@gmail.com
 *
 * < ? program bench_test ?>
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <fcntl.h>
#include <sys/mman.h>

int main (int argc, char ** argv)
{
	int fd;
	char ch;
	int i;
	struct stat d_stat;
	char * d_file;
	char * d_ptr;
	char *d_ptrs[1000];
	char buf[4096];
	int d_fd_out;
	
	if (argc == 1) 
		d_file = "/usr/src/linux/include/linux/ip.h";
	else
		d_file = argv[1];

	printf ("syscall test for file : %s\n", d_file);
	
/* Simple */
	printf ("\n");
	printf ("Simple :\n");
	Dtimer ();
	print_the_time (stdout);

/* fork */
	printf ("\nfork:\n");
	Dtimer ();
	fflush (stdout);
	i = fork ();
	if (i < 0) 
		perror ("fork");
	else if (i == 0)
		return 0;
	print_the_time (stdout);

/* Dfork */
	printf ("\nDfork: \n");
	Dtimer ();
	Dfork ("cat ./dlib.cpp |cat|head -n 1", NULL, &d_fd_out, NULL);
	print_the_time (stdout);
	
/* open */
	printf ("\nopen:\n");
	Dtimer ();
	fd = open (d_file, O_RDONLY);
	print_the_time (stdout);

/* lstat */
	printf ("\nlstat:\n");
	Dtimer ();
	lstat (d_file, &d_stat);
	print_the_time (stdout);
	
/* fstat */
	printf ("\nfstat\n");
	Dtimer ();
	fstat (fd, &d_stat);
	print_the_time (stdout);
	
/* read */
	printf ("\nread (4096b)\n");
	Dtimer ();
	read (fd, buf, 4096);
	print_the_time (stdout);

/* mmap */
	printf ("\nmmap\n");
	Dtimer ();
	d_ptr = (char *) mmap (NULL, 4096, PROT_READ, MAP_SHARED, fd, 0);
	ch = *d_ptr;
	print_the_time (stdout);
	munmap (d_ptr, 4096);

/* mmap (ANONYMOUS) */
	printf ("\nmmap (ANONYMOUS) (4096b)\n");
	Dtimer ();
	mmap (NULL, 4096, PROT_READ|PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	print_the_time (stdout);
	
/* mmap (ANONYMOUS) */
	printf ("\n[1000] mmap (ANONYMOUS) (4096b)\n");
	Dtimer ();
	for (i = 0; i < 1000; i++)
		d_ptrs[i] = (char *) mmap (NULL, 4096, PROT_READ|PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	print_the_time (stdout);

/* munmap */	
	printf ("\n[1000] unmmap (ANONYMOUS) (4096b)\n");
	Dtimer ();
	for (i = 0; i < 1000; i++)
		munmap (d_ptrs[i], 4096);
	print_the_time (stdout);
	
/* mremap */
	printf ("\n[1000] mremap (ANONYMOUS) (4096b->8192b)\n");
		
	d_ptr = (char *) mmap (NULL, 4096, PROT_READ|PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	Dtimer ();
	d_ptr = (char *) mremap (d_ptr, 4096, 8192, 0);
	print_the_time (stdout);
	
/* close */
	printf ("\nclose:\n");
	Dtimer ();
	close (fd);
	print_the_time (stdout);

/* malloc (not syscall) (4096)*/
	printf ("\n[1000] malloc (not syscall) (4096b)\n");
	Dtimer ();
	for (i = 0; i < 1000; i++)
		d_ptrs[i] = (char *) malloc (4096);
	print_the_time (stdout);

/* free (not syscall) (4096)*/
	printf ("\n[1000] free (not syscall) (4096b)\n");
	Dtimer ();
	for (i = 0; i < 1000; i++)
		free (d_ptrs[i]);
	print_the_time (stdout);
	
/* malloc (not syscall) (256) */
	printf ("\n[1000] malloc (not syscall) (256b)\n");
	Dtimer ();
	for (i = 0; i < 1000; i++)
		d_ptrs[i] = (char *) malloc (128);
	print_the_time (stdout);

//	write (fileno (stdout), ptr, size);
	return 0;	
}
