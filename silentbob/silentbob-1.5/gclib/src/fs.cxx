/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib.h>
#include <gclib_c.h>
#include <sys/syscall.h>

/* 2005 */
__export int Dreaddir_r (char * NAME, dirent ** OUTDIR)
{
	int i;
	int count = 128;
	DIR * m_dir;
	dirent * m_dirent = NULL;
	dirent * m_alldirent = NULL;
	int m_Res = 0;

	m_dir = opendir (NAME);
	if (OUTDIR == NULL)
		return 0;

	if (m_dir == NULL) {
		*OUTDIR = NULL;
		return 0;
	}

	m_alldirent = CNEW(dirent, count);

	i = 0;
	while (true) {
		for (; i < count; i++) {
			m_dirent = readdir (m_dir);
			if (m_dirent == NULL)
				break;
			m_Res++;
			memcpy (&m_alldirent[i], m_dirent, sizeof (struct dirent));
		}

		if (m_dirent == NULL)
			break;

		if (i == count) {
			m_alldirent = (dirent *) gc_realloc ((char *)m_alldirent,
					count * sizeof (struct dirent),
					(count << 1) * sizeof (struct dirent));
			count <<= 1;
		}
	}

	closedir (m_dir);
	if (OUTDIR)
		*OUTDIR = m_alldirent;

	DSRET (m_alldirent)
	return m_Res;
}

__export DArray * Dlstat_r (DArray * m_names)
{
	DArray * m_array = new DArray;
	struct stat * m_stat;
	int i;

	if (! m_names)
		return NULL;

	for (i = 0; i < m_names->get_size (); i++) {
		m_stat = new struct stat;
		lstat (m_names->get (i), m_stat);
		m_array->add (LPCHAR (m_stat));
	}

	DSRET (m_array);
	return m_array;
}

__export DArray * Dstat_r (char *__from, dirent * m_dirents, int count)
{
	DArray * m_array = new DArray;
	struct stat * m_stat;
	int i;
	char m_name[4096];

	if ((! m_dirents) || (! __from))
		return NULL;

	for (i = 0; i < count; i++) {
		m_stat = new struct stat;
		snprintf (m_name, 4096, "%s/%s", __from, m_dirents[i].d_name);
		stat (m_name, m_stat);
		m_array->add (LPCHAR (m_stat));
	}

	DSRET (m_array);
	return m_array;
}

/* 24/06/06 */
__export DList * Dreaddir_r (char * path)
{
	DIR * m_dir;
	DList * Ret;
	struct dirent * m_dirent;

	if (! path) 
		return NULL;
	
	m_dir = opendir (path);
	if (! m_dir)
		return NULL;
	
	Ret = new DList;
	while (true) {
		m_dirent = readdir (m_dir);
		if (! m_dirent)
			break;
		m_dirent = (struct dirent *) memdup (m_dirent, sizeof (struct dirent));
		Ret->add_tail (LPCHAR (m_dirent));
	}

	closedir (m_dir);
	return Ret;
}

// 24/06/06 
__export DArray * Dfiles (char * path)
{
	DIR * m_dir;
	DArray * Ret;
	struct dirent * m_dirent;

	if (! path) 
		return NULL;
	
	m_dir = opendir (path);
	if (! m_dir)
		return NULL;
	
	Ret = new DArray;
	while (true) {
		m_dirent = readdir (m_dir);
		if (! m_dirent)
			break;
		m_dirent = (struct dirent *) memdup (m_dirent, sizeof (struct dirent));
		Ret->add (LPCHAR (strdup (m_dirent->d_name)));
		DROP (m_dirent);
	}

	closedir (m_dir);
	return Ret;
}

#ifdef __NR_fadvise64
int Dposix_fadvise (int fd, int offset, int len, int advice)
{
	return posix_fadvise (fd, offset, len, advice);
//	return syscall (__NR_fadvise64, fd, 0, offset, len, advice);
}
#else
int Dposix_fadvise (int fd, int offset, int len, int advice)
{
#warning "Dposix_fadvise is broken.\n"
	return 0; 
} 
#endif

