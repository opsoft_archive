/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib_c.h>
#include <gclib.h>
#include <hv.h>
#include <best_names.h>

void __hv_free_item (char * ptr)
{
	ptr -= sizeof (char *);
	free (ptr);
}

HV::HV ()
{
	elements = new Array(4);
	dirty_elements = new EArray(1024);
	b_ac = false;
	dfa = 0;	
}

HV::~HV ()
{
	elements->foreach ((Dfunc_t) __hv_free_item);
	dirty_elements->foreach ((Dfunc_t) __hv_free_item);
	delete elements;
	delete dirty_elements;
}

char * HV::pack_item (char * key, char * value)
{
	char * pack;
	if (! key || ! value)
		return NULL;
	pack = CNEW (char, strlen (key) + 1 + sizeof (char *));
	*((char **) pack) = value;
	pack += sizeof (char *);
	strcpy (pack, key);
	return pack;
}

dkey_t * HV::unpackItem (char * pack, dkey_t * item)
{
	if (! pack || ! item)
		return NULL;
	item->KEY = pack;
	pack -= sizeof (char *);
	pack = *((char **) pack);
	item->VALUE = pack;
	return item;
}

char * HV::set_item_value (char * pack, char * value)
{
	pack -= sizeof (char *);
	*((char **) pack) = value;
	return value;
}

char * HV::get_item (char * key)
{
	char * ptr;
	int count;
	int i;

	if (! key)
		return NULL;

	i = elements->snfind_fast (key, 0);
	if (i >= 0) 
		return elements->get (i);

	count = dirty_elements->get_size ();
	for (i = 0; i < count; ++i) {
		ptr = dirty_elements->get (i);
		if (EQ (ptr, key))
			return ptr;
	}

	return NULL;
}

char * HV::get (char * key)
{
	dkey_t one;
	if (unpackItem (get_item (key), &one)) 
		return one.VALUE;
	return NULL;
}

char * HV::set (char * key, char * value)
{
	char * pack;

	if (b_ac) {
		pack = get_item (key);
		if (pack) 
			set_item_value (pack, value);
		return value;
	}

	dirty_elements->add (pack_item (key, value));
	if (dfa && (dirty_elements->get_size () >= dfa))
		do_sort (false);

	return value;
}

void HV::do_sort (bool b_clean)
{
	DHeapSort * heap;
	dkey_t item;
	char * S;
	int count;
	int dirty_count;
	int i;

	count = elements->get_size ();
	dirty_count = dirty_elements->get_size ();
	heap = new DHeapSort (count + dirty_count);
	for (i = 0; i < count; ++i) {
		if (b_clean) {
			unpackItem ((*elements)[i], &item);
			if (! item.VALUE)
				continue;
		}
		heap->add ((*elements)[i]);
	}

	for (i = 0; i < dirty_count; ++i) {
		S = dirty_elements->get (i);
		if (b_clean) {
			unpackItem (S, &item);
			if (item.VALUE)
				heap->add (S);
			else 
				delete (S - sizeof (char *));
		} else 
			heap->add (S);
	}

	delete elements;
	elements = new EArray (count + dirty_count);
	i = 0;
	while ((S = heap->extract_min ()) && S) { 
		++i;
		elements->add (S);
	}

	delete dirty_elements;
	dirty_elements = new EArray(1024);
	delete heap;
}

