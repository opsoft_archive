/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 * 
 */

#ifndef DEFINE_MMP_H
#define DEFINE_MMP_H

#ifndef DEFINE_MRIM_HDR
struct mrim_hdr;
#endif

class MMP
{
	public:
		MMP ();
		~MMP ();

		int init ();
		int login (char * login, char * pass);
		int logout ();
		int sendMessage (char * to, char * message, bool bulticast = false);
		int ping ();
		int pingInterval ();
		int socket ();
		int timeout ();
		int setTimeOut (int t);

	private:
		mrim_hdr * mk_pkt (u_long msg, char * attach = NULL, uint8_t len = 0);
		char * rcv (int * len);
		int real_connect (char *addr, int len);
		void LPS (char ** pkt, char *str);
		void UL (char ** pkt, int N);
		int hello ();
	
		int fd;
		char ** cmds_out;
		struct sockaddr_in caddr;
		struct sockaddr_in saddr;
		int last_seq;
		int m_ping;
		int m_timeout;

};


#endif

