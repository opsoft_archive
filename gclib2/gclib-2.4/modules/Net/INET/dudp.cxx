/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 * 
 */

#include <gclib2.h>

DUdp::DUdp ()
{
	ip = NULL;
	port = 0;
}

DUdp::~DUdp ()
{
	delete data ();
	DROP (ip);
}

int DUdp::init (char * data, int len, char * IP, uint16_t PORT)
{
	if (! IP || ! data)
		return -1;
	DPBuf::init (data, len);
	ip = strdup (IP);
	port = PORT;
	return 0;
}

