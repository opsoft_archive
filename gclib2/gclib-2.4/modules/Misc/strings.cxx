/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <gclib2.h>
#define __export

/// Увеличить размер блока (с копированием).
__export char * gc_realloc (char * PTR, int old_size, int new_size)
{
	int i;
	char * S = (char *) malloc (new_size);
	if (S == NULL)
		return NULL;

	i = (new_size >= old_size) ? old_size : new_size;
	memcpy (S, PTR, i);
	free (PTR);
	return S;
}

/// Скопировать блок памяти.
__export void * memdup (void * PTR, int size)
{
	char * Ret;
	Ret = (char *) malloc (size);
	memcpy (Ret, PTR, size);
	return (void *) Ret;
}

/* 2005 */
/// Разбить текст строкой (аналог "split" perl'а).
__export int Dsplit(char * lpsz_String, char *ch, char ** outbuffer, int int_buffersize)
{
	char * S;
	int i;
	
	i = 1;
	--int_buffersize;
	
	if(!lpsz_String)
		return 0;

	if(int_buffersize > 0) {
		S = lpsz_String;
		outbuffer[0] = S;
	} else 
		return 0;

	S = strstr(S,ch);
	while(S && (int_buffersize--)) {
		S[0] = 0;
		++S;
		outbuffer[i] = S;
		S = strstr(S,ch);
		++i;
	}
	return i;
}

/// lpsz_string =~ m/param1(.*)param2/
__export char * Dstrmid(char * lpsz_string,char * param1, char * param2)
{
	char * Result;
	char *S;
	char *S2;
	int int_strsize;

	if(! strlen (param1))
		return 0;

	S = strstr (lpsz_string,param1);
	if(! S)
		return 0; 
	S += strlen (param1);
	S2 = strstr (S,param2); 
	if(! S2)
		return 0;

	int_strsize = S2 - S;
	if(! int_strsize)
		return 0;
	
	Result = (char *) malloc (int_strsize + 1);
	memcpy (Result, S, int_strsize);
	Result [int_strsize] = 0;
	return Result;
}

/// Убить конец строки.
__export char * chomp (char * S)
{
	char * str;
	if (S == NULL)
		return NULL;
	str = strchrs (S, 13, 10, 0, 0);
	if (str)
		*str = 0;
	return S;
}

/// Найти символ (с конца строки). d_len - исходной строки (если известна).
__export char * strchr_r (char * S, char ch, int d_len)
{
	if (! d_len)
		d_len = strlen (S);

	S += d_len - 1;
	while (d_len > 0) {
		if (*S == ch)
			break;
		--S;
		--d_len;
	}

	return S;
}

/// Найти один из символов.
__export char * strchrs (char *S, char ch, char ch2, char ch3, char ch4)
{
	while (*S) {
		if (*S == ch)
			break;

		if (*S == ch2)
			break;

		if (*S == ch3)
			break;

		if (*S == ch4)
			break;

		++S;
	}

	if (*S == ch || *S == ch2 || *S == ch3 || *S == ch4)
		return S;

	return NULL;
}

/* 2006-05-25 */
/// Найти строку (с конца исходной).
__export char * Dstrstr_r (char *where, char * str) 
{
	char * S;
	int len;	

	if (! where || ! str || strlen (where) == 0)
		return NULL;

	S = &where[strlen (where)];
	len = strlen (str);

	while (--S != where) {
		if (! strncmp (S, str, len))
			return S;
	}

	return NULL;
}

/// Посчитать Количество символов от from до to-1.
__export int Dsyms (char * from, char * to, char sym)
{
	int count = 0;
	do {
		if (*from == sym)
			++count;
	} while (++from != to);
	return count;
}

/// Найти символ в буфере.
__export char * Dmemchr (char * from, int n, char ch)
{
	while (n--) {
		if (*from == ch)
			return from;
		++from;
	}
	return NULL;
}

/// Скопировать не более n символов строки ptr.
__export char * Dstrndup (char *ptr, int n)
{
	char *S;
	char *buf;
	
	if (ptr == NULL || n <= 0)
		return NULL;

	buf = (char *) malloc (n+1);
	S = buf;
	while (*ptr && n--) {
		*S = *ptr;
		++S; ++ptr;
	}
	*S = '\0';
	
	return buf;
}

/// Найти символ между ptr и end (включительно).
__export char * Dmid_strchr (char *ptr, char *end, char ch)
{
        while (ptr <= end) {
                if (*ptr == ch) 
                        return ptr;
                ++ptr;
        }
        return NULL;
}

/// Выбрать (с копирование памяти) строку между buf и end (включительно).
__export char * Dmid_getstr (char *buf, char *end)
{
        char *S;
        char *out;
        int s_len;
        
        if (! buf || ! end)
                return NULL;
        
        S = Dmid_strchr (buf, end, '\n');
        if (! S)
                S  = end;
        
        s_len = S-buf+1;
        out = (char *) malloc (s_len+1);
        memcpy (out, buf, s_len);
        out[s_len] = '\0';

        return out;
}

/// Получить рандомную строку на count символов.
__export char * Drand_str (char * buf, int count)
{
        int i;
        unsigned char ch;
	
        if (! buf)
                return NULL;

        --count;
        for (i = 0; i < count; ++i) {
                ch = rand () % ('z' - 'a' - 1);
                buf[i] = ch + 'a';
        }                

        buf[i] = 0;
        return buf;
}

/// Перевести число в строку (результат - новая LPSZ строка).
__export char * int2str (int i)
{
        char buf[64];
        sprintf (buf, "%i", i);
        return strdup (buf);
}

/// Вернуть указатель на последний ноль строки.
__export char * stail (char *S)
{
	if (! S)
		return NULL;
	return &S[strlen (S)];
}

/// Копировать строку. Результат - конец результирующей строки.
__export char * strmov (char *buf, char * S)
{
	if (! buf || ! S)
		return NULL;
	strcpy (buf, S);
	return buf+strlen (S);
}

__export char * strnmov (char *buf, char * S, int N)
{
	int len;

	if (! buf || ! S)
		return NULL;
	
	strncpy (buf, S, N);
	len = strlen (S);
	len = (len >= N) ? N : len;
	buf[len] = '\0';

	return &buf[len];
}

/// Убрать все начальные пробельные символы.
__export char * strip (char *str)
{
	char *S;
	if (! str)
		return NULL;
	S = str;
	while (*S && (*S == '\t' || *S == ' '))
		++S;
	if (S != str)
		strcpy (str, S);
	return str;
}

/// Убрать все конечные пробельные символы.
__export char * strip2 (char *str)
{
	char *S;
	if (! str)
		return NULL;
	S = stail (str);
	--S;
	while (S != str && (*S == ' ' || *S == '\t'))
		--S;
	++S;
	*S = '\0';
	return str;
}

/// Кросс-платформенный аналог memmem (3)
__export char * Dmemmem (char *haystack, size_t haystacklen, char *needle, size_t needlelen)
{
	char * ptr;
	char * end;

	if (! haystack || ! needle)
		return NULL;

	if (haystacklen < needlelen)
		return NULL;

	ptr = haystack;
	end = &haystack[haystacklen] - needlelen + 1;
	while (ptr != end && memcmp (ptr, needle, needlelen)) 
		++ptr;
	
	if (ptr == end)
		return NULL;

	return ptr;
}

/// Найти блок между begin и last (включительно).
__export char * Dmid_memmem (char * begin, char * last,	char * needle, int needlelen)
{
	char * ptr;

	if (! begin || ! needle)
		return NULL;

	if ((last - begin - 1) < needlelen)
		return NULL;

	last -= needlelen;
	++last;
	ptr = begin;
	while (ptr <= last && memcmp (ptr, needle, needlelen)) 
		++ptr;

	if (ptr > last)
		return NULL;

	return ptr;
}

/// Создать новую форматированную строку (не более 511 символов).
__export char * Dsprintf (char * fmt, ...)
{
	char m_buf[512];
	va_list ap;
	m_buf[511] = '\0';
	va_start (ap, fmt);
	vsnprintf (m_buf, 511, fmt, ap);
	va_end (ap);
	return strdup (m_buf);
}

__export char * strinsert (char * base, char *ptr, char *ins, int rewrite)
{
	int ins_len;
	int part1_len;
	int part2_len;
	char * m_buf;
	char *S;

	if (!base || !ptr || !ins)
		return NULL;

	ins_len = strlen (ins);
	part1_len = ptr - base;
	part2_len = strlen (&ptr[rewrite]);

	m_buf = CNEW (char, ins_len + part1_len + part2_len + 1);
	m_buf = CNEW (char, 4096);
	m_buf[0] = '\0';
	
	S = strnmov (m_buf, base, part1_len);
	S = strmov (S, ins);
	S = strmov (S, &ptr[rewrite]);

	return m_buf;
}

__export char * strreplace (char * buf, char * oldstr, char * newstr)
{
	List * m_list;
	char * m_buf;

	if (!buf || !oldstr || !newstr)
		return NULL;		

	m_list = split (buf, oldstr);
	if (! m_list)
		return NULL;
	m_buf = join (m_list, newstr);
	m_list->foreach (free);
	
	delete m_list;
	return m_buf;
}


