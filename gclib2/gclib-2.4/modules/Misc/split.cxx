/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>

List * split (char * buf, char * spl)
{
	List * m_list;
	char * S;
	char * ptr = buf;
	int spl_len;
	int count = 0;

	if (!buf || !spl)
		return NULL;

	m_list = new List;

	spl_len = strlen (spl);
	while((S = strstr (ptr, spl)) && S) {
		m_list->add_tail (Dstrndup (ptr, S - ptr));
		S += spl_len;
		ptr = S;
		++count;
	}

	if (strlen (ptr)) { 
		m_list->add_tail (strdup (ptr));
		++count;
	}

	return m_list;
}

List * bsplit (Buf * m_buf, int c)
{
	List * m_list;
	int count;
	Buf * b;

	if (! m_buf)
		return NULL;
	
	m_buf->seek (0, SEEK_SET);
	m_list = new List;
	while (true) {
		count = m_buf->available () >= c ? c : m_buf->available ();
		if (count <= 0)
			break;
		b = new Buf (count);
		memcpy (b->data (), m_buf->position (), count);
		m_buf->seek (count, SEEK_CUR);
		m_list->add (LPCHAR (b));
	}

	return m_list;
}

void __add_null_buf (List * m_list)
{
	Buf * b;

	b = new Buf;
	m_list->add (LPCHAR (b));
}

List * csplit (Buf * m_buf, Buf * spl, int c)
{
	List * m_list;
	Buf * b;
	char * newbuf;
	char * m_spl;
	int spl_len;
	char * ptr;
	char * m_old;
	int count;
	
	if (!m_buf || !spl)
		return NULL;

	m_spl = spl->data ();
	spl_len = spl->len ();
	m_list = new List;
	ptr = m_buf->data ();
	m_buf->seek (0, SEEK_SET);
	m_buf->ok = true;

	while (true) {
		m_old = ptr;
		ptr = m_buf->memmem (ptr, m_spl, spl_len);

		if (! ptr) 
			break;

		count = ptr - m_old;
		if (count < 0) 
			break;
		
		if (count == 0) {
			__add_null_buf (m_list);
			ptr += spl_len;
			continue;
		}

		newbuf = (char *) memdup ((void *) m_old, count);
		b = new Buf(newbuf, count);
		m_list->add (LPCHAR (b));

		ptr += spl_len;
		m_buf->set_pos (ptr);

		if (! m_buf->ok)
			break;

		if (c && (--c == 0)) 
			break;
	}

	count = m_buf->available ();
	if (! m_buf->ok) 
		__add_null_buf (m_list);
	else if (count > 0) {
		newbuf = (char *) memdup ((void *) m_old, count);
		b = new Buf (newbuf, count);
		m_list->add (LPCHAR (b));
	}

	m_buf->seek (0, SEEK_SET);
	return m_list;
}

