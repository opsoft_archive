/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <stdarg.h>
#include <Pkt.h>
#include "internals.h" 

int pack_countBuf (char * fmt, va_list ap)
{
	char *S;
	int len = 0;
	char * m_sarg;
	List * m_larg;
	Hash * m_harg;
	Buf * b;

	if (! fmt)
		return 0;
	
	S = fmt;
	while (*S) {
		switch (*S) {
			case 'a':					// uchar_t
				va_arg (ap, int);
				++len;
				break;
			case 'b':					// uint16_t
				va_arg (ap, int);
				len += 2;
				break;
			case 'c':					// int
				va_arg (ap, int);
				len += 4;
				break;
			case 'd':					// double
				va_arg (ap, double);
				len += sizeof (double);
				break;
			case 's': 					// Нуль-терминатед строка
				m_sarg = va_arg (ap, char *);
				len += strlen (m_sarg) + 1;
				break;

			case 'e':					// Buf *
				b = va_arg (ap, Buf *);
				if (b)
					len += b->len ();
				len += 4; 
				break;

			case 'l':					// List *  (char *)
				m_larg = va_arg (ap, List *);
				len += pack_countList (m_larg);
				break;
			case 'h':					// Hash *
				m_harg = va_arg (ap, Hash *);
				len += pack_countHash (m_harg);
				break;
		}
		++S;
	}

	return len;
}

