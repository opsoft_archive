/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <dns.h>

int main (int argc, char ** argv)
{
	DNS * m_dns;

	m_dns = new DNS;
	m_dns->async_A ("mail.ru");
	m_dns->async_A ("yandex.ru");
	m_dns->async_A ("google.ru");

	sleep (3);
	while (m_dns->loop ());


	printf ("%s\n", m_dns->async_A ("mail.ru"));
	printf ("%s\n", m_dns->async_A ("yandex.ru"));
	printf ("%s\n", m_dns->async_A ("google.ru"));

	delete m_dns;

	return EXIT_SUCCESS;
}

