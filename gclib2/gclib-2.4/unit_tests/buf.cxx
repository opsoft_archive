/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>

int main (int argc, char ** argv)
{
	Buf * m_buf;
	Buf * b;
	List * list;
	char * ptr;
	int len;

	if (argc != 2) {
		printf ("use: buf <file>\n");
		return EXIT_SUCCESS;
	}

	ptr = DFILE (argv[1], &len);
	m_buf = new Buf (ptr, len);

	list = bsplit (m_buf, 1024);
	b = bjoin (list, NULL);
	if (*m_buf != b)  {
		printf ("Тест провален !\n");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

