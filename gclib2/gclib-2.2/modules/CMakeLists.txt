include_directories ("../include")
add_library (gclib2 SHARED Core/String.cxx
	Core/dstack.cxx
	Core/dexec.cxx
	Core/dpbuf.cxx
	Core/earray.cxx
	Core/hv.cxx
	Core/ehash.cxx
	Core/djobs.cxx
	Core/Tree.cxx
	Core/dhash.cxx
	Core/elist.cxx
	Core/darray.cxx
	Core/dlist.cxx
	Misc/strings.cxx
	Misc/dsplit.cxx
	Misc/dheapsort.cxx
	Misc/Pkt.cxx
	Misc/misc.cxx
	Linux/linux_specific.cxx
	IPC/ipc.cxx
	Net/HTTP/http.cxx
	Net/FTP/ftp.cxx
	Net/FTP/ftp_connect.cxx
	Net/INET/network.cxx
	Net/INET/dudp.cxx
	Net/INET/dconnection.cxx
	Net/INET/dpoll.cxx
	Net/INET/url.cxx
	Net/DNS/dns.cxx
	IO/IO.cxx
	IO/dfiles.cxx
	Crypt/digests.cxx
	Crypt/scode.cxx
	Crypt/base64.cxx )
