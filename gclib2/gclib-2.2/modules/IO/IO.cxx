/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "gclib2.h"
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdarg.h>

#define __export

struct stat *cur_stat = NULL;

/*! \brief Записать непосредственно в файл.
 * \param p_lpsz_filename - имя требуемого файла.
 * \param p_lp_buffer - буфер данных.
 * \param int_size - размер буфера данных.
 */
__export int Dfnwrite (char * p_lpsz_filename, void * p_lp_buffer,int int_size)
{
	int result;
	FILE * myfile;
	myfile = fopen(p_lpsz_filename,"w");
	if(!myfile)
		return 0;
	result = fwrite(p_lp_buffer,1,int_size,myfile);
	fclose(myfile);
	return result;
}

/*! \brief Прочитать непосредственно из файла.
 * \param f_name - имя требуемого файла.
 * \param p_lp_buffer - буфер данных.
 * \param int_size - размер буфера данных.
 */
__export int Dfnread (char * f_name, void * p_lp_buffer, int int_size)
{
	int n_bytes = int_size > fsize (f_name) ? fsize (f_name) : int_size;
	int fd;

	fd = open (f_name, O_RDONLY);
	if (fd < 0)
		return fd;
	
	if (read (fd, p_lp_buffer, n_bytes) < 0) 
		n_bytes = -1;
		
	close (fd);
	return n_bytes;
}

/*! \brief Ожидать поступления данных (с одного дескриптора).
 * \param FILENO - требуемый дескриптор.
 * \param SEC - таймаут (секунды).
 * \param USEC - таймаут (микросекунды).
 */
__export int Dselect (int FILENO, int SEC, int USEC)
{
	struct timeval m_timeval;
	fd_set m_fdset;

	FD_ZERO (&m_fdset);
	FD_SET (FILENO, &m_fdset);
	m_timeval.tv_sec = SEC;
	m_timeval.tv_usec = USEC;

	if (!m_timeval.tv_sec && !m_timeval.tv_usec) 
		return select (FILENO+1, &m_fdset, NULL, NULL, NULL);// &m_timeval);
	else
		return select (FILENO+1, &m_fdset, NULL, NULL, &m_timeval);
}

/*! \brief Получить весь файл.
 * \param m_filename - имя требуемого файла.
 * \param rsize - размер результирующих данных.
 */
__export char * DFILE (const char * m_filename, int *rsize)
{
	char * m_file;
	struct stat m_stat;
	char * ptr;
	int count;
	int len;
	int fd;

	if (m_filename == NULL)
		return NULL;

	if (lstat (m_filename, &m_stat) < 0)
		return NULL;

	fd = open (m_filename, O_RDONLY);
	if (fd < 0)
		return NULL;

	m_file = CNEW (char, m_stat.st_size + 1); 
	if (m_file == NULL)
		return NULL;

	ptr = m_file;
	len = m_stat.st_size;
	while (-1) {
		count = read (fd, ptr, len);
		if (count <= 0)
			break;
		ptr+=count;
		len-=count;
	}	

	if (rsize)
		*rsize = m_stat.st_size;

	m_file[m_stat.st_size] = '\0';
	close (fd);
	return m_file;
}

/// stat (2) для файла (не реентрантная !).
__export struct stat * DSTAT (const char * S)
{
	if (!cur_stat)
		cur_stat = (struct stat *) malloc (sizeof (struct stat));
	stat (S, cur_stat);
	return cur_stat;
}

/// lstat (2) для файла (не реентрантная !).
__export struct stat * DLSTAT (const char * S)
{
	if (! cur_stat)
		cur_stat = (struct stat *) malloc (sizeof (struct stat));
	lstat (S, cur_stat);
	return cur_stat;
}

/// Возвращает размер доступных для чтения данных.
__export int DIONREAD (int fd)
{
	int ret = -1;
	if (ioctl (fd, FIONREAD, &ret) != 0)
		return -1;
	return ret;
}

/// Возвращает размер файла.
__export int fsize (const char * S)
{
	struct stat m_stat;
	if (lstat (S, &m_stat) < 0)
		return -1;
	return m_stat.st_size;
}

/// Возвращает размер файла (вариант для дескриптора).
__export int fdsize (int fd)
{
	struct stat m_stat;
	if (fstat (fd, &m_stat) < 0)
		return -1;
	return m_stat.st_size;
}

/// mmap'нуть все данные дескриптора.
__export char * DFDMAP (int fd)
{
	return (char *) mmap (NULL, fdsize (fd), PROT_READ, MAP_SHARED, fd, 0);
}

/*! \brief mmap'нуть весь файл.
 * \param d_file - имя нужного файла.
 * \param out_fd - результирующий дескриптор файл.
 * \param d_out_size - результирующий размер mmap'нутого блока.
 */
__export char * DFMAP (const char *d_file, int *out_fd, int *d_out_size)
{
	char *S = NULL;
	int d_size;
	int fd;

	fd = open (d_file, O_RDONLY);
	if (fd < 0)
		return NULL;

	d_size = fdsize (fd);

	if (d_out_size)
		*d_out_size = d_size;

	if (out_fd)
		*out_fd = fd;

	S = (char *) mmap (NULL, d_size, PROT_READ, MAP_SHARED, fd, 0);
	if ((long) S == -1) {
		close (fd);
		return NULL;
	}

	return S;
}

/*! \brief прочитать все данные из дескриптора.
 * \param fd - дескриптор
 * \param d_out_size - результирующий размер данных.
 */
__export char * Dread_to_eof (int fd, int *d_out_size)
{
	char * d_buf = (char *) malloc (4096);
	int d_size = 4096;
	int d_pos = 0;
	int d_ret = 0;

	if (fd < 0)
		return NULL;

	if (d_out_size)
		*d_out_size = 0;

	while (-1) {
		d_ret = read (fd, &d_buf[d_pos], d_size - d_pos - 1);
		if (d_ret == -1)
			return NULL;

		if (d_ret == 0) //EOF
			break;

		d_pos += d_ret;
		if ((d_size - d_pos) < 4096) {
			d_buf = gc_realloc (d_buf, d_size, d_size << 1);
			d_size<<=1;
			if (d_buf == NULL) {
				if (d_out_size)
					*d_out_size = 0;
				return NULL;
			}
		}
	}

	if (d_out_size)
		*d_out_size = d_pos;

	d_buf[d_pos] = 0;
	return d_buf;
}

__export char * allData (int fd, int * d_out_size) // v2.2
{
	int count;
	char * b;

	count = DIONREAD (fd);
	if (count <= 0)
		return NULL;
	b = CNEW (char, count + 1);
	read (fd, b, count);
	b[count] = '\0';

	if (d_out_size)
		*d_out_size = count;

	return b;
}	

/// Переписать (с блокировкой) все из fd_in в fd_out.
__export int move_stream (int fd_in, int fd_out)
{
	char * m_buf = NULL;
	int i;
	int Ret = 0;

	m_buf = (char *) malloc (4096);

	while (-1) {
		i = read (fd_in, m_buf, 4096);
		if (i <= 0)
			break;
		Ret += i;
		write (fd_out, m_buf, i);
	}

	free (m_buf);
	return Ret;
}

__export int move_stream_file (FILE * m_src, FILE * m_dst)
{
	char * m_buf = NULL;
	int i;
	int Ret = 0;

	if (!m_src || !m_dst)
		return -1;
	
	m_buf = (char *) malloc (4096);
	
	while (-1) {
		i = fread (m_buf, 1, 4096, m_src);
		if (i <= 0)
			break;
		Ret += i;
		fwrite (m_buf, 1, i, m_dst);
	}


	free (m_buf);
	return Ret;
}

/// Установить неблокируемый режим.
__export int Dnonblock(int fd)
{
	int flags = fcntl(fd, F_GETFL);
	return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

/// Закрыть трубу.
__export int close_pipe (int *fds)
{
	int Ret1 = 0;
	int Ret2 = 0;

	if (fds[0] != -1) {
		Ret1 = close (fds[0]);
		fds[0] = -1;
	}

	if (fds[1] != -1) {
		Ret2 = close (fds[1]);
		fds[1] = -1;
	}

	return Ret1 ? Ret1 : Ret2;	
}

/// Получить временный дескриптор.
__export int Dtmpfd (char *name)
{
	char m_buf[128];
	char tmpstr[64];
	int fd;

	Drand_str (tmpstr, 63);
	sprintf (m_buf, "/tmp/%s", tmpstr);
	fd = open (m_buf, O_CREAT | O_RDWR, 0600);
	if (fd < 0) {
		perror ("open");
		return -1;
	}

	if (name) {
		if (fd >= 0) 
			strcpy (name, m_buf);
		else 			
			name[0] = '\0';
	}
	
	return fd;
}

/// Получить временный файл.
__export FILE * Dtmpfile (char *name)
{
	char tmpstr[64];
	char m_buf[128];
	Drand_str (tmpstr, 63);
	sprintf (m_buf, "/tmp/%s", tmpstr);
	if (name)
		strcpy (name, m_buf);
	return fopen (m_buf, "w+");
}

/// Закрыть дескриптор *fd (если не -1) и установить *fd в -1.
__export int fdclose (int * fd)
{
	if (! fd)
		return 0;
	
	if (*fd != -1) {
		close (*fd);
		*fd = -1;
	}
	return 0;
}

/// Получить расширение файла.
__export char * fext (char *name)
{
	if (! name)
		return NULL;
	return rindex (name, '.');
}

/// Записать отформатированную строку непосредственно в файл.
__export int logToFile (char * fileName, char * fmt, ...)
{
	va_list alist;
	FILE * myfile;
	myfile = fopen (fileName, "a");
	if (! myfile) {
		myfile = fopen (fileName, "w");
		if (! myfile) 
			return -1;
	}
	va_start (alist, fmt);
	vfprintf (myfile, fmt, alist);
	va_end (alist);
	fclose (myfile);
	return 0;
}

/// Скопировать файл.
__export int copyFile (char * sourceName, char * destName)
{
	int sourceFD = -1;;
	int destFD = -1;
	struct stat st;
	char * copyBuf = NULL;
	int ret;
	int count = 0;

	if (! sourceName || ! destName)
		return -1;

	sourceFD = open (sourceName, O_RDONLY);
	fstat (sourceFD, &st);
	destFD = open (destName, O_WRONLY | O_CREAT, st.st_mode);
	if ((sourceFD < 0) || (destFD < 0)) {
		count = -1;
		goto copyFile_out;
	}

	copyBuf = CNEW (char, 4096);
	while (-1) {
		ret = read (sourceFD, copyBuf, 4096);
		if (ret <= 0)
			break;
		if (write (destFD, copyBuf, ret) < 0)
			break;
		count += ret;
	}

copyFile_out:

	DROP (copyBuf);
	fdclose (&sourceFD);
	fdclose (&destFD);
	return count;
}

/// Прочитать строку из файла (реентрантная).
__export char * DSTR (FILE * m_file)
{
	char *S;
	if (m_file == NULL)
		return NULL;
	
	S = (char *) malloc (256);
	if (fgets (S, 256, m_file) != S)
		return NULL;

	return S;
}

/// Копировать из m_source в m_dest не более N байт.
__export int Dcopyfd (int m_source, int m_dest, int N)
{
	int count;
	int len = 0;
	char * m_buf;

	m_buf = CNEW (char, 4096);
	while (N > 0) {
		count = N > 4096 ? 4096 : N;
		count = read (m_source, m_buf, count);
		if (count <= 0)
			break;
		write (m_dest, m_buf, count);
		len += count;
		N -= count;
	}

	DROP (m_buf);
	return len;
}

/// Копировать из m_source в m_dest не более N байт.
__export int Dcopyfile (FILE * m_source, FILE * m_dest, int N)
{
	int count;
	int len = 0;
	char * m_buf;

	if (! m_source || ! m_dest)
		return -1;

	m_buf = CNEW (char, 4096);
	while (N > 0) {
		count = N > 4096 ? 4096 : N;
		count = fread (m_buf, 1, count, m_source);
		if (count <= 0)
			break;
		fwrite (m_buf, 1, count, m_dest);
		len += count;
		N -= count;
	}

	DROP (m_buf);
	return len;
}

__export List * file (char * fileName)
{
	List * m_list;
	char * m_buf;
	FILE * m_file;

	if (! fileName)
		return NULL;

	m_file = fopen (fileName, "r");
	if (! m_file)
		return NULL;

	m_list = new List;
	m_buf = CNEW (char, 4096);
	m_buf[4095] = '\0';
	
	while (fgets (m_buf, 4095, m_file) > 0) 
		m_list->push (strdup (m_buf));

	DROP (m_buf);
	fclose (m_file);
	return m_list;
}

