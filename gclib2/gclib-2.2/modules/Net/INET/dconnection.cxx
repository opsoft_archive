/*
 * (c) Oleg Puchinin 2006-2008
 * graycardinalster@gmail.com
 *
 */

#include <time.h>
#include <gclib2.h>
#include <sys/ioctl.h>
#include <network.h>
#include <arpa/inet.h>
#include <netinet/in.h>

DConnection::DConnection ()
{
	c_sd = -1;
	c_type = 0;
	c_name = NULL;
	c_group = NULL;
	c_mtime = 0;
	c_ctime = 0;
	c_cname = NULL; // client address
	c_pname = NULL; // server address
	c_cport = 0;
	c_pport = 0;
	c_status = NULL; // other data
	c_poll_flags = 0;
	c_bcast = false;
	dirtySocket = false;
}

DConnection::~DConnection ()
{
	if (! dirtySocket && c_sd != -1)
		::close (c_sd);
	DROP (c_cname);
	DROP (c_group);
	DROP (c_pname);
}

/// Инициализировать объект для работы с IPv4 (AF_INET, SOCK_STREAM).
int DConnection::init ()
{
	if (c_sd != -1) 
		::close (c_sd);
	c_sd = ::socket (AF_INET, SOCK_STREAM, 0);
	dirtySocket = false;
	return c_sd;
}

/// Инициализировать объект для работы с IPv4 (AF_INET, SOCK_UDP).
int DConnection::initUdp ()
{
	if (c_sd != -1)
		return -1;
	c_sd = ::socket (AF_INET, SOCK_DGRAM, 0);
	dirtySocket = false;
	return c_sd;
}

/*! \brief Привязать сокет к порту.
 * \param ip - адрес формата "xxx.xxx.xxx.xxx"
 * \param port - порт (не сетевой формат)
 */
int DConnection::bind (char *ip, uint16_t port)
{
	if (! ip)
		return -1;
	
	DROP (c_cname);
	c_cname = strdup (ip);
	c_cport = port;
	return dBind (c_sd, ip, port);
}

/*! \brief Подключить сокет.
 * \param ip - адрес формата "xxx.xxx.xxx.xxx"
 * \param port - порт (не сетевой формат)
 */ 
int DConnection::connect (char *ip, uint16_t port)
{
	sockaddr_in s_addr;
	socklen_t len;
	int ret;
	
	ret = dConnect (c_sd, ip, port);
	if (ret < 0)
		return -1;
	
	c_pname = strdup (ip);
	c_pport = port;
	if (! c_cname) {
		len = sizeof (struct sockaddr_in);
		::getsockname (c_sd, (sockaddr *) &s_addr, &len);
		DROP (c_cname);
		c_cname = strdup (inet_ntoa (s_addr.sin_addr));
		c_cport = htons (s_addr.sin_port);
	}
	c_ctime = time (NULL);
	return ret;
}

/*! \brief Подключить сокет при помощи UDP.
  * \param ip - адрес формата "xxx.xxx.xxx.xxx"
  * \param port - порт (не сетевой формат) 
  */
int DConnection::connectUdp (char *ip, uint16_t port)
{
	if (! ip || c_sd < 0)
		return -1;
	
	DROP (c_pname);
	c_pname = strdup (ip);
	c_pport = port;
	return 0;
}

/*! \brief Послать пакет (TCP).
  * \param ip - адрес формата "xxx.xxx.xxx.xxx"
  * \param port - порт (не сетевой формат) 
  */
int DConnection::send (char * buf, int len)
{
	return ::send (c_sd, buf, len, 0);
}

/*! \brief Послать пакет (UDP).
 * \param buf - данные пакета.
 * \param len - длина пакета.
 */
int DConnection::sendTo (char * buf, int len)
{
	return sendToSocket (c_sd, buf, len, c_pname, c_pport);
}

/*! \brief Получить пакет (TCP)
 * \param buf - данные пакета.
 * \param len - максимальная длина пакета.
 */ 
int DConnection::recv (char * buf, int len)
{
	return ::recv (c_sd, buf, len, 0);
}

/*! \brief Получить пакет (TCP)
 * \param buf - данные пакета.
 * \param len - максимальная длина пакета.
 * \param IP - адрес отправителя (буфер не менее 16 байт).
 * \param PORT - порт отправителя.
 */ 
int DConnection::recvFrom (char *buf, int len, char * IP, uint16_t * PORT)
{
	return ::recvFrom (c_sd, buf, len, IP, PORT);
}

/// Количество данных доступных без блокировки.
int DConnection::ioNRead ()
{
	int ret;
	if (ioctl (c_sd, FIONREAD, &ret) != 0)
		return -1;
	return ret;
}

/*! \brief Обновить время создание соединения.
 * \param d_time - новое время (или 0 для текущего).
 */
void DConnection::updateCTime (time_t d_time)
{
	if (d_time)
		c_ctime = d_time;
	else
		c_ctime = time (NULL);
}

/*! \brief Обновить время модификации.
 * \param d_time - новое время (или 0 для текущего).
 */
void DConnection::updateMTime (time_t d_time)
{
	if (d_time)
		c_mtime = d_time;
	else
		c_mtime = time (NULL);
}

/*! \brief Клонировать соединение.
 * Копируются все данные по соединению, кроме дескриптора и флага "Broadcast".
 */
DConnection * DConnection::clone ()
{
	DConnection *c;
        
	c = new DConnection;
	if (c_name)
		c->c_name = strdup (c_name);
	if (c_group)
		c->c_group = strdup (c_group);
	if (c_cname)
		c->c_cname = strdup (c_cname);
	if (c_pname)
		c->c_pname = strdup (c_pname);
	
	c->c_type = c_type;
	c->c_mtime = c_mtime;
	c->c_ctime = c_ctime;
	c->c_cport = c_cport;
	c->c_pport = c_pport;
	c->c_status = c_status;
	c->c_poll_flags = c_poll_flags;
	return c;
}

/// Включить широковещательную рассылку.
int DConnection::setBroadcast ()
{
	c_bcast = true;
	return ::setBroadcast (c_sd);
}

/// Закрыть соединение.
int DConnection::close ()
{
	if (! dirtySocket && c_sd != -1) {
		::close (c_sd);
		c_sd = -1;
	}
	
	DROP (c_name);
	DROP (c_cname);
	DROP (c_pname);
	return 0;
}

/// listen(2)
int DConnection::listen (int N)
{
	return ::listen (c_sd, N);
}

/// Принять подключение.
DConnection * DConnection::accept ()
{
	DConnection * m_conn = NULL;
	sockaddr_in m_addr;
	socklen_t m_len;
	int Ret;

	m_len = sizeof (struct sockaddr_in);
	Ret = ::accept (c_sd, (sockaddr *) &m_addr, &m_len);
	if (Ret < 0)
		return NULL;
	
	m_conn = clone ();
	DROP (m_conn->c_pname);
	m_conn->c_pname = strdup (inet_ntoa (m_addr.sin_addr));
	m_conn->c_pport = htons (m_addr.sin_port);
	m_conn->c_sd = Ret;

	return m_conn;
}

/// Получить дейтаграмму.
DUdp * DConnection::recvUdp (int bufLen)
{
	DUdp * m_udp;
	char * m_buf;
	uint16_t port;
	char ip[32];
	int len;
	
	m_udp = new DUdp;
	m_buf = CNEW (char, bufLen);
	len = recvFrom (m_buf, bufLen, ip, &port);
	if (len <= 0) {
		delete m_udp;
		DROP (m_buf);
		return NULL;
	}
	m_udp->init (m_buf, len, ip, port);

	return m_udp;
}

/// Ждать поступления данных.
int DConnection::select (int secs, int usecs)
{
	return Dselect (c_sd, secs, usecs);
}

int DConnection::setSocket (int m_sd)
{
	dirtySocket = true;
	c_sd = m_sd;
	return 0;
}

void DConnection::setName (char * m_name) 
{
	c_name = strdup (m_name);
}

void DConnection::setGroup (char * m_group)
{
	c_group = strdup (m_group);
}
                
int DConnection::open_ro (char *f_name) 
{
	c_sd  = ::open (f_name, O_RDONLY);
	return c_sd;
}

int DConnection::open_rw (char *f_name) 
{
	c_sd = ::open (f_name, O_RDWR);
	return c_sd;
}

int DConnection::open (char *f_name, int openmode, int filemode) 
{
	c_sd = ::open (f_name, openmode, filemode);
	return c_sd;
}

int DConnection::read (char *m_buf, int len) 
{
	return ::read (c_sd, m_buf, len);
}

int DConnection::write (char *m_buf, int len) 
{
	return ::write (c_sd, m_buf, len);
}

void DConnection::setPollFlags (int flags) 
{
	c_poll_flags = flags;
}

int DConnection::pollFlags ()
{
	return c_poll_flags;
}

char * DConnection::getsockname () 
{
	sockaddr_in m_addr;
	socklen_t namelen = sizeof (sockaddr_in);

	if (c_cname)
		return c_cname;

	if (::getsockname (c_sd, (sockaddr *) &m_addr, &namelen) < 0)
		return NULL;
	c_cname = strdup (inet_ntoa (m_addr.sin_addr));
	
	return c_cname;
}

char * DConnection::getpeername () 
{
	sockaddr_in addr;
	socklen_t m_len;

	if (c_pname)
		return c_pname;
	m_len = sizeof (sockaddr_in);
	if (::getpeername (c_sd, (sockaddr *) &addr, &m_len) < 0)
		return NULL;
	c_pname = strdup (inet_ntoa (addr.sin_addr));

	return c_pname;
}

uint16_t DConnection::getpeerport () 
{
	sockaddr_in addr;
	socklen_t m_len;

	if (c_pport)
		return c_pport;
	m_len = sizeof (sockaddr_in);
	if (::getpeername (c_sd, (sockaddr *) &addr, &m_len) < 0)
		return 0;
	c_pport = htons (addr.sin_port);

	return c_pport;
}

uint16_t DConnection::getsockport () 
{
	sockaddr_in m_addr;
	socklen_t namelen = sizeof (sockaddr_in);

	if (c_cport)
		return c_cport;

	if (::getsockname (c_sd, (sockaddr *) &m_addr, &namelen) < 0)
		return 0;

	c_cport = htons (m_addr.sin_port);
	return c_cport;
}

time_t DConnection::get_ctime () 
{
	return c_ctime;
}

time_t DConnection::get_mtime () {
	return c_mtime;
}

bool DConnection::broadcast () 
{
	return c_bcast;
}

char * DConnection::name () 
{
	return c_name;
}

char * DConnection::group ()
{
	return c_group;
}

int DConnection::socket ()
{
	return c_sd;
}

