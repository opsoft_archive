/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 * 
 */

#ifndef DEFINE_POLL_H
#define DEFINE_POLL_H

class Poll
{
	public:
		Poll ();
		~Poll ();

		Connection * add (Connection * c);
		Connection * unlink (Connection * c);
		pollfd * poll_build (int * nfds);
		Connection * scan ();

	private:
		Connection * __findConnection (Connection * c);
		Connection * __findName (char * str);

		List * m_connections;
		pollfd * pull;
};

#endif

