/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#if 0

#include <gclib2.h>
#include <sys/socket.h>
#include "internals.h"

int ftp_cmd (connection_context * ctx, char * cmd)
{
	char m_buf[512];
	int opcode;
	int len;

	len = DIONREAD (ctx->serverSd);
	if (len) 
		recv (ctx->serverSd, m_buf, 512, 0);

	strcpy (m_buf, cmd);
	strcat (m_buf, "\r\n");
	if (send (ctx->serverSd, m_buf, strlen (m_buf), 0) <= 0)
		return -1;

	len = recv (ctx->serverSd, m_buf, 511, 0);
	if (len < 0) 
		return -1;

	m_buf[len] = '\0';
	sscanf (m_buf, "%d", &opcode);
	return opcode;
}

int ftp_login (connection_context * ctx, char * login, char * pass)
{
}

int ftp_get (char * url, char * login, char * password)
{
	connection_context * ctx;

	ctx = connection_context_alloc ();
	ctx->url = url;

	if (ftp_connect (ctx) < 0) {
		connection_context_clean (ctx);
		return -1;
	}

	ftp_login (ctx, login, password);

}

#endif

