/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#if 0

#include <gclib2.h>
#include <gclib2/dns.h>
#include <sys/socket.h>

int ftp_connect (connection_context * ctx)
{
	char * S;
	char m_buf[512];
	bool m_ip;
	int nDots = 0;

	if (! ctx)
		return -1;

	strcpy (m_buf, ctx->url);
	S = strstr (m_buf, "://");
	if (S) {
		*S = '\0';
		if (NE (m_buf, "ftp"))
			return -1;
		S += 3;
		strcpy (m_buf, S);
	}

	S = strchr (m_buf, '/');
	if (S) {
		ctx->file = strdup (S);
		*S = '\0';
	} else
		ctx->file = strdup ("/");
	ctx->serverName = strdup (m_buf);

	m_ip = true;
	S = ctx->serverName;
	while (*S) {
		if (*S == '.') 
			++nDots;
		else if (! isdigit (*S)) {
			m_ip = false;
			break;
		}
		++S;
	}

	if (nDots != 3)
		m_ip = false;

	if (m_ip) 
		ctx->serverIP = strdup (ctx->serverName);
	else
		ctx->serverIP = dns_A (ctx->serverName);
	
	if (! ctx->serverIP)
		return -1;

	ctx->serverSd = dSocket ();
	if (ctx->serverSd < 0)
		return -1;
	if (dConnect (ctx->serverSd, ctx->serverIP, 21)) {
		close (ctx->serverSd);
		return -1;
	}

	return ctx->serverSd;
}

#endif

