/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DNS_INTERNALS_H
#define DEFINE_DNS_INTERNALS_H

#ifndef DEFINE_GCLIB2_H
#include <gclib2.h>
#endif

#pragma pack(1)

struct dns_header 
{
	uint16_t id;
	uint16_t flags;
	uint16_t nr;
	uint16_t nrr;
	uint16_t nrr2;
	uint16_t nrr3;
};

struct dns_rheader
{
	uint16_t dr_type;
	uint16_t dr_class;
	uint32_t TTL;
	uint16_t len;
};

struct dns_reply
{
	char * domain;
	uint16_t dr_type;
	uint16_t dr_class;
	uint32_t TTL;
	char * data;
	char * pkt_data_ptr;
	uint16_t data_len;
};

struct dns_server
{
	char * IP;
	int ms;
};

struct dns_query
{
	uint16_t id;
	uint16_t flags;
	uint16_t dr_type;
       	uint16_t dr_class;
	char * host;
	char * reply_pkt;
	int reply_pkt_len;
	char * user_data;
};

#define DNS_A 1
#define DNS_NS 2
#define DNS_CNAME 5
#define DNS_PTR 12
#define DNS_HINFO 13
#define DNS_MX 15
#define DNS_ANY 255

#endif

