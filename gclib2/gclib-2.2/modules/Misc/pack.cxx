/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <stdarg.h>
#include <Pkt.h>

/*
 * a - char
 * b - int16_t
 * c - int
 * d - double
 * s - строка с нулём на конце
 * l - список строк. 
 * h - хэш
 * t - дерево
 *
 */

int __countList (List * m_list)
{
	int len = 4;
	char * S;

	if (! m_list)
		return 0;
	
	m_list->first ();
	while (m_list->get ()) {
		S = m_list->get ();
		len += strlen (S) + 1;
		m_list->next ();
	}

	return len;
}

int __countHash (Hash * m_hash)
{
	List * m_list;
	int len = 0;

	if (! m_hash)
		return 0;

	m_list = m_hash->keys ();
	len += __countList (m_list);
	delete m_list;

	m_list = m_hash->values ();
	len += __countList (m_list);
	delete m_list;

	return len;
}

int __countTreeNode (node_t * m_node)
{
	int len;

	if (! m_node)
		return 0;

	len = 4; // Node ID + Parent ID
	if (m_node->key) 
		len += strlen (m_node->key);
	++len;
	
	if (m_node->userData) 
		len += strlen (m_node->userData);
	++len;

	return len;
}

int __countTree (Tree * m_tree)
{
	node_t * node;

	if (! m_tree)
		return 0;
	node = m_tree->rootNode;
	
	while (node) {
	}

	return 0;
}

int __countBuf (char * fmt, va_list ap)
{
	char *S;
	int len = 0;
	char * m_sarg;
	List * m_larg;
	Hash * m_harg;

	if (! fmt)
		return 0;
	
	S = fmt;
	while (*S) {
		switch (*S) {
			case 's':
				m_sarg = va_arg (ap, char *);
				len += strlen (m_sarg) + 1;
				break;
			case 'l':
				m_larg = va_arg (ap, List *);
				len += __countList (m_larg);
				break;
			case 'h':
				m_harg = va_arg (ap, Hash *);
				len += __countHash (m_harg);
				break;
			case 'a':
				va_arg (ap, int);
				++len;
				break;
			case 'b':
				va_arg (ap, int);
				len += 2;
				break;
			case 'c':
				va_arg (ap, int);
				len += 4;
				break;
			case 'd':
				va_arg (ap, double);
				len += sizeof (double);
				break;
		}
		++S;
	}

	return len;
}

char * __pack_bytes (char * ptr, char fmt, int arg)
{
	switch (fmt) {
		case 'a':
			pkt_W8 (&ptr, arg);
			break;
		case 'b':
			pkt_W16 (&ptr, arg);
			break;
		case 'c':
			pkt_W32 (&ptr, arg);
			break;
	}

	return ptr;
}

char * __pack_double (char * ptr, double dbl)
{
	((double *) ptr)[0] = dbl;
	return ptr += sizeof (double);
}

char * __pack_list (char * ptr, List * m_list)
{
	char * S;

	if (!ptr || !m_list)
		return NULL;

	pkt_W32 (&ptr, m_list->count ());
	m_list->first ();
	while (true) {
		S = m_list->get ();
		if (! S)
			break;
		ptr = strmov (ptr, S);
		++ptr;
		m_list->next ();
	}

	return ptr;
}

char * __pack (char * buf, int len, char * fmt, va_list ap)
{
	char * ptr;
	char * S;
	char * sparam;
	List * lparam;
	Hash * hparam;
	int param;
	double dbl;

	if (!buf || !fmt)
		return NULL; 

	ptr = buf;
	S = fmt;
	while (*S) {
		switch (*S) {
			case 's':
				sparam = va_arg (ap, char *);
				ptr = strmov (ptr, sparam);
				++ptr;
				break;

			case 'l':
				lparam = va_arg (ap, List *);
				ptr = __pack_list (ptr, lparam);
				break;

			case 'h':
				hparam = va_arg (ap, Hash *);
				ptr = __pack_list (ptr, hparam->keys ());
				ptr = __pack_list (ptr, hparam->values ());
				break;
		
			case 'a':
			case 'b':
			case 'c':
				param = va_arg (ap, int);
				ptr = __pack_bytes (ptr, *S, param);
				break;

			case 'd':
				dbl = va_arg (ap, double);
				ptr = __pack_double (ptr, dbl);
				break;
		}
		++S;
	}

	return ptr;
}

Buf * pack (char * fmt, ...)
{
	va_list ap;
	Buf * m_buf = NULL;
	int len;

	va_start (ap, fmt);
	len = __countBuf (fmt, ap);
	va_end (ap);

	m_buf = new Buf(len);

	va_start (ap, fmt);
	__pack (m_buf->data (), m_buf->len (), fmt, ap);
	va_end (ap);

	return m_buf;
}

char * __unpack_list (char * ptr, List ** m_ret)
{
	int count;
	int len;
	int i;

	if (!ptr || !m_ret)
		return NULL;

	*m_ret = new List;
	count = pkt_R32 (&ptr);
	
	for (i = 0; i < count; ++i) {
		len = strlen (ptr);
		(*m_ret)->add(strdup (ptr));
		ptr += len + 1;
	}

	return ptr;
}

char * __unpack_hash (char * ptr, Hash ** m_hash)
{
	List * m_keys;
	List * m_values;
	char * key;
	char * value;

	if (!ptr || !m_hash)
		return NULL;

	ptr = __unpack_list (ptr, &m_keys);
	ptr = __unpack_list (ptr, &m_values);

	*m_hash = new Hash;
	m_keys->first ();
	m_values->first ();
	while (true) {
		key = m_keys->get ();
		value = m_values->get ();
		if (!key || !value) 
			break;
		(*m_hash)->set (key, strdup (value));
		m_keys->next ();
		m_values->next ();
	}

	m_keys->foreach (free);
	m_values->foreach (free);
	delete m_keys;
	delete m_values;

	return ptr;
}

int __unpack (Buf * buf, char * fmt, va_list ap)
{
	char * ptr;
	char *S;
	int len;
	char * aparam;
	int16_t * bparam;
	int * cparam;
	double * dparam;
	char ** sparam;
	List ** lparam;
	Hash ** hparam;

	if (!buf || !fmt)
		return -1;

	ptr = buf->data ();
	S = fmt;
	while (*S) {
		switch (*S) {
			case 'a':
				aparam = va_arg (ap, char *);
				*aparam = pkt_R8 (&ptr);
				break;
			case 'b':
				bparam = va_arg (ap, int16_t *);
				*bparam = pkt_R16 (&ptr);
				break;
			case 'c':
				cparam = va_arg (ap, int *);
				*cparam = pkt_R32 (&ptr);
				break;
			case 'd':				
				dparam = va_arg (ap, double *);
				memcpy ((char *) dparam, (char *) ptr, sizeof (double));
				ptr += sizeof (double);
				break;

			case 's':
				sparam = va_arg (ap, char **);
				len = strlen (ptr);
				*sparam = strdup (ptr);
				ptr += len + 1;
				break;

			case 'l':
				lparam = va_arg (ap, List **);
				ptr = __unpack_list (ptr, lparam);
				break;

			case 'h':
				hparam = va_arg (ap, Hash **);
				ptr = __unpack_hash (ptr, hparam);
				break;
		}
		++S;
	}

	return 0;
}

int unpack (Buf * m_buf, char * fmt, ...)
{
	va_list ap;
	int Ret;

	va_start (ap, fmt);
	Ret = __unpack (m_buf, fmt, ap);
	va_end (ap);

	return Ret;
}

