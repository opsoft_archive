/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_STRING_H
#define DEFINE_STRING_H

class String
{
	public:
		String ();
		String (char * str);
		String (const char * str);
		~String ();
		
		String & operator + (char * str);
		String & operator = (char * str);
		String & operator = (const char * str);
		inline operator char *() {
			return m_buf;
		}
		String & operator << (char * str);
		String & operator << (int num);

	private:
		char * m_buf;
		int m_len;
};

#endif


