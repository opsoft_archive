/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>

List * __init_list ()
{
	List * l;

	l = new List;
	*l << "GOP" << "SUP" << "VOBLA" << "KUKISH";
	return l;
}

Hash * __init_hash ()
{
	Hash * h;

	h = new Hash;
	h->set ("Date", "yesterday");
	h->set ("From", "Oleg");
	h->set ("To", "Papka");
	h->set ("Nyu", "Fu");

	return h;
}

int main (int argc, char ** argv)
{
	Buf * m_buf;
	List * m_list;
	Hash * m_hash;
	char * str;

	int i = 0;

	m_list = __init_list ();
	m_hash = __init_hash ();

	str = "HELLO";
	m_buf = pack ("clhs", 2, m_list, m_hash, str);

	m_list = NULL;
	m_hash = NULL;
	str = NULL;

	unpack (m_buf, "clhs", &i, &m_list, &m_hash, &str);
	printf ("i = %i\n", i);
	printf ("string = %s\n", str);
	printf ("LIST:\n");
	m_list->dump ();
	printf ("Hash:\n");
	m_hash->dump ();
}

