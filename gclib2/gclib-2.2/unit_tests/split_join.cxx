/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <network.h>

char * double_zero = "CHUKA\0";

void __buf_add_str (List * m_list, char * string)
{
	Buf * b;
	if (string)
		b = new Buf (strdup (string), strlen (string));
	else
		b = new Buf;
	m_list->add (LPCHAR (b));
}

int main (int argc, char ** argv)
{
	List * m_list;
	List * m_list2;
	Buf * bdz;
	Buf * b;

	m_list = new List;

	__buf_add_str (m_list, "11111");
	__buf_add_str (m_list, "11111");
	__buf_add_str (m_list,  NULL);
	__buf_add_str (m_list, "HULI");
	__buf_add_str (m_list, "GOP");
	__buf_add_str (m_list,  NULL);
	__buf_add_str (m_list,  NULL);
	__buf_add_str (m_list,  NULL);

	bdz = new Buf (strdup (double_zero), 6);
	b = bjoin (m_list, bdz);
	m_list2 = csplit (b, bdz);

	delete b;
	delete bdz;

	if (*m_list != m_list2) {
		printf ("Тест провален.\n");
		return EXIT_FAILURE;
	}

	m_list->foreach (buf_free);
	m_list2->foreach (buf_free);
	delete m_list;
	delete m_list2;

	return EXIT_SUCCESS;
}

