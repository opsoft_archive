/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_MACROSES_H
#define DEFINE_MACROSES_H

/// \brief Макрос, определяющий экспортировать ли функцию.
#define __export

/*! \brief Освободить память если занята затем установить в ноль.
 * \param arga - указатель для освобождения.
 */
#ifdef __cplusplus
#define DROP(arga) if (arga) { ::free (arga); arga = NULL; }
#else
#define DROP(arga) if (arga) { free (arga); arga = NULL; }
#endif

/*! \brief Выделить память.
 * \param arga - тип элементов.
 * \param argb - нужное количество элементов.
 */
#define CNEW(arga,argb) (arga *)malloc (sizeof (arga)*(argb))

/// \brief Эквивалент !strcmp.
#define EQ(arga, argb) (!strcmp (arga, argb))
/// \brief Эквивалент strcmp.
#define NE(arga, argb) (strcmp (arga, argb))

/*! Удалить последний символ в строке.
 * \param arga - соответственно строка.
 */
#define chop(arg1) arg1[strlen(arg1) - 1] = 0
#define LPCHAR(arg) ((char *) arg)

#endif

