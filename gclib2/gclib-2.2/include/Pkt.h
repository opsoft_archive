/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_PKT_H
#define DEFINE_PKT_H

/*** Функции низкоуровневой обработки пакетов. ***/
__export void pkt_W8 (char ** pkt, unsigned char data) ;
__export void pkt_W16 (char ** pkt, uint16_t data) ;
__export void pkt_W32 (char ** pkt, unsigned int data) ;
__export void pkt_WS (char ** pkt, char *S) ;
__export void pkt_WSZ (char ** pkt, char *S) ;
__export void pkt_WD (char ** pkt, char *S, int size) ;
__export unsigned char pkt_R8 (char ** pkt) ;
__export uint16_t pkt_R16 (char ** pkt) ;
__export uint32_t pkt_R32 (char ** pkt) ;
__export char * pkt_RD (char ** pkt, int len) ;
#define pkt_Wstruct(pkt, s) do { pkt_WD (pkt, (char *) s, sizeof (*s)); } while (0) 
#define pkt_Rstruct(arga, argb) (argb *) pkt_RD (arga, sizeof (argb))

#endif

