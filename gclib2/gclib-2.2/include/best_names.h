/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_BEST_NAMES_H
#define DEFINE_BEST_NAMES_H

class HV;
class EList;
class EArray;
class DConnection;
class DJobs;
class DPoll;
class DHeapSort;

typedef HV Hash;
typedef EList List;
typedef EArray Array;
typedef DConnection Connection;
typedef DJobs Jobs;
typedef DHeapSort HeapSort;

#endif

