/*
 * (c) Oleg Puchinin 2006-2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_PROTO_H
#define DEFINE_PROTO_H

#ifndef __export
#define __export
#endif

#ifdef __cplusplus
#include "earray.h"
#include <sys/poll.h>
#include "djob_t.h"

#ifdef __linux
__export char * dcp (char * S) ;
__export void Dzero (void * ptr) ;
__export int Dsched_yield () ;
int Dclone (int (*fn)(void *), void * param) ;
#endif

__export int Dpoll_add (EArray * d_array, int fd, short events) ;
__export int Dpoll_coallesce (EArray * d, struct pollfd ** p) ;
__export int Dsplit (char * STR, char *ch, DList * to_list) ;
__export int Dsplit (char * buf, size_t buflen, char *str, EList * to_list) ;

#undef __export
#define __export extern "C" 

#endif

__export uchar_t * base64_code (unsigned char * S, int SIZE) ;
__export uchar_t * base64_decode (unsigned char * S, int SIZE) ;
__export int dexec_wflags (int flags) ;
__export int dexec_rflags (int flags) ;
//__export int Dexec_op (struct __dexec_t *buf, int count, struct __djob_t *ctx) ;
__export void Dexec_init (struct __djob_t * ctx) ;
//__export int Dexec (int ops, struct __dexec_t *other_buf, int count, char *cmd, struct __djob_t *ctx) ;
__export __djob_t * Dexec (unsigned int opts, char * cmd);

__export int Dexec_done (struct __djob_t *ctx) ;

__export void Dtimer () ;
__export struct timeval *the_time () ;
__export void print_the_time (FILE * file_my) ;
__export int Dterm_one_kick (int fd) ;
__export char *Dversion () ;
__export char * Dtimestr (char * buf, int max) ;

#include <gc_strings.h>
#include <gc_io.h>
#include <ipc.h>

#endif

#undef __export
#define __export

