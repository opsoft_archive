/*
 * (c) Oleg Puchinin 2007,2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_HTTP_H
#define DEFINE_HTTP_H

char * http_get (char * url, int * size);

#endif

