/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <gc_network.h>
#include <FTP.h>
#include <dns.h>

FTP::FTP ()
{
	init ();
}

FTP::~FTP ()
{
}

List * FTP::waitReply ()
{
	List * Ret;
	char * S;
	int i = 0;

	while (true) {
		Ret = readStrings ();
		S = Ret->last ();
		if (S[3] != '-') 
			break;
		Ret->foreach (free);
		delete Ret;
	}

	return Ret;
}

int FTP::connect (char * server, uint16_t port)
{
	int Ret;

	if (isIP (server))
		Ret = Connection::connect (server, port);
	else
		Ret = Connection::connect (dns_A (server), port);

	if (Ret < 0)
		return Ret;

	checkReply (220);
	return 0;
}

int FTP::checkReply (int needle)
{
	List * res;
	char * S;
	int Ret = 0;
	int i;

	res = waitReply ();
	if (!res)
		return -1;

	S = res->last ();
	if (! S) {
		delete res;
		return -1;
	}
	
	if (atoi (S) != needle)
		Ret = -1;
	
	res->foreach (free);
	delete res;
	return Ret;
}

int FTP::login (char * l, char * p)
{
	char * m_buf;
	int count;
	char * S;
	int Ret = 0;
	
	if (! l || ! p)
		return -1;
	
	m_buf = CNEW (char, 4096);

	sprintf (m_buf, "USER %s\r\n", l);
	if (send (m_buf, strlen (m_buf)) < 0) {
		DROP (m_buf);
		return -1;
	}

	if (checkReply (331) < 0) {
		delete m_buf;
		return -1;
	}

	sprintf (m_buf, "PASS %s\r\n", p);
	if (send (m_buf, strlen (m_buf)) < 0) {
		DROP (m_buf);
		return -1;
	}

	if (checkReply (230)) {
		delete m_buf;
		return -1;
	}

	DROP (m_buf);	
	return Ret;
}

