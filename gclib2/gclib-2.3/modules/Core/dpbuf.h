/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DPBUF_H
#define DEFINE_DPBUF_H

/// Класс - устойчивый буфер.
class DPBuf
{
	public:
		DPBuf ();
		DPBuf (char *, int len);
		~DPBuf ();
		
		char * set_pos (char * ptr);
		void init (char *ptr, int len);
		FILE * file ();
		
		void s8 (void);
		void s16 (void);
		void s32 (void);
		void sd (int count);
	
		unsigned char r8 (char *ptr = NULL);
		uint16_t r16 (char *ptr = NULL);
		uint32_t r32 (char *ptr = NULL);
		char * rd (char *ptr, int len);
	
		void w8 (char ch);
		void w16 (uint16_t word);
		void w32 (uint32_t dword);
		void wd (char * buf, int len);

		bool check (char *ptr, int count);
		char * strcat (char *ptr, char *S);
                char * memmem (char * buf, char * needle, size_t needlelen);
                char * ch (char *buf, char ch);

		inline char * data () {
			return begin;
		}
		inline int len () {
			return end - begin;
		}

		bool ok;

	protected:
		char * begin;
		char * carret;
		char * end;
		FILE * m_file;
};

#endif

