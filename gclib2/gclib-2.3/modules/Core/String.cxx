/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <String.h>

String::String ()
{
	m_buf = strdup ("");
	m_len = 0;
}

String::String (const char * str)
{
	m_buf = strdup (str);
}

String::String (char * str)
{
	m_buf = str;
}

String::~String ()
{
	DROP (m_buf);
}

String & String::operator + (char * str)
{
	char * m_new;
	char * S;
	m_new = CNEW (char, m_len + strlen (str) + 1);
	S = strmov (m_new, m_buf);
	strmov (S, str);
	DROP (m_buf);
	m_buf = m_new;
	return *this;
}

String & String::operator = (char * str)
{
	DROP (m_buf);
	m_buf = str;
	return *this;
}

String & String::operator = (const char * str)
{
	DROP (m_buf);
	m_buf = strdup (str);
	return *this;
}

String & String::operator << (char * str)
{
	return (*this + str);
}

String & String::operator << (int num)
{
	char m_buf[64];
	sprintf (m_buf, "%i", num);
	return (*this + m_buf);
}

