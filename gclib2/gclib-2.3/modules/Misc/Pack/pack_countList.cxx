/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <stdarg.h>
#include <Pkt.h>

int pack_countList (List * m_list)
{
	int len = 4;
	char * S;

	if (! m_list)
		return 0;
	
	m_list->first ();
	while (m_list->get ()) {
		S = m_list->get ();
		len += strlen (S) + 1;
		m_list->next ();
	}

	return len;
}

