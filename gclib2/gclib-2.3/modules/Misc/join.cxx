/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>

char * join (List * m_list, char * jn)
{
	int jn_len;
	int len = 0;
	char * one;
	char *S;
	char * m_buf;

	if (!jn || !m_list)
		return NULL;

	jn_len = strlen (jn);

	m_list->first ();
	while (true) {
		S = m_list->get ();
		if (!S)
			break;
		len += strlen (S);
		m_list->next ();
	}
	
	len = len + jn_len * (m_list->count () - 1);
	m_buf = CNEW (char, len + 1);
	S = m_buf;
	m_list->first ();
	while (true) {
		one = m_list->get ();
		if (! one)
			break;
		S = strmov (S, one);
		if (m_list->next ())
			S = strmov (S, jn);
	}

	return m_buf;
}

Buf * bjoin (List * m_list, Buf * jn)
{
	int jn_len = 0;
	int count = 0;
	Buf * b;
	Buf * m_buf;
	char * m_pos;

	if (! m_list)
		return NULL;

	if (jn)
		jn_len = jn->len ();

	m_list->first ();
	while (true) {
		b = (Buf *) m_list->get ();
		if (! b)
			break;
		count += b->len ();
		if (m_list->next ()) 
			count += jn_len;
	}

	m_buf = new Buf (count);
	m_pos = m_buf->data ();
	m_list->first ();
	while (true) {
		b = (Buf *) m_list->get ();
		if (! b)
			break;
		
		if (b->len () > 0) {
			memcpy (m_pos, b->data (), b->len ());
			m_pos += b->len ();
		}

		if (m_list->next () && jn) {
			memcpy (m_pos, jn->data (), jn->len ());
			m_pos += jn_len;
		}
	}

	m_list->first ();
	return m_buf;
}

