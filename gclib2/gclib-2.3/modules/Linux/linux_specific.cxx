/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifdef __linux

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <asm/unistd.h>
#include <sched.h>
#include <gclib2.h>

/// Копировать выделенный блок (не строку) памяти.
__export char * dcp (char * S)
{
	int len = malloc_usable_size (S);
	char * ret = CNEW (char, len);
	if (!ret)
		return NULL;
	memcpy (ret, S, len);
	return ret;
}

/// Обнулить блок (не строку) памяти.
__export void Dzero (void * ptr)
{
	memset (ptr, 0, malloc_usable_size (ptr));
}

__export int Dsched_yield ()
{
	return sched_yield ();
}

int Dclone (int (*fn)(void *), void * param)
{
	char * m_buf;
	char * S;

	m_buf = CNEW (char, 4096);
	S = &m_buf[4088];
	return clone (fn, S, CLONE_FS | CLONE_FILES | CLONE_PTRACE | CLONE_VM, param);
}

#endif

