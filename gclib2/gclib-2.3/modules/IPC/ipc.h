/*
 * (c) oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_IPC_H
#define DEFINE_IPC_H

/*** ./ipc.cxx ***/
int sem_init (int * KEY) ;
int sem_get (int KEY) ;
int down (int sem) ;
int up (int sem) ;
int sem_init_rw (int * KEY) ;
int down_read (int sem) ;
int up_read (int sem) ;
int down_write (int sem) ;
int up_write (int sem) ;
int msg_init (int *KEY) ;

#endif

