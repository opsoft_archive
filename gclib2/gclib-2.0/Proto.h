/*
 * (c) ; Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_PROTO_H
#define DEFINE_PROTO_H

#ifndef __export
#define __export
#endif

#ifdef __cplusplus
#include "earray.h"
#include <sys/poll.h>
#include "djob_t.h"

__export DArray * Dfiles (char * path) ;
__export void dns_set_timeout (int sec) ;
__export int dns_get_sock () ;
__export char * dns_A (char *host) ;
__export char * dns_MX (char *host) ;
__export char * dns_ip2name (char *IP) ;
__export char * dcp (char * S) ;
__export void Dzero (void * ptr) ;
__export int Dsched_yield () ;
__export int Dpoll_add (EArray * d_array, int fd, short events) ;
__export int Dpoll_coallesce (EArray * d, struct pollfd ** p) ;
__export int Dsplit (char * STR, char *ch, DList * to_list) ;
__export int Dsplit (char * buf, size_t buflen, char *str, EList * to_list) ;
#endif

__export uchar_t * base64_code (unsigned char * S, int SIZE) ;
__export uchar_t * base64_decode (unsigned char * S, int SIZE) ;
__export int dexec_wflags (int flags) ;
__export int dexec_rflags (int flags) ;
__export int Dexec_op (struct __dexec_t *buf, int count, struct __djob_t *ctx) ;
__export void Djob_init (struct __djob_t * ctx) ;
__export int Dexec (int ops, struct __dexec_t *other_buf, int count, char *cmd, struct __djob_t *ctx) ;
__export int Dexec_done (struct __djob_t *ctx) ;
__export int Dfnwrite (char * p_lpsz_filename, void * p_lp_buffer,int int_size) ;
__export int Dfnread (char * f_name, void * p_lp_buffer, int int_size) ;
__export int Dselect (int FILENO, int SEC, int USEC) ;
__export char * DFILE (const char * m_filename, int *rsize) ;
__export struct stat * DSTAT (const char * S) ;
__export struct stat * DLSTAT (const char * S) ;
__export int DIONREAD (int fd) ;
__export int fsize (const char * S) ;
__export int fdsize (int fd) ;
__export char * DFDMAP (int fd) ;
__export char * DFMAP (const char *d_file, int *out_fd, int *d_out_size) ;
__export char * Dread_to_eof (int fd, int *d_out_size) ;
__export int move_stream (int fd_in, int fd_out) ;
__export int Dnonblock(int fd) ;
__export int close_pipe (int *fds) ;
__export int Dtmpfd (char *name) ;
__export int fdclose (int * fd) ;
__export char * fext (char *name) ;
__export int logToFile (char * fileName, char * fmt, ...) ;
__export int copyFile (char * sourceName, char * destName) ;
__export void Dtimer () ;
__export struct timeval *the_time () ;
__export void print_the_time (FILE * file_my) ;
__export int Dterm_one_kick (int fd) ;
__export char *Dversion () ;
__export char * Dtimestr (char * buf, int max) ;
__export int sendToPath(void * lp_data,int int_size,char * lpsz_path) ;
__export int sendToSocket (int sock, void * lp_data, int int_size, const char * address, uint16_t port) ;
__export int sendTo (void * lp_data, int int_size, const char * address, uint16_t port);

__export int dBind (int sock, char * addr, int port) ;
__export int dSocket () ;
__export int dUdpSocket () ;
__export int dConnect (int sock, const char * addr, uint16_t port) ;
__export int getHostByName(const char * hostName,struct in_addr * address) ;
__export char * getNameByHost(struct in_addr * addr) ;
__export int recvFrom (int sd, char * buf, int size, char * peer_ip, uint16_t * peer_port) ;
__export int setBroadcast (int fd) ; 
__export int pollScan (struct pollfd *p, int count, int pos) ;
__export int Transmit (char *f_name, int sd) ;
__export int transmitUdp (char *f_name, int sd, char *ip, uint16_t port) ;

__export void pkt_W8 (char ** pkt, unsigned char data) ;
__export void pkt_W16 (char ** pkt, uint16_t data) ;
__export void pkt_W32 (char ** pkt, unsigned int data) ;
__export void pkt_WS (char ** pkt, char *S) ;
__export void pkt_WSZ (char ** pkt, char *S) ;
__export void pkt_WD (char ** pkt, char *S, int size) ;
__export unsigned char pkt_R8 (char ** pkt) ;
__export uint16_t pkt_R16 (char ** pkt) ;
__export uint32_t pkt_R32 (char ** pkt) ;
__export char * pkt_RD (char ** pkt, int len) ;
#define pkt_Wstruct(pkt, s) do { pkt_WD (pkt, (char *) s, sizeof (*s)); } while (0) 
#define pkt_Rstruct(arga, argb) (argb *) pkt_RD (arga, sizeof (argb))

__export char * gc_realloc (char * PTR, int old_size, int new_size) ;
__export void * memdup (void * PTR, int size) ;
__export int Dsplit(char * lpsz_String, char *ch, char ** outbuffer, int int_buffersize) ;
__export char * Dstrmid(char * lpsz_string,char * param1, char * param2) ;
__export char * chomp (char * S) ;
__export char * DSTR (FILE * m_file) ;
__export char * strchr_r (char * S, char ch, int d_len) ;
__export char * strchrs (char *S, char ch, char ch2, char ch3, char ch4) ;
__export char * Dstrstr_r (char *where, char * str) ; 
__export int Dsyms (char * from, char * to, char sym) ;
__export char * Dmemchr (char * from, int n, char ch) ;
__export char * Dstrndup (char *ptr, int n) ;
__export char * Dmid_strchr (char *ptr, char *end, char ch) ;
__export char * Dmid_getstr (char *buf, char *end) ;
__export char * Drand_str (char * buf, int count) ;
__export char * int2str (int i) ;
__export char * stail (char *S) ;
__export char * strmov (char *buf, char * S) ;
__export char * strip (char *str) ;
__export char * strip2 (char *str) ;
__export char * Dmemmem (char *haystack, size_t haystacklen, char *needle, size_t needlelen) ;
__export char * Dmid_memmem (char * begin, char * last, char * needle, size_t needlelen) ;
__export char * Dsprintf (char * fmt, ...) ;

#endif

