#!/usr/bin/perl
use strict; use warnings;
foreach my $fileName (@ARGV) {
	print "/* $fileName */\n";
    fixFile ($fileName);
	print "\n";
}
sub fixFile {
    my $fileName = shift;
    my $line;
    my $docline;
    open my $flh, "< $fileName" or die "Не могу открыть файл $fileName";
    while (<$flh>) {
        chomp;
        if (m/(.*)\{/ and $docline) {
            $line = $1 if $1;
            print "$line -$docline\n";
			$docline = "";
        } elsif (/^\/{3}(.*)/) {
            $docline = $1;
        } else {
            $line = $_;
        }
    }
    close $flh;
}

