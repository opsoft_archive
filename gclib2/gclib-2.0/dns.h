/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DNS_H
#define DEFINE_DNS_H

#pragma pack(1)

struct dns_header 
{
	uint16_t id;
	uint16_t flags;
	uint16_t nr;
	uint16_t nrr;
	uint16_t nrr2;
	uint16_t nrr3;
};

struct dns_rheader
{
	uint16_t dr_type;
	uint16_t dr_class;
	uint32_t TTL;
	uint16_t len;
};

struct dns_reply
{
	char * domain;
	uint16_t dr_type;
	uint16_t dr_class;
	uint32_t TTL;
	char * data;
	char * pkt_data_ptr;
	uint16_t data_len;
};

struct dns_server
{
	char * IP;
	int ms;
};

#ifdef __cplusplus
char * __name2dns (char *name, int *len);
char * __dns_rd_chunk (char ** ptr, char * done);
char * __dns_resp_domain (DPBuf *p, char * domain, char **domain_end);
char * make_dns_pkt (uint16_t id, uint16_t flags, uint16_t d_type,
		uint16_t d_class, char * domain, int *len);
DList * dns_resp_split (dns_header *hdr, char *pkt, int len);
void dns_resp_clean (DList * list);
dns_reply * dns_scan (DList *list, uint16_t dr_type, uint16_t dr_class);
char * __dns_pack (DArray * heap, uint16_t flags, int *pkt_len);
#endif

#define DNS_A 1
#define DNS_NS 2
#define DNS_CNAME 5
#define DNS_PTR 12
#define DNS_HINFO 13
#define DNS_MX 15
#define DNS_ANY 255

#endif 

