/* 
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_IPV4_H
#define DEFINE_IPV4_H

/*** ./network.c ***/
__export int sendToPath (void * lp_data,int int_size,char * lpsz_path) ;
__export int sendToSocket (int sock, void * lp_data, int int_size, const char * address, uint16_t port) ;
__export int sendTo (void * lp_data, int int_size, const char * address, uint16_t port) ;
__export int dBind (int sock, char * addr, int port) ;
__export int dSocket () ;
__export int dUdpSocket () ;
__export int dConnect (int sock, const char * addr, uint16_t port) ;
__export int getHostByName (const char * hostName,struct in_addr * address) ;
__export char * getNameByHost (struct in_addr * addr) ;
__export int recvFrom (int sd, char * buf, int size, char * peer_ip, uint16_t * peer_port) ;
__export int setBroadcast (int fd) ;
__export int pollScan (struct pollfd *p, int count, int pos) ;
__export int Transmit (char *f_name, int sd) ;
__export int transmitUdp (char *f_name, int sd, char *ip, uint16_t port) ;

#endif

