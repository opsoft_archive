/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_GCLIB2_H
#define DEFINE_GCLIB2_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <dirent.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/mman.h>
#include <sys/poll.h>

typedef void (*Dfunc_t)(void *);
typedef void (*Dhash_f)(char *, char *);

struct dkey_t {
	char *KEY;
	char *VALUE;
};

#ifdef __cplusplus

typedef unsigned int uint32_t;
#include "dlist.h"
#include "elist.h"
#include "darray.h"
#include "earray.h"
#include "dhash.h"
#include "ehash.h"
#include "Tree.h"
#include "dheapsort.h"
#include "dpbuf.h"
#include "dconnection.h"
#include "dpoll.h"
#include "dstack.h"
#include "hv.h"
#include "djobs.h"
#endif
typedef unsigned char uchar_t;

#include "djob_t.h"
#include "dexec.h"
#include "Proto.h"
#include "dns.h"

#define OR(var,val1,val2) ((var == val1) || (var == val2))

#ifndef EQ
#define EQ(arga, argb) (!strcmp (arga, argb))
#endif

#ifndef NE
#define NE(arga, argb) (strcmp (arga, argb))
#endif

#ifndef chop
#define chop(arg1) arg1[strlen(arg1) - 1] = 0
#endif

#define DROP(arga) if (arga) { free (arga); arga = NULL; }
#define CNEW(arga,argb) (arga *)malloc (sizeof (arga)*(argb))
#define LPCHAR(arga) (char *) arga
#define container_of(var, type) ((type) var)

#ifndef __export
#define __export
#endif

#endif

