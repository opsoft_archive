/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_BEST_NAMES_H
#define DEFINE_BEST_NAMES_H

typedef DPBuf Buf;
typedef EHash Hash;
typedef EList List;
typedef EArray Array;
typedef DConnection Connection;
typedef DJobs Jobs;
typedef DPoll Poll;
typedef DHeapSort HeapSort;

#endif

