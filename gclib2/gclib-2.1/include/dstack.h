/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DSTACK_H
#define DEFINE_DSTACK_H

/// Класс - стек.
class DStack : public EList
{
	public:
		int push (char * S);
		char * pop ();
		char * top ();
};

#endif

