/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 * 
 */

#ifndef DEFINE_DCONNECTION_H
#define DEFINE_DCONNECTION_H

#include <fcntl.h>

#ifndef DEFINE_DUDP_H
#include <dudp.h>
#endif

class DConnection;
/// Класс "соединение".
class DConnection
{
        public:
                DConnection ();
                ~DConnection ();

                int init ();
                int initUdp ();
                int bind (char *ip, uint16_t port);
                int connect (char *ip, uint16_t port);
                int connectUdp (char *ip, uint16_t port);
		int listen (int N);
		DConnection * accept ();
                int send (char * buf, int len);
                int sendTo (char * buf, int len);
                int recv (char * buf, int len);
		int recvFrom (char *buf, int len, char * IP, uint16_t * PORT);
		DUdp * recvUdp (int bufLen = 65535);
                void updateCTime (time_t d_time = 0);
                void updateMTime (time_t d_time = 0);
                int ioNRead ();
		int select (int secs, int usecs);
                DConnection * clone ();
                int setBroadcast ();
		int setSocket (int m_sd);
		int socket ();
		int close ();

		void setName (char * m_name, int num = 0);
		int open_ro (char *f_name);
                int open_rw (char *f_name);
                int open (char *f_name, int openmode, int filemode = 0644);
                int read (char *m_buf, int len);
                int write (char *m_buf, int len);
		void setPollFlags (int flags);
		int pollFlags ();
                char * getsockname ();
                char * getpeername ();
                uint16_t getpeerport ();
                uint16_t getsockport ();
                time_t get_ctime ();
                time_t get_mtime ();
		bool broadcast ();
		char * name ();
		
	private:
		int c_sd;
		int c_type;
		char * c_name;
		time_t c_mtime;
		time_t c_ctime;
		char * c_cname; // client address
		char * c_pname; // server address                
		uint16_t c_cport;
		uint16_t c_pport;
		char * c_status; // other data
		char * c_user_data;
		int c_poll_flags;                
		bool c_bcast;
		bool dirtySocket;
};

#endif

