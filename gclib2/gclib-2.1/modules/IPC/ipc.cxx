/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/msg.h>

/// \file ipc.c Межпроцессорные функции.

/*! \page IPCModule Межпроцессорное взаимодействие.
 * \see ipc.c
 */

inline void __semset (struct sembuf * action, int num, int op) 
{
	action->sem_flg = SEM_UNDO;
	action->sem_num = num;
	action->sem_op = op;
}

enum {
	hold_read = 0,
	hold_write = 1,
};

inline void __sem_zero (int sem, int num) // = 0
{
	semctl (sem, num, SETVAL, 0);
}

int __sem_try_key (int *KEY, int count) // = 1 
{
	int key = 9900;
	int id;

	while (key < 10000) {
		id = semget (key, count, (0600|IPC_CREAT|IPC_EXCL)); 
		if (id < 0) {
			key++;
			continue;
		}
		
		if (KEY)
			*KEY = key;

		return id;
	}

	return -1;
}

int __sem_init_one (int key, int count) // = 1
{
	int id;
	id = semget (key, 1, (0600|IPC_CREAT|IPC_EXCL));
	if (id > 0) 
		__sem_zero (id, 0);
	return id;		
}

/*! \brief Инициализировать семафор.
 * \param KEY - указатель на номер ключа. Если не задан - выбирается рандомный и записывается в *KEY.
 * \return Код (id) семафора, который используется во всех остальных функциях.
 */
int sem_init (int * KEY) 
{
	int key = 9900;
	int id;

	if (KEY && *KEY) 
		id = __sem_init_one (*KEY, 1);
	else
		id = __sem_try_key (&key, 1);
	
	if (id < 0)
		return id;

	__sem_zero (id, 0);
	if (KEY)
		*KEY = key;

	return id;	
}

/*! \brief Заблокировать семафор.
 * \param sem - код (id) семафора.
 */
int down (int sem)
{
	struct sembuf actions[2];
	__semset (&actions[0], 0, 0);
	__semset (&actions[1], 0, +1);
	return semop (sem, actions, 2);
}

/*! \brief Разблокировать семафор.
 * \param sem - код (id) семафора.
 */
int up (int sem)
{
	struct sembuf actions[1];
	__semset (&actions[0], 0, -1);
	return semop (sem, actions, 1);
}

/*! \brief Инициализировать семафор (чтение/запись).
 * \param KEY - указатель на номер ключа. Если не задан - выбирается рандомный и записывается в *KEY.
 * \return Код (id) семафора, который используется во всех остальных функциях.
 */
int sem_init_rw (int * KEY)
{
	int id;

	if (KEY && *KEY)  
		id = __sem_init_one (*KEY, 2);
	else 
		id = __sem_try_key (KEY, 2);

	if (id < 0)
		return id;

	__sem_zero (id, hold_read);
	__sem_zero (id, hold_write);

	return id;
}

/*! \brief Заблокировать семафор (чтение).
 * \param sem - код (id) семафора.
 */
int down_read (int sem)
{
	struct sembuf actions[2];
	__semset (&actions[0], hold_write, 0);
	__semset (&actions[1], hold_read, +1);
	return semop (sem, actions, 2);
}

/*! \brief Разблокировать семафор (чтение).
 * \param sem - код (id) семафора.
 */
int up_read (int sem) 
{
	struct sembuf actions[1];
	__semset (&actions[0], hold_read, -1);
	return semop (sem, actions, 1);
}

/*! \brief Заблокировать семафор (запись).
 * \param sem - код (id) семафора.
 */
int down_write (int sem)
{
	struct sembuf actions[3];
	__semset (&actions[0], hold_read, 0);
	__semset (&actions[1], hold_write, 0);
	__semset (&actions[2], hold_write, +1);
	return semop (sem, actions, 3);
}

/*! \brief Разлокировать семафор (запись).
 * \param sem - код (id) семафора.
 */
int up_write (int sem)
{
	struct sembuf actions[2];
	__semset (&actions[0], hold_write, -1);
	return semop (sem, actions, 1);
}

int __msg_try_key (int *KEY)
{
	int key = 9900;
	int id;

	if (KEY && *KEY != 0) {
		id = msgget (key, (0600|IPC_CREAT|IPC_EXCL));
		return id;
	}

	while (key < 10000) {
		id = msgget (key, (0600|IPC_CREAT|IPC_EXCL)); 
		if (id < 0) {
			++key;
			continue;
		}
		
		if (KEY)
			*KEY = key;

		return id;
	}

	return -1;
}

/*! \brief Инициализировать пул сообщений.
 * \param KEY - указатель на номер ключа. Если не задан - выбирается рандомный и записывается в *KEY.
 * \return Код (id) пула сообщений, который используется во всех остальных функциях.
 */
int msg_init (int *KEY)
{
	return __msg_try_key (KEY);
}
	
