/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include "gclib2.h"
#include "dns.h"
#include "internals.h"

/// Найти в ответе запись нужного типа.
dns_reply * dns_scan (DList *list, uint16_t dr_type, uint16_t dr_class)
{
	__dlist_entry_t * one;
	dns_reply *Ret = NULL;
	dns_reply *reply = NULL;

	if (! list)
		return NULL;

	one = list->get_head ();
	if (! one)
		return NULL;

	while (one) {
		reply = (dns_reply *) one->data;
		if ((reply->dr_type == dr_type) && reply->dr_class == dr_class) {
			Ret = reply;
			break;
		}
		one = one->next;
	}

	return Ret;
}

__dlist_entry_t * dns_inquiries_scan (char * host, uint16_t dr_type, uint16_t dr_class)
{
	__dlist_entry_t * one;
	dns_query * q;

	one = dns_inquiries->get_head ();
	if (! one || ! host)
		return NULL;

	while (one) {
		q = (dns_query *) one->data;
		if (q->dr_type == dr_type && q->dr_class == dr_class &&
				EQ (q->host, host)) 
			return one;
		one = one->next;
	}

	return NULL;
}


