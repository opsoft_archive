/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include "dns.h"
#include "internals.h"
#include <network.h>

/// Инициализировать модуль DNS.
int dns_init ()
{
	FILE * f_resolv;
	dns_server * one;
	char m_buf[256];
	int n = 0;
	char *S;

	if (dns_sock != -1)
		return 0;

	f_resolv = fopen ("/etc/resolv.conf", "r");
	if (! f_resolv)
		return -1;

	dns_sock = dUdpSocket ();
	if (dns_sock < 0)
		return -1;

	dns_inquiries = new List;
	m_buf[255] = 0;
	dns_servers = new DList;
	while (fgets (m_buf, 255, f_resolv)) {
		chomp (m_buf);
		if (strncmp (m_buf, "nameserver", 10)) // != nameserver
                        continue;
                
                S = &m_buf[10];
                ++S;
                while (*S && (*S == ' ' || *S == '\t'))
                        ++S;
                
                if (! strlen (m_buf))
                        continue;
                
                one = CNEW (dns_server, 1);
                one->IP = strdup (S);
                one->ms = 0;
                dns_servers->add_tail (LPCHAR (one));
                ++n;		
	}

	if (n == 0)
		return -1;
	
	dns_last_id = rand ();
	fclose (f_resolv);

	if (! n) {
		close (dns_sock);
		dns_sock = -1;
		return -1;
	}
	
	return 0;
}

