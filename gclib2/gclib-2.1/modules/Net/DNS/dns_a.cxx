/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include "gclib2.h"
#include "dns.h"
#include "internals.h"

/// Получить адрес по имени.
__export char * dns_A (char *host)
{
	struct dns_reply *reply;
	DList * list;
	char * Ret = NULL;

	list = dns_query_sync (0x0100, DNS_A, 1, host);
	reply = dns_scan (list, DNS_A, 1);
	if (reply)
		Ret = strdup (inet_ntoa (*((in_addr *) reply->data)));

	dns_resp_clean (list);
	return Ret;
}

__export char * dns_async_A (char *host, dns_query ** qRet)
{
	__dlist_entry_t * one;
	struct dns_reply *reply;
	dns_query * q;
	DList * list;
	char * Ret = NULL;
	
	one = dns_inquiries_scan (host, DNS_A, 1);
	if (! one) {
		q = dns_query_async (0x0100, DNS_A, 1, host);
		if (qRet)
			*qRet  = q;
		return NULL;
	}

	q = (dns_query *) one->data;
	if (q->reply_pkt == NULL)
		return NULL;

	dns_inquiries->del (one);
	list = dns_resp_split ((dns_header *) q->reply_pkt, q->reply_pkt, q->reply_pkt_len);
	reply = dns_scan (list, DNS_A, 1);
	if (reply)
		Ret = strdup (inet_ntoa (*((in_addr *) reply->data)));

	dns_resp_clean (list);
	DROP (q->host);
	DROP (q->reply_pkt);
	DROP (q);
	return Ret;
}


