/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include "gclib2.h"
#include "dns.h"
#include "internals.h"

/// Получить имя сервера по IP.
__export char * dns_ip2name (char *IP)
{
	struct dns_reply *reply;
	char *buf;
	DList * list;
	char * Ret = NULL;
	char * resp;
	int resp_len;
	DPBuf * p;
	char * name;
	
	if (! IP)
		return NULL;

	buf = ip2arpa (IP);
	list = dns_query_sync (0x0100, DNS_PTR, 1, buf, &resp, &resp_len);
	DROP (buf);
	reply = dns_scan (list, DNS_PTR, 1);

	if (! reply) 
		goto dip_out;

	name = reply->pkt_data_ptr;
	p = new DPBuf (resp, resp_len);
	Ret = __dns_resp_domain (p, name, NULL);
	delete p;
	DROP (resp);
	
dip_out:
	dns_resp_clean (list);
	return Ret;
}

__export char * dns_async_ip2name (char *IP, dns_query ** qRet)
{
	__dlist_entry_t * one;
	struct dns_reply *reply;
	dns_query * q;
	DList * list;
	char * Ret = NULL;
	char * arpa_name;
       	DPBuf * p;
	char * name;

	arpa_name = ip2arpa (IP);
	if (! arpa_name)
		return NULL;

	one = dns_inquiries_scan (arpa_name, DNS_PTR, 1);
	if (! one) {
		q = dns_query_async (0x0100, DNS_PTR, 1, arpa_name);
		if (qRet)
			*qRet  = q;
		return NULL;
	}

	q = (dns_query *) one->data;
	if (q->reply_pkt == NULL)
		return NULL;
	dns_inquiries->del (one);

	list = dns_resp_split ((dns_header *) q->reply_pkt, q->reply_pkt, q->reply_pkt_len);
	reply = dns_scan (list, DNS_PTR, 1);
	if (! reply) {
		clean_query (q);
		dns_resp_clean (list);
		return NULL;
	}

	name = reply->pkt_data_ptr;
	p = new DPBuf (q->reply_pkt, q->reply_pkt_len);
	Ret = __dns_resp_domain (p, name, NULL);
	delete p;

	clean_query (q);
	dns_resp_clean (list);
	return Ret;
}

