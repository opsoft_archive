/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DNS_H
#define DEFINE_DNS_H

#ifndef DEFINE_GCLIB2_H
#include <gclib2.h>
#endif

struct dns_query
{
	uint16_t id;
	uint16_t flags;
	uint16_t dr_type;
       	uint16_t dr_class;
	char * host;
	char * reply_pkt;
	int reply_pkt_len;
	char * user_data;
};

int dns_init ();
int dns_get_sock ();
void dns_set_timeout (int sec);
char * dns_A (char *host);
char * dns_MX (char *host);
char * dns_ip2name (char *IP);
char * dns_async_A (char *host, dns_query ** qRet = NULL);
char * dns_async_ip2name (char *IP, dns_query ** qRet = NULL);
char * dns_async_MX (char *IP, dns_query ** qRet = NULL);
dns_query * dns_loop ();
void dns_clean ();

#endif 

