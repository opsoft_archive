/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include "gclib2.h"
#include "dns.h"
#include "internals.h"

int dns_sock = -1;
int dns_last_id = 0;
int dns_timeout = 30;
DList * dns_servers = NULL;
List * dns_inquiries;

char * bestServer ()
{
	dns_server * d_server;
	__dlist_entry_t * one;
	char * Ret;

	one = dns_servers->get_head ();
	d_server = (dns_server *) one->data;
	Ret = d_server->IP;
	dns_servers->rm (one);
	dns_servers->add_tail (LPCHAR (d_server));

	return Ret;
}

/// Установить время ожидания DNS сервера (в секундах).
__export void dns_set_timeout (int sec)
{
	dns_timeout = sec;
}

__export int dns_get_sock ()
{
	return dns_sock;
}

__export void dns_clean ()
{
	__dlist_entry_t * one;
	dns_query * q;

	while ((one = dns_inquiries->get_head ()) && one) {
		q = (dns_query *) one->data;
		clean_query (q);
		dns_inquiries->del (one);
	}
}

