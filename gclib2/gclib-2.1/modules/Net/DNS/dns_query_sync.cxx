/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include "dns.h"
#include "internals.h"
#include <network.h>

DList * dns_query_sync(uint16_t flags, uint16_t dr_type, uint16_t dr_class, 
		char * host, char ** d_resp, int *d_resp_len)
{
	char * pkt;
	int len;
	int count = 0;
	int resp_len;
	char * buf;
	struct dns_header *hdr;
	DList * list;

	if (dns_sock == -1) {
		if (dns_init () == -1)
			return NULL;
	}
	
	if (! host)
		return NULL;
	
	pkt = make_dns_pkt (++dns_last_id, 0x0100, dr_type, dr_class, host, &len);
	if (sendToSocket (dns_sock, pkt, len, bestServer (), 53) < 0) {
		DROP (pkt);
		return NULL;
	}

	if (Dselect (dns_sock, dns_timeout, 0) <= 0) {
		DROP (pkt);
		return NULL;
	}
	
	count = DIONREAD (dns_sock);
	buf = CNEW (char, count);
	resp_len = recvFrom (dns_sock, buf, count, NULL, 0);
	if (resp_len <= 0) 
		return NULL;
	
	hdr = (dns_header *) pkt;
	list = dns_resp_split (hdr, buf, resp_len);
	
	if (d_resp) 
		*d_resp = buf;
	else
		DROP (buf);

	if (d_resp_len)
		*d_resp_len = resp_len;
	
	return list;
}

