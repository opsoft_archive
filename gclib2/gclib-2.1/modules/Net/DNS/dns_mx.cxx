/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include "dns.h"
#include "internals.h"
#include <Pkt.h>

dns_reply * bestMXReply (DList * list)
{
	__dlist_entry_t * one;
	struct dns_reply *reply;
	struct dns_reply *best_reply = NULL;
	uint16_t d_min = 0xFFFF;
	char * ptr;
	int i;

	if (! list)
		return NULL;

	one = list->get_head ();
	if (! one)
		return NULL;

	while (one) {
		reply = (dns_reply *) one->data;
		if (reply->dr_type == 15) {
			ptr = one->data;
			i = htons (pkt_R16(&ptr));
			if (i < d_min) {
				d_min = i;
				best_reply = reply;
			}
		}
		one = one->next;
	}

	return best_reply;
}

/// Получить (лучшую) MX запись для хоста.
__export char * dns_MX (char *host)
{
	struct dns_reply *best_reply;
	DList * list;
	char * Ret = NULL;
	char * resp;
	int resp_len;
	char * mx;
	DPBuf * p;

	list = dns_query_sync (0x0100, DNS_MX, 1, host, &resp, &resp_len);
	if (! list)
		return NULL;

	best_reply = bestMXReply (list);
	if (! best_reply) {
		dns_resp_clean (list);
		return NULL;
	}

	mx = best_reply->pkt_data_ptr;
	mx += 2;
	p = new DPBuf (resp, resp_len);
	Ret = __dns_resp_domain (p, mx, NULL);

	delete p;
	DROP (resp);
	dns_resp_clean (list);
	return Ret;
}

__export char * dns_async_MX (char *IP, dns_query ** qRet)
{
	__dlist_entry_t * one;
	struct dns_reply *reply;
	dns_query * q;
	DList * list;
	char * Ret = NULL;
       	DPBuf * p;
	char * mx;

	if (! IP)
		return NULL;

	one = dns_inquiries_scan (IP, DNS_MX, 1);
	if (! one) {
		q = dns_query_async (0x0100, DNS_MX, 1, IP);
		if (qRet)
			*qRet  = q;
		return NULL;
	}

	q = (dns_query *) one->data;
	if (q->reply_pkt == NULL)
		return NULL;
	dns_inquiries->del (one);

	list = dns_resp_split ((dns_header *) q->reply_pkt, q->reply_pkt, q->reply_pkt_len);
	reply = bestMXReply (list);

	if (! reply) {
		clean_query (q);
		dns_resp_clean (list);
		return NULL;
	}

	mx = reply->pkt_data_ptr;
	mx += 2;
	p = new DPBuf (q->reply_pkt, q->reply_pkt_len);
	Ret = __dns_resp_domain (p, mx, NULL);
	
	delete p;
	clean_query (q);
	dns_resp_clean (list);
	return Ret;
}

