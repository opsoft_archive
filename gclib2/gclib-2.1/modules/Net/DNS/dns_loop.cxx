/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include "dns.h"
#include "internals.h"
#include <network.h>

__export dns_query * dns_loop ()
{
	int count;
	char * buf = NULL;
	int resp_len;
	dns_header * hdr = NULL;
	dns_query * q = NULL;
	dns_query * Ret = NULL;
	__dlist_entry_t * one = NULL;

dloop_check_again:
	count = DIONREAD (dns_sock);
	if (count <= 0)
		return Ret;

	buf = CNEW (char, count);
	resp_len = recvFrom (dns_sock, buf, count, NULL, 0);
	if (resp_len < (int) sizeof (dns_header)) 
		goto dq_outclean;
	
	hdr = (dns_header *) buf;
	one = dns_inquiries->get_head ();
	while (one) {
		q = (dns_query *) one->data;
		if (htons (hdr->id) == q->id) {
			q->reply_pkt = buf;
			q->reply_pkt_len = resp_len;
			break;
		}
		one = one->next;
	}

	if (q) {
		Ret = q;
		goto dloop_check_again;
	}

dq_outclean:
	DROP (buf);
	return Ret;
	
}

