/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include "dns.h"
#include "internals.h"
#include <network.h>

dns_query * dns_query_async(uint16_t flags, uint16_t dr_type, uint16_t dr_class, char * host)
{
	dns_query * q;
	char * pkt;
	int len;

	if (dns_sock == -1) {
		if (dns_init () == -1)
			return NULL;
	}
	
	if (! host)
		return NULL;
	
	pkt = make_dns_pkt (++dns_last_id, 0x0100, dr_type, dr_class, host, &len);
	if (sendToSocket (dns_sock, pkt, len, bestServer (), 53) < 0) {
		DROP (pkt);
		return NULL;
	}

	q = CNEW (dns_query, 1);
	memset (q, 0, sizeof (dns_query));
	
	q->id = dns_last_id;
	q->flags = flags;
	q->dr_type = dr_type;
	q->dr_class = dr_class;
	q->host = strdup (host);
	dns_inquiries->add (LPCHAR (q));

	return q;
}

