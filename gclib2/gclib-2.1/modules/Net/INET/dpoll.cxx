/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include "gclib2.h"
#include <sys/poll.h>

DPoll::DPoll ()
{
	 connections = new EHash;
	 pull = NULL;
	 p_pos = 0;
	 p_ptr = NULL;
}

DPoll::~DPoll ()
{
	dkey_t * key;
	__dlist_entry_t * one;
	DConnection * c;

	one = connections->get_head ();
	while (one) {
		key = (dkey_t *) one->data;
		c = (DConnection *) key->VALUE;
		delete c;
		one = one->next;
	}
}

/*! Инициализировать пул первым элементом.
 * \param name - Имя соединения.
 * \param num - Порядковый номер соединения (или ноль).
 * \param fd - Дескриптор соединения.
 */
DConnection * DPoll::newConnection (char * name, uint32_t num, int fd)
{
	char m_buf[512];
	DConnection * one;
	
	if (! name) {
		Drand_str (m_buf, 32);
		name = m_buf;
	}

	one = new DConnection;
	one->setSocket (fd);
	if (num)
		sprintf (m_buf, "%s%i", name, num);
	else
		sprintf (m_buf, "%s", name);
	one->setName (m_buf);
	one->setPollFlags (POLLIN | POLLHUP);
	connections->set (m_buf, LPCHAR (one));
	return one;
}

/// Добавить соединение к пулу.
DConnection * DPoll::attach (DConnection *c)
{
	char tmpstr[64];

	if (c == NULL)
		return NULL;
	if (c->name () == NULL) {
		Drand_str (tmpstr, 63);
		c->setName (tmpstr);
	}
	connections->set (c->name (), LPCHAR (c));
	if (! c->pollFlags ())
		c->setPollFlags (POLLIN | POLLHUP);

	return c;
}

/*! \brief Поиск соединения по имени и номеру.
 * \param c_name - имя соединения.
 * \param num - номер соединения.
 */
DConnection * DPoll::by_name (char * c_name, int num)
{
	char m_buf[512];
	if (num)
		sprintf (m_buf, "%s%i", c_name, num);
	else
		sprintf (m_buf, "%s", c_name);
	return (DConnection *) connections->get (m_buf);
}

/*! \brief убить соединение.
 * \param c_name - имя соединения.
 * \param num - номер соединения.
 */
int DPoll::unlink (char * c_name, int num)
{
	DConnection *c;
	char m_buf[512];
	
	if (num)
		sprintf (m_buf, "%s%i", c_name, num);
	else 
		strcpy (m_buf, c_name);

	c = (DConnection *) connections->del (m_buf);
	if (! c)
		return -1;
	delete c;
	return 0;
}

/// Убить соединение.
int DPoll::unlink (DConnection * c_conn)
{
	if (! c_conn)
		return -1;
	connections->del (c_conn->name ());
	delete c_conn;
	return 0;
}

/// Собрать пул для poll(2).
pollfd * DPoll::poll_build (int *d_nfds)
{
	DConnection *c;
	dkey_t * key;
	__dlist_entry_t * one;
	int count;
	int i = 0;

	count = connections->count ();
	pull = CNEW (pollfd, count);
	
	one = connections->get_head ();
	while (one) {
		key = (dkey_t *) one->data;
		c = (DConnection *) key->VALUE;
		pull[i].fd = c->socket ();
		pull[i].events = c->pollFlags();
		pull[i].revents = 0;
		one = one->next;		
		++i;
	}
	
	if (d_nfds)
		*d_nfds = count;
	nfds = count;
	return pull;
}

/// Пересобрать пул для poll(2).
pollfd * DPoll::poll_rebuild (int *d_nfds)
{
	if (pull) 
		DROP (pull);
	return poll_build (d_nfds);
}

/*! \brief Ожидать поступление данных.
 * \param timeout - таймаут (в миллисекундах).
 */
int DPoll::poll (int timeout)
{
	int Ret = 0;
	   
	if (! pull)
		poll_build (&nfds);
	if (nfds <= 0)
		return 0;
	Ret = ::poll (pull, nfds, timeout);
	connections->first ();
	p_pos = 0; 
	return Ret;	   
}

/// Найти "готовое" соединение.
DConnection * DPoll::scan ()
{
	DConnection * c;
	dkey_t * key;
	
	key = (dkey_t *) connections->get ();
	while (key) {
		c = (DConnection *) key->VALUE;
		key = (dkey_t *) connections->next ();
		if (pull[p_pos].revents)
			return c;
		++p_pos;
	}
	
	return NULL;
}

/// Выводит список соединений.
int DPoll::connections_dump ()
{
	dkey_t * key;
	__dlist_entry_t * one;
	DConnection * c;
	int i = 0;
	
	one = connections->get_head ();
	while (one) {
		key = (dkey_t *) one->data;
		c = (DConnection *) key->VALUE;
		printf ("%s\n", c->name ());
		one = one->next;
		++i;
	}

	return i;
}

/// Убирает соединение из пула.
DConnection * DPoll::detach (char *c_name, int num)
{
	char m_buf[512];

	if (num)
		sprintf (m_buf, "%s%i", c_name, num);
	else
		strcpy (m_buf, c_name);

	return (DConnection *) connections->del (m_buf);
}

DConnection * DPoll::detach (DConnection *c)
{
	return (DConnection *) detach (c->name (), 0);
}

/// Вернуть пул и количество элементов.
pollfd * DPoll::get_poll (int * count)
{
	if (count)
		*count = nfds;
	return pull;
}

