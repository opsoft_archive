/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <dns.h>
#include <sys/socket.h>
#include "http.h"
#include "internals.h"

const char * TransferEncoding =  "Transfer-Encoding: ";

int http_parse_result (http_context * ctx)
{
	char m_buf[512];
	bool m_chunked;
	char * S;
	int header_len = 0;
	FILE * m_resFile;

	if (! ctx)
		return -1;

	lseek (ctx->resFd, 0, SEEK_SET);
	m_resFile = fdopen (ctx->resFd, "r+");
	fgets (m_buf, 512, m_resFile);
	header_len = strlen (m_buf);
	
	sscanf (m_buf, "HTTP/1.1 %d", &ctx->retCode);
	m_chunked = false;
	while (fgets (m_buf, 512, m_resFile)) {
		header_len += strlen (m_buf);
		chomp (m_buf);
		if (strlen (m_buf) == 0)
			break;

		if (! strncmp (m_buf, TransferEncoding, strlen (TransferEncoding))) {
			S = strstr (m_buf, ": ");
			S += 2;
			if (EQ (S, "chunked"))
				m_chunked = true;
		}
	}
	fclose (m_resFile);
	ctx->resFd = open (ctx->resFileName, O_RDONLY);
	ctx->resOffset = header_len;

	if (m_chunked) 
	       http_chunked_result_join (ctx);
	
	return 0;
}

