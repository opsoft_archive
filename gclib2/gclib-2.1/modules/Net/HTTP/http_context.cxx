/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include "http_context.h"

http_context * http_context_alloc ()
{
	http_context * ctx;
	ctx = CNEW (http_context, 1);
	memset (ctx, 0, sizeof (http_context));
	ctx->serverSd = -1;
	ctx->resFd = -1;
	return ctx;
}

int http_context_clean (http_context * ctx)
{
	if (! ctx)
		return -1;

	fdclose (&ctx->serverSd);
	fdclose (&ctx->resFd);
	
	DROP (ctx->file);
	DROP (ctx->serverName);
	DROP (ctx->serverIP);
	if (strlen (ctx->resFileName))
		unlink (ctx->resFileName);
	DROP (ctx);

	return 0;
}

