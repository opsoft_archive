/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include "http.h"
#include "internals.h"
#include <stdint.h>

int http_write_header (http_context * ctx, int tmpfd)
{
	char * m_header;
	
	if (! ctx)
		return -1;
	m_header = CNEW (char, ctx->resOffset);
	read (ctx->resFd, m_header, ctx->resOffset);
	write (tmpfd, m_header, ctx->resOffset);
	DROP (m_header);

	return 0;
}

int http_chunked_result_join (http_context * ctx)
{
	char tmpfilename[512];
	char * m_buf;
	int m_tmpfd;
	uint32_t count = 0;
	FILE * m_oldfile;
	FILE * m_newfile;
	
	if (! ctx)
		return -1;
	
	m_tmpfd = Dtmpfd (tmpfilename);
	if (m_tmpfd < 0) 
		return -1;

	http_write_header (ctx, m_tmpfd);

	m_oldfile = fdopen (ctx->resFd, "r");
	if (m_oldfile == NULL) 
		return -1;

	fseek (m_oldfile, ctx->resOffset, SEEK_SET);
	m_newfile = fdopen (m_tmpfd, "r+");

	m_buf = CNEW (char, 4096);
	while (true) {
		m_buf[0] = 0;
		fgets (m_buf, 4096, m_oldfile);
		if (feof (m_oldfile))
			break;

		sscanf (m_buf, "%x", &count); 
		if (count == 0)
			break;
		fread (m_buf, 1, 2, m_oldfile);
		Dcopyfile (m_oldfile, m_newfile, count);
	}
	fclose (m_oldfile);
	fclose (m_newfile);
	DROP (m_buf);
	
	unlink (ctx->resFileName);
	ctx->resFd = open (tmpfilename, O_RDONLY | O_LARGEFILE);
	strcpy (ctx->resFileName, tmpfilename);
	lseek (ctx->resFd, ctx->resOffset, SEEK_SET);
	
	return 0;
}

