/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <dns.h>
#include <sys/socket.h>
#include <time.h>
#include "http.h"
#include "internals.h"
#include "http_context.h"

int http_send_request (int sd, char * m_server, char * m_link)
{
	int Ret;
	char m_buf[512];
	sprintf (m_buf, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", m_link, m_server);
	Ret = send (sd, m_buf, strlen (m_buf), 0);
	shutdown (sd, 1);
	return Ret;
}

int http_recv_response (http_context * ctx)
{
	int N;
	char m_buf[256];

	srand (time (NULL));

	if (! ctx)
		return -1;

	ctx->resFd = Dtmpfd (ctx->resFileName);
	if (ctx->resFd < 0)
		return -1;
	
	move_stream (ctx->serverSd, ctx->resFd);
	lseek (ctx->resFd, 0, SEEK_SET);

	return ctx->resFd;
}

http_context * __http_get (char * addr)
{
	http_context * ctx;
	char m_buf[512];
	char * S;
	int sd;

	if (! addr)
		return NULL;

	ctx = http_context_alloc ();
	strcpy (m_buf, addr);
	S = strstr (m_buf, "://");
	if (S) {
		*S = '\0';
		if (NE (m_buf, "http"))
			return NULL;
		S += 3;
		strcpy (m_buf, S);
	}

	S = strchr (m_buf, '/');
	if (S) {
		ctx->file = strdup (S);
		*S = '\0';
	} else
		ctx->file = strdup ("/");

	ctx->serverName = strdup (m_buf);

	if (http_connect (ctx) < 0)
		goto httperrout;

	if (http_send_request (ctx) < 0)
		goto httperrout;

	if (http_recv_response (ctx) < 0)
		goto httperrout;

	if (http_parse_result (ctx) < 0)
		goto httperrout;

	return ctx;

httperrout:
	http_context_clean (ctx);
	return NULL;
}

char * http_get (char * url, int * m_size)
{
	http_context * ctx;
	char * m_buf;

	ctx = __http_get (url);
	if (! ctx) {
		if (m_size)
			*m_size = 0;
		return NULL;
	}

	lseek (ctx->resFd, ctx->resOffset, SEEK_SET);
	m_buf = Dread_to_eof (ctx->resFd, m_size);
	http_context_clean (ctx);
	return m_buf;
}

