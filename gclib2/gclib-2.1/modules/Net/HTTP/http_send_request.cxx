/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <dns.h>
#include <sys/socket.h>
#include "http.h"
#include "internals.h"

int http_send_request (http_context * ctx)
{
	int Ret;
	char m_buf[512];
	
	if (! ctx)
		return -1;
	sprintf (m_buf, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", ctx->file, ctx->serverName);
	Ret = send (ctx->serverSd, m_buf, strlen (m_buf), 0);
	shutdown (ctx->serverSd, 1);

	return Ret;
}

