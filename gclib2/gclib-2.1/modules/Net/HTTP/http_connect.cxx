/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <network.h>
#include <dns.h>
#include <sys/socket.h>
#include "http.h"
#include "internals.h"

/// Подключиться к HTTP серверу.
int http_connect (http_context * ctx)
{
	char * S;
	bool m_ip;
	int nDots = 0;

	if (! ctx)
		return -1;

	m_ip = true;
	S = ctx->serverName;
	while (*S) {
		if (*S == '.') 
			++nDots;
		else if (! isdigit (*S)) {
			m_ip = false;
			break;
		}
		++S;
	}

	if (nDots != 3)
		m_ip = false;

	if (m_ip) 
		ctx->serverIP = strdup (ctx->serverName);
	else
		ctx->serverIP = dns_A (ctx->serverName);
	
	if (! ctx->serverIP)
		return -1;

	ctx->serverSd = dSocket ();
	if (ctx->serverSd < 0)
		return -1;
	if (dConnect (ctx->serverSd, ctx->serverIP, 80)) {
		close (ctx->serverSd);
		return -1;
	}

	return ctx->serverSd;
}

