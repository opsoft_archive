/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_STRINGS_H
#define DEFINE_STRINGS_H

/*** ./strings.cxx ***/
__export char * gc_realloc (char * PTR, int old_size, int new_size) ;
__export void * memdup (void * PTR, int size) ;
__export int Dsplit (char * lpsz_String, char *ch, char ** outbuffer, int int_buffersize) ;
__export char * Dstrmid (char * lpsz_string,char * param1, char * param2) ;
__export char * chomp (char * S) ;
__export char * strchr_r (char * S, char ch, int d_len) ;
__export char * strchrs (char *S, char ch, char ch2, char ch3, char ch4) ;
__export char * Dstrstr_r (char *where, char * str) ;
__export int Dsyms (char * from, char * to, char sym) ;
__export char * Dmemchr (char * from, int n, char ch) ;
__export char * Dstrndup (char *ptr, int n) ;
__export char * Dmid_strchr (char *ptr, char *end, char ch) ;
__export char * Dmid_getstr (char *buf, char *end) ;
__export char * Drand_str (char * buf, int count) ;
__export char * int2str (int i) ;
__export char * stail (char *S) ;
__export char * strmov (char *buf, char * S) ;
__export char * strip (char *str) ;
__export char * strip2 (char *str) ;
__export char * Dmemmem (char *haystack, size_t haystacklen, char *needle, size_t needlelen) ;
__export char * Dmid_memmem (char * begin, char * last, char * needle, int needlelen) ;
__export char * Dsprintf (char * fmt, ...) ;

#endif

