/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 * 30/01/07 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "gclib2.h"

Tree::Tree ()
{
	rootNode = NULL;
}

Tree::~Tree ()
{
	freeNode (rootNode, NULL);
}

/// Удалить нод функцией f.
char * Tree::freeNode (node_t * node, Dfunc_t f)
{
	EList * list;
	char * userData;
	node_t * n;
	if (! node)
		return NULL;

	if (node->parentNode) {
		list = node->parentNode->childNodes;
		list->first ();
		while ((n = (node_t *) list->get ()) && n && n != node)
			list->next ();
		if (n)
			list->rm ();
		node->parentNode = NULL;
	}

	userData = node->userData;
	list = node->childNodes;
	while ((n = (node_t *) list->first ()) && n) {
		n->parentNode = NULL;
		freeNode (n, f);
		list->rm ();
		if (f)
			f (n);
	}

	if (f)
		f (node);

	delete list;
	delete node;
	return userData;
}

/// Добавить нод.
node_t * Tree::newNode (node_t * parent, char * key, char * userData)
{
	node_t * n;
	
	n = CNEW (node_t, 1);
	memset (n, 0, sizeof (node_t));
	n->key = key;
	n->userData = userData;
	n->childNodes = new EList;
	
	if (parent) {
		n->parentNode = parent;
		parent->childNodes->add (LPCHAR (n));
	} else
		rootNode = n;
	
	return n;
}

/// Получить список всех дочерних нодов.
EList * Tree::childs (node_t * node)
{
	if (! node)
		return NULL;
	return node->childNodes;
}

/// Искать ключ (вниз по дереву).
node_t * Tree::searchDown (node_t * node, char * key)
{
	EList * list;
	node_t * n;

	if (! node || ! key)
		return NULL;
	
	list = node->childNodes;
	if (! list)
		return NULL;

	list->first ();
	while ((n = (node_t *) list->get ()) && n) {
		if (EQ (n->key, key))
			return n;
		list->next ();
	}

	list->first ();
	while ((n = (node_t *) list->get ()) && n) {
		n = searchDown (n, key);
		if (n)
			return n;
		list->next ();
	}

	return NULL;
}

/// Искать ключ (вверх).
node_t * Tree::searchUp (node_t * node, char * key)
{
	node_t * n;
	
	if (! node || ! key)
		return NULL;
	
	n = node->parentNode;
	while (n && NE (n->key, key))
		n = n->parentNode;
	
	return n;
}

/// Вернуть все дочерние ноды ключа key.
EList * Tree::keyChilds (node_t * node, char * key)
{
	node_t * n;

	if (node)
		n = node;
	else
		n = rootNode;
	n = searchDown (n, key);
	if (n)
		return n->childNodes;

	return NULL;
}

