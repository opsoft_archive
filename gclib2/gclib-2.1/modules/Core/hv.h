/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_HV_H
#define DEFINE_HV_H

/// Класс - хэш с сортировкой ключей.
class HV 
{
	public:
		HV ();
		~HV ();
	
		char * get (char * key) ;
		char * set (char * key, char * value) ;
		dkey_t * unpackItem (char * item, dkey_t * data); 

		inline void sort () {
			do_sort (false);
		}
		inline void clean () {
			do_sort (true);
		}
		inline int autoSort () {
			return dfa;
		}
		inline int setAutoSort (int d_set) {
			dfa = d_set;
			return dfa;
		}
		inline bool autoCheck () {
			return b_ac;
		}
		inline bool setAutoCheck (bool d_set) {
			b_ac = d_set;
			return b_ac;
		}
		inline EArray * sortedElements () {
			return elements;
		}
		inline EArray * dirtyElements () {
			return dirty_elements;
		}

	private:
		dkey_t * __find_item (char * key) ;
		dkey_t * __find_dirty_item (char * key) ;
		void do_sort (bool b_clean) ;
		char * pack_item (char * key, char * value) ;
		char * set_item_value (char * pack, char * value) ;
		char * get_item (char * key) ;

		char ** heap;
		int dfa; // Dirty elements before autosorting.
		bool b_ac;
		EArray * elements;
		EArray * dirty_elements;
};

#endif

