/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */

#include <gclib2.h>

DPBuf::DPBuf ()
{
	init (NULL, 0);
}

DPBuf::DPBuf (char *ptr, int len)
{
	init (ptr, len);
}

DPBuf::~DPBuf ()
{
	if (m_file) {
		fclose (m_file);
		m_file = NULL;
	}
}

/// Прочитать байт из пакета.
unsigned char DPBuf::r8 (char *ptr)
{
	unsigned char *S;
	if (! ptr)
		S = (unsigned char *) carret;
	else
		S = (unsigned char *) ptr;
	
        if (check ((char *) S, 1))
			return *S;
	return 0;
}

/// Прочитать слово из пакета.
uint16_t DPBuf::r16 (char *ptr)
{
	uint16_t *S;
	if (! ptr)
		S = (uint16_t *) carret;
	else
		S = (uint16_t *) ptr;

	if (check ((char *) S, 2))
		return *S;
	return 0;
}

/// Прочитать двойное слово из пакета.
uint32_t DPBuf::r32 (char *ptr)
{
	uint32_t *S;
	if (! ptr)
		S = (uint32_t *) carret;
	else
		S = (uint32_t *) ptr;

	if (check ((char *) S, 4))
		return *S;
	return 0;
}

/// Прочитать данные из пакета.
char * DPBuf::rd (char *ptr, int len)
{
	char *S;
	if (! ptr)
		S = carret;
	else 
		S = ptr;

	if (check ((char *)S, len))
		return S;

	return 0;	
}

/// Установить позицию чтения.
char * DPBuf::set_pos (char * ptr)
{
	if (check (ptr, 1)) {
		carret = ptr;
		return ptr;
	} else
		return carret;
}

/// Инициализировать устойчивый буфер для пакета.
void DPBuf::init (char *ptr, int len)
{
	m_file = NULL;

	if (ptr && len) {
		begin = ptr;
		end = begin+len;
	} else {
		begin = NULL;
		end = NULL;
	}
	ok = true;
}

/// Пропустить байт.
void DPBuf::s8 (void)
{
	check (carret, 1);
	++carret;
}

/// Пропустить слово.
void DPBuf::s16 (void) 
{
	check (carret, 2);
	carret += 2;
}

/// Пропустить двойное слово.
void DPBuf::s32 (void)
{
	check (carret, 4);
	carret += 4;
}

/// Пропустить блок.
void DPBuf::sd (int len)
{
	check (carret, len);
	carret += len;
}

/// Записать байт.
void DPBuf::w8 (char ch)
{
	if (! check (carret, 1)) 
		return;
	carret[0] = ch;
	++carret;
}

/// Записать слово.
void DPBuf::w16 (uint16_t word)
{
	if (! check (carret, 2)) 
		return;
	((uint16_t *) carret)[0] = word; 
}

/// Запиать двойное слово.
void DPBuf::w32 (uint32_t dword)
{
	if (! check (carret, 4))
		return;
	((uint32_t *) carret)[0] = dword;
}

/// Проверить корректность операции.
bool DPBuf::check (char *ptr, int count)
{
	if (((ptr + count) > end) || ptr < begin) {
		ok = false;
		return false;
	}
	return true;
}

/// Конкатенция строк.
char * DPBuf::strcat (char *ptr, char *S)
{
	int len;
	if (! S) {
		ok = false;
		return NULL;
	}

	if (! ptr)
		ptr = carret;
	
	if (! check (ptr, 1))
		return NULL;

	len = strlen (S);
	if (! check (ptr, len+1)) {
		ok = false;
		return NULL;
	}

	memcpy (ptr, S, len);
	ptr[len] = '\0';
	return &ptr[len];
}

/// Найти блок needle.
char * DPBuf::memmem (char * buf, char * needle, size_t needlelen)
{
	int count;
	char * ptr;
	
	ptr = buf ? buf : carret;
	if (! check (ptr, 1))
		return NULL;
	
	count = end-ptr;
	ptr = (char *) Dmemmem (ptr, count, needle, needlelen);
	if (! ptr)
		ok = false;
	
	return ptr;
}

/// Найти следующий символ ch.
char * DPBuf::ch (char *buf, char ch)
{
	if (! buf)
		buf = carret;
	
	if (! check (buf, 1))
		return NULL;
	
	while (buf != end && *buf != ch)
		++buf;
	
	if (buf == end) {
		ok = false;
		return NULL;
	} else
		return buf;
}

FILE * DPBuf::file ()
{
	if (m_file != NULL) 
		fclose (m_file);
	m_file = fmemopen (data (), len (), "rw");
	return m_file;
}

