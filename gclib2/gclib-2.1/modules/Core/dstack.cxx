/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 * 05/10/06 Rewrited for EList.
 * 
 */

#include "gclib2.h"

int DStack::push (char *S)
{
	add_tail (S);
	return 0;
}

char * DStack::pop ()
{
        char * S;
        last ();
        S = get ();
        rm ();
	return S;
}

char * DStack::top ()
{
        last ();
	return get ();
}


