/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_TREE_H
#define DEFINE_TREE_H

#include "elist.h"
struct node_t;
struct node_t
{
	char * key;
	char * userData;
	node_t * parentNode;
	EList * childNodes; // node_t *
};

/// Класс - дерево.
class Tree 
{
	public:
		Tree ();
		~Tree ();

		node_t * rootNode;
		node_t * newNode (node_t * parent, char * key = NULL, char * userData = NULL);
		char * freeNode (node_t * node, Dfunc_t f);
		EList * childs (node_t * node);
		node_t * searchDown (node_t * node, char *key);
		node_t * searchUp (node_t * node, char *key);
		EList * keyChilds (node_t * node, char * key);
};

#endif
